   <script>
          scrollDate = [];
        </script><?php
    include_once 'jdf.php';
$projectName=$params[1];
$project = $sqlOPR -> select('projects', '', "REPLACE(name#*' '#*'-' )=$projectName");
$projectId=$_SESSION['projectId']=$project[0]['id'];
$projectStartDate=$project[0]['startDate'];
$projectName=$_SESSION['projectName']=$project[0]['name'];
$projectUsers = $sqlOPR -> selectJoin('projectuser', 'users.id as userId1,users.*,projectuser.*,permitions.rules as rules', 'users,permitions', 'users.id=projectuser.userId,permitions.id=projectuser.permitionId', "projectId=$projectId");
$projectUsers = $projectUsers['rows'];
$projectResources = $sqlOPR -> select('projectResource', '',"projectId=$projectId");
$_SESSION['ajaxRequest'] = 'schedulelogedin';
    $description = "بخش دعوت از دوستان برای پروژه";
    $title = "ساخت نمودار گانت | رسم گانت آنلاین| گانت | Gantt Diagram | گانت با تاریخ فارسی | دیاگرام گانت با تاریخ هجری شمسی";
    $style = "
    <link href='" . URL . "css/list.css' rel='stylesheet'>
    <link href='" . URL . "css/gantt.css' rel='stylesheet'>
    <link href='" . URL . "css/persian-datepicker.css' rel='stylesheet'>
    <style>
        /* GLOBAL STYLES
        -------------------------------------------------- */
        /* Padding below the footer and lighter body text */
        body {
          padding-bottom: 40px;
          color: #5a5a5a;
      }
      /* CUSTOMIZE THE NAVBAR
      -------------------------------------------------- */
      /* Special class on .container surrounding .navbar, used for positioning it into place. */
      .navbar-wrapper {
          position: absolute;
          top: 0;
          left: 0;
          right: 0;
          z-index: 10;
          margin-top: 20px;
          margin-bottom: -90px; /* Negative margin to pull up carousel. 90px is roughly margins and height of navbar. */
      }
      .navbar-wrapper .navbar {
      }
      /* Remove border and change up box shadow for more contrast */
      .navbar .navbar-inner {
          border: 0;
          -webkit-box-shadow: 0 2px 10px rgba(0,0,0,.25);
          -moz-box-shadow: 0 2px 10px rgba(0,0,0,.25);
          box-shadow: 0 2px 10px rgba(0,0,0,.25);
      }

      /* Downsize the brand/project name a bit */
      .navbar .brand {
          padding: 14px 20px 16px; /* Increase vertical padding to match navbar links */
          font-size: 16px;
          font-weight: bold;
          text-shadow: 0 -1px 0 rgba(0,0,0,.5);
      }

      /* Navbar links: increase padding for taller navbar */
      .navbar .nav > li > a {
          padding: 15px 20px;
      }

      /* Offset the responsive button for proper vertical alignment */
      .navbar .btn-navbar {
          margin-top: 10px;
      }



      /* CUSTOMIZE THE CAROUSEL
      -------------------------------------------------- */

      /* Carousel base class */
      .carousel {
          margin-bottom: 60px;
      }

      .carousel .container {
          position: relative;
          z-index: 9;
      }

      .carousel-control {
          height: 80px;
          margin-top: 0;
          font-size: 120px;
          text-shadow: 0 1px 1px rgba(0,0,0,.4);
          background-color: transparent;
          border: 0;
          z-index: 10;
      }

      .carousel .item {
          height: 500px;
      }
      .carousel img {
          position: absolute;
          top: 0;
          left: 0;
          min-width: 100%;
          height: 500px;
      }

      .carousel-caption {
          background-color: transparent;
          position: static;
          max-width: 550px;
          padding: 0 20px;
          margin-top: 200px;
      }
      .carousel-caption h1,
      .carousel-caption .lead {
          margin: 0;
          line-height: 1.25;
          color: #fff;
          text-shadow: 0 1px 1px rgba(0,0,0,.4);
      }
      .carousel-caption .btn {
          margin-top: 10px;
      }



      /* MARKETING CONTENT
      -------------------------------------------------- */

      /* Center align the text within the three columns below the carousel */
      .marketing .span4 {
          text-align: center;
      }
      .marketing h2 {
          font-weight: normal;
      }
      .marketing .span4 p {
          margin-left: 10px;
          margin-right: 10px;
      }


      /* Featurettes
      ------------------------- */

      .featurette-divider {
          margin: 80px 0; /* Space out the Bootstrap <hr> more */
      }
      .featurette {
          padding-top: 120px; /* Vertically center images part 1: add padding above and below text. */
          overflow: hidden; /* Vertically center images part 2: clear their floats. */
      }
      .featurette-image {
          margin-top: -120px; /* Vertically center images part 3: negative margin up the image the same amount of the padding to center it. */
      }

      /* Give some space on the sides of the floated elements so text doesn't run right into it. */
      .featurette-image.pull-left {
          margin-right: 40px;
      }
      .featurette-image.pull-right {
          margin-left: 40px;
      }

      /* Thin out the marketing headings */
      .featurette-heading {
          font-size: 50px;
          font-weight: 300;
          line-height: 1;
          letter-spacing: -1px;
      }



      /* RESPONSIVE CSS
      -------------------------------------------------- */

      @media (max-width: 979px) {

          .container.navbar-wrapper {
            margin-bottom: 0;
            width: auto;
        }
        .navbar-inner {
            border-radius: 0;
            margin: -20px 0;
        }

        .carousel .item {
            height: 500px;
        }
        .carousel img {
            width: auto;
            height: 500px;
        }

        .featurette {
            height: auto;
            padding: 0;
        }
        .featurette-image.pull-left,
        .featurette-image.pull-right {
            display: block;
            float: none;
            max-width: 40%;
            margin: 0 auto 20px;
        }
    @media (max-width: 767px) {

      .navbar-inner {
        margin: -20px;
    }

    .carousel {
        margin-left: -20px;
        margin-right: -20px;
    }
    .carousel .container {

    }
    .carousel .item {
        height: 300px;
    }
    .carousel img {
        height: 300px;
    }
    .carousel-caption {
        width: 65%;
        padding: 0 70px;
        margin-top: 100px;
    }
    .carousel-caption h1 {
        font-size: 30px;
    }
    .carousel-caption .lead,
    .carousel-caption .btn {
        font-size: 18px;
    }

    .marketing .span4 + .span4 {
        margin-top: 40px;
    }

    .featurette-heading {
        font-size: 30px;
    }
    .featurette .lead {
        font-size: 18px;
        line-height: 1.5;
    }

}

</style>";
$intervall = 42;
$today = date('Y-m-d');
$projectStartDate=$project[0]['startDate'];
$projectEndDate=$project[0]['endDate'];
$start = new DateTime($projectStartDate);
$end = new DateTime($projectEndDate);
$interval = new DateInterval('P1D');

$startTimeStamp=strtotime($projectStartDate);
$endTimeStamp=strtotime($projectEndDate);
$periodTimeStamp=$endTimeStamp-$startTimeStamp;
$daysNumber=($periodTimeStamp/60/60/24);
$daysNumber=floor($daysNumber);
//echo "$daysNumber aaaaaaaaaaaaaaaaaaaaaaaaaa";
$period = new DatePeriod($start, $interval, $end);

$week_number = 1;
$i = 1;
$month_number = 1;
$scrollToday = 1;
$flgTodayPlus = true;
$scrollDate = 1;
foreach ($period as $dt) 
{
  if ($dt->format("Y-m-d") == $today) 
  {
    $scrollToday+=$intervall;
    $flgTodayPlus = false;
  } 
  else if ($flgTodayPlus == true) 
  {
    $scrollToday+=$intervall;
  }

$year = $dt->format("Y");
$month = $dt->format("m");
$day = $dt->format("d");
$day_word = $dt->format("l");
$dt->format("Y-m-d");
$day_word_shamsi = '';

$shamsi = gregorian_to_jalali($year, $month, $day);

echo "
<script>
scrollDate[$scrollDate]='$shamsi[0]-$shamsi[1]-$shamsi[2]';
scrollDate[$scrollDate-1]='$shamsi[0]-$shamsi[1]-$shamsi[2]';
scrollDate[$scrollDate-2]='$shamsi[0]-$shamsi[1]-$shamsi[2]';
scrollDate[$scrollDate-3]='$shamsi[0]-$shamsi[1]-$shamsi[2]';
scrollDate[$scrollDate-4]='$shamsi[0]-$shamsi[1]-$shamsi[2]';
scrollDate[$scrollDate+1]='$shamsi[0]-$shamsi[1]-$shamsi[2]';
scrollDate[$scrollDate+2]='$shamsi[0]-$shamsi[1]-$shamsi[2]';
scrollDate[$scrollDate+3]='$shamsi[0]-$shamsi[1]-$shamsi[2]';
scrollDate[$scrollDate+4]='$shamsi[0]-$shamsi[1]-$shamsi[2]';
</script>
";
}
include_once 'header.php';
    //print_r($_GET);
?>
<body onload="">

    <!-- NAVBAR onload="updateScrollbar(document.getElementById('content-schedule'),document.getElementById('scrollbar-schedule'));updateScrollbar(document.getElementById('tasks'),document.getElementById('scrollbar-task'));"
    ================================================== -->
    <?php
    include_once 'ganttHeader.php';
    ?>

        <div class="menu container-fluid" >
            <div class="menu-content">
                <div class="menus ">
                    <!--<label class="checkbox inline without-timeline">
                        <input class="multi-filter" type="checkbox" checked="checked" value="multi-filter" >اعمال چند فیلتره
                    </label>-->
                    <div class="btn-group ">
                        <a class="btn btn-info dropdown-toggle" data-toggle="dropdown" href="#">
                            فیلتر اشخاص
                            <span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu stop-close-persons">
                            <div class="check-row">
                                <a href="" id='remove-person-filters' onclick="$('.persons-resources-filter').prop('checked',false);return false;" style="font-size:12px;text-decoration:underline;color:#000;"><i class='icon-remove' style='margin-top:4px; margin-left:6px;'></i>حذف فیلتر</a>
                            </div>
                            
                            <?php
                             /*echo'<div class="check-row" >
                            <label class="checkbox person-checks">
                            <input class="persons-resources-filter" checked="checked" type="checkbox" value="all"><strong>
                           نمایش همه
                           </strong>
                            </label>
                            </div>';*/

                            echo'<div class="check-row">
                            <label class="checkbox person-checks">
                            <input class="persons-resources-filter" type="checkbox" value="unassigned"><strong>
                            تخصیص داده نشده
                            </strong></label>
                            </div>';
                            echo '<strong>'.'اشخاص'. '</strong><hr style="margin:0px;">';
                            foreach ($projectUsers as $projectUser) {
                                if($projectUser['userId1']==$_SESSION['userId'])
                                {
                                  if($projectUser['permitionId']!=0)
                                    $permition=$_SESSION['permition'.$projectId]=$projectUser['rules'];
                                  else
                                    $permition=134217727;
                                }
                                echo'<div class="check-row">
                                <label class="checkbox person-checks">
                                <input class="persons-resources-filter" type="checkbox" value="' . $projectUser['name'] . '" >' . $projectUser['name'] . '
                                </label>
                                </div>';
                            }
                            echo '<strong>'.'منابع'
                            . '</strong><hr style="margin:0px;">';
                            foreach ($projectResources as $projectResource) {
                                echo'<div class="check-row">
                                <label class="checkbox person-checks">
                                <input class="persons-resources-filter" type="checkbox" value="' . $projectResource['name'] . '" >' . $projectResource['name'] . '
                                </label>
                                </div>';
                            }
                            ?>

                        </ul>
                    </div>

                    <div class="btn-group">
                        <a class="btn btn-info dropdown-toggle" data-toggle="dropdown" href="#">
                            فیلتر با پیشرفت
                            <span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu stop-close-percent">
                            <!--<div class="check-row">
                                <label class="checkbox person-checks">
                                    <input class="percent-filter" checked="checked" type="checkbox" value="all">
                                    نمایش همه
                                </label>
                            </div>-->
                            <div class="check-row">
                                <a href="" id='remove-percent-filters' onclick="$('.percent-filter').prop('checked',false);return false;" style="font-size:12px;text-decoration:underline;color:#000;"><i class='icon-remove' style='margin-top:4px; margin-left:6px;'></i>حذف فیلتر</a>
                            </div>
                            
                            <div class="check-row">
                                <label class="checkbox person-checks">
                                    <input class="percent-filter" type="checkbox" value="notdone">
                                    انجام نشده ها
                                </label>
                            </div>
                            <div class="check-row">
                                <label class="checkbox person-checks">
                                    <input class="percent-filter" type="checkbox" value="done">تکمیل شده ها                                     
                                </label>
                            </div>
                            <div class="check-row">
                                <label class="checkbox person-checks">
                                    <input class="percent-filter" type="checkbox" value="under">
                                    زیر
                                    <input type="text" id="percent-under" value="50" style="width:25px;height:10px;margin:0px;">
                                    % انجام شده
                                </label>
                            </div>
                            <div class="check-row">
                                <label class="checkbox person-checks">
                                    <input class="percent-filter" type="checkbox" value="above">
                                    بالای
                                    <input type="text" id="percent-above" value="49" style="width:25px;height:10px;margin:0px;">
                                    % انجام شده
                                </label>
                            </div>

                                    <input class="percent-filter" type="checkbox" value="between">
                                    بین
                                    <input type="text" id="percent-above-bet" value="25" style="width:25px;height:10px;margin:0px;">
                                    % و
                                    <input type="text" id="percent-under-bet" value="75" style="width:25px;height:10px;margin:0px;">%

                        </ul>
                    </div>
                    <div class="btn-group">
                        <a class="btn btn-info dropdown-toggle" data-toggle="dropdown" href="#">
                            فیلتر تاریخ
                            <span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu date-filters">
                            <!--<div class="check-row" id="showAllTasks">
                                <label class="checkbox person-checks">
                                    <input class="date-filter" name="week" type="radio" value="all">
                                    نمایش همه
                                </label>
                            </div>-->
                            <div class="check-row">
                                <a href="" id='remove-date-filters' onclick="$('.date-filter').prop('checked',false);return false;" style="font-size:12px;text-decoration:underline;color:#000;"><i class='icon-remove' style='margin-top:4px; margin-left:6px;'></i>حذف فیلتر </a>
                            </div>
                            <div class="check-row">
                                <label class="checkbox person-checks">
                                    <input class="date-filter" name="week" type="radio" value="oneweek">
                                    یک هفته پیشرو
                                </label>
                            </div>
                            <div class="check-row">
                                <label class="checkbox person-checks">
                                    <input class="date-filter" name="week" type="radio" value="towweek">
                                    دو هفته پیشرو
                                </label>
                            </div>
                            <div class="check-row">
                                <label class="checkbox person-checks">
                                    <input class="date-filter" name="week" type="radio" value="fourweek">
                                    چهار هفته پیشرو
                                </label>
                            </div>

                            <div class="check-row">
                                <label class="checkbox person-checks">
                                    <input class="date-filter" name="week" type="radio" value="inprogress">
                                    درحال انجام
                                </label>
                            </div>
                            <div class="check-row">
                                <label class="checkbox person-checks">
                                    <input class="date-filter" name="week" type="radio" value="untilltoday">
                                    تا قبل از امروز
                                </label>
                            </div>
                            <div class="check-row">
                                <label class="checkbox person-checks">
                                    <input class="date-filter" name="week" type="radio" value="aftertoday">
                                    بعد از امروز
                                </label>
                            </div>
                            <div class="check-row">
                                <label class="checkbox person-checks">
                                    <input class="date-filter" name="week" type="radio" value="befortoday">
                                    تاریخ گذشته ها
                                </label>
                            </div>
                        </ul>
                    </div>

                    <label class="checkbox inline without-timeline">
                        <input class="without-timeline-checked" type="checkbox" checked="checked" value="show" >بدون خط زمان
                    </label>
                </div>
            </div>
        </div>
    <div class="popover popover-person fade bottom in" style="border-radius: 2px;">
        <button type="button" class="close popover-close" style="padding: 2px 4px;" aria-hidden="true">
            &times;
        </button>
        <div class="task-row  right-panel-row">
            <input type="text" class="input-task" style="display: block !important;"/>
        </div>
        <div class="popover-content">
            <h3 class="popover-title" style="height:auto; padding: 4px 4px; color: #A3A3A3; background: #ffffff; width:150px;">افراد پروژه <span class="btn popover-ok set-persons"><i class='icon-ok'></i> ثبت</span></h3>
            <?php

            //$projectResources=$projectResources['rows'];
            echo '<div class="persons">';
            echo '
                    <div class="check-row">
                    <span style="display:none;">' . $_SESSION['userName'] . '</span>
                    <label class="checkbox person-checks">
                    <input class="persons" type="checkbox" value="' . $_SESSION['userName'] . '" >' . $_SESSION['userName'] . '
                    </label>
                    </div>';

            foreach ($projectUsers as $projectUser) {
                echo '
                    <div class="check-row">
                    <span style="display:none;">' . $projectUser['name'] . '</span>
                    <label class="checkbox person-checks">
                    <input class="persons" type="checkbox" value="' . $projectUser['userId1'] . '" >' . $projectUser['name'] . '
                    </label>
                    </div>';
            }
            echo'</div><h3 class="popover-title" style="height:auto; padding: 4px 4px; color: #A3A3A3; background: #ffffff; width:150px;">منابع</h3>'
            . '<div class="resources">';
            foreach ($projectResources as $projectResource) {
                echo '
                    <div class="check-row">
                    <span style="display:none;">' . $projectResource['name'] . '</span>
                    <label class="checkbox resource-checks">
                    <input class="resources" type="checkbox" value="' . $projectResource['id'] . '" >' . $projectResource['name'] . '
                    </label>
                    </div>';
            }
            ?>
        </div>
            </div>
    </div>
    <input id='date-selected' style='display:none;top: 60px;font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;position: relative;' value="">
    
    <div class="container marketing" style="width: 100%;">
        <h3 style="position:relative;top:50px;" class=""><?php echo $project[0]['name']; ?></h3>
        <hr class="featurette-divider" style="margin:15px 0px 0px;">
        <div class="row-fluid marketing" style="padding-top:35px;">
            <div class="tabbable"> <!-- Only required for left/right tabs -->
                    <div class="span12 right-body " id='right-body'>
                        <div class="tasks" id="tasks">
                            <?php
                            foreach ($projectUsers as $projectUser) {
                                if($projectUser['userId1']==$_SESSION['userId'])
                                {
                                  if($projectUser['permitionId']!=0)
                                    $permition=$_SESSION['permition'.$projectId]=$projectUser['rules'];
                                  else
                                    $permition=134217727;
                                }
                            }

                            $groups = $sqlOPR -> selectJoin('projects', 'projects.id as pid,projects.*,groups.*', 'groups', 'projects.id=groups.projectId', "projects.id=$projectId");
                            $groups=$groups['rows'];
                            $countGroup=count($groups);
                            $arangeGroups = '';
                            $groupss = '';
                            $g = 0;
                            $scripts='';
                            for ($j = 0; $j < count($groups); $j++) {
                                for ($i = 0; $i < count($groups); $i++) {
                                    if ($j == 0) {
                                        if ($groups[$i]['prev'] == $groups[$i]['id'] || $groups[$i]['prev'] == 0) {
                                            $start = $groups[$i]['id'];
                                            $groupss[$g++] = $groups[$i];
                                            $arangeGroups .= $start . ',';
                                            break;
                                        }
                                    } else {
                                        if ($groups[$i]['prev'] == $start) {
                                            $start = $groups[$i]['id'];
                                            $groupss[$g++] = $groups[$i];
                                            $arangeGroups .= $start . ',';
                                            break;
                                        }
                                    }
                                }
                            }
                            $arange = trim($arangeGroups, ',');
                            //   $sqlOPR->selectJoin($table, $selects, $jointbl, $on, $where);
                            $tasks = $sqlOPR -> selectJoin('groups', 'groups.name as gname,groups.id as gid,groups.*,groups.prev as gprev,tasks.*', 'tasks,projects', 'groups.id=tasks.groupId,projects.id=groups.projectId',"projects.id=$projectId", 'left join', '', ' FIELD(groups.id,' . $arange . ')');
                            //echo $tasks['query'];
                            $tasks = $tasks['rows'];
                            $countTasks=count($tasks);
                            $taskCount = 240;
                            //print_r($tasks);
                            $persons = '';
                            $percents = '';
                            $schedules = '';
                            $groupNumber = 0;
                            $dateStart='';
                            $dateEnd='';

                            $allTasks = $tasks;
                            //    var_dump($tasks);
                            for ($j = 0; $j < count($groups); $j++) {
                                $tasks = '';
                                $k = 0;
                                $allTasks = array_values($allTasks);
                                $cu = count($allTasks);
                                for ($x = 0; $x < $cu; $x++) {
                                    if ($allTasks[$x]['groupId'] == $groupss[$j]['id'] && $allTasks[$x]['name'] != '') {
                                        $tasks[$k++] = $allTasks[$x];
                                        unset($allTasks[$x]);
                                    }
                                }
                                //print_r($tasks);
                                // print_r($allTasks);
                                $g = 0;
                                $taskss = '';
                                $cut = count($tasks);
                                //echo "aaaaaa ".$tasks[0]['name']." aaaaaaa $cut aaaaa";
                                for ($f = 0; $f < $cut; $f++) {
                                    $tasks = @array_values($tasks);
                                    $cut1 = count($tasks);
                                    for ($i = 0; $i < $cut1; $i++) {
                                        if ($f == 0) {
                                            if ($tasks[$i]['prev'] == $tasks[$i]['id'] || $tasks[$i]['prev'] == 0) {
                                                $start = $tasks[$i]['id'];
                                                $taskss[$g++] = $tasks[$i];
                                                unset($tasks[$i]);
                                            }
                                        } else {
                                            if ($tasks[$i]['prev'] == $start) {
                                                $start = $tasks[$i]['id'];
                                                $taskss[$g++] = $tasks[$i];
                                                $arangeGroups .= $start . ',';
                                                unset($tasks[$i]);
                                            }
                                        }
                                    }
                                }
                                // print_r($taskss);
                                $tasks = $taskss;

                                $percentTotal=0;
                                $percentNumber=0;
                                $scheduleGroupWidth=0;
                                $scheduleMaxWidth=0;
                                $scheduleGroupRight=99999999999999;
                                //if($tasks!=''&&count($tasks)>0)
                                for ($i = 0; $i < count($tasks); $i++) {
                                    $percentTotal+=@$tasks[$i]['percent'];
                                    $percentNumber++;

                                    $scheduleWidth=@$tasks[$i]['width'];
                                    $scheduleMarginRight=@$tasks[$i]['marginRight'];
                                    if($scheduleMarginRight<$scheduleGroupRight&&$scheduleMarginRight>0)
                                    {
                                        $scheduleGroupRight=$scheduleMarginRight;
                                        $scheduleGroupStart=$scheduleWidth;
                                    }
                                    if($scheduleWidth+$scheduleMarginRight>$scheduleMaxWidth)
                                    {
                                        $scheduleMaxWidth=$scheduleWidth+$scheduleMarginRight;
                                        $scheduleMaxRight=$scheduleMarginRight;
                                    }
                                    $scheduleGroupWidth+=$scheduleWidth;
                                    $width = rand(50, 4000);
                                    $mod = $width % 20;
                                    $width -= $mod;
                                    $margin = rand(0, 1000);
                                    $mod = $margin % 22;
                                    $margin -= $mod;
                                    $percent = @$tasks[$i]['percent'];

                                    if ($i == 0 || $tasks[$i]['gname'] != $tasks[$i - 1]['gname']) {
                                        $style = "padding-right:0px;font-weight:bolder;";
                                        $startDiv = "
                                        <div class='ctask-group' id='itask-group" . $groupss[$j]['id'] . "' sid='" . $groupss[$j]['id'] . "' prev='" . @$groupss[$j - 1]['id'] . "' number='" . $groupNumber . "' next='" . @$next . "'>
                                        <div class='group-row right-panel-row' id='group-" . $groupss[$j]['id'] . "' sid='" . $groupss[$j]['id'] . "'><i class='icon-remove icon-group-remove'></i><i class='icon-minus icon-group'></i>
                                        <div id='group-name-" . $groupss[$j]['id'] . "' class='ctask' style='$style'>" . $groupss[$j]['name'] . "</div>
                                        <input type='text' class='input-group' sid='" . $groupss[$j]['id'] . "' id='input-group-" . $groupss[$j]['id'] . "' value=''>
                                        </div>
                                        <div class='groups-task'>";
                                        echo $startDiv;
                                        $groupNumber++;

                                        $schedules .= "<div class='schedule-group' id='schedule-group" . $groupss[$j]['id'] . "'>";
                                        $schedules .= "<div class='schedule-row right-panel-row' id='schedule-row" . $groupss[$j]['id'] . "' sidg='" . $groupss[$j]['id'] . "'>

                                            <div></div>
                                        </div>";
                                        $persons .= "<div class='person-group' id='person-group" . $groupss[$j]['id'] . "'>";
                                        $persons .= "<div class='person-row right-panel-row' id='person" . $groupss[$j]['id'] . "' sidg='" . $groupss[$j]['id'] . "' >
                                            افراد تخصیص داده شده
                                        </div>";

                                        $dateStart .= "<div class='dates-group' id='dates-group" . $groupss[$j]['id'] . "'>";
                                        $dateStart .= "<div class='dates-row right-panel-row' id='dates" . $groupss[$j]['id'] . "' sidg='" . $groupss[$j]['id'] . "' >
                                            تاریخ شروع
                                        </div>";
                                        $dateEnd .= "<div class='datee-group' id='datee-group" . $groupss[$j]['id'] . "'>";
                                        $dateEnd .= "<div class='datee-row right-panel-row' id='datee" . $groupss[$j]['id'] . "' sidg='" . $groupss[$j]['id'] . "' >
                                            تاریخ پایان
                                        </div>";

                                        $percents .= "<div class='percent-group' id='percent-group" . $groupss[$j]['id'] . "'>";
                                        $percents .= "<div class='percent-row right-panel-row' id='percent-row" . $groupss[$j]['id'] . "' sidg='" . $groupss[$j]['id'] . "' ></div>";
                                    }

                                    if (@$tasks[$i]['name'] != '')
                                    {
                                        $newCommentState=$tasks[$i]['newCommentState'];
                                        $commentTime=$tasks[$i]['newCommentTime'];
                                        $taskId=$tasks[$i]['id'];
                                        $nowTime=  time();
                                        $taskCommentIcon='comments2.png';
                                        if($newCommentState==1)
                                        {
                                            if(isset($_COOKIE['taskVisitCookie'.$taskId]))
                                            {
                                                if($commentTime>$_COOKIE['taskVisitCookie'.$taskId])
                                                    $taskCommentIcon='comments.png';
                                                else
                                                    $taskCommentIcon='comments2.png';

                                            }
                                            else
                                            {
                                                $taskCommentIcon='comments.png';
                                                setcookie('taskVisitCookie'.$taskId, 0, time()+(60*60*3600*30*12*24), "/");
                                            }
                                        }
                                        if($tasks[$i]['marginRight']==0)
                                        {
                                            $displayDragResize='none';
                                            $show='false';
                                        }
                                        else
                                        {
                                            $displayDragResize='block';
                                            $show='true';
                                        }
                                        $percentDiv='';
                                        if($tasks[$i]['type']=='0')
                                        {
                                            $type='';
                                            $percentDiv="<div class='percent80'>
                                                    <div class='percent' style='width:" . $percent . "%;' sid='" . $tasks[$i]['id'] . "'></div>
                                                    </div>";
                                        }else{
                                            $type='mile';

                                        }

                                        $schedules .= "
                                        <div class='schedule-row right-panel-row' id='schedule-row-" . $tasks[$i]['id'] . "' sid='" . $tasks[$i]['id'] . "' show='".$show."'>
                                        <div>
                                        <div class='resize-drag $type ui-widget-content' right1='".$tasks[$i]['marginRight']."' width1='".$tasks[$i]['width']."' style='right:0px !important; display:".$displayDragResize.";' id='schedule-" . $tasks[$i]['id'] . "' sid='" . $tasks[$i]['id'] . "'>
                                        $percentDiv
                                        </div></div></div>
                                        ";

                                        /* ----------PERSONS----- */
                                        $persons .= "
                                        <div class='person-row right-panel-row' id='person-" . $tasks[$i]['id'] . "' sid='" . $tasks[$i]['id'] . "' >" . $tasks[$i]['users'] . "</div>";
                                        /* ----------PERCENTS---------- */
                                        $percents .= "
                                        <div class='percent-row right-panel-row' id='percent-row-" . $tasks[$i]['id'] . "' sid='" . $tasks[$i]['id'] . "' >
                                        <div id='percent-" . $tasks[$i]['id'] . "' sid='" . $tasks[$i]['id'] . "' class='cpercent' style='height:100%;'>$percent</div>
                                        <input type='text' class='input-percent' sid='" . $tasks[$i]['id'] . "' id='input-percent-" . $tasks[$i]['id'] . "' value=''>
                                        </div>
                                        ";

                                        $dateStart .= "<div class='dates-row right-panel-row' id='date-start-row-" . $tasks[$i]['id'] . "' sid='" . $tasks[$i]['id'] . "' >

                                        <input id='date-start-input-" . $tasks[$i]['id'] . "' style='width:20px' sid='" . $tasks[$i]['id'] . "' class='date-start-input date-picker' style='height:100%;' value=''>
                                        <div id='date-start-" . $tasks[$i]['id'] . "'  sid='" . $tasks[$i]['id'] . "' class='date-start-" . $tasks[$i]['id'] . "' style='height:100%;'>" . $tasks[$i]['startDate'] . "</div>
                                        </div>
                                        ";

                                        $dateEnd .= "<div class='datee-row right-panel-row' id='date-end-row-" . $tasks[$i]['id'] . "' sid='" . $tasks[$i]['id'] . "' >

                                        <input id='date-end-input-" . $tasks[$i]['id'] . "' style='' sid='" . $tasks[$i]['id'] . "' class='date-end-input date-picker' style='height:100%;' value=''>
                                        <div id='date-end-" . $tasks[$i]['id'] . "' sid='" . $tasks[$i]['id'] . "' class='date-end-" . $tasks[$i]['id'] . " dates' style='height:100%;'>" . $tasks[$i]['endDate'] . "</div>
                                        </div>
                                        ";


                                        $style = "padding-right:14px;";
                                        $prev=(@$tasks[$i - 1]['id'])?$tasks[$i - 1]['id']:0;
                                        echo "<div class='task-row right-panel-row' id='task-" . $tasks[$i]['id'] . "' sid='" . $tasks[$i]['id'] . "' prev='" .  $prev. "'>
                                        <i class='icon-remove icon-task-remove'></i>
                                        <div id='task-name-" . $tasks[$i]['id'] . "' class='ctask' style='$style'>" . $tasks[$i]['id'] . $tasks[$i]['name'] . "</div>
                                        <input type='text' class='input-task' sid='" . $tasks[$i]['id'] . "' id='input-task-" . $tasks[$i]['id'] . "' value=''>
                                        </div>";
                                    }
                                    if (@$tasks[$i]['gname'] != @$tasks[$i + 1]['gname'] || $cut == 1) {
                                        $next = @$tasks[$i + 2]['gid'];
                                        $gid=$groupss[$j]['id'];
                                        $lastPercent=$percentTotal/$percentNumber;
                                        $percentGroup=round($lastPercent,1);
                                        $scheduleGroupRight-=2;
                                        $scheduleGroupWidth=$scheduleMaxWidth-$scheduleGroupRight-4;
                                        $scripts.= "$('#percent-row".$gid."').html('".$percentGroup."');"
                                                . "$('#schedule-group-$gid').css({'right':'$scheduleGroupRight"."px','width':'$scheduleGroupWidth"."px'});";
                                        /* -------------SCHEDULES----------- */
                                        /*style='width:" . $tasks[$i]['width'] . "px; right:" . ($tasks[$i]['marginRight'] - 2) . "px;'*/
                                        $schedules .= "

                                        </div>

                                        ";
                                        /* ----------PERSONS----- */
                                        $persons .= "

                                        </div>";
                                        /* ----------PERCENTS---------- */
                                        $percents .= "
                                        </div>
                                        ";
                                        $dateEnd.='</div>';
                                        $dateStart.='</div>';
                                        $muted='';
                                        if($permition<2)$muted='muted';

                                        echo "</div></div>";
                                        $flg1 = 1;
                                    }
                                }
                            }
                            ?>
                            </div>
                    <div class="span9 schedules" id="content-schedule" >



                        <div class="schedule-rows" id='schedule-rows'>
                            <?php
                                                echo $schedules;

                            ?>
                        </div>
                    </div>
                <div class="set-person" id="set-person">
                    <?php
                    echo $persons;
                    /* for ($i=0; $i < $taskCount; $i++)
                     {
                     echo "
                     <div class='person-row' id='person_$i' sid='$i' >
                     <div style='height:100%;'>$i pp</div>
                     </div>
                     ";
                     } */
                    ?>
                </div>
                <div class="set-percent" id="set-percent">
                    <?php
                                    echo $percents;
                    ?>
                </div>
                <div class="date-start" id="st">
                    <?php
                                    echo $dateStart;
                    ?>
                </div>
                <div class="date-end" id="ed">
                    <?php
                                    echo $dateEnd;
                    ?>
                </div>

            </div>
                        </p>
            </div>
        </div>
        <?php
    //print_r($_POST);

        if (isset($_POST['done'])) {
            $string = "Refresh: 3;url=" . URL . "gantt/$params[1]/schedule";
            header($string);
        }
        include_once "footer.php";
        echo "<script>"
        . $scripts
        . "</script>";
        ?>
        <script>
            scrollSchedule ='<?php echo $scrollToday; ?>';
            daysNumber = '0';
            userLogin = '<?php echo $_SESSION['userLogin']; ?>';
            userName = '<?php echo $_SESSION['userName']; ?>';
            projectId = '<?php echo $projectId; ?>';
            permition = <?php echo $permition; ?>;
            ajaxRequest = '<?php echo $_SESSION['ajaxRequest']; ?>';
            projectStartDate = '<?php echo $projectStartDate; ?>';
        </script>
        <script src=http://127.0.0.1/gantt/js/default.js></script>

        <script src=<?php echo URL; ?>js/list.js></script>
        <script src=<?php echo URL; ?>js/sortableList.js></script>
        <script src=<?php echo URL; ?>js/persian-datepicker.js></script>
        <script src=<?php echo URL; ?>js/persian-date.js></script>
        <script src=<?php echo URL; ?>js/invite.js></script>
        <script src=<?php echo URL; ?>js/filtersList.js></script>

