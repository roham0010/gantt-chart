<div class="navbar-wrapper" style="margin-top:0px;">
        <!-- Wrap the .navbar in .container to center it within the absolutely positioned parent. -->
        <div class="">

            <div class="navbar navbar-inverse">
                <div class="navbar-inner">
                    <!-- Responsive Navbar Part 1: Button for triggering responsive navbar (not covered in tutorial). Include responsive CSS to utilize. -->
                    <button type="button" class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="brand" href="<?=URL?>"><?=SITENAME?></a>
                    <!-- Responsive Navbar Part 2: Place all navbar contents you want collapsed withing .navbar-collapse.collapse. -->
                    <div class="nav-collapse collapse">
                        <ul class="nav">
<!--                            <li class="active"><a href="<?=URL?>">صفحه اصلی</a></li>-->
                            <li><a href="#contact"></a></li>
                            <li><a href="#about">راهنما</a></li>
                            <!-- Read about Bootstrap dropdowns at http://twitter.github.com/bootstrap/javascript.html#dropdowns -->
                            <li><img class="profile-pic" src="<?=URL."profiles/".$_SESSION['userId'].'.jpg'?>"></li>
                            <li class="dropdown pull-right">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown"><?=$_SESSION['userName']?> <b class="caret"></b></a>
                                <ul class="dropdown-menu">
                                    <li><a href="<?=URL?>profile">ویرایش پروفایل</a></li>
                                    <li><a href="<?=URL?>repass">ویرایش رمز عبور</a></li>
                                    <li class="divider"></li>
                                    <li><a href="<?=URL?>exit">خروج</a></li>
                                </ul>
                            </li>
                        </ul>
                    </div><!--/.nav-collapse -->
<!--                    <a class="btn btn-primary" href="<?php echo URL;?>exit" role="button" data-toggle="modal" style="float: left;margin-top: 8px; padding:5px 39px 16px; height: 10px;">خروج</a>-->
                </div><!-- /.navbar-inner -->
            </div><!-- /.navbar -->
        </div> <!-- /.container -->
    </div>