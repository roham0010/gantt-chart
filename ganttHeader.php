<?php
if((!isset($_SESSION['userId']) || $_SESSION['userId']=='') && (!isset($_COOKIE['hashCookie']) || $_COOKIE['hashCookie']==''))
{
	signin();
}
if(isset($_COOKIE['hashCookie']) && !isset($_SESSION['userName']) && $_COOKIE['hashCookie']!='')
{
	$hashCookie=$_COOKIE['hashCookie'];
	$user=$sqlOPR->select('users','',"hashCookie=$hashCookie");
	if(count($user)>0)
	{
		if($user[0]['block']==0)
		{
			include DIR . "subfiles/sessionSet.php";
		}
		else
		{
			$errMessageForm='کاربر گرامی اکانت شما در سایت بلاک شده است.<br> لطفا در صورت اعتراض با مدیریت سایت تماس حاصل فرمایید';
			$signinError=1;
		}
	}
	else
		signin();
}
if(!isset($_SESSION['userId']))
{
	signin();
}
$page=(isset($params[2])&&$params[2]!='')?$params[2]:'schedule';
$arrayActive=array(''=>'','schedule'=>'','invite'=>'','accessLevel'=>'','list'=>'');
$arrayActive[$page]='active';
?>
    <div class="navbar-wrapper" style="margin:0px; height: 30px;">
		<!-- Wrap the .navbar in .container to center it within the absolutely positioned parent. -->

        <a class="brand brand1" href="<?= URL.'ganttmanager' ?>"   style="padding-left: 0px !important;"><?=SITENAME?></a>
        <span id='loading'><svg  id='loading1' xmlns="http://www.w3.org/2000/svg" viewBox="0 0 50 32" width="50" height="32" fill="white" style='width:40px;height:29px;'>
              <path transform="translate(2)" d="M 0 12 V 20 H 4 V 12 Z"> 
                <animate attributeName="d" values="M0 12 V20 H4 V12z; M0 4 V28 H4 V4z; M0 12 V20 H4 V12z; M0 12 V20 H4 V12z" dur="1.2s" repeatCount="indefinite" begin="0" keytimes="0;.2;.5;1" keySplines="0.2 0.2 0.4 0.8;0.2 0.6 0.4 0.8;0.2 0.8 0.4 0.8" calcMode="spline"/>
              </path>
              <path transform="translate(8)" d="M 0 12 V 20 H 4 V 12 Z">
                <animate attributeName="d" values="M0 12 V20 H4 V12z; M0 4 V28 H4 V4z; M0 12 V20 H4 V12z; M0 12 V20 H4 V12z" dur="1.2s" repeatCount="indefinite" begin="0.15" keytimes="0;.2;.5;1" keySplines="0.2 0.2 0.4 0.8;0.2 0.6 0.4 0.8;0.2 0.8 0.4 0.8" calcMode="spline"/>
              </path>
              <path transform="translate(14)" d="M 0 10.3705 V 21.6295 H 4 V 10.3705 Z">
                <animate attributeName="d" values="M0 12 V20 H4 V12z; M0 4 V28 H4 V4z; M0 12 V20 H4 V12z; M0 12 V20 H4 V12z" dur="1.2s" repeatCount="indefinite" begin="0.30" keytimes="0;.2;.5;1" keySplines="0.2 0.2 0.4 0.8;0.2 0.6 0.4 0.8;0.2 0.8 0.4 0.8" calcMode="spline"/>
              </path>
              <path transform="translate(20)" d="M 0 4.82537 V 27.1746 H 4 V 4.82537 Z">
                <animate attributeName="d" values="M0 12 V20 H4 V12z; M0 4 V28 H4 V4z; M0 12 V20 H4 V12z; M0 12 V20 H4 V12z" dur="1.2s" repeatCount="indefinite" begin="0.45" keytimes="0;.2;.5;1" keySplines="0.2 0.2 0.4 0.8;0.2 0.6 0.4 0.8;0.2 0.8 0.4 0.8" calcMode="spline"/>
              </path>
              <path transform="translate(26)" d="M 0 6.29171 V 25.7083 H 4 V 6.29171 Z">
                <animate attributeName="d" values="M0 12 V20 H4 V12z; M0 4 V28 H4 V4z; M0 12 V20 H4 V12z; M0 12 V20 H4 V12z" dur="1.2s" repeatCount="indefinite" begin="0.60" keytimes="0;.2;.5;1" keySplines="0.2 0.2 0.4 0.8;0.2 0.6 0.4 0.8;0.2 0.8 0.4 0.8" calcMode="spline"/>
              </path>
              <path transform="translate(32)" d="M 0 0 V 20 H 4 V 0 Z">
            <animate attributeName="d" values="M0 12 V20 H4 V12z; M0 4 V28 H4 V4z; M0 12 V20 H4 V12z; M0 12 V20 H4 V12z" dur="1.2s" repeatCount="indefinite" begin="0.75" keytimes="0;.2;.5;1" keySplines="0.2 0.2 0.4 0.8;0.2 0.6 0.4 0.8;0.2 0.8 0.4 0.8" calcMode="spline"/>
          </path>
          <path transform="translate(38)" d="M 0 12 V 20 H 4 V 12 Z">
            <animate attributeName="d" values="M0 12 V20 H4 V12z; M0 4 V28 H4 V4z; M0 12 V20 H4 V12z; M0 12 V20 H4 V12z" dur="1.2s" repeatCount="indefinite" begin="0.90" keytimes="0;.2;.5;1" keySplines="0.2 0.2 0.4 0.8;0.2 0.6 0.4 0.8;0.2 0.8 0.4 0.8" calcMode="spline"/>
          </path>
            </svg></span>
        <span id='logo'>
        <svg  id='logo1' xmlns="http://www.w3.org/2000/svg" viewBox="0 0 50 32" width="50" height="32" fill="white" style='width:40px;height:29px;'>
		              <path transform="translate(2)" d="M 0 14 V 20 H 4 V 14 Z">
		                <!-- <animate attributeName="d" values="M0 12 V20 H4 V12z; M0 4 V28 H4 V4z; M0 12 V20 H4 V12z; M0 12 V20 H4 V12z" dur="1.2s" repeatCount="indefinite" begin="0.2" keytimes="0;.2;.5;1" keySplines="0.2 0.2 0.4 0.8;0.2 0.6 0.4 0.8;0.2 0.8 0.4 0.8" calcMode="spline"/> -->
		              </path>
		              <path transform="translate(8)" d="M 0 12 V 20 H 4 V 12 Z">
		                <!-- <animate attributeName="d" values="M0 12 V20 H4 V12z; M0 4 V28 H4 V4z; M0 12 V20 H4 V12z; M0 12 V20 H4 V12z" dur="1.2s" repeatCount="indefinite" begin="0.2" keytimes="0;.2;.5;1" keySplines="0.2 0.2 0.4 0.8;0.2 0.6 0.4 0.8;0.2 0.8 0.4 0.8" calcMode="spline"/> -->
		              </path>
		              <path transform="translate(32)" d="M 0 0 V 20 H 4 V 0 Z">
		                <!-- <animate attributeName="d" values="M0 12 V20 H4 V12z; M0 4 V28 H4 V4z; M0 12 V20 H4 V12z; M0 12 V20 H4 V12z" dur="1.2s" repeatCount="indefinite" begin="0.2" keytimes="0;.2;.5;1" keySplines="0.2 0.2 0.4 0.8;0.2 0.6 0.4 0.8;0.2 0.8 0.4 0.8" calcMode="spline"/> -->
		              </path>
		              <path transform="translate(38)" d="M 0 12 V 20 H 4 V 12 Z">
		                <!-- <animate attributeName="d" values="M0 12 V20 H4 V12z; M0 4 V28 H4 V4z; M0 12 V20 H4 V12z; M0 12 V20 H4 V12z" dur="1.2s" repeatCount="indefinite" begin="0.2" keytimes="0;.2;.5;1" keySplines="0.2 0.2 0.4 0.8;0.2 0.6 0.4 0.8;0.2 0.8 0.4 0.8" calcMode="spline"/> -->
		              </path>
		              <path transform="translate(14)" d="M 0 10 V 20 H 4 V 10 Z">
		                <!-- <animate attributeName="d" values="M0 12 V20 H4 V12z; M0 4 V28 H4 V4z; M0 12 V20 H4 V12z; M0 12 V20 H4 V12z" dur="1.2s" repeatCount="indefinite" begin="0.2" keytimes="0;.2;.5;1" keySplines="0.2 0.2 0.4 0.8;0.2 0.6 0.4 0.8;0.2 0.8 0.4 0.8" calcMode="spline"/> -->
		              </path>
		              <path transform="translate(20)" d="M 0 7 V 20 H 4 V 7 Z">
		                <!-- <animate attributeName="d" values="M0 12 V20 H4 V12z; M0 4 V28 H4 V4z; M0 12 V20 H4 V12z; M0 12 V20 H4 V12z" dur="1.2s" repeatCount="indefinite" begin="0.2" keytimes="0;.2;.5;1" keySplines="0.2 0.2 0.4 0.8;0.2 0.6 0.4 0.8;0.2 0.8 0.4 0.8" calcMode="spline"/> -->
		              </path>
		              <path transform="translate(26)" d="M 0 4 V 20 H 4 V 4 Z"> 
		                <!-- <animate attributeName="d" values="M0 12 V20 H4 V12z; M0 4 V28 H4 V4z; M0 12 V20 H4 V12z; M0 12 V20 H4 V12z" dur="1.2s" repeatCount="indefinite" begin="0" keytimes="0;.2;.5;1" keySplines="0.2 0.2 0.4 0.8;0.2 0.6 0.4 0.8;0.2 0.8 0.4 0.8" calcMode="spline"/> -->
		              </path>
		            </svg>
        </span>
		<div class="container" style='margin:0px;padding:0px;'>
                <div class="navbar navbar-inverse" style="margin:0 auto;float:none;">
				<div class="navbar-inner" style="min-height: 30px;height: 30px; border-radius: 0px;">
					<!-- Responsive Navbar Part 1: Button for triggering responsive navbar (not covered in tutorial). Include responsive CSS to utilize. -->
					<button type="button" class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<!-- Responsive Navbar Part 2: Place all navbar contents you want collapsed withing .navbar-collapse.collapse. -->
					<div class="nav-collapse collapse" style="margin-right: 105px;">
						<ul class="nav nav-tabs" style="float:none;height: 26px; padding-top:2px;">
                            <li class="<?=$arrayActive['schedule'].$arrayActive['']?> h30px" style="margin-right:2% !important;">
								<a href="<?php echo URL.$params[0].'/'.$params[1]."/schedule?projectId=$_GET[projectId]"; ?>" class="h30px">زمانبندی ها</a>
							</li>
<!--
							<li class="<?php echo $arrayActive['list'].$arrayActive[''];?> h30px">
								<a href="<?php echo URL.$params[0].'/'.$params[1]."/list?projectId=$_GET[projectId]"; ?>" class="h30px">لیست کارها</a>
							</li>
-->
							<li class="<?php echo $arrayActive['invite'].$arrayActive[''];?> h30px">
								<a href="<?php echo URL.$params[0].'/'.$params[1]."/invite?projectId=$_GET[projectId]"; ?>" class="h30px">کارمندان</a>
							</li>
							<li class="<?php echo $arrayActive['accessLevel'].$arrayActive[''];?> h30px">
								<a href="<?php echo URL.$params[0].'/'.$params[1]."/accessLevel?projectId=$_GET[projectId]"; ?>" class="h30px">سطوح دسترسی</a>
							</li>
							<!-- Read about Bootstrap dropdowns at http://twitter.github.com/bootstrap/javascript.html#dropdowns -->

						</ul>
					</div><!--/.nav-collapse -->
<!--
                    <ul class="nav nav-tabs" style="border:0px;" >
						<li class="dropdown" style="float: left;height: 30px;">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown" style="margin: 0px;padding: 0px; min-height: 30px;"> <img src="<?php echo URL; ?>img/lists4.png" width="28px" height="20px" style="background-color: #F3F3F3;border-radius: 25px;border: 1px solid #FFFFFF;" /> </a>
							<ul class="dropdown-menu" style="border-radius: 0px;  right: -128.5px;">
								<li>
									<a href="#">عمل</a>
								</li>
								<li>
									<a href="#">عملی دیگر</a>
								</li>
								<li>
									<a href="#">یک چیز دیگر</a>
								</li>
								<li class="divider"></li>
								<li class="nav-header">
									عنوان ناوبری
								</li>
								<li>
									<a href="#">پیوند جداشده</a>
								</li>
								<li>
									<a href="#">یک پیوند جداشده دیگر</a>
								</li>
							</ul>
						</li>
					</ul>
-->
				</div><!-- /.navbar-inner -->
			</div><!-- /.navbar -->

		</div>
		<!-- /.container -->
        <script type="text/javascript">
            $('#loading').hide();
            $('#loading1').hide();
            // $('#logo').hide();
            // $('#logo1').hide();
        </script>
	</div><!-- /.navbar-wrapper -->
