<?php
$description="از این صفحه کاربران مشتاق به استفاده از نمودار گانت ساز فارسی یا Gantt Diagram Maker ثبت نام کرده و رایگان از آن استفاده کنند.";
$title="ساخت نمودار گانت | رسم گانت آنلاین| گانت | Gantt Diagram | گانت با تاریخ فارسی | دیاگرام گانت با تاریخ هجری شمسی | ثبت نام گانت";
{
$style="
<style>
body {
	        padding-top: 40px;
	        padding-bottom: 40px;
	        background-color: #f5f5f5;
	      }

	      .form-signin {
	        max-width: 300px;
	        padding: 19px 29px 29px;
	        margin: 0 auto 20px;
	        background-color: #fff;
	        border: 1px solid #e5e5e5;
	        -webkit-border-radius: 5px;
	           -moz-border-radius: 5px;
	                border-radius: 5px;
	        -webkit-box-shadow: 0 1px 2px rgba(0,0,0,.05);
	           -moz-box-shadow: 0 1px 2px rgba(0,0,0,.05);
	                box-shadow: 0 1px 2px rgba(0,0,0,.05);
	      }
	      .form-signin .form-signin-heading,
	      .form-signin .checkbox {
	        margin-bottom: 10px;
	      }
	      .form-signin input[type='text'],
	      .form-signin input[type='password'] {
	        font-size: 16px;
	        height: auto;
	        margin-bottom: 15px;
	        padding: 7px 9px;
	      }
</style>";
}
include_once 'header.php';
global $validateMsg;
echo '<div class="container">';
if(isset($_GET['params'])&&$_GET['params']!='')
{
	$params=explode('/', $_GET['params']);

	$email=@$params[1];
	$hashEmail=@$params[2];
	$hashEmail2=@$params[3];
//    strip_tags();
	//echo $hashEmail;
    $user="";
    if(isset($params[1]) || isset($_POST['signup-gantt']))
    {
        include_once DIR.'classes/formHelper.php';
        $formHelper=new formHelper();
        $caption='';
        $name='frm-Gantt-Maker-Persian';
        $action='';
        $method='post';
        $onsubmit="name|r| |2#-| | |onblur,phone| |nu|8#-|+98| |onblur,pass|r| |6#-| | |onblur";
        $formHelper->form($caption, $name, $action, $method,$onsubmit,'','form-signin','border-radius:0px;',"multipart/form-data");
        echo '<h1 class="signin-head"> تکمیل ثبت نام گانت ساز فارسی </h1>';
        $user=$sqlOPR->select('users','hash,email,pass,id,type,name',"hash=$hashEmail,email=$email");
        if(sizeof($user)==0 || md5($hashEmail)!=$hashEmail2)
        {
            signin(8);

            message( "این لینک قبلا اسفاده شده است و منقضی شده است، لطفا وارد شوید یا اگر رمز عبور خود را فراموش کرده اید از بخش ورود عملیات مربوطه با انجام دهید!<br>تا چند لحظه دیگر به صفحه ورود منتقل میشوید.");
        }
        elseif(sizeof($user)>0 && $user[0]['pass']!='')
        {
            message("شما قبلا ثبت نام کرده اید.<br> پس از چند ثانیه به صفحه ورود منتقل میشوید");
            myHeader("signin/$email",5);
        }
//        else
//            $user=$sqlOPR->select('users','hash,email',"hash=$hashEmail,email=$email");
        if(count($user)>0 && md5($hashEmail)==$hashEmail2)
        {
            $user=$user[0];
            //print_r($user);
            if(isset($_POST['signup-gantt']))
            {
                $name=$_POST['name'];
                $phone=$_POST['phone'];
                $company=$_POST['company'];
                $web=$_POST['web'];
                $pass=$_POST['pass'];
                $profilePic=$_FILES['profilePic']['name'];
                $signupError=0;
                include_once DIR.'classes/formValidation.php';
                $validation=new formValidation();
                $inputValid = array(
                    'name' =>'نام|r|len=3#-',
                    'phone' =>'نام|nu|+98',
                    'pass' => 'رمز عبور|sc|len=6#-',
                    );
                $validateMsg=$validation->validation($inputValid);
                //pr($validateMsg);
                if($validateMsg==''||count($validateMsg)==0)
                {
                    if($name==''||$pass=='')
                    {
                        $errMessageFrom='نام کاربری و رمز عبور اجباری اند';
                        $signupError=1;
                    }
                    else
                    {
                        //uplooooooooad
                        if($profilePic!='')
                        {
                            $target_dir = "profiles/";

                            $target_file = $target_dir . basename($user['email']);
                            $imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
                            // Check if image file is a actual image or fake image
                            $check = @getimagesize($_FILES["profilePic"]["tmp_name"]);
                            if($check == false) {
                                $errMessageFrom='برای تصویر باید فایل عکس انتخاب کنید';
                                $signupError=1;
                            }
                            // Check if file already exists
                            if (file_exists($target_file)) {
                                $errMessageFrom='تصویر قبلا ثبت شده است';
                                $signupError=1;
                            }
                            // Check file size
                            if ($_FILES["profilePic"]["size"] > 500000) {
                               $errMessageFrom='حجم تصویر باید کمتر از 500 کیلوبایت باشد';
                                $signupError=1;
                            }
                            // Allow certain file formats
                            $sub=explode('.', $profilePic);
                            $ln=count($sub);
                            $type=$sub[$ln-1];
                            $imageFileType=$type;
        //					echo "$type";
                            if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
                            && $imageFileType != "gif" ) {
                                $errMessageFrom='نوع فایل پشتیبانی نمیشود';
                                $signupError=1;
                            }
                            $target_file.='.'.$imageFileType;
                            // Check if $uploadOk is set to 0 by an error
                            if ($signupError == 1) {

                            // if everything is ok, try to upload file
                            } else {
                                include_once DIR.'classes/hashMaker.php';
                                $hash=new hashMaker();
                                $pass=$hash->hash($pass);
                                if (move_uploaded_file($_FILES['profilePic']['tmp_name'], $target_file)) {
                                    $updates="name=$name,phone=$phone,company=$company,web=$web,pass=$pass,hash=";
                                    $where="email=$user[email]";
                                    if($sqlOPR->update('users', $updates, $where))
                                    {
                                        $user=$sqlOPR->select('users','',"email=$email");
                                        print_r($user);
                                        include_once DIR."subfiles/sessionSet.php";
                                        $string="location:".URL."ganttmanager/first";
                                        header($string);

                                    }
                                } else {
                                    $signupError=1;
                                    $errMessageFrom="متاسفانه خطایی رخ داد لطفا دوباره تلاش کنید";
                                }
                            }
                        }
                        else
                        {

                            $where="email=$user[email]";
                            $user=$sqlOPR->select('users','',"email=$email");

                            include_once DIR.'classes/hashMaker.php';
                            $hash = new hashMaker();
                            $hashCookie=$hash->hash($user[0]['email'].$user[0]['name'].$user[0]['id'].time());
                            $pass=$hash->hash($pass);
                            $updates="name=$name,phone=$phone,company=$company,web=$web,pass=$pass,hash=,hashCookie=$hashCookie";
                            $user[0]['hashCookie']=$hashCookie;
                            if($sqlOPR->update('users', $updates, $where))
                            {
                                include_once DIR."subfiles/sessionSet.php";
                                myHeader("ganttmanager/first",4);
                                message("ثبت نام تکمیل شد، پس از چند ثانیه به صفحه کاربری منتقل اگر نشدید، در غیر این صورت <a href='".URL."ganttmanager/first'>اینجا</a> کلیک کنید.");
                            }
                        }
                    }
                }
                else
                {
                    $signupError=1;
                    $errMessageFrom='لطفا داده ها را بررسی کنید!';
                }

            }
            if(!isset($_POST['signup-gantt'])||$signupError==1)
            {

                echo @$errMessageFrom;
                echo "<span class='input-block-level uneditable-input'>$user[email]</span>";
                $formHelper->input('1نام'
                                   ,"1نام خود را وارد کنید",'name','name|r| |2#-| | |onblur','','text',"input-block-level",'','');
                $formHelper->input('1شماره همراه'
                                   ,'1شماره همراه','phone','phone| |nu| |+98| |onblur','','',"input-block-level");
                $formHelper->input('1نام شرکت'
                                   ,"1نام شرکت",'company','','','text',"input-block-level",'','');
                $formHelper->input('1آدرس وب'
                                   ,"1آدرس وب",'web','','','text',"input-block-level",'','');
                $formHelper->input('1کلمه عبور'
                                   ,"1رمز عبور",'pass','pass|r| |6#-|sc| |onblur','','text',"input-block-level",'','');
                ?>

                        <?php
                        //	 <input name="profilePic" type="file" class="btn" style="margin-right: -20px;">

                         echo "

                         <div class='browse-wrap'>
        <div class='title'>تصویر پروفایل</div>";
                         $formHelper->input('0آپلود تصویر پروفایل'
                                            ,"1فایل",'profilePic','','','file',"btn upload",'','');
                echo "</div>
    <span class='upload-path'></span>
    <script>
                         var span = document.getElementsByClassName('upload-path');
                        // Button
                        var uploader = document.getElementsByName('profilePic');
                        // On change
                        for( item in uploader ) {
                          // Detect changes
                          uploader[item].onchange = function() {
                            // Echo filename in span
                            span[0].innerHTML = this.files[0].name;
                          }
                        }
                        </script>";

                        ?>
                <?php
                $formHelper->submit('signup-gantt', "ثبت نام رایگان گانت ساز",'btn w100','',"");
            }
	   }
    }
	else
	{
        include_once DIR . 'classes/formHelper.php';
        $formHelper = new formHelper();
        $caption = '';
        $name = 'signup-Gantt';
        $action = "";
        $method = 'post';
        $formHelper -> form($caption, $name, $action, $method, 'email|r| | |@| |onblur','','form-signin','margin-top:100px;');
        echo '<h1 class="signin-head">ثبت نام '.SITENAME.'</h1>';
        if (isset($_POST['signup-gantt1']))
        {
                $email = $_POST['email'];
                $user = $sqlOPR -> select('users', '', "email=$email");
//                    print_r($user);
                if (count($user) == 0 || (isset($user[0]['pass']) && $user[0]['pass']==''))
                {
                    include_once 'classes/hashMaker.php';
                    $hashMaker = new hashMaker();
                    $emailHash = $hashMaker -> hash($email);
        //            echo "email sended check your email";
//                        echo $inserts;
                    if(count($user) > 0)
                    {
                        $inserts = "email=$email,hash=$emailHash,hashCookie=".time();
                        $userId=$user[0]['id'];
                        $id = $sqlOPR -> update('users', $inserts,"id=$userId");
                    }
                    else
                    {
                        $inserts = "email=$email;hash=$emailHash;hashCookie=".time();
                        $id = $sqlOPR -> insert1('users',array('email','hash','hashCookie'),array($email,$emailHash,time()));
                    }
                    if($id)
                    {
                        $emailHash2 = md5($emailHash);
                        $text="با تشکر از انتخاب شما <br>
                        جهت تکمیل ثبت نام بر روی لینک زیر کلیک کنید:<br><a href='" . URL . "signup/$email/$emailHash/$emailHash2'>تکمیل ثبت نام</a>";
                        $subject='ثبت نام در '.SITENAME;
                        $arraySMS=array('to'=>$email,'text'=>$text,'subject'=>$subject);
                        if(Mailgun($arraySMS))
                        {
                            echo "درخواست ثبت نام شما ثبت شد، ایمیلی حاوی پیوند تکمیل ثبت نام به ایمیل شما ارسال شده لطفا برای تکمیل ثبت نام بر روی آن کلیک کنید";
                            echo $text;
                            $id=$id['id'];
//                            $sqlOPR->delete('users',"id=$id");
                        }
                        else
                        {
                            $id=$id['id'];
                            $sqlOPR->delete('users',"id=$id");
                            echo 'لطفا بعدا دوباره تلاش کنید!';
                        }
                    }
                    else
                        echo "ثبت نام ناموفق";
                }
                else if (count($user) > 0)
                {
                    $email = $_POST['email'];
                    $emailSite = explode('@', $email);
                    $ln = count($emailSite) - 1;
                    $errMessageForm = "
                    ایمیل قبلا استفاده شده برای استفاده از امکانات
                    <a href='".URL."signin' class='label label-info' target='_blank'>
                     وارد شوید</a>
                      ";
//                    echo $errMessageForm;
                    $emailSend = 0;
                }

        }
//        else
//        {

        echo "<span id='errMes'>" . @$errMessageForm . "</span>";
        $formHelper -> input('1ایمیل',
                             "1مثال  {name@mail.com}", 'email', '', '', 'text', 'input-block-level', '');
        $formHelper -> submit('signup-gantt1', "ثبت نام رایگان گانت ساز", 'btn w100', '', "",0);
        echo "<div class='footer-signin'><a class='signup-lnk' href='".URL."signin' >ورود به ".SITENAME."</a></div></form>";

//        }
	}
}
else
{
	echo "
	<script type='text/javascript'>
	alert('خطایی رخ داده لطفا با ما تماس حاصل فرمایید');
	</script>";
	$string='location:'.URL.'404.php';
	//header($string);
}
		include_once 'footer.php';
?>
