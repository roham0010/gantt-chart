<?php
define('SITENAME', "آقای گانت");
define('SITENAMEEN', "Gantt Chart");
define('SITEEMAIL', "info@itmyteam.com");
define('DIR', $_SERVER['DOCUMENT_ROOT'] . "/gantt/");
define('URL', 'http://' . $_SERVER['HTTP_HOST'] . "/gantt/");
define('DOMAIN', 'http://' . $_SERVER['HTTP_HOST']);

define('USERTABLE', 'users');
define('SITETITTLE', 'نمودار، دیاگرام  گانت ساز آنلاین');
//define('SITENAME', 'گانت');
//topics defines
define('TOPICTABLE', 'topic');
define('VOTETABLE', 'vote');
define('SUBVOTETABLE', 'subVote');
global $globals;
$global = array(
    'REGISTEREMAILBODY' =>
    "با سلام خدمت شما کاربر گرامی...<br>این ایمیل نسبت به درخواست شما برای ثبت نام در وب سایت " . SITENAME . " ارسال گردیده است لطفا بر روی لینک زیر جهت تکمیل مراحل ثبت نام کلیک نمایید.<br>

    <br>با تشکر...",
);

if ($_SERVER['HTTP_HOST'] == '127.0.0.1' || $_SERVER['HTTP_HOST'] == '192.168.1.113') {
//    echo 'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa';
    define('HOST', '127.0.0.1');
    define('DBN', 'gantt');
    define('USER', 'root1');
    define('PASS', '');
    function pe($arr)
    {
        echo print_r($arr);exit();
    }
    function pr($arr)
    {
        echo print_r($arr);
    }
    function ee($str = 'testtttttttttttttttttttt', $number = '___')
    {
        echo "<br>" . $number . ': ' . $str;exit();
    }
    function ec($str = 'testtttttttttttttttttttt', $number = '___')
    {
        echo "<br>" . $number . ': ' . $str . "<br>";
    }

} else {
    define('DBN', 'itmyteam_gantt');
    define('HOST', 'itmyteam.com');
    define('USER', 'itmyteam_ganttdb');
    define('PASS', 'zB2UCh230h');
    function pe($arr)
    {
        echo ''; //print_r ($arr);exit();
    }
    function pr($arr)
    {
        echo ''; //print_r ($arr);
    }
    function ee($str = 'testtttttttttttttttttttt', $number = '___')
    {
        echo ''; //"<br>".$number.': '.$str;exit();
    }
    function ec($str = 'testtttttttttttttttttttt', $number = '___')
    {
        echo ''; //"<br>".$number.': '.$str."";
    }
}

// $prompts=array(
//     'accessDenied'=>'متعصفانه دسترسی شما توصط مدیر پروژه محدود شده'
//     ,'unsuccess'=>'خطای سیستم لطفا دوباره تلاش کنید'
//     ,''=>''
//     ,''=>''
//     ,''=>''
//     ,''=>''
//     ,''=>''
//     ,''=>''
//     );
// $accessCode=4;
$rules = array(
    1 => "ایجاد کار"
    , 2 => "ایجاد گروه"
    , 4 => "ایجاد مایل استون"
    , 8 => "ویرایش کار"
    , 16 => "ویرایش گروه"
    //,32=>"ویرایش مایل استون"
    , 64 => "ویرایش درصد پیشرفت"
    , 128 => "اختصاص کاربر به کار"
    , 256 => "جابه جایی کارها"
    , 512 => "جابه جایی گروه"
    , 1024 => "ویرایش ابتدا و انتهای کارها"
    , 2048 => "ویرایش خط زمان گروه"
    , 4096 => "تغییر انتهای کار"
    //,8192=>"تغییر درصد پیشرفت"
    //,16384=>"تغییر درصد پیشرفت"
    , 32768 => "درج کامنت"
    , 65536 => "حذف کامنت"
    , 131072 => "آپلود فایل"
    , 262144 => "حذف فایل"
    //,524288=>"اضافه کردن افراد به پروژه"
    //,1048576=>"ویرایش افراد اضافه شده به پروژه"
    , 2097152 => "حذف کار"
    , 4194304 => "حذف گروه"
    , 8388608 => "حذف مایل استون"
    //,16777216=>"ارسال ایمیل دعوت"
    , 33554432 => "ارسال ایمیل هنگام تخصیص",
    //,67108864=>"حذف افراد پروژه"
);
$rulesUser = array(
    'insertTask' => 1
    , 'insertGroup' => 2
    , 'insertMile' => 4
    , 'editTask' => 8
    , 'editGroup' => 16
    , 'editPercent' => 64
    , 'personToTask' => 128
    , 'sortTask' => 256
    , 'sortGroup' => 512
    , 'dragTask' => 1024
    , 'dragGroup' => 2048
    , 'resizeTask' => 4096
    , 'insertComment' => 32768
    , 'deleteComment' => 65536
    , 'upload' => 131072
    , 'deleteFile' => 262144
    , 'deleteTask' => 2097152
    , 'deleteGroup' => 4194304
    , 'deleteMile' => 8388608
    , 'sendEmail' => 33554432,
);
$managerPermition = 48734175;
$siteName = 'آقای گانت';
$suffixT = " | وب سایت $siteName ";
$prefixD = "وب سایت $siteName , ";
$atd = array(
    'signin' => array('title' => "ورود" . $suffixT, 'des' => $prefixD . "صفحه ورود به بخش پروفایل کاربری.", 'author' => "$siteName"),
    'signup' => array('title' => "ثبت نام در سایت" . $suffixT, 'des' => $prefixD . "صفحه ثبت نام جهت ثبت نام کاربران در سایت و استفاده کامل از تمام امکانات سایت میباشد", 'author' => "$siteName"),
    'home' => array('title' => "صفحه نخست" . $suffixT, 'des' => $prefixD . "صفحه نخست" . "، وب سایت رسم گانت چارت آنلاین", 'author' => "$siteName"),
    '' => array('title' => "صفحه نخست" . $suffixT, 'des' => $prefixD . "صفحه نخست، صفحه اول وب سایت" . "، وب سایت رسم گانت چارت آنلاین", 'author' => "$siteName"),
    'gantt' => array('title' => "مدیریت پروژه " . $suffixT, 'des' => $prefixD . "بخش مدیریت پروژه، " . @$params[1], 'author' => "$siteName"),
    'ganttmanager' => array('title' => "مدیریت تمام پروژه ها " . $suffixT, 'des' => $prefixD . "نمایش تمامی پروژه ها در این صفحه صورت میپذیرد", 'author' => "$siteName"),
    'profile' => array('title' => "ویرایش پروفایل" . $suffixT, 'des' => $prefixD . "صفحه ویرایش پروفایل", 'author' => "$siteName"),
    'repass' => array('title' => "ویرایش رمز عبور" . $suffixT, 'des' => $prefixD . "صفحه ویرایش رمز عبور", 'author' => "$siteName"),
    'timeline' => array('title' => "پروفایل کاربری" . $suffixT, 'des' => $prefixD . "پروفایل کاربران وب سایت که در این میتوانند از تمام امکانات وب سایت استفاده نمایند", 'author' => "$siteName"),
    'about' => array('title' => "درباره ما" . $suffixT, 'des' => $prefixD . "این مربوط به معرفی وب سایت همقلک و همچنین نحوه ارتباط با ما میباشد.", 'author' => "$siteName"),
    'laws' => array('title' => "قوانین سایت" . $suffixT, 'des' => $prefixD . "این مربوط به قوانین وب سایت همقلک میباشد که کاربران برای ثبت نام ابتدا باید این قوانین را مطالعه کرده و بپذیرند.", 'author' => "$siteName"),
    'questions' => array('title' => "سوالات متداول" . $suffixT, 'des' => $prefixD . "این مربوط به سوالات متداولی که برای کاربران پیش می آید و همچنین جواب های به آنها میباشد به جهت ایجاد شفافیت در عمل و آگاهی کاربران نسبت به نحوه کار وب سایت همقلک", 'author' => "$siteName"),
    'manager' => array('title' => "مدیریت" . $suffixT, 'des' => $prefixD . "بخش مدیریت وب سایت ", 'author' => "$siteName"),
);
if (!isset($params[0]) || !isset($atd[$params[0]])) {
    $params[0] = 'home';
}

function dateDifference($firstDate, $secondDate)
{
    list($fdY, $fdM, $fdD) = explode('-', $firstDate);
    list($sdY, $sdM, $sdD) = explode('-', $secondDate);
    $fts = jmktime(0, 0, 0, $fdM, $fdD, $fdY);
    $sts = jmktime(0, 0, 0, $sdM, $sdD, $sdY);
    $diff = $sts - $fts;
    return round($diff / 86400);
}

function signin($time = 0)
{
    if ($time == 0) {
        header('location:' . URL . 'signin');
    } else {
        header("refresh:$time; url=" . URL . "signin");
    }

}
function myHeader($url, $time = 0)
{
    if ($time == 0) {
        header('location:' . URL . $url);
    } else {
        header("refresh:$time; url=" . URL . $url);
    }

}
function sendEmail($array)
{
    if ($_SERVER['HTTP_HOST'] != '127.0.0.1') {
        $to = $array['to'];
        $text = $array['text'];
        $subject = $array['subject'];
        $headers = 'From: info@hamghollak.com' . "\r\n" .
        'Reply-To: info@hamghollak.com' . "\r\n" .
        'X-Mailer: PHP/' . phpversion();
        echo $to . '--' . $message . '--' . $headers . '--' . $subject;
        include_once DIR . 'classes/PHPMailer/PHPMailerAutoload.php';

        $mail = new PHPMailer;

        //$mail->SMTPDebug = 3;                               // Enable verbose debug output

        //    $mail->isSMTP();                                      // Set mailer to use SMTP
        //    $mail->Host = 'ns1.andriman.com;ns2.andriman.com';  // Specify main and backup SMTP servers
        //    $mail->SMTPAuth = true;                               // Enable SMTP authentication
        //    $mail->Username = 'user@example.com';                 // SMTP username
        //    $mail->Password = 'secret';                           // SMTP password
        //    $mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
        //    $mail->Port = 587;                                    // TCP port to connect to

        $mail->setFrom('info@hamghollak.com', 'Mailer');
        $mail->addAddress($to); // Add a recipient
        $mail->addReplyTo('info@hamghollak.com', 'همقلک');
        //    $mail->addCC('cc@example.com');
        //    $mail->addBCC('bcc@example.com');

        //    $mail->addAttachment('/var/tmp/file.tar.gz');         // Add attachments
        //    $mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name
        $mail->isHTML(false); // Set email format to HTML

        $mail->Subject = $subject;
        $mail->Body = $text;
        $mail->AltBody = $text . '!';

        if (!$mail->send()) {
            echo 'Message could not be sent.';
            echo 'Mailer Error: ' . $mail->ErrorInfo;
        } else {
            echo 'Message has been sent';
        }
    } else {
        return true;
    }

}
