
RewriteCond %{REQUEST_URI} !(js|img|css)
RewriteCond %{REQUEST_URI} !(\.css|\.js|\.png|\.jpg|\.gif|robots\.txt)$ [NC]
RewriteCond %{REQUEST_FILENAME} !-f
RewriteCond %{REQUEST_FILENAME} !-d

RewriteRule ^(.*)$ index.php?params=$1 [L,NC]

RewriteRule ^(.*)/(.*)$ index.php?params=$1/$2 [L,NC]