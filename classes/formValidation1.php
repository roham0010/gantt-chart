
<?php
/*
* use:
*	$validation=new formValidation();
	$inputValid = array(
 		'phone' => 'همراه|r|+98|uniq=tusers' ,
		'userName' => 'نام کاربری|len=6#-|uniq=tusers',
		'pass' => 'رمز عبور|sc|nu|len=6#-',
		'cartNumber' => 'شماره کارت|len=16',
		'accountNumber' => 'شماره حساب|len=10#-'
		);
	$validateMsg=$validation->validation($inputValid);
*
*/
class formValidation
{

	public $validationError='';

	public $typeRequire = 'r';//required fields
	public $typeNumber = 'nu';//number='nu' or text=''
	public $typeUnique = 'uniq';//unique of ''
	public $typeUniqueEdit = 'uniqEdit';//unique of ''
	public $typePass = 'sc';//password fields type
	public $typeNationalCode = 'nc';//nationality code type
	public $typeEmail = '@';//email type
	public $typeTell = '+98';//tell type
	public $typeLen = 'len';
	public $law = 'law';


	public $spliter = '|';
	public $subSpliter = '#-';
	public $err = [
	//0
	    "لطفا *s را وارد کنید!",
	//1
	    "لطفا درستی *s را چک کنید!",
	//2
	    "تعداد کاراکتر *s نباید کمتر از 6 حرف باشد!",
	//3
	    "*s باید عدد باشد!",
	//4
	    "رمز های عبور با یکدیگر همخوانی ندارند!",
	//5
	    "تعداد کاراکتر *s باید 10 حرف باشد!",
	//6
	    "متاسفانه این *s قبلا ثبت شده!",
	//7
	    " *s قابل ثبت میباشد!",
    //8
	    "لطفا قوانین را مطالعه کنید."
	];
	public $result='';
	function fillLable($text, $caption, $key)
	{
	    $errValidation = str_replace('*s', $caption, $text);
	    if(!isset($this->result[$key]))$this->result[$key]=$errValidation;
	}
	function validation($validate)
	{
		global $sqlOPR;
	    foreach ($validate as $key => $validString)
	    {
	    	$value=@$_POST[$key];
	    	$validArray=explode($this->spliter, $validString);
	    	$caption=$validArray[0];
	    	//echo '-----------------<br>-----------';
	    	foreach ($validArray as $validType)
	    	{
	    		$validTypes=explode('=', $validType);
	    		$validKey=$validTypes[0];
	    		$validValue=@$validTypes[1];
				switch ($validKey)
				{
					case $this->typeRequire:
						if ($value == null || $value == '' || $value == "")
			            {
			                $validationError = 1;
			                $this->fillLable(''.$this->err[0], $caption, $key);

			            }
						break;
					case $this->typeLen:
						if(strlen($value)>0)
						{
							$lenStr=$validValue;
							if (!is_numeric($lenStr))
					        {
					            $lens = explode($this->subSpliter, $lenStr);
					            $lense = $lens[1] ? $lens[1] : 2000000;
					            if (strlen($value) < $lens[0] || strlen($value) > $lense)
					            {
					                $lenend = '';
					                $lenend = $lens[1]? "طول *s باید بین " . $lens[0] . " تا " . $lens[1] . " باشد! " :
					                        " تعداد کاراکتر *s باید حد اقل " . $lens[0] . " باشد!";
					                $validationError = 1;
					                $this->fillLable($lenend, $caption, $key);

					            }
					        }
					        else if ($lenStr != ' ')
					        {
					            $lens = explode($this->subSpliter, $lenStr);
					            $lense = @$lens[1] ? $lens[1] : 2000000;
					            if (strlen($value) != $lens[0])
					            {
					                $validationError = 1;
					                $this->fillLable(" طول *s باید " . $lens[0] . " باشد! ", $caption, $key);

					            }
					        }
					    }
						break;
					case $this->typeNumber:
						if(strlen($value)>0)
						{
							if (!is_numeric($value))
				            {
				                $validationError = 1;
				                $this->fillLable(''.$this->err[3], $caption, $key);

				            }
				        }
						break;
					case $this->typeEmail:
						if(strlen($value)>0)
						{
                            $reg = "^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$";
                            $address = $value;
                            if(preg_match("/$reg/", $value) === 0) {
                               $validationError = 1;
			                    $this->fillLable(''.$this->err[1], $caption, $key);
                           }
//			                $mail = explode('@', $value);
//			                if (count(@$mail) != 2)
//			                {
//			                    $validationError = 1;
//			                    $this->fillLable(''.$this->err[1], $caption, $key);
//
//			                }
//			                $domain = explode('.', $mail[1]);
//			                if (count(@$domain) != 2)
//			                {
//			                    $validationError = 1;
//			                    $this->fillLable(''.$this->err[1], $caption, $key);
//
//			                }
//			                if (strlen($domain[1]) < 2)
//			                {
//			                    $validationError = 1;
//			                    $this->fillLable(''.$this->err[1], $caption, $key);
//
//			                }
			            }
		                break;
	                ////////////////tell validation
	            	case $this->typeTell:
	            		if(strlen($value)>0)
	            		{
				            if (!is_numeric($value))
				            {
				                $validationError = 1;
				                $this->fillLable(''.$this->err[3], $caption, $key);

				            }
				            if (strlen($value) < 10 || strlen($value) > 12)
				            {
				                $valueidationError = 1;
				                $this->fillLable(''.$this->err[1], $caption, $key);

				            }
				        }
			            break;
	                ///////////////// national code $valueidation
	            	case $this->typeNationalCode:
	            		if(strlen($value)>0)
	            		{
			                if (is_numeric($value) || $value == null || $value == '' || $value == "")
			                {
			                    $validationError = 1;
			                    $this->fillLable(''.$this->err[3], $caption, $key);

			                }
			                if (strlen($value) == 10)
			                {
			                    if ($value == '1111111111' ||
			                            $value == '0000000000' ||
			                            $value == '2222222222' ||
			                            $value == '3333333333' ||
			                            $value == '4444444444' ||
			                            $value == '5555555555' ||
			                            $value == '6666666666' ||
			                            $value == '7777777777' ||
			                            $value == '8888888888' ||
			                            $value == '9999999999')
			                    {
			                        $validationError = 1;
			                        $this->fillLable(''.$this->err[1], $caption, $key);

			                    }
			                    $c = int($value[9]);
			                    $n = int($value[0]) * 10 +
			                            int($value[1]) * 9 +
			                            int($value[2]) * 8 +
			                            int($value[3]) * 7 +
			                            int($value[4]) * 6 +
			                            int($value[5]) * 5 +
			                            int($value[6]) * 4 +
			                            int($value[7]) * 3 +
			                            int($value[8]) * 2;
			                    $r = $n - int(n / 11) * 11;
			                    if (!(($r == 0 && $r == $c) || ($r == 1 && $c == 1) || ($r > 1 && $c == 11 - $r)))
			                    {
			                        $validationError = 1;
			                        $this->fillLable(''.$this->err[1], $caption, $key);

			                    }
			                }
			                else if (strlen($value) != 10)
			                {
			                    $validationError = 1;
			                    $this->fillLable(''.$this->err[5], $caption, $key);

			                }
			            }
		                break;
	                ///////////////// password and repassword validation
	            	case $this->typePass:
		                if (strlen($value) < 6)
		                {
		                    $validationError = 1;
		                    $this->fillLable(''.$this->err[2], $caption, $key);

		                }
		                $repass = $_POST[$key.'re'];
		                if ($repass != $value)
		                {
		                    $validationError = 1;
		                    $this->fillLable(''.$this->err[4], $caption, $key);

		                }

		                break;
	                ///////////// number validatin
	            	case $this->typeNumber:
		                if (!is_numeric($value))
		                {
		                    $validationError = 1;
		                    $this->fillLable(''.$this->err[3], $caption, $key);

		                }
		                if (strlen($value) <11)
		                {
		                    $validationError = 1;
		                    $this->fillLable(''.$this->err[5], $caption, $key);

		                }
		                break;
		            case $this->typeUnique:
		            	$sqlOPR = new sqlOPR();
		            	$table=$validValue;;
						$where="$key=$value";
						$res=$sqlOPR->select($table,'userName,usId',$where);
						//print_r($res);exit;
						(isset($res[0]) && count($res)>0)?$this->fillLable(''.$this->err[6], $caption, $key):'';
		            	break;
		            case $this->typeUniqueEdit:
		            	$sqlOPR = new sqlOPR();
		            	$table=$validValue;;
						$where="$key=$value";
						$res=$sqlOPR->selectNumRow($table,$where);
						($res>1)?$this->fillLable(''.$this->err[6], $caption, $key):'';
		            	break;
                    case $this->law:
						if (!isset($value) || $value == null || $value == '' || $value == "")
			            {
			                $validationError = 1;
			                $this->fillLable(''.$this->err[8], $caption, $key);

			            }
		            	break;
		            default :
		            	break;
				}//end switch
			}//end foreach2
		}//end foreach1
		return $this->result;
	}//end validation
}//END Class

?>
