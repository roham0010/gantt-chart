<?php
class pagination
{
	function pagination()
	{

	}
	/**
	 * documented function newPagination
	 *
	 * @return string array ['pagins'] and ['rows']
	 * @author Hassan Shojaei
	 * @param $table- table name
	 * @param $orderBy- order by in query string ex: 'id'
	 * @param $sort- sort by in query string ex: 'id'
	 * @param $limit- limit of selected data ex:0,10 or 20
	 * @param $numberPageLinkShow- number showed pagin number per page ex:2= pagin: 1 2
	 * @call
	 */
	function newPagination($page,$rowsNum,$url='',$stuffAtPage=0)
	{
		//echo $page.'---'.$rowsNum.'<br>';
		global $sqlOPR;
		echo "
		<script>function setCookie(name,value)
        {
            var date = new Date();
            date.setTime(date.getTime() + (31 * 24 * 60 * 60 * 1000));
            var expire = \"; expires=\" + date.toGMTString();
            document.cookie = name + '=' + value + '' + expire + '; path=/';
        }
        function getCookie(name)
        {
            var cookiename = name + \"=\";
            var ca = document.cookie.split(';');
            for(var i=0;i < ca.length;i++)
            {
                var c = ca[i];
                while (c.charAt(0)==' ') c = c.substring(1,c.length);
                if (c.indexOf(cookiename) == 0) return c.substring(cookiename.length,c.length);
            }
            return null;
        }function numberAtPagee()
		{
			select=document.getElementById(\"numberAtPage\");
			setCookie('stuffAtPage',select.value);
			//window.location='?page=1';
			href=window.location.href;
			// var str   = 'asd-0.testing';
			var regex = /page=\d/;
			newHref = href.replace(regex, 'page=1');
			location.href=newHref;
		}</script>";
		$params=(isset($_GET['params']))?$_GET['params']:'';
		$params=explode('/', $params);
		if(!isset($_COOKIE['stuffAtPage']))
		{
			setcookie('stuffAtPage',12,time()+(1000*60*60*24*12),'/',NULL,0);
		}
		$limit=((!isset($stuffAtPage) || $stuffAtPage==0))?@$_COOKIE['stuffAtPage']:$stuffAtPage;
		$numberPageLinkShow=5;

		$limitStart=($page*$limit)-$limit;
		$limitNum=$page*$limit;
		$result['pagin']='';

		$rowsRoundNum=ceil($rowsNum/$limit);
		$endPagin=($rowsRoundNum>($page+$limit))?$rowsRoundNum:($page+$limit);
		//links style
		$style="position:relative;display:block;background-color:#f3f3f3; border-radius:5px;align:center; text-align:center; text-decoration:none;
						min-width:25px;width:auto;heigth:30px;";

		$result['stuffAtPage']="<select id='numberAtPage' class='browser-default right' style='width:150px;' onchange='numberAtPagee()'>
				<option value='".@$_COOKIE['stuffAtPage']."'>".@$_COOKIE['stuffAtPage']."</option>
				<option value=12>12</option>
				<option value=1>1</option>
				<option value=24>24</option>
				<option value=48>48</option>
			  </select>";
		$divPages="<li class='waves-effect'>";
		if($url!='')
			$url.='&';
		//input for goto page
		$result['pagin']="<ul class='pagination center'>";
		if($page>1)$result['pagin'].="<li><a href='?".$url."page=".($page-1)."'><i class='material-icons'>chevron_right</i></a></li>";
//		$result['pagin'].="<li><ol>";

		// <div class="pagination">
		// 	<ul>
		// 		<li><a href="#">&lt;</a></li>
		// 		<li>
		// 			<ol>
		// 				<li class="active"><a>1</a></li>
		// 				<li><a href="#">2</a></li>
		// 				<li><a href="#">3</a></li>
		// 				<li><a href="#">4</a></li>
		// 			</ol>
		// 		</li>
		// 		<li class="next"><a href="#">&gt;</a></li>
		// 	</ul>
		// </div>

		if($page!=1 && $page<5)
		{
			$numberPageLinkShow--;
		 	$result['pagin'].=$divPages."<a href='?".$url."page=".(1)."'>".(1)."</a></li>";
		}
		if($page!=2&&$page>2  && $page<8)
		{
		 	$numberPageLinkShow--;
		 	$result['pagin'].=$divPages."<a href='?".$url."page=".(2)."'>".(2)."</a></li>";
		}
		if(($page/10)>5)
		{
			$numberPageLinkShow--;
			$result['pagin'].=$divPages."<a href='?".$url."page=25'>25</a></li>";
		}
		if(($page/10)>10&&$page/10<50)
		{
			$numberPageLinkShow--;
			$result['pagin'].=$divPages."<a href='?".$url."page=".ceil($page/2)."'>.".ceil($page/2).".</a></li>";
		}
		if(($page/10)>50)
		{
			$numberPageLinkShow--;
			$result['pagin'].=$divPages."<a href='?".$url."page=".ceil($page/2)."'>.".ceil($page/2).".</a></li>";
		}
		if($page-3>0&&$page!=5&&$page!=4 && $numberPageLinkShow>4)
		 {
		 	$numberPageLinkShow--;
		 	$result['pagin'].=$divPages."<a href='?".$url."page=".($page-3)."'>".($page-3)."</a></li>";
		 }
		if($page-2>0&&$page!=4&&$page!=3 && $numberPageLinkShow>2)
		{
			$numberPageLinkShow--;
		 	$result['pagin'].=$divPages."<a href='?".$url."page=".($page-2)."'>".($page-2)."</a></li>";
		}
		if($page-1>0&&$page!=3&&$page!=2 && $numberPageLinkShow>1)
		{
			$numberPageLinkShow--;
		 	$result['pagin'].=$divPages."<a href='?".$url."page=".($page-1)."'>".($page-1)."</a></li>";
		}
		//$page=$page-4>5?$page-5:$page;
		// echo $page.'p---'.$numberPageLinkShow.'s---'.$rowsRoundNum.'==';
		for($pagini=$page;$pagini<=$page+$numberPageLinkShow&&$pagini<=$rowsRoundNum;$pagini++)
		{
			$class='';
			if($pagini==$page){
				$class='active';
				$href='';
				$style2='font-weight: bold;text-decoration:underline;';
			}
			else {
				$href="href='?".$url."page=$pagini'";
				$style2='';
			}
				$result['pagin'].= "
						<li class='$class'>
							<a style='' $href>".($pagini)."</a>
						</li>
						";
		}
		if($page<$rowsRoundNum) $result['pagin'].="
				</li>
				<li class='next'><a href='?".$url."page=".($page+1)."'><i class='material-icons'>chevron_left</i></a></li>
			</ul>
		</div>";
		return $result;
	}
}
?>
