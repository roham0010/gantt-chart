<?php
include_once '../config/config.php';
class topic
{
	function topic()
	{

	}
	function newTopic($subject,$text)
	{
        global $sqlOPR;
		global $sqlOPR;
		if($subject==''||$text=='')
			return false;
		//$fields="group=$group,subject=$subject,text=$text,likes=0";
		$fields="subject=$subject;text=$text";
		return $sqlOPR->insert(TOPICTABLE,$fields)?TRUE:FALSE;
	}
	function vote($topicId,$text)
	{
        global $sqlOPR;
        return $sqlOPR->insert(VOTETABLE,"topic_id=$topicId;text=$text;state=1;likes=0")?TRUE:FALSE;
	}
	function subVote($voteId,$text)
	{
        global $sqlOPR;
        return $sqlOPR->insert(SUBVOTETABLE,"voteId=$voteId;text=$text;likes=0")?TRUE:FALSE;
	}
	function likeTopic($topicId,$voteId,$subVoteId)
	{
        global $sqlOPR;
        if($topicId!=0&&$voteId!=0&&$subVoteId!=0)
        {
            return $sqlOPR->update(SUBVOTETABLE,"likes=likes+1","voteid=$voteId,subvoteid=$subVoteId")?TRUE:FALSE;
        }
        else if($topicId!=0&&$voteId!=0)
        {
            return $sqlOPR->update(VOTETABLE,"likes=likes+1","topicid=$topicId,voteid=$voteId")?TRUE:FALSE;
        }
        else if($topicId!=0)
        {
            return $sqlOPR->update(TOPICTABLE,"likes=likes+1","topicid=$topicId")?TRUE:FALSE;
        }
        else
            return false;

	}
    function select($topicId,$selectType='topic')
    {
        global $sqlOPR;

        if($selectType=='mastervote')
        {
            return $sqlOPR->selectSimple(VOTETABLE,"id=$topicId");
        }
        else if($selectType=='votes')
        {
            return $sqlOPR->selectSimple(VOTETABLE,"topic_id=$topicId");
        }
        else if($selectType=='topic')
        {
            return $sqlOPR->selectSimple(TOPICTABLE,"id=$topicId");
        }
        else
            return false;

    }
	function selectAll()
	{
		include_once '../classes/paginationTopic.php';

		$pagin=new pagination();
		$news=$pagin->newPagination('topic','','id','desc');
			return $news;
	}
}
if(isset($_POST['callClass']))
{
    $topic=new topic();

    $callClass=$_POST['callClass'];
    switch ($callClass)
    {
        case 'topic':
            $group=$_POST['group'];
            $subject=$_POST['subject'];
            $text=$_POST['text'];
            return $topic->newTopic($group,$subject,$text)?TRUE:FALSE;;
        break;
        case 'vote':
            $topicId=$_POST['topicid'];
            $text=$_POST['text'];
            return $topic->vote($topicId,$text)?TRUE:FALSE;;
        break;
        case 'subVote':
            $voteId=$_POST['voteId'];
            $text=$_POST['text'];
            return $topic->subVote($voteId,$text)?TRUE:FALSE;;
        break;
        default:
            return false;
        break;
    }
}

?>
