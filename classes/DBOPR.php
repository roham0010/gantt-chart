<?php
/**
 * documented class sql Oprerations
 *
 * @package sql operations
 * @author  Hassan Shojaei
 * @copyright   Copyright (C) . All rights reserved.
 */

 include_once DIR.'config/defines.php';

class sqlOPR
{
	private $DBH;
	private $DBName;
	private $select=0;
	private $selectNumRow=0;
	private $update=0;
	private $insert=01;
	private $join=0;
	private $joinp=0;
	private $get=0;
//	private $columnsNotEscape=array('');
	function __construct()
	{
		$this->DBH=sqlOPR::connect();
	}
	function test()
	{
		echo 'opr is in access';
	}
	function connect()
	{
            try
            {
       		$DBH1= new PDO("mysql:host=".HOST.";dbname=".DBN.";charset=utf8",USER,PASS);
		$DBH1->exec("set names \'utf8\'");
		return $DBH1;
	    }
	    catch(PDOException $e){
	       echo "ERROR: ". $e->getMessage();
	    }
	}

	function filter($data)
	{
		//prepare to sql code
		//return mysql_real_escape_string($data);
		//$var=stripslashes($data);
        //$var=htmlentities($var);
        //$var=strip_tags($var);
        //$data=htmlspecialchars($var);
		return $data;
	}
	function spliter($str)
	{
		$splits=array('=','<','>','<>','!=');
		//echo $str.'<br>';
		if(preg_match('/=/', $str))
		{
			//echo $splits[0];

			$split=explode($splits[0], $str);
			return $splits[0];

		}
		else if(preg_match('/<>/', $str))
		{
			//echo $splits[3];
			$split=explode($splits[3], $str);
			return $splits[3];

		}
		else if(preg_match('/</', $str))
		{
			//echo $splits[1];
			$split=explode($splits[1], $str);
			return $splits[1];

		}
		else if(preg_match('/>/', $str))
		{
			//echo $splits[2];
			$split=explode($splits[2], $str);
			return $splits[2];

		}
		else if(preg_match('/!=/', $str))
		{
			//echo $splits[3];
			$split=explode($splits[4], $str);
			return $splits[4];

		}
	}
	//(name=(#*userName) | name is null) , id=11
	 function where($where)
	 {
	 	$query=array();
		$query['query']='';
		$query['params']='';
		// echo '<br>'.$where.'<br>';
	 	if($where!='')
		{
			$query['query'].=' WHERE ';
			$whereArray=preg_split( "/[,|]/", $where );
			preg_match_all( '/[,|]/', $where,$andor);
			$i=0;
			$arr='';
			foreach ($andor as $key )
			{
				foreach ($key as $key => $val)
				{

					$arr[$i++]=($val==','||$val==' ,'||$val==', '||$val==' , ')?'and':'or';
					 // echo '<br>'.$arr[$i-1].'<br>';
				}
			}
			$i=0;
			foreach ($whereArray as $whereRow)
			{
				if(@$whereRow[0].@$whereRow[1]!='#*')
				{
					$split=$this->spliter($whereRow);
					//echo '1111'.$split.'1111';
					//echo "<br>".$valueArray[1][0]."<br>";
					if($split!='')
					{
						$valueArray=explode($split,$whereRow);
						if(@$valueArray[1][0].@$valueArray[1][1]!='#*'||!isset($valueArray[1][2]))
						{
							$flagfield=0;
							$valueArray[0]=preg_replace('/#\*/', ',', $valueArray[0]);
							$query['query'].=" ".$valueArray[0]." ".$split." ?";
							$cuar=strlen($valueArray[1]);
							if($cuar-1>0 && @$valueArray[1][$cuar-1]==')')
								$query['query'].=")";
						}
						else
						{
							//echo "aaaaaaaaaaaaaaaaaaaaaa$valueArray[1]<br>";
							$flagfield=1;
							$valueArray[1]=trim($valueArray[1],'#*');
							$valueArray[0]=preg_replace('/#\*/', ',', $valueArray[0]);
							$query['query'].=" ".$valueArray[0]."".$split.$valueArray[1];
							// $cuar=strlen($valueArray[1]);
							// if($cuar-1>0 && $valueArray[1][$cuar-1]==')')
							// 	$query['query'].=")";
						}
					}
					else
					{
						$flagfield=1;
						//$valueArray[0]=trim($valueArray[0],'#*');
						$query['query'].=$whereRow.' ';
					}
					if(isset($valueArray[1]))
					{
						$cu1=strlen($valueArray[1])-1;
						if(@$valueArray[1][$cu1]==')')
							$valueArray[1][$cu1]=preg_replace('/\)/', '', $valueArray[1][$cu1]);
					}

					if($whereRow!='')
					{
						if(is_numeric($whereRow) && $flagfield==0)
						{
							$query['params'][$i]=$valueArray[1];
						}
						else if($flagfield==0)
						{
							$query['params'][$i]=$valueArray[1];
						}
					}
					$query['query'].=" ".@$arr[$i++]." ";
				}
				else
				{
					$whereRow=preg_replace('/#\*/', '', $whereRow);
					if($i>1) $query['query'].=" ".@$arr[$i++]." ".$whereRow;
					else $query['query'].=$whereRow." ".@$arr[$i++]." ";
				}
			}

		}
		/*
		$str=$query['query'];
		$suffix=substr($str, strlen($prefix), strlen($prefix));
		if ($suffix == $prefix) {
		    $str = substr($str, strlen($prefix));
		} */

		//$query['query']=rtrim($query['query'],' and AND , WHERE , =');
		//$query['query'].=" ".@$arr[$i++]." ";
		// $query['query'] = preg_replace('/(and |and |and| where |where |where)$/', '', $query['query']);
		$query['query'] = preg_replace('/ and $/', '', $query['query']);
		$query['query'] = preg_replace('/and $/', '', $query['query']);
		$query['query'] = preg_replace('/and$/', '', $query['query']);
		$query['query'] = preg_replace('/ or $/', '', $query['query']);
		$query['query'] = preg_replace('/or $/', '', $query['query']);
		$query['query'] = preg_replace('/or$/', '', $query['query']);
		$query['query'] = preg_replace('/ where $/', '', $query['query']);
		$query['query'] = preg_replace('/where $/', '', $query['query']);
		$query['query'] = preg_replace('/where$/', '', $query['query']);
		return $query;
	 }
    function where1($where)
	 {
	 	$query=array();
		$query['query']='';
		$query['params']='';
		// echo '<br>'.$where.'<br>';
	 	if($where!='')
		{
			$query['query'].=' WHERE ';
			$whereArray=preg_split( "/[;|]/", $where );
			preg_match_all( '/[;|]/', $where,$andor);
			$i=0;
			$arr='';
			foreach ($andor as $key )
			{
				foreach ($key as $key => $val)
				{

					$arr[$i++]=($val==';'||$val==' ;'||$val=='; '||$val==' ; ')?'and':'or';
					 // echo '<br>'.$arr[$i-1].'<br>';
				}
			}
			$i=0;
			foreach ($whereArray as $whereRow)
			{
				if(@$whereRow[0].@$whereRow[1]!='#*')
				{
					$split=$this->spliter($whereRow);
					//echo '1111'.$split.'1111';
					//echo "<br>".$valueArray[1][0]."<br>";
					if($split!='')
					{
						$valueArray=explode($split,$whereRow);
						if(@$valueArray[1][0].@$valueArray[1][1]!='#*'||!isset($valueArray[1][2]))
						{
							$flagfield=0;
							$valueArray[0]=preg_replace('/#\*/', '', $valueArray[0]);
							$query['query'].=" ".$valueArray[0]." ".$split." ?";
							$cuar=strlen($valueArray[1]);
							if($cuar-1>0 && @$valueArray[1][$cuar-1]==')')
								$query['query'].=")";
						}
						else
						{
							//echo "aaaaaaaaaaaaaaaaaaaaaa$valueArray[1]<br>";
							$flagfield=1;
							$valueArray[1]=trim($valueArray[1],'#*');
							$valueArray[0]=preg_replace('/#\*/', ',', $valueArray[0]);
							$query['query'].=" ".$valueArray[0]."".$split.$valueArray[1];
							// $cuar=strlen($valueArray[1]);
							// if($cuar-1>0 && $valueArray[1][$cuar-1]==')')
							// 	$query['query'].=")";
						}
					}
					else
					{
						$flagfield=1;
						//$valueArray[0]=trim($valueArray[0],'#*');
						$query['query'].=$whereRow.' ';
					}
					if(isset($valueArray[1]))
					{
						$cu1=strlen($valueArray[1])-1;
						if(@$valueArray[1][$cu1]==')')
							$valueArray[1][$cu1]=preg_replace('/\)/', '', $valueArray[1][$cu1]);
					}

					if($whereRow!='')
					{
						if(is_numeric($whereRow) && $flagfield==0)
						{
							$query['params'][$i]=$valueArray[1];
						}
						else if($flagfield==0)
						{
							$query['params'][$i]=$valueArray[1];
						}
					}
					$query['query'].=" ".@$arr[$i++]." ";
				}
				else
				{
					$whereRow=preg_replace('/#\*/', '', $whereRow);
					if($i>1) $query['query'].=" ".@$arr[$i++]." ".$whereRow;
					else $query['query'].=$whereRow." ".@$arr[$i++]." ";
				}
			}

		}
		/*
		$str=$query['query'];
		$suffix=substr($str, strlen($prefix), strlen($prefix));
		if ($suffix == $prefix) {
		    $str = substr($str, strlen($prefix));
		} */

		//$query['query']=rtrim($query['query'],' and AND , WHERE , =');
		//$query['query'].=" ".@$arr[$i++]." ";
		// $query['query'] = preg_replace('/(and |and |and| where |where |where)$/', '', $query['query']);
		$query['query'] = preg_replace('/ and $/', '', $query['query']);
		$query['query'] = preg_replace('/and $/', '', $query['query']);
		$query['query'] = preg_replace('/and$/', '', $query['query']);
		$query['query'] = preg_replace('/ or $/', '', $query['query']);
		$query['query'] = preg_replace('/or $/', '', $query['query']);
		$query['query'] = preg_replace('/or$/', '', $query['query']);
		$query['query'] = preg_replace('/ where $/', '', $query['query']);
		$query['query'] = preg_replace('/where $/', '', $query['query']);
		$query['query'] = preg_replace('/where$/', '', $query['query']);
		return $query;
	 }
     function updates($where)
	 {
	 	$query=array();
		$query['query']='';
		$query['params']='';
		//$query['query'].=' set ';
	 	if($where!='')
		{
			$whereArray=preg_split( "/[,|]/", $where );
			preg_match_all( '/[,|]/', $where,$andor);
			$i=0;
			$arr='';
			foreach ($andor as $key )
			{
				foreach ($key as $key => $val)
				{
					$arr[$i++]=',';
				}
			}
			$i=0;
			foreach ($whereArray as $whereRow)
			{

				$split=$this->spliter($whereRow);
				//echo '1111'.$split.'1111';
				$valueArray=explode($split,$whereRow);
				//echo "<br>".$valueArray[1][0]."<br>";
				if(@$valueArray[1][0].@$valueArray[1][1]!='#*'||!isset($valueArray[1][2]))
				{
					$flagfield=0;
					$valueArray[0]=preg_replace('/#\*/', ',', $valueArray[0]);
					$query['query'].=" ".$valueArray[0]." ".$split." ?";
				}
				else
				{
					//echo "aaaaaaaaaaaaaaaaaaaaaa$valueArray[1]<br>";
					$flagfield=1;
					$valueArray[1]=trim($valueArray[1],'#*');
					$valueArray[0]=preg_replace('/#\*/', ',', $valueArray[0]);
					$query['query'].=" ".$valueArray[0]."".$split.$valueArray[1];
				}
				  	$query['query'].=" ".@$arr[$i]." ";
				if(is_numeric($valueArray[1]) && $flagfield==0)
				{
					$query['params'][$i]=$valueArray[1];
				$i++;
				}
				else if($flagfield==0)
				{
					$query['params'][$i]=$valueArray[1];
				$i++;
				}
			}
		}
		$query['query']=rtrim($query['query'],' and AND , WHERE , =');
		return $query;
	 }

	/**
	 * documented function htmlescapestring
	 * @return it's by param no any return
	 * @author Hassan Shojaei
	 *
	 * @param $result
	 * array of selected data to escapehtml
	 */
    private $tables;
    function htmlEscape(&$result)
    {
        global $joinTables;
//        $tbl=$tabel;
//        global $tabel;


//            global $tabel;
        array_walk_recursive($result, function (&$value,$key) {
            if(in_array('tuserNotif',$this->tables) && $key=='text')
            {
                ;
            }
            else
                $value = htmlspecialchars($value, ENT_QUOTES, 'UTF-8');
        });
    }
	/**
	 * documented function select
	 * @return array of selected table
	 * @author Hassan Shojaei
	 *
	 * @param $selects
	 * fields for select      example:'field1,field2,*'!
	 * @param $table
	 * name of selected table ex:'tableName'
	 * @param $where
	 * condition of select    ex:'field1=value1,field2=value2'
	 */
	function select($table,$selects='*',$where='',$groupBy='',$sort='',$limit='')
	{
		$selects=($selects=='')?'*':$selects;

		//anti hack
		$selects=sqlOPR::filter($selects);
		$table=sqlOPR::filter($table);
		$where=sqlOPR::filter($where);
        //end anti hackers
        ////make query
		$query="SELECT ".$selects;
		$query.=" FROM ".$table."";

		$res=$this->where($where);

		$query.=$res['query'];
		//echo "$query<br>";
		$params=$res['params'];
		//print_r($params);
		if(!empty($groupBy))
		{
			$query.=' group by '.$groupBy;
		}
		if($sort!='')
		{
			$query.=' order by '.$sort;
		}
		if(!empty($limit))
		{
			$query.=' limit '.$limit;
		}
		if($this->select==1)
		{
			echo '--------------------<br>'.$query."<br>";
			print_r($params);
			echo '<br>';
		}
                //echo $query;
		$preparedQuery=$this->DBH->prepare($query);
		$i=0;
		$result='';
		if(isset($params)&&$params!=''&&count($params)>0)
		{
			$preparedQuery->execute($params);
		}
		else
		{
			$preparedQuery->execute();
		}
		//$preparedQuery->execute($res['params']);
		/*while($row=$preparedQuery->fetch())
		{
			$result[$i++]=$row;
		}*/

		$result=$preparedQuery->fetchall(PDO::FETCH_ASSOC);
        $this->tables[]=$table;
        $this->htmlEscape($result);
		return $result;
	}
	function selectJoin($table,$selects,$jointbl,$on,$where,$joinType='left join',$groupBy='',$sort='',$limit='')
	{
		$selects=($selects=='')?'*':$selects;

		//anti hack
		$selects=sqlOPR::filter($selects);
		$table=sqlOPR::filter($table);
		$where=sqlOPR::filter($where);
        //end anti hackers
        ////make query
		$query="SELECT ".$selects;
		$query.=" FROM `".$table."`";
		$joinTables=explode(',', $jointbl);
		$on=explode(',', $on);
		$i=0;
                if($joinType=='')$joinType='left join';
		foreach ($joinTables as $tablejoin)
		{
			$on1=$on[$i++];

			$on1=$on1?' on '.$on1:'';
			$query.=' '.$joinType.' '.$tablejoin.$on1.' ';
		}
		$res=$this->where($where);
		$query.=$res['query'];
		$params=$res['params'];
		if($groupBy!='')
		{
			$query.=' group by '.$groupBy;
		}
		if($sort!='')
		{
			$query.=' order by '.$sort;
		}
		if($limit!='')
		{
			$query.=' limit '.$limit;
		}
		$result='';
		$result['query']=$query;
		if($this->join==1)echo '--------------------<br>'.$query."<br>";
		if($this->join==1)print_r($params);
		$preparedQuery=$this->DBH->prepare($query);
		$i=0;

		if(isset($params)&&$params!=''&&count($params)>0)
		{
			$preparedQuery->execute($params);
		}
		else
		{
			$preparedQuery->execute();
		}
		/*while($row=$preparedQuery->fetch())
		{
			$result[$i++]=$row;
		}*/

		$result['rows']=$preparedQuery->fetchall(PDO::FETCH_ASSOC);
        $result['params']=$params;
        $joinTables[]=$table;
//        global $joinTables;
        $this->tables=$joinTables;
        $this->htmlEscape($result);
		return $result;
	}
    function selectJoin1($table,$selects,$jointbl,$on,$where,$joinType='left join',$groupBy='',$sort='',$limit='')
	{
		$selects=($selects=='')?'*':$selects;

		//anti hack
		$selects=sqlOPR::filter($selects);
		$table=sqlOPR::filter($table);
		$where=sqlOPR::filter($where);
        //end anti hackers
        ////make query
//        preg_replace();
		$query="SELECT ".$selects;
		$query.=" FROM `".$table."`";
		$joinTables=explode(',', $jointbl);
		$on=explode(';', $on);
		$i=0;
                if($joinType=='')$joinType='left join';
		foreach ($joinTables as $tablejoin)
		{
			$on1=$on[$i++];

			$on1=$on1?' on '.$on1:'';
			$query.=' '.$joinType.' '.$tablejoin.$on1.' ';
		}
		$res=$this->where1($where);
		$query.=$res['query'];
		$params=$res['params'];
		if($groupBy!='')
		{
			$query.=' group by '.$groupBy;
		}
		if($sort!='')
		{
			$query.=' order by '.$sort;
		}
		if($limit!='')
		{
			$query.=' limit '.$limit;
		}
		$result='';
		$result['query']=$query;
		if($this->join==1)echo '--------------------<br>'.$query."<br>";
		if($this->join==1)print_r($params);
		$preparedQuery=$this->DBH->prepare($query);
		$i=0;

		if(isset($params)&&$params!=''&&count($params)>0)
		{
			$preparedQuery->execute($params);
		}
		else
		{
			$preparedQuery->execute();
		}
		/*while($row=$preparedQuery->fetch())
		{
			$result[$i++]=$row;
		}*/

		$result['rows']=$preparedQuery->fetchall(PDO::FETCH_ASSOC);
        $result['params']=$params;
		return $result;
	}
    function selectJoinNumRow($table,$selects,$jointbl,$on,$where,$joinType='left join',$groupBy='',$sort='',$limit='')
	{
		$selects=($selects=='')?'*':$selects;

		//anti hack
		$selects=sqlOPR::filter($selects);
		$table=sqlOPR::filter($table);
		$where=sqlOPR::filter($where);
        //end anti hackers
        ////make query
		$query="SELECT count(*)";
		$query.=" FROM `".$table."`";
		$joinTables=explode(',', $jointbl);
		$on=explode(',', $on);
		$i=0;
                if($joinType=='')$joinType='left join';
		foreach ($joinTables as $tablejoin)
		{
			$on1=$on[$i++];

			$on1=$on1?' on '.$on1:'';
			$query.=' '.$joinType.' '.$tablejoin.$on1.' ';
		}
		$res=$this->where($where);
		$query.=$res['query'];
		$params=$res['params'];
		if($groupBy!='')
		{
			$query.=' group by '.$groupBy;
		}
		if($sort!='')
		{
			$query.=' order by '.$sort;
		}
		if($limit!='')
		{
			$query.=' limit '.$limit;
		}
		$result='';
		$result['query']=$query;
		if($this->join==1)echo '--------------------<br>'.$query."<br>";
		if($this->join==1)print_r($params);
		$preparedQuery=$this->DBH->prepare($query);
		$i=0;

		if(isset($params)&&$params!=''&&count($params)>0)
		{
			$preparedQuery->execute($params);
		}
		else
		{
			$preparedQuery->execute();
		}
		/*while($row=$preparedQuery->fetch())
		{
			$result[$i++]=$row;
		}*/
        $result=$preparedQuery->fetch();
		$result=$result['count(*)'];
        $res=$result;
		return $res;
	}
	function selectSimple($table,$where)
	{
		$selects='*';

		//anti hack
		$selects=sqlOPR::filter($selects);
		$table=sqlOPR::filter($table);
		$where=sqlOPR::filter($where);
        //end anti hackers
        ////make query
		$query="SELECT ".$selects;
		$query.=" FROM `".$table."`";

		$res=$this->where($where);

		$query.=$res['query'];

		$params=$res['params'];

		//echo $query;
		$preparedQuery=$this->DBH->prepare($query);
		$i=0;
		$result='';

		if(isset($params)&&$params!=''&&count($params)>0)
		{
			$preparedQuery->execute($params);
		}
		else
		{
			$preparedQuery->execute();
		}
		/*while($row=$preparedQuery->fetch())
		{
			$result[$i++]=$row;
		}*/
		$result=$preparedQuery->fetchall(PDO::FETCH_ASSOC);

        return $result;
	}
	/**
	 * documented function selectNumRow
	 * @return a number
	 * @author Hassan Shojaei
	 *
	 * @param $table- name of selected table ex:'tableName'
	 * @param $where- condition of select    ex:'field1=value1,field2=value2'
	 */
	function selectNumRow($table,$where='')
	{
		$selects='count(*)';

		//anti hack
		$table=sqlOPR::filter($table);
		$where=sqlOPR::filter($where);
        //end anti hackers
        ////make query
       // echo $where;
		$query="SELECT ".$selects;
		$query.=" FROM `".$table."`";

		$res=$this->where($where);

		$query.=$res['query'];

		$params=$res['params'];

		if($this->selectNumRow==1)
        {
            echo '--------------------<br>'.$query."<br>";
            print_r($params);
        }
		$preparedQuery=$this->DBH->prepare($query);
		$i=0;
		$result='';
		if(isset($params)&&$params!=''&&count($params)>0)
		{
			$preparedQuery->execute($params);
		}
		else
		{
			$preparedQuery->execute();
		}
		$result=$preparedQuery->fetch();
		$result=$result['count(*)'];
		$res=$result;
		return $res;

	}

  	/**
	 * documented function update
	 * @return bool
	 * @author Hassan Shojaei
	 *
	 * @param $updates
	 * fields and value to update          ex:'field1=value1,field2=value2'
	 * @param $table
	 * name of selected table	 ex:'tableName'
	 * @param $where
	 * condition of select       ex:'field1=value1,field2=value2'
	 */
 	function update($table,$updates,$where)
 	{

		//anti hackers
		$updates=sqlOPR::filter($updates);
		$table=sqlOPR::filter($table);
		$where=sqlOPR::filter($where);
		////make query
		$query="UPDATE  `".$table."` SET";
		////data and value
		if($updates!='')
		{
			/*$updates=explode(",",$updates);
			foreach ($updates as $field)
			{
				$data=explode("=",$field);
				$query.=" `".$data[0]."`='".$data[1]."', ";
			}
			$query=trim($query,' , ');
		*/
                    $res=$this->updates($updates);

                    $query.=$res['query'];
                    $params=$res['params'];
		}
		else
		{
			return FALSE;
		}
		////where
		if($where!="")
		{
                    $res=$this->where($where);

                    $query.=$res['query'];
                    $params1=$res['params'];
		}
		else
		{
			return FALSE;
		}
                if(count($params)>0&&count($params1)>0&&$params!=''&&$params1!='')
                    $params=array_merge($params,$params1);
                elseif(count($params1)>0&&$params1!='')
                    $params=$params1;
                elseif(count($params)>0&&$params!='')
                    $params=$params;
                //print_r($params);
                //echo $query;
		if($this->update==1)echo '--------------------<br>'.$query."<br>";
		if($this->update==1)print_r($params);
		$preparedQuery=$this->DBH->prepare($query);
		if(isset($params)&&$params!=''&&count($params)>0)
		{
			if($preparedQuery->execute($params))
				return TRUE;
			else {
				return FALSE;
			}
		}
		else
		{
			if($preparedQuery->execute())
			return TRUE;
			else {
				return FALSE;
			}
		}
 	}
 	/**
	 * documented function insert
     * sec- 50 of 100
	 * @return bool
	 * @author Hassan Shojaei
	 *
	 * @param $inserts
	 * fields and value to insert          ex:'field1=value1,field2=value2'
	 * @param $table
	 * name of selected table	 ex:'tableName'
	 */
 	function insert($table,$inserts)
 	{
 		//anti hackers
		$inserts=sqlOPR::filter($inserts);
		$table=sqlOPR::filter($table);
		////make query
		$query="INSERT INTO `".$table."` (";
		////to separate data and value
		if(!empty($inserts))
		{
			$inserts=explode(";",$inserts);
			$field='';
			$value='';
			$i=0;
//            print_r($inserts);
			foreach ($inserts as $fieldValue)
			{
				$fv=explode("=",$fieldValue);
				$field.="".$fv[0]." , ";
				if(@$fv[1][0].@$fv[1][1]!='#*'||!isset($fv[1][2]))
                {
                    $flagfield=0;
                    $fv[0]=preg_replace('/#\*/', ',', $fv[0]);
                    $value.="? , ";
//                    $query.="$fv[0],";
                    $params[$i]=$fv[1];
                    $i++;
                }
                else
                {
                    //echo "aaaaaaaaaaaaaaaaaaaaaa$fv[1]<br>";
                    $flagfield=1;
                    $fv[1]=trim($fv[1],'#*');
                    $fv[0]=preg_replace('/#\*/', ',', $fv[0]);
                    $fv[1]=preg_replace('/#\*/', ',', $fv[1]);
                    $value.="'".$fv[1]."' , ";
//                    $query.="$fv[0],";
                    // $cuar=strlen($valueArray[1]);
                    // if($cuar-1>0 && $valueArray[1][$cuar-1]==')')
                    // 	$query['query'].=")";
                }
			}
			$field=trim($field,' , ');
			$value=trim($value,' , ');
		}
		$query.=$field.")VALUES (";
		$query.=$value;
		$query.=")";
		if($this->insert==1)echo '--------------------<br>'.$query."<br>";
		if($this->insert==1)print_r($params);
		//echo $query."<br>";
		$result['query']=$query;
		$query=$this->DBH->prepare($query);
		if($query->execute($params))
		{
		    $result['id']=$this->DBH->lastInsertId();
			return $result;
		}
		else
			return false;
 	}
    /**
	 * documented function insert1
     * sec- 50 of 100
	 * @return id
	 * @author Hassan Shojaei
	 *
	 * @param fields
	 * fields insert          ex:'array(field1,field2)'
	 * @param values
	 * values insert          ex:'array(value1,value2)'
	 * @param $table
	 * name of selected table	 ex:'tableName'
	 */
 	function insert1($table,$fields,$values)
 	{
 		//anti hackers
//		$inserts=sqlOPR::filter($inserts);
//		$table=sqlOPR::filter($table);
		////make query
		$query="INSERT INTO `".$table."` (";
		////to separate data and value
        $field=implode(',',$fields);
		if(!empty($values))
		{
//			$inserts=explode(";",$inserts);
//			$field='';
			$value1='';
			$i=0;
//            print_r($inserts);
			foreach ($values as $value)
			{
				$fv=$value;
//				$field.="".$fv[0]." , ";
				if(@$fv[1][0].@$fv[1][1]!='#*'||!isset($fv[1][2]))
                {
                    $flagfield=0;
                    $fv=preg_replace('/#\*/', ',', $fv);
                    $value1.="? , ";
//                    $query.="$fv[0],";
                    $params[$i]=$fv;
                    $i++;
                }
                else
                {
                    //echo "aaaaaaaaaaaaaaaaaaaaaa$fv[1]<br>";
                    $flagfield=1;
                    $fv[1]=trim($fv[1],'#*');
                    $fv[0]=preg_replace('/#\*/', ',', $fv[0]);
                    $fv[1]=preg_replace('/#\*/', ',', $fv[1]);
                    $value1.="'".$fv[1]."' , ";
                }
			}
			$field=trim($field,' , ');
			$value1=trim($value1,' , ');
		}
		$query.=$field.")VALUES (";
		$query.=$value1;
		$query.=")";
		if($this->insert==1)echo '--------------------<br>'.$query."<br>";
		if($this->insert==1)print_r($params);
//		print_r($params);
//		echo $query."<br>";
		$result['query']=$query;
		$query=$this->DBH->prepare($query);
		if($query->execute($params))
		{
		    $result['id']=$this->DBH->lastInsertId();
			return $result;
		}
		else
			return false;
 	}

	/**
	 * documented function delete
	 * @return bool
	 * @author Hassan Shojaei
	 *
	 * @param $deletes
	 * fields and value to insert          ex:'field1=value1,field2=value2'
	 * @param $table
	 * name of selected table	 ex:'tableName'
	 */
  	function delete($table,$where,$limit='')
 	{

 		//anti hackers
		$table=sqlOPR::filter($table);
		$where=sqlOPR::filter($where);
		///make query
		$query="delete FROM  `".$table."`";

		$res=$this->where($where);

		$query.=$res['query'];

		$params=$res['params'];
		if($limit!='')
		{
			$query.=" limit $limit ";
		}
		//echo $query;
		$preparedQuery=$this->DBH->prepare($query);
		if(isset($params)&&$params!=''&&count($params)>0)
		{
			if($preparedQuery->execute($params))
			return true;

		}
		else
		{
			if($preparedQuery->execute())
                            return true;
		}
 	}

	function deleteJoin($table,$joinTable,$on,$where='',$limit='')
 	{

 		//anti hackers
		$table=sqlOPR::filter($table);
		$where=sqlOPR::filter($where);
		///make query
		$query="delete `".$table."` FROM  `".$table."` inner join $joinTable on $on WHERE ";

		$res=$this->where($where);

		$query.=$res['query'];

		$params=$res['params'];
		if($limit!='')
		{
			$query.=" limit $limit ";
		}
		echo $query;
		$preparedQuery=$this->DBH->prepare($query);
		if(isset($params)&&$params!=''&&count($params)>0)
		{
			if($preparedQuery->execute($params))
			return true;

		}
		else
		{
			$preparedQuery->execute();
		}
 	}
	function get($query)
	{
		if($this->get==1)echo '--------------------<br>'.$query."<br>";
		$preparedQuery=$this->DBH->prepare($query);
		$preparedQuery->execute();
		return $preparedQuery->fetchAll();
	}
}
?>
