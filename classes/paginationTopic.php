<?php
class pagination
{
	function pagination()
	{

	}
	/**
	 * documented function newPagination
	 *
	 * @return string array ['pagins'] and ['rows']
	 * @author Hassan Shojaei
	 * @param $table- table name
	 * @param $orderBy- order by in query string ex: 'id'
	 * @param $sort- sort by in query string ex: 'id'
	 * @param $limit- limit of selected data ex:0,10 or 20
	 * @param $numberPageShow- number showed row per page ex:10
	 * @call
	 */
	function newPagination($table,$where='',$orderBy='id',$sort='desc',$joinTable='',$joinSelects='',$joinOn='')
	{
		global $sqlOPR;
		global $params;

		echo "
		<script>

		function setCookie(name,value)
        {
            var date = new Date();
            date.setTime(date.getTime() + (31 * 24 * 60 * 60 * 1000));
            var expire = \"; expires=\" + date.toGMTString();
            document.cookie = name + '=' + value + '' + expire + '; path=/';
        }

        function getCookie(name)
        {
            var cookiename = name + \"=\";
            var ca = document.cookie.split(';');
            for(var i=0;i < ca.length;i++)
            {
                var c = ca[i];
                while (c.charAt(0)==' ') c = c.substring(1,c.length);
                if (c.indexOf(cookiename) == 0) return c.substring(cookiename.length,c.length);
            }
            return null;
        }

		function numberAtPagee()
		{
			url='/$params[0]/';
			select=document.getElementById(\"numberAtPage\");
			setCookie('stuffAtPage',select.value);
			window.location.replace(url+1);
			//alert(getCookie('stuffAtPage'));

		}

		</script>
		";



		$paramsLen=count($params);
		$page=(isset($params[$paramsLen-1])&&$params[$paramsLen-1]!=''&&is_numeric($params[$paramsLen-1]))?$params[$paramsLen-1]:'1';

		$paramsLen=count($params);
		if(isset($_POST['go']))
		{
			$page=$_POST['page'];
			/*$url='';
			$url.=($_POST['category']!='')?"/$_POST[category]" :'';
			$url.=($_POST['class']!='')?"/$_POST[class]":'';
			$url.=($_POST['group']!='')?"/$_POST[group]":'';
			$url.=($_POST['type']!='')?"/$_POST[type]":'';
			echo "llllllllllllllllllllllllllllll";
			$location="location:/$params[0]$url/$_POST[page]";
			header($location);*/
		}
		if(!isset($_COOKIE['stuffAtPage']))
		{
			setcookie('stuffAtPage',12,time()*1000*60*60*24*30,'/',NULL,0);
		}
		$limit=$_COOKIE['stuffAtPage'];
		$numberPageShow=20;
		//echo "$_COOKIE[stuffAtPage]";
		//echo "aaaaaaaaaaaaa $page $limit aaaaaaaaaaa";

		$limitStart=($page*$limit)-$limit;
		$limitNum=$page*$limit;
		$result['pagin']='';
		//data selected returned in $result['rows']
		if($joinTable=='')
		{
			$result['rows']=$sqlOPR->select($table,'',$where,$orderBy,$sort,"$limitStart,$limit");
			$rowsNum=$sqlOPR->selectNumRow($table,$where);
		}
		else
		{
			$result['rows']=$sqlOPR->selectJoin($table,$joinSelects,$joinTable,$joinOn,$where,$orderBy,$sort,"$limitStart,$limit");
			$rowsNum=count($sqlOPR->selectJoin($table,$joinSelects,$joinTable,$joinOn,$where,'','',""));
		}
		echo'<br>';
		$rowsRoundNum=ceil($rowsNum/$limit);
		$endPagin=($rowsRoundNum>($page+$limit))?$rowsRoundNum:($page+$limit);
		//links style
		$style="position:relative;display:block;background-color:#f3f3f3; border-radius:5px;align:center; text-align:center; text-decoration:none;
						min-width:25px;width:auto;heigth:30px;";

		echo "<select id='numberAtPage' onchange='numberAtPagee()'>
				<option value=$_COOKIE[stuffAtPage]>$_COOKIE[stuffAtPage]</option>
				<option value=12>12</option>
				<option value=1>1</option>
				<option value=24>24</option>
				<option value=48>48</option>
			  </select>";
		echo "";
		$divPages="<div class=pagination style='position:relative; float:left;margin-right:3px;'>";
		//input for goto page
		$form="<form action='' method='post'style='float:left;'>";
		$inputs="
		<input name='page' size=2 style='width:25px;height:15px;' type='text'>
		";
		$go="<input name='go' type='submit' value='Go'>";
		$endForm="</form>";
		$result['pagin']="<div class='pagination'>$form$inputs$go$endForm";

		$url='';
		$url="?name=$name&family=$family&pas=$pas&type=$type&state=$state";
		if($page!=1)
		{
			$numberPageShow--;
		 	$result['pagin'].=$divPages."<a style='$style' href='$url&page=".(1)."'>".(1)."</a></div>";
		}
		if($page!=2&&$page>2)
		{
		 	$numberPageShow--;
		 	$result['pagin'].=$divPages."<a style='$style' href='$url&page=".(2)."'>".(2)."</a></div>";
		}
		if(($page/10)>5)
		{
			$numberPageShow--;
			$result['pagin'].=$divPages."<a style='$style' href='$url&page=25'>25</a></div>";
		}
		if(($page/10)>10&&$page/10<50)
		{
			$numberPageShow--;
			$result['pagin'].=$divPages."<a style='$style' href='$url&page=".ceil($page/2)."'>.".ceil($page/2).".</a></div>";
		}
		if(($page/10)>50)
		{
			$numberPageShow--;
			$result['pagin'].=$divPages."<a style='$style' href='$url&page=".ceil($page/2)."'>.".ceil($page/2).".</a></div>";
		}
		if($page-3>0&&$page!=5&&$page!=4)
		 {
		 	$numberPageShow--;
		 	$result['pagin'].=$divPages."<a style='$style' href='$url&page=".($page-3)."'>".($page-3)."</a></div>";
		 }
		if($page-2>0&&$page!=4&&$page!=3)
		{
			$numberPageShow--;
		 	$result['pagin'].=$divPages."<a style='$style' href='$url&page=".($page-2)."'>".($page-2)."</a></div>";
		}
		if($page-1>0&&$page!=3&&$page!=2)
		{
			$numberPageShow--;
		 	$result['pagin'].=$divPages."<a style='$style' href='$url&page=".($page-1)."'>".($page-1)."</a></div>";
		}
		//$page=$page-4>5?$page-5:$page;
		for($i=$page;$i<=$page+$numberPageShow&&$i<=$rowsRoundNum;$i++)
		{
			if($i==$page)
				$style2='font-weight: bold;text-decoration:underline;';
			else {
				$style2='';
			}
				$result['pagin'].= "
						$divPages
						<a class='pages' style='$style$style2' href='$url&page=$i'>".($i)."</a>
					</div>
				";
		}
		$result['pagin'].="$form$go$inputs$endForm</div>";
		return $result;
	}
}
?>
