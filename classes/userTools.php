<?php

 /**
  * documented class userTools
  *
  * @package user tools (insert, update, create, validation,)
  * @author  Hassan Shojaei
  * @copyright   Copyright (C) . All rights reserved.
  */


class userTools
{

	/*this is defined in config file :)
	 * private $sqlOPR;
  	private $DBH;
	function userTools()
	{
		$this->sqlOPR=new sqlOPR(DBN);
  		$this->DBH=$this->sqlOPR->connect();
	}*/
	/**
	 * undocumented function login
	 * login user and set user session
	 * @return bool
	 * @author  Hassan Shojaei
	 *
	 * @param userName string
	 * input field of user name
	 * @param pass1 string
	 * input field of password
	 * @param remember bool
	 * user remember=1 user not remember=0;
	 *
	 */
	function login($userName,$pass,$remember=0)
	{
		if(empty($userName)||empty($pass))
		{
			return 'لطفا تمام فيلها را پر کنید!';
		}
		else
		{
			if($this->signupTest($userName,$pass))
			{
				if($this-> sessionSet($userName))
				{
					if($remember==1)
					{
						if(!isset($_COOKIE['userName']))
						{
							setcookie('userName',$userName,time()*3600*24*30*12);
						}
						else if($_COOKIE['userName']!=$_SESSION['userName'])
						{
							echo 'aaaaaaaaaaaaaaaaaaaaaaaaaa';
							unset($_COOKIE['userName']);
							setcookie('userName',$userName,time()*3600*24*30*12);
							$_COOKIE['userName']=$_SESSION['userName'];
						}
						echo "string";
					}
					return TRUE;
				}
				else
					return FALSE;
			}
			else
			{
				return FALSE;
			}
		}
	}
	function signup($person)
	{
		global $sqlOPR;
		if($sqlOPR->insert(USERTABLE,$person))
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}
	/**
	 * undocumented function signupTest
	 * test user for signed up of no
	 * @return bool
	 * @author  Hassan Shojaei
	 *
	 * @param userName string
	 * input field of user name
	 */
	function signupTest($userName)
	{
		global $sqlOPR;
		if($sqlOPR->selectNumRow('game','session=5')>0)
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}
	/**
	 * undocumented function sessionSet
	 * set session for user in site on login
	 * @return bool
	 * @author  Hassan Shojaei
	 *
	 * @param userName string
	 * input field of user name
	 */
	function sessionSet($userName)
	{
		$_SESSION['userName']=$userName;
		return TRUE;
	}
	/**
	 * undocumented function sessionSet
	 * delete session of users in site or signout user
	 * @return bool
	 * @author  Hassan Shojaei
	 */
	function signout()
	{
		unset($_SESSION['userName']);
		unset($_COOKIE['userName']);
		return TRUE;
	}
	/**
	 * undocumented function validation
	 * test validatoin user in site
	 * @return bool
	 * @author  Hassan Shojaei
	 */
	function validation()
	{
		if(isset($_SESSION['userName']))
			return TRUE;
		else
		{
			return FALSE;
		}
	}
}

?>
