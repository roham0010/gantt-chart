<?php
/**
 * documented class formHelper
 *
 * @package formHelper
 * @author  Hassan Shojaei
 */


class formHelper {
	function formHelper() {
        if(isset($_POST))
        echo "<script>
                setTimeout(function(){
                    elements=document.getElementsByClassName('captioninps');
                    for (var i = 0; i < elements.length; i++){
                        elements[i].innerHTML = '';
                    }
                },5000);
                </script>";
	}
	/**
	 * documented function newForm
	 *
	 * @return string
	 * @author Hassan Shojaei
	 * @param $caption- caption of form will set in above of forms ex:Register
	 * @param $onsubmit- validation that inputs which need it ex:'username|r|nu|6#-32|@|uniq,lastname|r|nu|6#-32|@|uniq'
	 */
	function form($caption, $name, $action, $method, $onsubmit = '', $id = 'myform', $class = 'myform',$style='',$enctype="") {
		//echo "$onsubmit";
		$action = $action ? 'action="' . $action . '"' : '';
		$method = $method ? $method : 'post';
		//$input="<input name='validationInput' value='$onsubmit' type='hidden'>";
		$onsubmit = (isset($onsubmit) && $onsubmit != '') ? 'onsubmit="return validation('."'".$onsubmit ."'".',event)' . '"' : '';
		$id = $id ? $id : 'myform';
		$class = $class ? $class : 'myform';
		$style=$style!=''?'style="'.$style.'"':'';
		$form = '';
		if ($caption != '' && $caption != ' ')
			$form .= "<h3><label name=caption-$name>$caption</label></h3>";
		$form .= "<form name='$name' $action method='$method' $onsubmit id='$id' class='$class' enctype='$enctype'  $style accept-charset='utf-8'>
					";
		echo $form;
	}

	/**
	 * documented function input
	 *
	 * @return string
	 * @author Hassan Shojaei
	 * @param $caption- caption of form will set in above of forms ex:Register
	 * 1 at first of his string is equal placeholder caption and 0 is equal lable caption
	 * @param $onblur- validation that inputs which need it ex:'username|r|nu|6#-32|@|uniq'
	 */
	function input($caption, $plchdr = '', $name = '', $onblur = '', $val = '', $type = '', $class = '', $attr = '',$style='',$importantValue=0) {
		$input = '';
		$caption = $caption ? $caption : '';

		if ($caption)
			if ($caption[0] == '1') {
				$caption = preg_replace('/0|1/', '', $caption);
				$input = "<label class='captioninp' id='cap-$name'  for='$name'>$caption</label>\n";
			} else {
				$caption = preg_replace('/0|1/', '', $caption);
//				$input = "<label class='captioninp' style='display:none;' id='caption-$name' >$caption </label>\n";
			}

		$plchdr = $plchdr ? $plchdr : '';
		$plchdr = (isset($plchdr[0]) && $plchdr[0] == '1') ? 'placeholder="' . preg_replace('/0|1/', '', $plchdr) . '"' : '';

		$name = $name ? $name : 'iname';
		$onblur = $onblur ? $onblur : '';
		$val1=($val!='')?'value='.$val:'';
		$valt=($val!='')?$val:'';
		/*if($val)
		if ($val[0] == '-')
		{
			$val = preg_replace('/0|1/', '', $val);
			$val='value='.$val;
		}
		else {
			$val = (isset($_POST[$name])&&$_POST[$name]!=''&&$submitted==false) ? 'value="' . $_POST[$name] .'"': $val1;
		}*/
		global $submitted;
		$val = (isset($_POST[$name])&&$_POST[$name]!=''&&$importantValue==0&&$type!='password') ? 'value="' . $_POST[$name] .'"': $val1;
		$val2 = (isset($_POST[$name])&&$_POST[$name]!=''&&$importantValue==0) ?  $_POST[$name] : $valt;
		$type = $type ? $type : 'text';
		$class = $class ? $class : 'myform';
		$style=$style!=''?'style="'.$style.'"':'';
		if ($onblur != '' && $onblur != ' ') {
			$uniq = explode('|', $onblur);
			//$uniq = $uniq[5];
			$event=$uniq[6];
			if($event!=''&&$event)$onblur = "$event=" . '"' . "return validation('$onblur',event)" . '"' . "";
		} else {
			$onblur = '';
			$uniq = '';
		}
		if($type!='textarea')
		{
			$input .= "<input name='$name' type='$type' $val $onblur caption='$caption' id='$name' $plchdr class=".'"'.$class.'"'." $attr $style>\n";


		}
		else
		{
			$input.="<textarea rows='5' cols='60' name='$name' caption='$caption' $onblur id='$name' $plchdr class='$class' $attr $style>$val2</textarea>\n";
		}
		global $validateMsg;
		// print_r($validateMsg);
			if (isset($validateMsg[$name]) && $validateMsg[$name]!='' && count($validateMsg[$name])>0)
                $input .= "<span class='captioninps' id='error-$name' for='$name 1' style='font-size: 0.8rem;color:red;'>$validateMsg[$name]</span>";
//				$input .= $validateMsg[$name].'<div style=""></div>';
			else
                $input .= "<span class='captioninps' id='error-$name' for='$name 1' style='font-size: 0.8rem;color:red;'></span>";
//				$input .= '';
		echo $input;
	}

	function submit($name, $value,$class='',$id='',$style='',$endForm='1',$validation='', $attr = '') {
		$class = $class ? 'class="'.$class.' btn-success"' : 'myform  btn btn-success';
		$style = $style ? 'style="'.$style.'"' : 'style=""';
		$id = $id ? 'id='.$id : '';
        if ($validation != '' && $validation != ' ') {
            $validation = "onclick=" . '"' . "return validation('$validation',event)" . '"' . "";
        } else {
            $validation = '';
            $uniq = '';
        }
		if($endForm=='1')
			$submit = "<button name='$name' $class $style  type='submit' value='$value' $validation $attr>$value</button>\n</form>";
		else
			$submit="<button name='$name' $class $style  type='submit' value='$value' $validation>$value</button>\n";
		echo $submit;
	}

}
?>
