<?php
include_once '../config/config.php';
include_once 'ajaxFuncs.php';
if(isset($_SESSION['userId']))
{
    if($_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest' && ispost(array('projectId')))
    {
        //Request identified as ajax request
        $projectId=$_POST['projectId'];
        if(isset($_SESSION['ajaxRequest']) && $_SESSION['ajaxRequest']=='schedulelogedin')
        {
            if(isset($_POST['action']))
            {
                if ($_POST['action'] == 'invite' && ispost(array('nameOrEmail','projectId','selectedUserId')))
                {
                    if($rows=$sqlOPR->selectJoin('projects','permitions.rules,projectuser.permitionId','projectuser,permitions',"projects.id=projectuser.projectId,projectuser.permitionId=permitions.id",
                    "projectuser.projectId=$projectId,projectuser.userId=$_SESSION[userId]"))
                    {
                        $rows=$rows['rows'];
                        if(sizeof($rows)>0)
                        {
                            $rows=$rows[0];
                            if(checkOwner($rows['permitionId']))
                            {
//                                $name = $_POST['name'];
                                $nameOrEmail=$_POST['nameOrEmail'];
                                $text=$_POST['text'];
                                $projectId=$_POST['projectId'];
                                $selectedUserId=@$_POST['selectedUserId'];
                                $permitionId=@$_POST['permition'];
                                if($permitionId==0)
                                    $permitionId=-1;
                                if($selectedUserId!='')
                                {
                                    $user=$sqlOPR->select('users','id,email',"name=$nameOrEmail|email=$nameOrEmail");
                                    if(count($user)>0)
                                    {
                                        $email=$user[0]['email'];
                                    }
                                    $userId=$selectedUserId;
                                }
                                else 
                                {
                                    $user=$sqlOPR->select('users','id,email',"name=$nameOrEmail|email=$nameOrEmail");
                                    if(count($user)>0)
                                    {
                                        $email=$user[0]['email'];
//                                        echo $email;
                                        $text.="\n  بخش پروژه ها: \n".URL."ganttmanager";
                                        $flagUserSigned=true;
                                        $userId=$user[0]['id'];
                                    }
                                    else 
                                    {
                                        include_once DIR.'classes/formValidation.php';
                                        $validation=new formValidation();
                                        $inputValid = array(
                                            'nameOrEmail' => 'ایمیل|r|@'
                                        );
                                        global $validationMsg;
                                        $validationMsg=$validation->validation($inputValid);
                                        if(count($validationMsg)==0 || $validationMsg=='')
                                        {
                                            $email=$nameOrEmail;
                                            include_once DIR.'classes/hashMaker.php';
                                            $flagUserSigned=false;
                                            $hashMaker = new hashMaker();
                                            $emailHash = $hashMaker -> hash($email);
                                            $inserts = "email=$email;hash=$emailHash";
                                            $emailHash2 = md5($emailHash);
                                            $text.="\n لینک ثبت نام تکمیلی:" . URL . "signup/".$email."/" . $emailHash . "/$emailHash2'";
//                                            echo $text;
                                            $user=$sqlOPR->insert1('users',array('email','hash'),array($email,$emailHash));
                                            $userId=$user['id'];
                                        }
                                        else
                                            unSuccess('لطفا ایمیل را به درستی وارد کنید!');
                                    }
                                }
                                if(!$sqlOPR->select('projectuser','id',"userId=$userId,projectId=$projectId") && $res = $sqlOPR -> insert1('projectuser',array('userId','projectId','text','permitionId'),array($userId,$projectId,$text,$permitionId)))
                                {
//                                    pr($res);
                                    $projectUserId=$res['id'];
                                    $res1['userId']=$userId;
                                    if (!empty($res['id'])) 
                                    {
                                        success($res['id'],$res1);
                                        $subject='دعوت به پروژه';
                                        $arraySMS=array('to'=>$email,'text'=>$text,'subject'=>$subject);
                                        Mailgun($arraySMS);
                                    }
                                    else
                                    {
                                        $sqlOPR->delete('projectuser',"id=$res[id]");
                                        unSuccess('ffffffffffff');
                                    }
                                }
                                else
                                    unSuccess('کاربری که انتخاب کرده این در پروژه وجود دارد.');
                            }
                            else
                                accessDenied();                
                        }
                        else
                            tryAgain();
                    }
                    else
                        tryAgain();
                } 
                else if ($_POST['action'] == 'search' && ispost(array('nameOrEmail')))
                {
                    if($rows=$sqlOPR->selectJoin('projects','permitions.rules,projectuser.permitionId','projectuser,permitions',"projects.id=projectuser.projectId,projectuser.permitionId=permitions.id",
                    "projectuser.projectId=$projectId,projectuser.userId=$_SESSION[userId]"))
                    {
                        $rows=$rows['rows'];
                        if(sizeof($rows)>0)
                        {
                            $rows=$rows[0];
                            if(checkOwner($rows['permitionId']))
                            {
                                $nameOrEmail = $_POST['nameOrEmail'];
                                $param=array($nameOrEmail,$nameOrEmail);
                                $projectId=$_POST['projectId'];
                                $whereNameOrEmail='';
                                $whereEmail='';

                                $userInProject=$sqlOPR->select('projectuser','userId','projectId='.$projectId);
                                $ids=$_SESSION['userId'].",";
                                foreach ($userInProject as $id)
                                {
                                    $ids.=$id['userId'].',';
                                }
                                $ids=trim($ids,' , ');
                                $notin='';
                                if($nameOrEmail!='')
                                {
                                    $whereNameOrEmail='name like :nameOrEmail or Email like :nameOrEmail';
                                    $notin.="and id not in($ids)";
                                }
                                else {
                                    $notin="id not in($ids)";
                                }
//                                if($whereEmail!='' || $whereNameOrEmail!='')

                                $query="select * from users where ($whereNameOrEmail) $notin limit 0,10";
                                $query=$DBH->prepare($query);
//                                $email="%".$email."%";
                                $nameOrEmail="%".$nameOrEmail."%";
//                                if($email!='%%')$query->bindParam(':email',$email);
                                if($nameOrEmail!='%%')$query->bindParam(':nameOrEmail',$nameOrEmail);
                                $query->execute();
                                $res['rows']=$query->fetchall();
                                //$res = $sqlOPR -> select('users','', "nameOrEmail=$nameOrEmail|", "id=$id");
                                if (!empty($res['rows'])) 
                                {
                                    success('',$res);
                                }
                                else
                                    emptySuccess();
                            }   
                            else
                                accessDenied();                
                        }
                        else
                            tryAgain();
                    }
                    else
                        tryAgain();
                }
                else if ($_POST['action'] == 'delete' && ispost(array('sid'))) 
                {
                    if($rows=$sqlOPR->selectJoin('projects','permitions.rules,projectuser.permitionId','projectuser,permitions',"projects.id=projectuser.projectId,projectuser.permitionId=permitions.id",
                    "projectuser.projectId=$projectId,projectuser.userId=$_SESSION[userId]"))
                    {
                        $rows=$rows['rows'];
                        if(sizeof($rows)>0)
                        {
                            $rows=$rows[0];
                            if(checkOwner($rows['permitionId']))
                            {
                                $sid = $_POST['sid'];
                                if ($sqlOPR->delete('projectuser', "userId=$sid,projectId=$projectId")) 
                                    success();
                                else
                                    unSuccess();
                            }   
                            else
                                accessDenied();                
                        }
                        else
                            tryAgain();
                    }
                    else
                        tryAgain();
                }
                elseif ($_POST['action']=='invitAgain' && ispost(array('sid','email'))) 
                {
                    if($rows=$sqlOPR->selectJoin('projects','permitions.rules,projectuser.permitionId','projectuser,permitions',"projects.id=projectuser.projectId,projectuser.permitionId=permitions.id",
                    "projectuser.projectId=$projectId,projectuser.userId=$_SESSION[userId]"))
                    {
                        $rows=$rows['rows'];
                        if(sizeof($rows)>0)
                        {
                            $rows=$rows[0];
                            if(checkOwner($rows['permitionId']))
                            {
                                $id=$_POST['sid'];
                                $email=$_POST['email'];
                                $info=$sqlOPR->select('projectuser','',"id=$id");
                                $text=@$info[0]['text'];
                                $subject='دعوت به پروژه';
                                $arraySMS=array('to'=>$email,'text'=>$text,'subject'=>$subject);
                                if (Mailgun($arraySMS))
                                    success();
                                else
                                    unSuccess();
                            }   
                            else
                                accessDenied();                
                        }
                        else
                            tryAgain();
                    }
                    else
                        tryAgain();
                }
                elseif ($_POST['action']=='resource-insert' && ispost(array('name'))) 
                {
                    if($rows=$sqlOPR->selectJoin('projects','permitions.rules,projectuser.permitionId','projectuser,permitions',"projects.id=projectuser.projectId,projectuser.permitionId=permitions.id",
                    "projectuser.projectId=$projectId,projectuser.userId=$_SESSION[userId]"))
                    {
                        $rows=$rows['rows'];
                        if(sizeof($rows)>0)
                        {
                            $rows=$rows[0];
                            if(checkOwner($rows['permitionId']))
                            {
                                $name=$_POST['name'];
                                if ($resourceId=$sqlOPR->insert1('projectresource',array('name','projectId'),array($name,$projectId)))
                                    success($resourceId['id']);
                                else
                                    unSuccess();
                            }   
                            else
                                accessDenied();                
                        }
                        else
                            tryAgain();
                    }
                    else
                        tryAgain();
                }
                elseif ($_POST['action']=='resource-delete') 
                {
                    if($rows=$sqlOPR->selectJoin('projects','permitions.rules,projectuser.permitionId','projectuser,permitions',"projects.id=projectuser.projectId,projectuser.permitionId=permitions.id",
                    "projectuser.projectId=$projectId,projectuser.userId=$_SESSION[userId]"))
                    {
                        $rows=$rows['rows'];
                        if(sizeof($rows)>0)
                        {
                            $rows=$rows[0];
                            if(checkOwner($rows['permitionId']))
                            {
                                $id=$_POST['sid'];
                                if ($sqlOPR->delete('projectresource',"id=$id")) 
                                    success();
                                else
                                    unSuccess();
                            }   
                            else
                                accessDenied();                
                        }
                        else
                            tryAgain();
                    }
                    else
                        tryAgain();
                }
                else
                    tryAgain();
            }
            else
                tryAgain();
        }
        else
            ajaxRequestError();
    }
    else
        httpRequestError();
}
else
    loginError();
