<?php
include_once '../config/config.php';
include_once 'ajaxFuncs.php';
if(isset($_SESSION['userId']))
{
    if ($_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest' && ispost(array('projectId'))) 
    {
        //Request identified as ajax request
        $projectId=$_POST['projectId'];
        if (isset($_SESSION['ajaxRequest']) && $_SESSION['ajaxRequest'] == 'schedulelogedin') 
        {
            if (isset($_POST['action'])) 
            {
//                    echo 'aaaaaaaaaaaaaaaaaaa';
                if ($_POST['action'] == 'new' && ispost(array('name','prevId')))
                {
                    $rows=$sqlOPR->selectJoin('projects','permitions.rules,projectuser.permitionId','projectuser,permitions',"projects.id=projectuser.projectId,projectuser.permitionId=permitions.id",
                    "projectuser.projectId=$projectId,projectuser.userId=$_SESSION[userId]");
                    $rows=$rows['rows'];
                    if(sizeof($rows)>0)
                    {
                        $rows=$rows[0];
                        if(checkOwner($rows['permitionId']) || ($rulesUser['editGroup'] & $rows['rules'])>0)
                        {
                            $name = $_POST['name'];
                            //$gid = $_POST['gid'];
                            $prevId = $_POST['prevId'];
                            $nextId = @$_POST['nextId'];
                            
                            if($res=$sqlOPR -> insert1('groups',array('name','prev','projectId'),array($name,$prevId,$projectId)))
                            {
                                if(isset($nextId))
                                {
                                    if(is_numeric($nextId) && $sqlOPR -> update('groups', "prev=$res[id]", "id=$nextId"))
                                        success($res['id']);
                                    else
                                    {
                                        $sqlOPR -> delete('groups', "id=$res[id]");
                                        unSuccess();
                                    }
                                }
                                else
                                    success($res['id']);
                            }
                            else
                                unSuccess();
                            
                        }
                        else
                            accessDenied();
                    }
                    else
                        tryAgain();
                } 
                else if ($_POST['action'] == 'editGroup') 
                {
                    $id = $_POST['id'];
                    $rows=$sqlOPR->selectJoin('projects','groups.id,permitions.rules,permitions.id,permitionId','projectuser,permitions,groups',"projects.id=projectuser.projectId,projectuser.permitionId=permitions.id,projects.id=groups.projectId",
                    "projectuser.projectId=$projectId,projectuser.userId=$_SESSION[userId],groups.id=$id");
                    $rows=$rows['rows'];
                    if(sizeof($rows)>0)
                    {
                        $rows=$rows[0];
                        if(checkOwner($rows['permitionId']) || ($rulesUser['editGroup'] & $rows['rules'])>0)
                        {
                            $name = $_POST['name'];
                            $res = $sqlOPR -> update('groups', "name=$name", "id=$id");
                            if($res)
                                success();
                            else
                                unSuccess();
                        }
                        else
                            accessDenied();                
                    }
                    else
                        tryAgain();
                }
                else
                    tryAgain();
            }
            else
                tryAgain();
        }
        else
            ajaxRequestError();
    }
    else
        httpRequestError();
}
else
    loginError();
