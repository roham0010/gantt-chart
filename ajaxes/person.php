<?php
include_once '../config/config.php';
include_once 'ajaxFuncs.php';
if(isset($_SESSION['userId']))
{
    if($_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest' && ispost(array('projectId')))
    {
        //Request identified as ajax request
        $projectId=$_POST['projectId'];
        if(isset($_SESSION['ajaxRequest']) && $_SESSION['ajaxRequest']=='schedulelogedin')
        {
            if(isset($_POST['action']))
            {
                if ($_POST['action'] == 'set' && ispost(array('taskId','personsResourcesStringName'))) 
                {
                    $id=$_POST['taskId'];
                    if($rows=$sqlOPR->selectJoin('projects','tasks.id,permitions.rules,projectuser.permitionId','projectuser,permitions,groups,tasks',"projects.id=projectuser.projectId,projectuser.permitionId=permitions.id,projects.id=groups.projectId,groups.id=tasks.groupId",
                    "projectuser.projectId=$projectId,projectuser.userId=$_SESSION[userId],tasks.id=$id"))
                    {
                        $rows=$rows['rows'];
                        if(sizeof($rows)>0)
                        {
                            $rows=$rows[0];
                            if((checkOwner($rows['permitionId']) || ($rulesUser['personToTask'] & $rows['rules'])>0))
                            {
                                $taskId = $_POST['taskId'];
                                $personResource = $_POST['personsResourcesStringName'];
                                $personId = @$_REQUEST['personId'];
                                $personsRemoveId=@$_REQUEST['personsRemoveId'];
                                $resourceId = @$_REQUEST['resourceId'];
                                $resourcesRemoveId=@$_REQUEST['resourcesRemoveId'];
                                $res['up'] = $sqlOPR -> update('tasks', "users=$personResource", "id=$taskId");
                                $i = 0;
                                if(count($personId)>0)
                                    foreach ($personId as $id) {
                                        $res['q' . $i] = $sqlOPR -> insert1('taskUser',array('userId','taskId'),array($id,$taskId));
                                        $res[$i++] = $id;
                                    }
                                if(count($personsRemoveId)>0)
                                    foreach ($personsRemoveId as $id) {
                                        $sqlOPR -> delete('taskUser', "userId=$id,taskId=$taskId");
                                    }
                                $i = 0;
                                if(count($resourceId)>0)
                                    foreach ($resourceId as $id) {
                                        $res['qq' . $i] = $sqlOPR -> insert1('taskResource',array('resourceId','taskId'),array($id,$taskId));
                                        $res[$i++] = $id;
                                    }
                                if(count($resourcesRemoveId)>0)
                                    foreach ($resourcesRemoveId as $id) {
                                        $sqlOPR -> delete('taskResource', "resourceId=$id,taskId=$taskId");
                                    }

                                if (!empty($res['id'])) {
                                    $res['answer'] = array('درج با موفقیت انجام شد');
                                    $res['code'] = 1;
                                }
                                echo json_encode($res);
                            }
                            else
                                accessDenied();                
                        }
                        else
                            tryAgain();
                    }
                    else
                        tryAgain();
                } 
                // else if ($_POST['action'] == 'edit') 
                // {
                //     $id=$_POST['taskId'];
                //     if($rows=$sqlOPR->selectJoin('projects','tasks.id,permitions.rules,projectuser.permitionId','projectuser,permitions,groups,tasks',"projects.id=projectuser.projectId,projectuser.permitionId=permitions.id,projects.id=groups.projectId,groups.id=tasks.groupId",
                //     "projectuser.projectId=$projectId,projectuser.userId=$_SESSION[userId],tasks.id=$id"))
                //     {
                //         $rows=$rows['rows'];
                //         if(sizeof($rows)>0)
                //         {
                //             $rows=$rows[0];
                //             if((checkOwner($rows['permitionId']) || ($rulesUser['personToTask'] & $rows['rules'])>0))
                //             {
                //                 $name = $_POST['name'];
                //                 $id = $_POST['id'];
                //                 $res = $sqlOPR -> update('groups', "name=$name", "id=$id");
                //                 if (!empty($res['id'])) {
                //                     $res['answer'] = array('ویرایش با موفقیت انجام شد');
                //                     $res['code'] = 1;
                //                 }
                //                 echo json_encode($res);
                //             }
                //             else
                //                 accessDenied();                
                //         }
                //         else
                //             tryAgain();
                //     }
                //     else
                //         tryAgain();
                // }
                else
                    tryAgain();
            }
            else
                tryAgain();
        }
        else
            ajaxRequestError();
    }
    else
        httpRequestError();
}
else
    loginError();
