<?php
include_once '../config/config.php';
//echo $_FILES['file']['type'];
$projectId=$_POST['projectId'];
if(isset($_POST['fileId'])&&$_POST['fileId']!='')
{
    $fileId=$_POST['fileId'];
    $table=$_POST['table'];
    $fileName=$_POST['fileName'];
    if($sqlOPR->delete('tasksfile',"id=$fileId"))
    {
        $filePath=DIR.'/tasksFile/'.$fileName;
        $res['err']='حذف شد رفت پی کارش';
        $res['filePath']=$filePath;
        $res['fileName']=$fileName;
        $res['id']=$fileId;
        echo json_encode($res);
        if(file_exists($filePath))
        {
            unlink($filePath);
        }
    }
}
elseif (isset($_FILES["file"]) && isset($_REQUEST["taskId"]) && isset($_POST["user"])) {
    $taskId = $_POST['taskId'];
    $user = $_POST['user'];
    $userName = $_POST['userName'];
    $uploadDirectory = DIR.'tasksFile/'; //specify upload directory ends with / (slash)
    /*
      Note : You will run into errors or blank page if "memory_limit" or "upload_max_filesize" is set to low in "php.ini".
      Open "php.ini" file, and search for "memory_limit" or "upload_max_filesize" limit
      and set them adequately, also check "post_max_size".
     */
    //check if this is an ajax request
    if (!isset($_SERVER['HTTP_X_REQUESTED_WITH'])) {
        die("-1");
    }
    //Is file size is less than allowed size.
    if ($_FILES["file"]["size"] > 5242880) {
        die("-2");
    }
    //allowed file type Server side check
    switch (strtolower($_FILES['file']['type'])) {
        //allowed file types
        case 'image/png':
        case 'image/gif':
        case 'image/jpeg':
        case 'image/pjpeg':
        case 'text/plain':
        case 'text/html': //html file
        case 'application/x-zip-compressed':
        case 'application/zip':
        case 'application/pdf':
        case 'application/vnd.ms-excel':
        case 'application/octet-stream':
        case 'application/msword':
        case 'video/mp4':
        case 'application/vnd.openxmlformats-officedocument.wordprocessingml.document':
            break;
        default:
            die("-3"); //output error
    }
    $fileName = strtolower($_FILES['file']['name']);
    $suffix = substr($fileName, strrpos($fileName, '.')); //get file extention
    $rand=  rand(1, 9999);
    $newName = time() . $rand . $suffix; //new file name

    if (move_uploaded_file($_FILES['file']['tmp_name'], $uploadDirectory . $newName)) {
        $id=$sqlOPR->insert1('tasksfile',array('oldName','newName','user','userName','taskId'),array($fileName,$newName,$user,$userName,$taskId));
        $file['oldName']=$fileName;
        $file['newName']=$newName;
        $file['fileId']=$id['id'];
        echo json_encode($file);
    } else {
        die('-4');
    }
} else {
    die('-5');
}
