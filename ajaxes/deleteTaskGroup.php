<?php
include_once '../config/config.php';
include_once 'ajaxFuncs.php';
if(isset($_SESSION['userId']))
{
    if ($_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest' && ispost(array('projectId'))) 
    {
        //Request identified as ajax request
        $projectId=$_POST['projectId'];
        if (isset($_SESSION['ajaxRequest']) && $_SESSION['ajaxRequest']=='schedulelogedin') 
        {
            if (isset($_POST['action'])) 
            {
                if ($_POST['action'] == 'delete' && ispost(array('id','table'))) 
                {
                    $id = $_POST['id'];
                    $table=$_POST['table'];
                    switch ($table) 
                    {
                        case 'groups':
                            if(!$rows=$sqlOPR->selectJoin('projects','groups.id,permitions.rules,groups.prev,projectuser.permitionId','projectuser,permitions,groups',"projects.id=projectuser.projectId,projectuser.permitionId=permitions.id,projects.id=groups.projectId,groups.id=tasks.groupId",
                            "projectuser.projectId=$projectId,projectuser.userId=$_SESSION[userId],groups.id=$id"))
                            {
                                tryAgain();
                                break;
                            }
                            $permition=$rulesUser['deleteGroup'];
                            break;
                        
                        case 'tasks':
                            if(!$rows=$sqlOPR->selectJoin('projects','tasks.id,permitions.rules,projectuser.permitionId,tasks.prev','projectuser,permitions,groups,tasks',"projects.id=projectuser.projectId,projectuser.permitionId=permitions.id,projects.id=groups.projectId,groups.id=tasks.groupId",
                            "projectuser.projectId=$projectId,projectuser.userId=$_SESSION[userId],tasks.id=$id"))
                            {
                                tryAgain();
                                break;
                            }
                            $permition=$rulesUser['deleteTask'];
                            break;
                    }
                    $rows=$rows['rows'];
                    if(sizeof($rows)>0)
                    {
                        $rows=$rows[0];
                        
                        if((checkOwner($rows['permitionId']) || ($permition & $rows['rules'])>0))
                        {
                            $thisPrev = $_POST['thisPrev'];
                            $nextId = @$_POST['nextId'];
                            $selfPrev = $rows['prev'];
                            if (is_numeric($thisPrev) || is_numeric($nextId)) 
                            {
                                if(is_numeric($nextId))
                                {
                                    if($sqlOPR -> update($table, "prev=$thisPrev", "id=$nextId"))
                                    {
                                        if($sqlOPR->delete($table, "id=$id"))
                                            success();
                                        else
                                        {
                                            $sqlOPR -> update($table, "prev=$selfPrev", "id=$nextId");
                                            unSuccess();    
                                        }
                                    }
                                    else
                                        unSuccess();
                                }
                                else
                                {
                                    if($sqlOPR->delete($table, "id=$id"))
                                        success();
                                    else
                                        unSuccess();
                                }
                            }
                            else
                                unSuccess();
                        }
                        else
                            accessDenied();                
                    }
                    else
                        tryAgain();
                }
                else
                    tryAgain();
            }
            else
                tryAgain();
        }
        else
            ajaxRequestError();
    }
    else
        httpRequestError();
}
else
    loginError();