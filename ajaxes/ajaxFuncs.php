<?php
include_once '../config/config.php';

function success($id='',$data='',$answer='ویرایش با موفقیت انجام شد',$code=1)
{
	$response['answer']=$answer;
	$response['id']=$id;
    $response['response']=$code;
    $response['data']=$data;

    echo json_encode($response);
}
function unSuccess($answer='خطای سیستم لطفا دوباره تلاش کنید',$code=2)
{
	$response['answer']=$answer;
    $response['response']=$code;
    echo json_encode($response);
}
function accessDenied($answer='دسترسی به این مورد ندارید',$code=3)
{
	$response['answer']=$answer;
    $response['response']=$code;
    echo json_encode($response);
}
function tryAgain($answer='لطفا دوباره تلاش کنید',$code=4)
{
	$response['answer']=$answer;
    $response['response']=$code;
    echo json_encode($response);
}
function ajaxRequestError($answer='خطای ارسال اطلاعات',$code=5)
{
	$response['answer']=$answer;
    $response['response']=$code;
    echo json_encode($response);
}
function loginError($answer='خطای پایان جلسه',$code=6)
{
	$response['answer']=$answer;
    $response['response']=$code;
    echo json_encode($response);
}
function httpRequestError($answer='خطای روش ارسال اطلاعات',$code=7)
{
	$response['answer']=$answer;
    $response['response']=$code;
    echo json_encode($response);
}
function emptySuccess($answer='داده ای موجود نیست',$code=8)
{
	$response['answer']=$answer;
    $response['response']=$code;
    echo json_encode($response);
}
function checkOwner($permition)
{
	if($permition<=0)
		return true;
	else
		return false;
}
function ispost($array)
{
	foreach ($array as $key => $value) {
		if(!isset($_POST[$value]))
			return false;
	}
	return true;
}
if(isset($_COOKIE['hashCookie']) && !isset($_SESSION['userId']) && $_COOKIE['hashCookie']!='')
{
	$hashCookie=$_COOKIE['hashCookie'];
	$user=$sqlOPR->select('users','',"hashCookie=$hashCookie");
	if(count($user)>0)
	{
        $_SESSION['ajaxRequest']='schedulelogedin';
		if($user[0]['block']==0)
		{
			include DIR ."subfiles/sessionSet.php";
		}
	}
}
?>
