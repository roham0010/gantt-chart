<?php
include_once '../config/config.php';
include_once '../jdf.php';
include_once 'ajaxFuncs.php';
if(isset($_SESSION['userId']))
{
    if($_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest' && ispost(array('projectId')))
    {
        //Request identified as ajax request
        $projectId=$_POST['projectId'];
        if(isset($_SESSION['ajaxRequest']) && $_SESSION['ajaxRequest']=='schedulelogedin')
        {
            if(isset($_POST['action']))
            {
                if($_POST['action']=='edit' && ispost(array('taskId','dateStart','dateType')))
                {
                    $taskId==$id=$_POST['taskId'];
                    if($rows=$sqlOPR->selectJoin('projects','tasks.id,permitions.rules,projectuser.permitionId','projectuser,permitions,groups,tasks',"projects.id=projectuser.projectId,projectuser.permitionId=permitions.id,projects.id=groups.projectId,groups.id=tasks.groupId",
                    "projectuser.projectId=$projectId,projectuser.userId=$_SESSION[userId],tasks.id=$id"))
                    {
                        $rows=$rows['rows'];
                        if(sizeof($rows)>0)
                        {
                            $rows=$rows[0];
                            if((checkOwner($rows['permitionId']) || ($rulesUser['dragTask'] & $rows['rules'])>0) || sizeof($sqlOPR->select('taskUser','id',"taskUser.userId=$_SESSION[userId],taskUser.taskId=$id"))>0)
                            {
                                $name=$_POST['name'];
                                $dateStart=$_POST['dateStart'];
                                $dateEnd=$_POST['dateEnd'];
                                $dateType=$_POST['dateType'];
                                $projectStartDate=$_POST['projectStartDate'];
                                //$taskDate=preg_replace(' PM', '', $taskDate);

                                list($year, $month, $day) = explode('-', $dateStart);
                                list($hour, $minute, $second) = explode(':', $dateStart);
                                $jalaliDateStart=jalali_to_gregorian($year,$month,$day,'-');

                                list($year, $month, $day) = explode('-', $dateEnd);
                                list($hour, $minute, $second) = explode(':', $dateEnd);
                                $jalaliDateEnd=jalali_to_gregorian($year,$month,$day,'-');

                                $datetime1 = new DateTime($projectStartDate);
                                $datetime2 = new DateTime($jalaliDateStart);
                                $interval = $datetime1->diff($datetime2);
                                $right=$interval->format('%a');
                                $right=($right*42);

                                $datetime1 = new DateTime($jalaliDateStart);
                                $datetime2 = new DateTime($jalaliDateEnd);
                                $interval = $datetime1->diff($datetime2);
                                $width=$interval->format('%a');
                                $width=($width*42)+2;
                                //echo 'aaaaaaaaaaaa'.count($period).'aaaaaaaaaaaaa';
                                //print_r($period);
                                //echo $right.'-----'.$width."<br>";
                                if($dateType=='startDate')
                                    $selectedDate="startDate=$dateStart";
                                else
                                    $selectedDate="endDate=$dateEnd";
                                if($sqlOPR->update('tasks',"$selectedDate,marginRight=$right,width=$width","id=$taskId"))
                                    success();
                                else
                                    unSuccess();
                            }
                            else
                                accessDenied();                
                        }
                        else
                            tryAgain();
                    }
                    else
                        tryAgain();
                }  
                else
                    tryAgain();
            }
            else
                tryAgain();
        }
        else
            ajaxRequestError();
    }
    else
        httpRequestError();
}
else
    loginError();