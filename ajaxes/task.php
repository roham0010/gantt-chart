<?php
include_once '../config/config.php';
include_once 'ajaxFuncs.php';
if(isset($_SESSION['userId']))
{
    if($_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest' && ispost(array('projectId')))
    {
        //Request identified as ajax request
        $projectId=$_POST['projectId'];
        if(isset($_SESSION['ajaxRequest']) && $_SESSION['ajaxRequest']=='schedulelogedin')
        {
            if(isset($_POST['action']))
            {
                if($_POST['action']=='new' && ispost(array('name','gid','prevId','type')))
                {
                    $name=$_POST['name'];
                    $gid=$_POST['gid'];
                    $prevId=$_POST['prevId'];
                    
                    if($rows=$sqlOPR->selectJoin('projects','permitions.rules,projectuser.permitionId','projectuser,permitions,groups',"projects.id=projectuser.projectId,projectuser.permitionId=permitions.id,projects.id=groups.projectId",
                    "projectuser.projectId=$projectId,projectuser.userId=$_SESSION[userId],groups.id=$gid"))
                    {
                        $rows=$rows['rows'];
                        if(sizeof($rows)>0)
                        {
                            $rows=$rows[0];
                            if(checkOwner($rows['permitionId']) || ($rulesUser['insertTask'] & $rows['rules'])>0)
                            {
                                $type=$_POST['type'];
                                $res=$sqlOPR->insert1('tasks',array('name','groupId','prev','percent','type'),array($name,$gid,$prevId,1,$type));

                                if(!empty(@$res['id']))
                                    success($res['id']);
                                else
                                    unSuccess();
                            }
                            else
                                accessDenied();                
                        }
                        else
                            tryAgain();
                    }
                    else
                        tryAgain();
                }
                else if($_POST['action']=='resize' && ispost(array('id','endDate','width')))
                {
                    $id=$_POST['id'];
                    if($rows=$sqlOPR->selectJoin('projects','tasks.id,permitions.rules,projectuser.permitionId','projectuser,permitions,groups,tasks',"projects.id=projectuser.projectId,projectuser.permitionId=permitions.id,projects.id=groups.projectId,groups.id=tasks.groupId",
                    "projectuser.projectId=$projectId,projectuser.userId=$_SESSION[userId],tasks.id=$id"))
                    {
                        $rows=$rows['rows'];
                        if(sizeof($rows)>0)
                        {
                            $rows=$rows[0];
                            if((checkOwner($rows['permitionId']) || ($rulesUser['resizeTask'] & $rows['rules'])>0) || sizeof($sqlOPR->select('taskUser','id',"taskUser.userId=$_SESSION[userId],taskUser.taskId=$id"))>0)
                            {
                                $endDate=$_POST['endDate'];
                                $width=$_POST['width'];
                               if($sqlOPR->update('tasks', "endDate=$endDate,width=$width","id=$id"))
                                    success();
                                else
                                    unSuccess();
                            }
                            else
                                accessDenied();                
                        }
                        else
                            tryAgain();
                    }
                    else
                        tryAgain();
                }
                else if($_POST['action']=='editName' && ispost(array('id','name')))
                {
                    $id=$_POST['id'];
                    if($rows=$sqlOPR->selectJoin('projects','tasks.id,permitions.rules,projectuser.permitionId','projectuser,permitions,groups,tasks',"projects.id=projectuser.projectId,projectuser.permitionId=permitions.id,projects.id=groups.projectId,groups.id=tasks.groupId",
                    "projectuser.projectId=$projectId,projectuser.userId=$_SESSION[userId],tasks.id=$id"))
                    {
                        $rows=$rows['rows'];
                        if(sizeof($rows)>0)
                        {
                            $rows=$rows[0];
                            if((checkOwner($rows['permitionId']) || ($rulesUser['editTask'] & $rows['rules'])>0) || sizeof($sqlOPR->select('taskUser','id',"taskUser.userId=$_SESSION[userId],taskUser.taskId=$id"))>0)
                            {
                                $name=$_POST['name'];
                                if($sqlOPR->update('tasks', "name=$name","id=$id"))
                                    success();
                                else
                                    unSuccess();
                            }
                            else
                                accessDenied();                
                        }
                        else
                            tryAgain();
                    }
                    else
                        tryAgain();
                }
                else if($_POST['action']=='drag' && ispost(array('id','startDate','endDate','marginRight','width')))
                {
                    $id=$_POST['id'];
                    if($rows=$sqlOPR->selectJoin('projects','tasks.id,permitions.rules,projectuser.permitionId','projectuser,permitions,groups,tasks',"projects.id=projectuser.projectId,projectuser.permitionId=permitions.id,projects.id=groups.projectId,groups.id=tasks.groupId",
                    "projectuser.projectId=$projectId,projectuser.userId=$_SESSION[userId],tasks.id=$id"))
                    {
                        $rows=$rows['rows'];
                        if(sizeof($rows)>0)
                        {
                            $rows=$rows[0];
                            if((checkOwner($rows['permitionId']) || ($rulesUser['dragTask'] & $rows['rules'])>0) || sizeof($sqlOPR->select('taskUser','id',"taskUser.userId=$_SESSION[userId],taskUser.taskId=$id"))>0)
                            {
                                $startDate=$_POST['startDate'];
                                $endDate=$_POST['endDate'];
                                $marginRight=$_POST['marginRight'];
                                $width=$_POST['width'];
                                if($sqlOPR->update('tasks', "startDate=$startDate,endDate=$endDate,marginRight=$marginRight,width=$width","id=$id"))
                                    success();
                                else
                                    unSuccess();
                            }
                            else
                                accessDenied();                
                        }
                        else
                            tryAgain();
                    }
                    else
                        tryAgain();
                }
                else if($_POST['action']=='fixeTimeline' && ispost(array('id','width','startDate','endDate','marginRight')))
                {
                    
                    $id=$_POST['id'];
                    if($rows=$sqlOPR->selectJoin('projects','tasks.id,permitions.rules,projectuser.permitionId','projectuser,permitions,groups,tasks',"projects.id=projectuser.projectId,projectuser.permitionId=permitions.id,projects.id=groups.projectId,groups.id=tasks.groupId",
                    "projectuser.projectId=$projectId,projectuser.userId=$_SESSION[userId],tasks.id=$id"))
                    {
                        $rows=$rows['rows'];
                        if(sizeof($rows)>0)
                        {
                            $rows=$rows[0];
                            if((checkOwner($rows['permitionId']) || ($rulesUser['insertTask'] & $rows['rules'])>0) || sizeof($sqlOPR->select('taskUser','id',"taskUser.userId=$_SESSION[userId],taskUser.taskId=$id"))>0)
                            {
                                $startDate=$_POST['startDate'];
                                $endDate=$_POST['endDate'];
                                $marginRight=$_POST['marginRight'];
                                $width=$_POST['width'];
                                if($sqlOPR->update('tasks', "startDate=$startDate,endDate=$endDate,marginRight=$marginRight,width=$width","id=$id"))
                                    success();
                                else
                                    unSuccess();
                            }
                            else
                                accessDenied();                
                        }
                        else
                            tryAgain();       
                    }
                    else
                        tryAgain();
                }
                else if($_POST['action']=='update-percent' && ispost(array('id','percent')))
                {
                    $id=$_POST['id'];
                    if($rows=$sqlOPR->selectJoin('projects','tasks.id,permitions.rules,projectuser.permitionId','projectuser,permitions,groups,tasks',"projects.id=projectuser.projectId,projectuser.permitionId=permitions.id,projects.id=groups.projectId,groups.id=tasks.groupId",
                        "projectuser.projectId=$projectId,projectuser.userId=$_SESSION[userId],tasks.id=$id"))
                    {
                        $rows=$rows['rows'];
                        if(sizeof($rows)>0)
                        {
                            $rows=$rows[0];
                            if((checkOwner($rows['permitionId']) || ($rulesUser['editPercent'] & $rows['rules'])>0) || sizeof($sqlOPR->select('taskUser','id',"taskUser.userId=$_SESSION[userId],taskUser.taskId=$id"))>0)
                            {
                                $percent=$_POST['percent'];
                                if($sqlOPR->update('tasks', "percent=$percent","id=$id"))
                                    success();
                                else
                                    unSuccess();
                            }
                            else
                                accessDenied();                
                        }
                        else
                            tryAgain();
                    }
                    else
                        tryAgain();
                }
                else if($_POST['action']=='update-groups-of-task' && ispost(array('tasksId','pxOfDraggGroup')))
                {

                    $tasksId=$_REQUEST['tasksId'];
                    // print_r($tasksId);
                    $taskIds='(tasks.id='.implode('|tasks.id=',$tasksId).')';
                    // echo $taskIds;
                    if($rows=$sqlOPR->selectJoin('projects','tasks.id,permitions.rules,projectuser.permitionId','projectuser,permitions,groups,tasks',"projects.id=projectuser.projectId,projectuser.permitionId=permitions.id,projects.id=groups.projectId,groups.id=tasks.groupId",
                    "projectuser.projectId=$projectId,projectuser.userId=$_SESSION[userId],$taskIds"))
                    {
                        $rows=$rows['rows'];
                        if(sizeof($rows)>0)
                        {
                            $rows=$rows[0];
                            if((checkOwner($rows['permitionId']) || ($rulesUser['dragGroup'] & $rows['rules'])>0))
                            {
                                $pxOfDraggGroup=$_POST['pxOfDraggGroup'];
                                $pxOfDraggGroup-=1;
                                if($sqlOPR->update('tasks', "marginRight=#*marginRight+$pxOfDraggGroup","$taskIds,marginRight<>0"))
                                    success();
                                else
                                    unSuccess();
                            }
                            else
                                accessDenied();                
                        }
                        else
                            tryAgain();
                    }
                    else
                        tryAgain();
                }
                else
                    tryAgain();
            }
            else
                tryAgain();
        }
        else
            ajaxRequestError();
    }
    else
        httpRequestError();
}
else
    loginError();
