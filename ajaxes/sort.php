<?php
include_once '../config/config.php';
include_once 'ajaxFuncs.php';
if(isset($_SESSION['userId']))
{
    if($_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest' && ispost(array('projectId')))
    {
        //Request identified as ajax request
        $projectId=$_POST['projectId'];
        if(isset($_SESSION['ajaxRequest']) && $_SESSION['ajaxRequest']=='schedulelogedin')
        {
            if(isset($_POST['action']))
            {
                // if($_POST['action']=='new' )
                // {
                //     if(isset($_POST['name'])&&isset($_POST['gid'])&&isset($_POST['prevId']))
                //     {
                //         $name=$_POST['name'];
                //         $gid=$_POST['gid'];
                //         $prevId=$_POST['prevId'];
                //         $res=$sqlOPR->insert('tasks', "name=$name;groupId=$gid;prev=$prevId;percent=1");
                //         if(!empty($res['id']))
                //         {
                //             $res['answer']=array('درج با موفقیت انجام شد');
                //             $res['errCode']=1;
                //         }
                //         echo json_encode($res);
                //     }
                //     else
                //     {
                //         $res['answer']=array('لطفا دوباره تلاش کنید');
                //         $res['errCode']=0;
                //         echo json_encode($res);
                //     }
                // }
                // else 
                if($_POST['action']=='task' && ispost(array('startgid','stopgid','thisId')))
                {
                    $id=$_POST['thisId'];
                    if($rows=$sqlOPR->selectJoin('projects','tasks.id,permitions.rules,projectuser.permitionId','projectuser,permitions,groups,tasks',"projects.id=projectuser.projectId,projectuser.permitionId=permitions.id,projects.id=groups.projectId,groups.id=tasks.groupId",
                    "projectuser.projectId=$projectId,projectuser.userId=$_SESSION[userId],tasks.id=$id"))
                    {
                        $rows=$rows['rows'];
                        if(sizeof($rows)>0)
                        {
                            $rows=$rows[0];
                            if((checkOwner($rows['permitionId']) || ($rulesUser['sortTask'] & $rows['rules'])>0))
                            {
                                $startgid=@$_POST['startgid'];
                                $stopgid=@$_POST['stopgid'];

                                $startNextId=@$_POST['startNextId'];
                                $thisId=@$_POST['thisId'];
                                //$startThisPrev=$_POST['startThisPrev'];
                                $startNextPrev=@$_POST['startNextPrev'];
                                $startSelfPrev=@$_POST['startSelfPrev'];

                                $stopNextId=@$_POST['stopNextId'];
                                $stopNextPrev=@$_POST['startNextPrev'];
                                $stopThisPrev=@$_POST['stopThisPrev'];
                                if($startNextId!=-1)
                                {
                                    $res=$sqlOPR->update('tasks', "prev=$startNextPrev","id=$startNextId");
                                }

                                $updates=false;
                                if($stopNextId!=-1 && ( ($startNextId!=-1 && $res) || ($startNextId==-1) ) )
                                {
                                    $res1=$sqlOPR->update('tasks', "prev=$thisId","id=$stopNextId");
                                }
                                else
                                {
                                    $res=$sqlOPR->update('tasks', "prev=$startSelfPrev","id=$startNextId");
                                    unsuccess();
                                    exit;
                                }
                                if( ($res1 && $stopNextId!=-1) || ($stopNextId==-1))
                                {
                                    if($sqlOPR->update('tasks', "prev=$stopThisPrev,groupId=$stopgid","id=$thisId"))
                                        success();
                                    else
                                    {
                                        $res=$sqlOPR->update('tasks', "prev=$startSelfPrev","id=$startNextId");
                                        $res1=$sqlOPR->update('tasks', "prev=$stopThisPrev","id=$stopNextId");
                                        unsuccess();
                                        exit;
                                    }
                                }
                                else
                                    unsuccess();
                            }
                            else
                                accessDenied();                
                        }
                        else
                            tryAgain();
                    }
                    else
                        tryAgain();
                }
                else if($_POST['action']=='group')
                {
                    $gid=@$_POST['thisId'];
                    if($rows=$sqlOPR->selectJoin('projects','permitions.rules,projectuser.permitionId','projectuser,permitions,groups',"projects.id=projectuser.projectId,projectuser.permitionId=permitions.id,projects.id=groups.projectId",
                    "projectuser.projectId=$projectId,projectuser.userId=$_SESSION[userId],groups.id=$gid"))
                    {
//                        pr($rows);
                        $rows=$rows['rows'];
                        if(sizeof($rows)>0)
                        {
//                                echo 'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa';
                            $rows=$rows[0];
                            if((checkOwner($rows['permitionId']) || ($rulesUser['sortGroup'] & $rows['rules'])>0))
                            {
                                $startgid=@$_POST['startgid'];
                                $stopgid=@$_POST['stopgid'];

                                $startNextId=@$_POST['startNextId'];
                                $thisId=@$_POST['thisId'];
                                $startThisPrev=@$_POST['startThisPrev'];
                                $startNextPrev=@$_POST['startNextPrev'];

                                $stopNextId=@$_POST['stopNextId'];
                                $stopNextPrev=@$_POST['startNextPrev'];
                                $stopThisPrev=@$_POST['stopThisPrev'];

                                if($startNextId!=-1)
                                {
                                    $res=$sqlOPR->update('groups', "prev=$startNextPrev","id=$startNextId");
                                }
//                                if($stopNextId!=-1)
//                                {
//                                    $res=$sqlOPR->update('groups', "prev=$thisId","id=$stopNextId");
//                                }
//                                $res=$sqlOPR->update('groups', "prev=$stopThisPrev","id=$thisId");
//
//                                if(!empty($res['id']))
//                                {
//                                    $res['answer']=array('ویرایش با موفقیت انجام شد');
//                                    $res['errCode']=1;
//                                }
                               // echo json_encode($res);

                                if($stopNextId!=-1 && ( ($startNextId!=-1 && $res) || ($startNextId==-1) ) )
                                {
                                    $res1=$sqlOPR->update('groups', "prev=$thisId","id=$stopNextId");
//                                    $res1=$sqlOPR->update('tasks', "prev=$thisId","id=$stopNextId");
                                }
                                else
                                {
                                    $res=$sqlOPR->update('groups', "prev=$startThisPrev","id=$startNextId");
                                    unsuccess();
                                    exit;
                                }
                                if( ($res1 && $stopNextId!=-1) || ($stopNextId==-1))
                                {
                                    if($sqlOPR->update('groups', "prev=$stopThisPrev","id=$thisId"))
                                        success();
                                    else
                                    {
                                        $res=$sqlOPR->update('groups', "prev=$startThisPrev","id=$startNextId");
                                        $res1=$sqlOPR->update('groups', "prev=$stopThisPrev","id=$stopNextId");
                                        unsuccess();
                                        exit;
                                    }
                                }
                                else
                                    unsuccess();
                            }
                            else
                                accessDenied();
                        }
                        else
                            tryAgain();
                    }
                    else
                        tryAgain();
                }
                else
                    tryAgain();
            }
            else
                tryAgain();
        }
        else
            ajaxRequestError();
    }
    else
        httpRequestError();
}
else
    loginError();
