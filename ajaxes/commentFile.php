<?php
include_once '../config/config.php';
include_once 'ajaxFuncs.php';
if(isset($_SESSION['userId']))
{
    if ($_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest' && ispost(array('projectId'))) 
    {
        //Request identified as ajax request
        $projectId=$_POST['projectId'];
        if (isset($_SESSION['ajaxRequest']) && $_SESSION['ajaxRequest']=='schedulelogedin') 
        {
            if (isset($_POST['action'])) 
            {
                if($_POST['action']=='new' && ispost(array('taskId','comment','percent')))
                {
                    $id=$_POST['taskId'];
                    if($rows=$sqlOPR->selectJoin('projects','tasks.id,permitions.rules,projectuser.permitionId','projectuser,permitions,groups,tasks',"projects.id=projectuser.projectId,projectuser.permitionId=permitions.id,projects.id=groups.projectId,groups.id=tasks.groupId",
                    "projectuser.projectId=$projectId,projectuser.userId=$_SESSION[userId],tasks.id=$id"))
                    {
                        $rows=$rows['rows'];
                        if(sizeof($rows)>0)
                        {
                            $rows=$rows[0];
                            if((checkOwner($rows['permitionId']) || ($rulesUser['insertComment'] & $rows['rules'])>0) || sizeof($sqlOPR->select('taskUser','id',"taskUser.userId=$_SESSION[userId],taskUser.taskId=$id"))>0)
                            {
                                $taskId=$_POST['taskId'];
                                $comment=$_POST['comment'];
                                $percent=$_POST['percent'];
                                $nowTime=  time();
                               if($res=$sqlOPR->insert1('taskcomment',array('taskId','userId','percent','comment','userName','userEmail','intTime'),array($taskId,$_SESSION['userId'],$percent,$comment,$_SESSION['userName'],$_SESSION['userLogin'],$nowTime)))
                               {
                                    $sqlOPR->update('tasks',"newCommentState=1,newCommentTime=$nowTime","id=$taskId");
                                    success($res['id']);
                               }
                                else
                                    unSuccess();
                            }
                            else
                                accessDenied();                
                        }
                        else
                            tryAgain();
                    }
                    else
                        tryAgain();                    
                }
                else if($_POST['action']=='select' && ispost(array('taskId')))
                {
                    $id=$_POST['taskId'];
                    if($rows=$sqlOPR->selectJoin('projects','tasks.id,permitions.rules,projectuser.permitionId','projectuser,permitions,groups,tasks',"projects.id=projectuser.projectId,projectuser.permitionId=permitions.id,projects.id=groups.projectId,groups.id=tasks.groupId",
                    "projectuser.projectId=$projectId,projectuser.userId=$_SESSION[userId],tasks.id=$id"))
                    {
                        $rows=$rows['rows'];
                        if(sizeof($rows)>0)
                        {
                            $rows=$rows[0];
                            if((checkOwner($rows['permitionId']) || ($rows['rules'] & $rows['rules'])>0) || sizeof($sqlOPR->select('taskUser','id',"taskUser.userId=$_SESSION[userId],taskUser.taskId=$id"))>0)
                            {
                                $taskId=$_POST['taskId'];
                                $taskComments=$sqlOPR->select('taskcomment','', "taskId=$taskId",'','id desc');
                                
                                //$files=$sqlOPR->select('tasksFile','',"taskId=$taskId",'','id desc');
                                include_once (DIR.'jdf.php');
                                $tmpTaskComments = $taskComments;
                                $timezone = 0;//برای 3:30 عدد 12600 و برای 4:30 عدد 16200 را تنظیم کنید
                                if(count($taskComments)>0)
                                {
                                    $cdate=$taskComments[0]['cdate'];
                                    $dt='';
                                    $dt=explode(' ',$cdate);
                                    $d=$dt[0];
                                    $t=$dt[1];
                                    $date=strtotime($d);
                                    $time=strtotime($t);
                                    $now = date("Y-m-d", $date);
                                    $time = date("H:i:s", (int)$time);
                                    list($year, $month, $day) = explode('-', $now);
                                    list($hour, $minute, $second) = explode(':', $time);
                                    $timestamp = mktime($hour, $minute, $second, $month, $day, $year);
                                    //echo $timestamp."<br>";
                                    $jalali_date = jdate("H:i:s Y/m/d",$timestamp);
                                    $taskComments[0]['cdate']=$jalali_date;

                                    $nowTime=time();
                                    $taskComments[0]['cookie']=$_COOKIE['taskVisitCookie'.$taskId];
                                    setcookie('taskVisitCookie'.$taskId, $nowTime, time()+(60*60*3600*30*12*24), "/");
                                }

                                //$taskComments[0]['cdate']=$jalali_date;
                                foreach($tmpTaskComments as $i=> $taskComment)
                                {
                                    $cdate=$taskComments[$i]['cdate'];
                                    $dt='';
                                    $dt=explode(' ',$cdate);
                                    $d=$dt[0];
                                    $t=$dt[1];
                                    $date=strtotime($d);
                                    $time=strtotime($t);
                                    $now = date("Y-m-d", $date);
                                    $time = date("H:i:s", (int)$time);
                                    list($year, $month, $day) = explode('-', $now);
                                    list($hour, $minute, $second) = explode(':', $time);
                                    $timestamp = mktime($hour, $minute, $second, $month, $day, $year);
                                    //echo $timestamp."<br>";

                                    $jalali_date = jdate("H:i:s Y/m/d",$timestamp);
                                    $taskComments[$i]['cdate']=$jalali_date;
                                }
                                $taskCommentsFiles['comments']=$taskComments;
                                if(count($taskComments)>0)
                                    success('',$taskCommentsFiles);
                                else
                                    emptySuccess();
//                               }
//                                else
//                                    success('',array);
                            }
                            else
                                accessDenied();                
                        }
                        else
                            tryAgain();
                    }
                    else
                        tryAgain();  
                }
                else if($_POST['action']=='select-file' && ispost(array('taskId')))
                {
                    $id=$_POST['taskId'];
                    if($rows=$sqlOPR->selectJoin('projects','tasks.id,permitions.rules,projectuser.permitionId','projectuser,permitions,groups,tasks',"projects.id=projectuser.projectId,projectuser.permitionId=permitions.id,projects.id=groups.projectId,groups.id=tasks.groupId",
                    "projectuser.projectId=$projectId,projectuser.userId=$_SESSION[userId],tasks.id=$id"))
                    {
                        $rows=$rows['rows'];
                        if(sizeof($rows)>0)
                        {
                            $rows=$rows[0];
                            if((checkOwner($rows['permitionId']) || ($rows['rules'] & $rows['rules'])>0) || sizeof($sqlOPR->select('taskUser','id',"taskUser.userId=$_SESSION[userId],taskUser.taskId=$id"))>0)
                            {
                                $taskId=$_POST['taskId'];
                                $files=$sqlOPR->select('tasksfile','',"taskId=$taskId",'','id desc');
                                $taskComments[0]['cookie']=@$_COOKIE['taskVisitCookie'.$taskId];
                                //setcookie('taskVisitCookie'.$taskId, $nowTime, time()+(60*60*3600*30*12*24), "/");
                                $taskCommentsFiles['files']=$files;
                                success('',$taskCommentsFiles);
                            }
                            else
                                accessDenied();                
                        }
                        else
                            tryAgain();
                    }
                    else
                        tryAgain(); 
                }
                elseif($_POST['action']=='delete' && ispost(array('commentId')))
                {
                    $commentId=$_POST['commentId'];
                    if($comment=$sqlOPR->select('taskcomment','id,taskId',"taskcomment.userId=$_SESSION[userId],taskcomment.id=$commentId"))
                    {
                        $id=$comment[0]['taskId'];
                        if($rows=$sqlOPR->selectJoin('projects','tasks.id,permitions.rules,projectuser.permitionId','projectuser,permitions,groups,tasks',"projects.id=projectuser.projectId,projectuser.permitionId=permitions.id,projects.id=groups.projectId,groups.id=tasks.groupId",
                        "projectuser.projectId=$projectId,projectuser.userId=$_SESSION[userId],tasks.id=$id"))
                        {
                            $rows=$rows['rows'];
                            if(sizeof($rows)>0)
                            {
                                $rows=$rows[0];
                                if((checkOwner($rows['permitionId']) || ($rulesUser['deleteComment'] & $rows['rules'])>0))
                                {
                                    if($res=$sqlOPR->delete('taskcomment', "id=$commentId"))
                                    {
                                        success();
                                    }
                                    else
                                        unSuccess();
                                }
                                else
                                    accessDenied();                
                            }
                            else
                                tryAgain();
                        }
                        else
                            tryAgain();
                    }
                    else
                        accessDenied(); 
                }
                else
                    tryAgain();
            }
            else
                tryAgain();
        }
        else
            ajaxRequestError();
    }
    else
        httpRequestError();
}
else
    loginError();


/*
<?php

<?php
include_once '../config/config.php';

if($_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest')
{
    //Request identified as ajax request
    if(isset($_SESSION['ajaxRequest'])&&$_SESSION['ajaxRequest']==$_POST['token'])
    {
        if(isset($_POST['name'])&&isset($_POST['gid'])&&isset($_POST['prevId']))
        {
            $name=$_POST['name'];
            $gid=$_POST['gid'];
            $prevId=$_POST['prevId'];
            $res=$sqlOPR->insert('tasks', "name=$name;groupId=$gid;prev=$prevId");
            if(!empty($res['id']))
            {
                $res['answer']=array('درج با موفقیت انجام شد');
                $res['errCode']=1;
            }
            echo json_encode($res);
        }
        else
        {
            $res['answer']=array('لطفا دوباره تلاش کنید');
            $res['errCode']=0;
            echo json_encode($res);
        }
    }
    else
    {

        if(!isset($_SESSION['ajaxRequest']))
        {
            $res['answer']='پسر جان برو دنبال یه کار دیگه!!!!';
            $res['errcode']=-1;
            echo json_encode($res);
        }
        header('Location: '.URL);
    }

}
else
{
header('Location: '.URL);
}
 *

 * */
