<?php
include_once '../config/config.php';
include_once 'ajaxFuncs.php';
if(isset($_SESSION['userId']))
{
    if($_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest')
    {
        //Request identified as ajax request
        if(isset($_SESSION['ajaxRequest']) && $_SESSION['ajaxRequest']=='schedulelogedin')
        {
            if(isset($_POST['action']))
            {
                if ($_POST['action'] == 'select-next' && ispost(array('projectType','page')) && $_POST['page']>=0)
                {
                    $type=$_POST['projectType'];
                    $page=$_POST['page'];
                    $limit=5;
                    if($type=='invited')
                    {
                        $projects = $sqlOPR->selectJoin('projects','projects.*,projectuser.projectId,projectuser.permitionId','projectuser',"projectuser.projectId=projects.id","projectuser.userId=$_SESSION[userId],permitionId<>0",'inner join','','projects.id asc',"$page,$limit");
                        $projects=$projects['rows'];
                    }
                    elseif($type=='myproject')
                    {
                        $projects = $sqlOPR->selectJoin('projects','projects.*,projectuser.projectId,projectuser.permitionId','projectuser',"projectuser.projectId=projects.id","projectuser.userId=$_SESSION[userId],permitionId=0",'inner join','','projects.id asc',"$page,$limit");
                        $projects=$projects['rows'];
                    }
                    else
                    {
                        $projects = $sqlOPR->selectJoin('projects','projects.*,projectuser.projectId,projectuser.permitionId','projectuser',"projectuser.projectId=projects.id","projectuser.userId=$_SESSION[userId]",'inner join','','projects.id asc',"$page,$limit");
                        $projects=$projects['rows'];
                    }
                    $rows='';
                    $project=$projects;
                    for ($i=0;$i<5;$i++)
                    {
                        if(@$projects[$i]['name']=='')
                            break;
                        $projectUser=$sqlOPR->selectJoin('projectuser','','users','users.id=projectuser.userId',"projectId=".@$project[$i]['projectId']);
                        $projectUser=$projectUser['rows'];
                        $usersArray = array_map(function($el){ if($el['userId']!=$_SESSION['userId'])return $el['name']; }, $projectUser);
                        $users='شما- '.trim(implode('- ', $usersArray),' - ');
                        $users=trim($users,' - ');
                        $projectName = @$project[$i]['name'];
                        $projectName = preg_replace('/ /', '-', $projectName);
                        $aProjectId=$project[$i]['id'];
                        $query="select SUM( percent ) / COUNT( * ) AS percents from projects
                        left join groups on projects.id=groups.projectId
                        left join tasks on groups.id=tasks.groupId where projects.id=".@$project[$i]['id'];
                        $query=$DBH->prepare($query);
                        $query->execute();
                        $percent=$query->fetchall();
                        $percent=round(@$percent[0]['percents'],1);
                        $creator=$project[$i]['permitionId']==0?'شما':'همکاران';
                        $rows.="
                        <tr>
                            <td>
                                <a href='" . URL . "gantt/$projectName?projectId=$aProjectId' class='thumbnail' alt='".@$project[$i]['name']."' title='".@$project[$i]['text']."'
                                style='width:auto; text-decoration:none; margin-right:15px; vertical-align: middle; float: right;'>
                                ".@$project[$i]['name']."
                                </a>
                            </td>
                            <td>
                                <div class='percent80' style='position:relative !important;'>
                                    <div class='percent' style='width: $percent%; right: 0px;margin-top:0px;position: absolute !important;height:100%;border-radius:0px;'>
                                    </div>
                                    <div>$percent%</div>
                                </div>
                            </td>
                            <td>$creator</td>
                            <td><a href='".URL."gantt/$projectName/invite?projectId=".$project[$i]['projectId']."' title='$users' alt='$users'>$users</td>
                        </tr>
                        ";
                    }
                    for (;$i<5;$i++)
                    {
                        $rows.="
                        <tr style='height:39px'>
                            <td ></td><td></td><td></td><td></td>
                        </tr>
                        ";
                    }
                    if(count($projects)>0)
                        $rowsAr['end']=0;
                    else
                        $rowsAr['end']=1;
                        $rowsAr['rows']=$rows;

                    success('',$rowsAr);
                }
                else
                    emptySuccess();
            }
            else
                tryAgain();
        }
        else
            ajaxRequestError();
    }
    else
        httpRequestError();
}
else
    loginError();
        