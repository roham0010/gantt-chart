<?php
$_SESSION['ajaxRequest'] = 'schedulelogedin';
$style='';
include_once 'header1.php';
?>
<body>
    <?php
    include_once 'menu2.php';
    ?>
    <div class="container marketing" style="margin-top:90px;">
        <p><span class="label label-info"><h1 class="text-center">ویرایش پروفایل</h1></span></p>
        <?php
        $actives=array('profile'=>'','repass'=>'',2=>'');
        $actives[$params[0]]='active';
        
        $validateMsg='';
        global $validateMsg;
        $user=$sqlOPR->select('users','',"id=$_SESSION[userId]");
        ?>
        <hr class="">
        <ul class="nav nav-pills">
            <li class="<?=$actives['profile']?>"><a href="<?=URL?>profile">ویرایش پروفایل</a></li>
            <li class="<?=$actives['repass']?>"><a href="<?=URL?>repass">ویرایش رمز عبور</a></li>
        </ul>
        <div class="row">
            <div class="span4"></div>
            <div class="span3 text-center">

            <?php
    
        if(sizeof($user)>0)
        {
            $user=$user[0];
            if(isset($_POST['edit-info']))
            {
                $_POST=filter_input_array(INPUT_POST,FILTER_SANITIZE_STRING);
                include_once DIR.'classes/formValidation.php';
                $validation=new formValidation();
                $inputValid = array(
                    'phone' => 'همراه|r|+98|nu' ,
                    'name' => 'نام|len=2#-'
                    );
                $validateMsg=$validation->validation($inputValid);
                // pr($validateMsg);
                if($validateMsg==''||count($validateMsg)==0)
                {
                    $sqlOPR = new sqlOPR();
                    $phone=$_POST['phone'];
                    $name=$_POST['name'];
                    $company=$_POST['company'];
                    $web=$_POST['web'];
                    //uplooooooooad
                    $profilePic=@$_FILES['profilePic']['name'];
                    if($profilePic!='')
                    {
                        // $imagick = new Imagick(realpath($_FILES["profilePic"]["tmp_name"]));
                     //    $imagick->setbackgroundcolor('rgb(64, 64, 64)');
                     //    $imagick->thumbnailImage(100, 100, true, true);
                     //    $_FILES["profilePic"]["tmp_name"]=$imagick->getImageBlob();
                     //    $profilePic=$_FILES['profilePic']['name'];
                        $target_dir = DIR."profiles/";
                        $target_file = $target_dir . basename($user['id']);
                        $imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
                        // Check if image file is a actual image or fake image
                        $check = @getimagesize($_FILES["profilePic"]["tmp_name"]);
                        if($check == false)
                        {
                            $errMessageFromUp='فایل عکس انتخاب کنید...';
                            $signupError=1;
                        }

                        // Check file size
                        if ($_FILES["profilePic"]["size"] > 5000000) {
                           $errMessageFromUp='حجم تصویر باید کمتر از 5 مگابایت باشد...';
                            $signupError=1;
                        }
                        // Allow certain file formats
                        $sub=explode('.', $profilePic);
                        $ln=count($sub);
                        $type=$sub[$ln-1];
                        $imageFileType=$type;
                        if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
                        && $imageFileType != "gif" ) {
                            $errMessageFromUp='نوع فایل پشتیبانی نمیشود';
                            $signupError=1;
                        }
                        if(isset($_FILES['profilePic']['name']) && $_FILES['profilePic']['error'] == 0)
                        {
                            $photo = &$_FILES['profilePic'];
                            // echo 'aaaaaaaaaaaaaaaaaaaaafd dfas dsa a'.$photo['type'];
                            switch(strtolower($photo['type']))
                            {
                                case 'image/gif':
                                    $src = ImageCreateFromGIF($photo['tmp_name']);
                                    break;
                                case 'image/jpeg':
                                    $src = ImageCreateFromJPEG($photo['tmp_name']);
                                    break;
                                case 'image/png':
                                    $src = ImageCreateFromPNG($photo['tmp_name']);
                                    ImageAlphaBlending($src, false);
                                    ImageSaveAlpha($src, true);
                                    break;
                                case 'image/bmp':
                                    $src = ImageCreateFromWBMP($photo['tmp_name']);
                                    break;
                                default:
                                    $errMessageFromUp='نوع تصویر پشتیبانی نمیشود...';
                                    $signupError=1;
                                break;
                            }
                        }
                        if (@$signupError == 1) {
                            ;
                        }
                        else
                        {
                            $imagex=ImageSX($src);
                            $imagey=ImageSY($src);
                            while(!(($imagex/2)<200 && ($imagex/2)>128) && !(($imagex/2)<128))
                            {
                                $imagex=$imagex/2;
                            }
                            if($imagex<64)
                            {
                                $bet=64-$imagex;
                                $imagex+=$bet;
                            }
                            while(!(($imagey/2)<200 && ($imagey/2)>128) && !(($imagey/2)<128))
                            {
                                $imagey=$imagey/2;
                            }
                            if($imagey<128)
                            {
                                $bet=128-$imagey;
                                $imagey+=$bet;
                            }
                            $imagey-=$imagey-$imagex;
                            $dst = ImageCreateTrueColor($imagex, $imagey);
                            $white = ImageColorAllocate($dst,  255, 255, 255);
                            ImageFilledRectangle($dst, 0, 0, ImageSX($src), ImageSY($src), $white);
                            ImageCopyResampled($dst, $src, 0, 0, 0, 0, $imagex,$imagey, ImageSX($src), ImageSY($src));

                            ImageJPEG($dst, $target_file . '.jpg', 100);
                            ImageDestroy($dst);
                            ImageDestroy($src);
                        }
                                // if(move_uploaded_file($_FILES['profilePic']['tmp_name'], $target_file))

                    }
                    $updates="name=$name,phone=$phone,company=$company,web=$web";
                    $where="id=$user[id]";
                    if($sqlOPR->update('users', $updates, $where))
                    {
                        $errMessageFrom='ویرایش انجام شد.';
                        $user=$sqlOPR->select('users','',"id=".$user['id']);
                        include_once DIR . "subfiles/sessionSet.php";
                        $signinError=0;
                        $user=$user[0];
                    }
                    else
                    {
                        $errMessageFrom='ویرایش انجام نشد.';
                    }
                }
            }
            if(!isset($_POST['signup-user'])||($validateMsg!=''||count($validateMsg)>0))
            {
                echo '<script src="'.URL.'js/formValidation.js"></script>';
                include_once DIR.'classes/formHelper.php';
                $formHelper=new formHelper();
                $caption='';
                $name='edit-info';
                $action='';
                $method='post';
                $onsubmit="name| | |2#-| | |onkeyup,company| | | | | |onkeyup,address| | | | | |onkeyup";
                $formHelper->form($caption, $name, $action, $method,$onsubmit,'','form-signin','',"multipart/form-data");
                // echo @$errMessageFrom.' '.@$errMessageFromUp."";
                ?>
                <label><?=@$errMessageFromUp?></label>
                <div class="">
                    <?php $formHelper->input('1نام',"نام...",'name','name| | |2#-| | |onblur',"$user[name]",'text',"input_ed rec",'',''); ?>
                </div>
                <div class="input-field col s12 l9">
                    <?php $formHelper->input('1شماره همراه','شماره همراه خود را وارد کنید...','phone','phone|r|nu| |+98| |onblur',"$user[phone]",'',"input_ed rec ltr"); ?>
                </div>
                <div class="input-field col s12 l9">
                        <?php $formHelper->input('1نام شرکت',
                                                 "نام شرکت...",'company','',"$user[company]",'text',"input_ed rec ltr",'',''); ?>
                </div>
                <div class="input-field col s12 l9">
                    <?php $formHelper->input('1آدرس وب'
                                             ,"آدرس وب...",'web','',"$user[web]",'text',"",'',''); ?>
                </div>
                <div class="input-field col s12 l9">
<!--                    <input type="file" name="profilePic" class="field-file-upload-native" accept="image/*" />-->
                    <?php $formHelper->input('1عکس پروفایل'
                                             ,"آدرس وب...",'profilePic','',"$user[web]",'file',"",'',''); ?>
                </div>
                <div class="input-field col s12 l9">
                    <?php $formHelper->submit('edit-info', "ثبت ویرایش",'waves-effect waves-light btn','',"",0);?>
                </div>
                <?php
            }
        }
        else
            signin();
        ?>
         
        </div>
        </div>
        <hr class="">

        <script type="text/javascript">
            ajaxRequest = "<?php echo $_SESSION['ajaxRequest']; ?>";
            userLogin = "<?php echo $_SESSION['userLogin']; ?>";
            userName = "<?php echo $_SESSION['userName']; ?>";
            projectType = "<?php echo $projectType; ?>";
        </script>
<?php
include_once "footer1.php";