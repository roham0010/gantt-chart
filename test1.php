سناریویی که این کار رو انجام مبدین واسه این مورد درست نیست...
شما باید این تگ select رو که توی فایل php میسازید رو توی javascript قسمت ajax اون رو بسازید...
به این صورت که dataType تابع ajax رو برابر با json میذارید و دیتاها گه همون دیتاهای سلکت هستن و به صورت آرایه از دیتابیس توی فایل php میگیرید رو json_encode کنید و توی صفحه echo کنید...
و اینجا هربار که این سلکت ساخته میشه همونطور که گفتم توی بخش ajax همه اطلاعات این سلکت رو دارید دیگه id اون رو میذاریم توی یک متغیر تا بفهمیم آخرین سلکتی که گرفتیم id اون چی بوده و ازش استفاده کنیم...
یه مثال از کدهای خودم میزنم ببینید:
[JAVASCRIPT]
 $.ajax({
                   url: URL + 'ajaxes/newGroup.php',
                   type: 'post',
                   data: {/*data*/},
                   dataType: 'json',
                   success: function (data) {
var latestSelectId=data['selectId'];
for(/*peymayeshe data*/)
{
//create select tag
}
}
});
[/JAVASCRIPT]
[PHP]
$posts=$sqlOPR->select('posts',);
echo json_encode($posts);
[/PHP]
