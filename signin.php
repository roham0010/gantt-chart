<?php
$description="از این صفحه کاربران مشتاق به استفاده از نمودار گانت ساز فارسی یا Gantt Diagram Maker ورود کرده و رایگان از آن استفاده کنند.";
$title="ساخت نمودار گانت | رسم گانت آنلاین| گانت | Gantt Diagram | گانت با تاریخ فارسی | دیاگرام گانت با تاریخ هجری شمسی | ورود گانت";
{
$style="<style>
body
  {
    padding-top: 40px;
    padding-bottom: 40px;
    background-color: #f5f5f5;
  }

  .form-signin
  {
    max-width: 300px;
    padding: 19px 29px 29px;
    margin: 80px auto 20px;
    background-color: #fff;
    border: 1px solid #e5e5e5;
    -webkit-border-radius: 5px;
       -moz-border-radius: 5px;
            border-radius: 5px;
    -webkit-box-shadow: 0 1px 2px rgba(0,0,0,.05);
       -moz-box-shadow: 0 1px 2px rgba(0,0,0,.05);
            box-shadow: 0 1px 2px rgba(0,0,0,.05);
  }
  .form-signin .form-signin-heading,
  .form-signin .checkbox
  {
    margin-bottom: 10px;
  }
  .form-signin input[type='text'],
  .form-signin input[type='password']
  {
    font-size: 16px;
    height: auto;
    margin-bottom: 15px;
    padding: 7px 9px;
  }
</style>";
}
//session_start();
//echo $_SESSION['captcha1']['code'];
include_once 'header.php';
include_once DIR.'classes/formHelper.php';
include_once DIR.'classes/hashMaker.php';
echo '<div class="container">
<div class="form-signin">';

global $validateMsg;

if(isset($_GET['params'])&&$_GET['params']!='')
{
	global $params;
    if(isset($params[1])&&$params[1]=='new-password')
	{
		if(isset($_POST['repass'])&&isset($_POST['pass']))
		{
			if(isset($_POST['repass'])==isset($_POST['pass']))
			{
				$pass=$_POST['pass'];
				include_once DIR.'classes/formValidation.php';
				$validation=new formValidation();
				$inputValid = array('pass' => 'رمز عبور|sc|len=6#-');

				$validateMsg=$validation->validation($inputValid);
				$hash=new hashMaker();
				$code=$hash->hash($params[3]);
				$pass=$hash->hash($pass);
				$email=$_POST['email'];
				if($validateMsg==''||count($validateMsg)==0)
				{
					$user=$sqlOPR->select("users",'',"email=$email,forgetPassCode=$code");
					if(count($user)>0)
					{
						if($sqlOPR->update("users","pass=$pass","email=$email"))
						{
							$_SESSION['enteredCode']=$code;
							$_SESSION['phoneRet']=$email;
							$string='location:'.URL."signin/pass-changed";
							header($string);
						}
						else
						{
							$validateMsg='اطلاعات صحیح نیست دوباره تلاش کنید';
							header("refresh:3; url=".URL);
						}
					}
					else
					{
						$validateMsg='اطلاعات صحیح نیست دوباره تلاش کنید';
						header("refresh:3; url=".URL);
					}
				}
			}
		}
		if(!isset($_POST['repass'])||($validateMsg!=''||count($validateMsg)>0))
		{
			include_once DIR.'classes/formHelper.php';
			//echo '<script src="'.URL.'js/formValidation.js"></script>';
			$formHelper=new formHelper();
			$caption='';
			$name='frm-enter-code';
			$method='post';
			$email=$params[2];
			$code=$params[3];
			$action=URL."signin/new-password/$email/$code";
			$formHelper->form($caption, $name, $action, $method,'passre|r| |6#-|sc| |onblur','','form-signin',"");
			echo '
			<div class="row margin">
                <div class="input-field col s12">
                    <i class="material-icons prefix">lock_outline</i>
			';
			$formHelper->input('رمز عبور','1رمز عبور','pass','pass|r| | |sc| | ','','password',"input_ln");
			echo '
                </div>
            </div>
            <div class="row margin">
                <div class="input-field col s12">
                    <i class="material-icons prefix">lock_outline</i>';
			$formHelper->input('تکرار رمز عبور','1تکرار رمز عبور','passre','passre|r| |6#-|sc| |onblur','','password',"input_ln");
			echo'
                </div>
            </div>';
			$formHelper->input('','','email','',$email,'hidden',"input_ln");
			$formHelper->input('','','code','',$code,'hidden',"input_ln");

			$md=md5(rand(1,99999999).time());
			$_SESSION['tokenSignIn']=$md;
			$formHelper->input('','','token','',$_SESSION['tokenSignIn'],'hidden','','','',1);
			echo '<div class="row margin">
                <div class="input-field col s12">';
		    $formHelper->submit('repass', "تعویض",'btn blue waves-effect waves-light col s12','',"",1);
		    echo "</div></article>

					<div class='shadow'></div>
				</div></div>";
		}
		else
		{
			$string='location:'.URL."signin/forget-password";
			header($string);
		}
	}
	elseif(isset($params[1])&&$params[1]=='change-pass')
	{
        echo'<h1 class="blue-grey-text center">ویرایش رمز عبور</h1>';
        $reciveCode=@$params[2];
        $user=$sqlOPR->select('users','id',"forgetPassCode=$reciveCode");
        $errFlag=true;
        if(count($user)>0)
        {
            if(isset($_POST['editPass']))
            {
                $userId=$user[0]['id'];
                include_once DIR.'classes/formValidation.php';
                $validation=new formValidation();
                $inputValid = array(
                    'pass' => 'رمز عبور|sc|len=6#-',
                    );
                $validateMsg=$validation->validation($inputValid);
                //pr($validateMsg);
                if($validateMsg==''||count($validateMsg)==0)
                {
                    $sqlOPR = new sqlOPR();
                    $rePass=$_POST['passre'];
                    $pass=$_POST['pass'];
                    $inputValid = array(
                    'pass' => 'رمز عبور|sc|len=6#-'
                    );
                    include_once DIR.'classes/hashMaker.php';
                    $hash=new hashMaker();
                    $pass=$hash->hash($pass);
                    if($validateMsg==''||count($validateMsg)==0)
                    {
                        if($sqlOPR->update('users',"pass=$pass","id=$userId"))
                        {
                            message('رمز عبور با موفقیت تغییر یافت، پس از چند ثانیه به صفحه ورود منتقل خواهید شد.');
                            $errFlag=false;
                            signin(4);

                        }
                        else
                            $validateMsg='لطفا مقادیر ورودی را چک کنید!';
                        //print_r($user);
                    }
                    else
                        echo 'fdsa';


                        //END uploooooooooad////////
                }
            }

            if(!isset($_POST['editPass'])||($validateMsg!=''||count($validateMsg)>0) && $errFlag==true)
            {
                include_once DIR.'classes/formHelper.php';
                echo '<script src="'.URL.'js/formValidation.js"></script>
                <div class="edit-row">';
                $formHelper=new formHelper();
                $caption='';
                $name='signup';
                $action='';
                $method='post';
                $onsubmit="passre|r| |6#-|sc| |onblur";
                $formHelper->form($caption, $name, $action, $method,$onsubmit,'','','',"multipart/form-data");

                echo @$validateMsg;
                $formHelper->input('رمز عبور','1رمز عبور جدید','pass','pass|r| |6#-|sc| | ',"",'password',"input-block-level");

                $formHelper->input('تکرار رمز عبور',
                                   '1تکرار رمز عبور جدید','passre','passre|r| |6#-| | |onblur','','password',"input-block-level");
                $formHelper->submit('editPass', "ثبت ویرایشات",'btn w100','',"");
            }
        }
       else
       {
           $errMsg=message("متعضفانه این لینک استفاده شده است! <br>پس از چند ثانیه به صفحه ورود متقل خواهید شد...");
           echo $errMsg;
           signin(5);
       }
	}
	elseif(isset($params[1])&&$params[1]=='forget-password')
	{

        $errMsg=0;
        $formHelper=new formHelper();
        $caption='';
        $name='frm-forget-password-gantt';
        $action=URL.'signin/forget-password';
        $method='post';
        $formHelper->form($caption, $name, $action, $method,'','','',"");
			echo '<h3 class="form-signin-heading"> فرم بازیابی رمز عبور </h3>';
		if(isset($_POST['forget-password-gantt'])&&$_SESSION['tokenSignIn']==$_POST['token'])
		{
			$md=md5(rand(1,99999999).time());
			$_SESSION['tokenSignIn']=$md;
            $email=$_POST['email'];
            $user=$sqlOPR->select("users","id","email=$email");

            if(count($user)>0)
            {
                $md=rand(1,99999999);
                $hash=new hashMaker();
                $code=$hash->hash($md);
                if(!$sqlOPR->update("users","forgetPassCode=$code","email=$email"))
                {
                    echo 'دوباره تلاش کنید!';
                    $errMsg=1;
                }
                else
                {
                    $link="برای تغییر رمز عبور بر روی <a href='".URL."signin/change-pass/$code'>لینک</a> کلیک کنید!";
                    $to=$email;
                    $text="با سلام خدمت شما کاربر گرامی.<br> پیوند فراموشی رمز عبور برای شما ارسال شده اگر شما درخواست آن را نداده اید میتوانید این ایمیل در نظر نگیرید!<br>
                    پیوند ویرایش رمز عبور: $link<br>
                    با تشکر";
                    $subject="فراموشی رمز عبور";
                    if(Mailgun(array('to'=>$email,'text'=>$text,'subject'=>$subject)))
                    {
                        $string="پیوندی به ایمیلتان ارسال شده است لطفا برای تغییر رمز عبور به ایمیل خود مراجعه کرده و بر روی پیوند ذکر شده کلیک کنید!";
                        echo "$string";
                    }
                }
            }
            else
                message("کسی تا به حال با این ایمیل ثبت نام نکرده اید لطفا ابتدا از <a href='".URL."signup'>اینجا ثبت نام</a> کنید");
		}
		elseif(isset($_POST['forget-password-gantt'])&&$_SESSION['tokenSignIn']!=$_POST['token'])
		{
			$string='location:'.URL."signin";
			echo "$string";
			header($string);
		}
		if(!isset($_POST['forget-password-gantt']) || $errMsg===1)
		{
			$formHelper->input('1ایمیل'
                               ,"1ایمیل خود را وارد کنید",'email','','','text','input-block-level','');

			$md=md5(rand(1,99999999).time());
			$_SESSION['tokenSignIn']=$md;
			$formHelper->input('','','token','',$_SESSION['tokenSignIn'],'hidden','','','',1);

		    $formHelper->submit('forget-password-gantt', "ارسال رمز عبور",'btn w100','',"",0);
            echo "<div class='footer-signin'><a class='signup-lnk' href='".URL."signin' >ورود به ".SITENAME."</a></div>";
	    }
        echo "</form>";
	}
	else
	{
        $signinError=0;
		$state=@$params[1];
		switch ($state) {
			case 'empty-login':
				$errMessageForm='نام کاربری و رمز عبور را وارد کنید';
				$signinError=1;
				break;
			case 'wrong-login':
				$errMessageForm='نام کاربری یا رمز عبور اشتباه است';
				$signinError=1;
				break;
		}

		if(isset($_POST['signin-gantt']) && ($_POST['token']==@$_SESSION['tokenSignIn1'] || $_POST['token']==@$_SESSION['tokenSignIn2']))
		{
			if(!isset($_SESSION['token']))
				$_SESSION['token']=1;
			else
				$_SESSION['token']++;

			$email=$_POST['email'];
			$pass=$_POST['pass'];
			if($email==''||$pass=='')
			{
				$errMessageForm='نام کاربری و رمز عبور را وارد کنید';
				$signinError=1;
			}
			elseif(isset($_POST['captcha1'])&&strtolower($_SESSION['captcha1']['code'])!=strtolower($_POST['captcha1']))
			{
				echo '<br>'.$_SESSION['captcha1']['code']."!=".$_POST['captcha1'];
				$errMessageForm='تصویر امنیتی را به درستی وارد کنید';
				$signinError=1;
			}
			else
			{
                if(isset($_POST['captcha1'])&&strtolower($_SESSION['captcha1']['code'])==strtolower($_POST['captcha1']))
                    $_SESSION['token']=1;
				$sqlOPR=new sqlOPR();
                include_once DIR.'classes/hashMaker.php';
                $hash=new hashMaker();
                $pass=$hash->hash($pass);
				$user=$sqlOPR->select('users','',"email=$email,pass=$pass");
//				$user1=$sqlOPR->select('users','',"email=$email,hash=$pass");
				if(count($user)>0)
				{

                    $flagUserSigned=false;
                    $hash = new hashMaker();
                    $hashCookie=$hash->hash($user[0]['email'].$user[0]['name'].$user[0]['id'].time());
                    $sqlOPR->update('users',"hashCookie=$hashCookie","id=".$user[0]['id']);
                    $user[0]['hashCookie']=$hashCookie;

					include DIR . "/subfiles/sessionSet.php";
					$string="location:".URL."ganttmanager/true-login";
//					echo "sign innnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnn";
					header($string);
				}
//				elseif(count($user1)>0)
//				{
//				    $user=$user1;
//					include DIR . "/subfiles/sessionSet.php";
//					$sqlOPR->update('users', "pass=$pass,hash=", "email=$email");
//					$string="location:".URL."ganttmanager/true-login";
//                    echo "sign innnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnn222222";
//					//header($string);
//				}
				else
				{
					$errMessageForm='نام کاربری یا رمز عبور اشتباه است';
					$signinError=1;
				}
			}

		}

		/*elseif(isset($params[1]))
		{
			$signinError=1;
			$errMessageForm='خطایی رخ داده است دوباره تلاش کنید';
		}*/
//        echo '<br>'.$_SESSION['captcha1']['code'];
		if(!isset($_POST['signup-gantt']) || $signinError==1)
		{
			$formHelper=new formHelper();
			$caption='';
			$name='frm-Gantt-Maker-Persian';
			$action=URL.'signin';
			$method='post';
			$formHelper->form($caption, $name, $action, $method,'','','','border-radius:0px;');
			echo '<h1 class="form-head"> فرم  ورود گانت ساز فارسی </h1>';
//			if(isset($_SESSION['token']))
//			{
//				$leftTry=5-$_SESSION['token'];
//				if($leftTry>0 && $leftTry<3)echo "
//				<p class='text-center' style='color:red; font-size:16px;'></p>
//				<div class='alert'>
//				  <button type='button' class='close' data-dismiss='alert'>×</button>
//				  <strong>هشدار!</strong> $leftTry تلاش دیگر تا نمایش کد امنیتی
//				</div>
//				";
//			}
			if(isset($errMessageForm))
            {
                alert($errMessageForm);//<strong>هشدار!</strong>
            }
			$formHelper->input('1ایمیل',
                               "1مثال  {name@mail.com}",'email','',@$params[1],'text','input-block-level','');
			$formHelper->input('1رمز عبور',
                               "1مثال {••••••••••••}",'pass','','','password','input-block-level','');
            echo "
                    <input id='remember-me' type='checkbox' name='remember' style='float:right;'><label for='remember-me' class='remember'> مرا به خاطر بسپار </label>
                    </br>
            ";
			$md=md5(rand(1,99999999).time());
			$_SESSION['tokenSignIn1']=$md;
			$formHelper->input('','','token','',$_SESSION['tokenSignIn1'],'hidden','','','',1);
			if(isset($_SESSION['token'])&&$_SESSION['token']>=5)
			{
				include_once DIR."captcha/simple-php-captcha-master/simple-php-captcha.php";
				$_SESSION['captcha1'] = simple_php_captcha(array(
															    'min_font_size' => 25,
															    'max_font_size' => 25,
															    'color' => '#666',
															    'angle_min' => 0,
															    'angle_max' => 4));
//                echo $_SESSION['captcha1']['code'];
				echo "<img width='90px;' height='40px;' src='".$_SESSION['captcha1']['image_src']."' style='fload:right;'>";
				$formHelper->input('کد تصویر',"1کد تصویر بالا را وارد کنید",'captcha1','',$_SESSION['captcha1']['code'],'text','input-captcha','','',1);

			}
		    $formHelper->submit('signin-gantt', "ورود به گانت ساز",'btn center w100','',"",0);

			echo "
            <div class='footer-signin'><a class='forget-pass' href='".URL."signin/forget-password' >رمز عبورم یادم نیست! :|</a></form>";
			echo "<a class='signup-lnk' href='".URL."signup' >ثبت نام</a></div></form>";
		}
	}
	?>
</div>

<?php
include_once 'footer.php';
}

?>
