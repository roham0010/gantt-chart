<script src="<?php echo URL; ?>js/bootstrap-transition.js"></script>
    <script src="<?php echo URL; ?>js/bootstrap-alert.js"></script>
    <script src="<?php echo URL; ?>js/bootstrap-modal.js"></script>
    <script src="<?php echo URL; ?>js/bootstrap-dropdown.js"></script>
    <script src="<?php echo URL; ?>js/bootstrap-scrollspy.js"></script>
    <script src="<?php echo URL; ?>js/bootstrap-tab.js"></script>
    <script src="<?php echo URL; ?>js/bootstrap-tooltip.js"></script>
    <script src="<?php echo URL; ?>js/bootstrap-popover.js"></script>
    <script src="<?php echo URL; ?>js/bootstrap-button.js"></script>
    <script src="<?php echo URL; ?>js/bootstrap-collapse.js"></script>
    <script src="<?php echo URL; ?>js/bootstrap-carousel.js"></script>
    <script src="<?php echo URL; ?>js/bootstrap-typeahead.js"></script>
    <script src="<?php echo URL; ?>js/bootstrap-tour.js"></script>
    <script src="<?php echo URL; ?>js/home.js"></script>
    <script src="<?php echo URL; ?>js/formValidation.js"></script>
    <script src="<?php echo URL; ?>js/jquery-ui.js"></script>
     <script>
      !function ($) {
        $(function(){
          // carousel demo
          $('#myCarousel').carousel()
        })
      }(window.jQuery)
    </script>
    <script src="<?php echo URL; ?>js/holder.js"></script>
  </body>
</html>
