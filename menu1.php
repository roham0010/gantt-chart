<?php
$activeMenu=array('home'=>'','help'=>'','pricing'=>'','ganttmanager'=>'','pricing'=>'','pricing'=>'');
$params[0]=(!isset($params[0]) || empty($params[0]))?'home':$params[0];
$activeMenu[@$params[0]]='active';
?>
<div class="navbar-wrapper">
		<!-- Wrap the .navbar in .container to center it within the absolutely positioned parent. -->
		<div class="">

			<div class="navbar navbar-inverse">
				<div class="navbar-inner">
					<!-- Responsive Navbar Part 1: Button for triggering responsive navbar (not covered in tutorial). Include responsive CSS to utilize. -->
					<button type="button" class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<a class="brand" href="<?=URL?>"><?=SITENAME?></a>
                    <span id='loading'><svg  id='loading1' xmlns="http://www.w3.org/2000/svg" viewBox="0 0 50 32" width="50" height="32" fill="white" style='width:40px;height:49px;margin-right:-20px;'>
                      <path transform="translate(2)" d="M 0 12 V 20 H 4 V 12 Z"> 
                        <animate attributeName="d" values="M0 12 V20 H4 V12z; M0 4 V28 H4 V4z; M0 12 V20 H4 V12z; M0 12 V20 H4 V12z" dur="1.2s" repeatCount="indefinite" begin="0" keytimes="0;.2;.5;1" keySplines="0.2 0.2 0.4 0.8;0.2 0.6 0.4 0.8;0.2 0.8 0.4 0.8" calcMode="spline"/>
                      </path>
                      <path transform="translate(8)" d="M 0 12 V 20 H 4 V 12 Z">
                        <animate attributeName="d" values="M0 12 V20 H4 V12z; M0 4 V28 H4 V4z; M0 12 V20 H4 V12z; M0 12 V20 H4 V12z" dur="1.2s" repeatCount="indefinite" begin="0.15" keytimes="0;.2;.5;1" keySplines="0.2 0.2 0.4 0.8;0.2 0.6 0.4 0.8;0.2 0.8 0.4 0.8" calcMode="spline"/>
                      </path>
                      <path transform="translate(14)" d="M 0 10.3705 V 21.6295 H 4 V 10.3705 Z">
                        <animate attributeName="d" values="M0 12 V20 H4 V12z; M0 4 V28 H4 V4z; M0 12 V20 H4 V12z; M0 12 V20 H4 V12z" dur="1.2s" repeatCount="indefinite" begin="0.30" keytimes="0;.2;.5;1" keySplines="0.2 0.2 0.4 0.8;0.2 0.6 0.4 0.8;0.2 0.8 0.4 0.8" calcMode="spline"/>
                      </path>
                      <path transform="translate(20)" d="M 0 4.82537 V 27.1746 H 4 V 4.82537 Z">
                        <animate attributeName="d" values="M0 12 V20 H4 V12z; M0 4 V28 H4 V4z; M0 12 V20 H4 V12z; M0 12 V20 H4 V12z" dur="1.2s" repeatCount="indefinite" begin="0.45" keytimes="0;.2;.5;1" keySplines="0.2 0.2 0.4 0.8;0.2 0.6 0.4 0.8;0.2 0.8 0.4 0.8" calcMode="spline"/>
                      </path>
                      <path transform="translate(26)" d="M 0 6.29171 V 25.7083 H 4 V 6.29171 Z">
                        <animate attributeName="d" values="M0 12 V20 H4 V12z; M0 4 V28 H4 V4z; M0 12 V20 H4 V12z; M0 12 V20 H4 V12z" dur="1.2s" repeatCount="indefinite" begin="0.60" keytimes="0;.2;.5;1" keySplines="0.2 0.2 0.4 0.8;0.2 0.6 0.4 0.8;0.2 0.8 0.4 0.8" calcMode="spline"/>
                      </path>
                      <path transform="translate(32)" d="M 0 0 V 20 H 4 V 0 Z">
                    <animate attributeName="d" values="M0 12 V20 H4 V12z; M0 4 V28 H4 V4z; M0 12 V20 H4 V12z; M0 12 V20 H4 V12z" dur="1.2s" repeatCount="indefinite" begin="0.75" keytimes="0;.2;.5;1" keySplines="0.2 0.2 0.4 0.8;0.2 0.6 0.4 0.8;0.2 0.8 0.4 0.8" calcMode="spline"/>
                  </path>
                  <path transform="translate(38)" d="M 0 12 V 20 H 4 V 12 Z">
                    <animate attributeName="d" values="M0 12 V20 H4 V12z; M0 4 V28 H4 V4z; M0 12 V20 H4 V12z; M0 12 V20 H4 V12z" dur="1.2s" repeatCount="indefinite" begin="0.90" keytimes="0;.2;.5;1" keySplines="0.2 0.2 0.4 0.8;0.2 0.6 0.4 0.8;0.2 0.8 0.4 0.8" calcMode="spline"/>
                  </path>
                    </svg></span>
                <span id='logo'>
                <svg  id='logo1' xmlns="http://www.w3.org/2000/svg" viewBox="0 0 50 32" width="50" height="32" fill="white" style='width:40px;height:49px;margin-right:-20px;'>
                              <path transform="translate(2)" d="M 0 14 V 20 H 4 V 14 Z">
                                <!-- <animate attributeName="d" values="M0 12 V20 H4 V12z; M0 4 V28 H4 V4z; M0 12 V20 H4 V12z; M0 12 V20 H4 V12z" dur="1.2s" repeatCount="indefinite" begin="0.2" keytimes="0;.2;.5;1" keySplines="0.2 0.2 0.4 0.8;0.2 0.6 0.4 0.8;0.2 0.8 0.4 0.8" calcMode="spline"/> -->
                              </path>
                              <path transform="translate(8)" d="M 0 12 V 20 H 4 V 12 Z">
                                <!-- <animate attributeName="d" values="M0 12 V20 H4 V12z; M0 4 V28 H4 V4z; M0 12 V20 H4 V12z; M0 12 V20 H4 V12z" dur="1.2s" repeatCount="indefinite" begin="0.2" keytimes="0;.2;.5;1" keySplines="0.2 0.2 0.4 0.8;0.2 0.6 0.4 0.8;0.2 0.8 0.4 0.8" calcMode="spline"/> -->
                              </path>
                              <path transform="translate(32)" d="M 0 0 V 20 H 4 V 0 Z">
                                <!-- <animate attributeName="d" values="M0 12 V20 H4 V12z; M0 4 V28 H4 V4z; M0 12 V20 H4 V12z; M0 12 V20 H4 V12z" dur="1.2s" repeatCount="indefinite" begin="0.2" keytimes="0;.2;.5;1" keySplines="0.2 0.2 0.4 0.8;0.2 0.6 0.4 0.8;0.2 0.8 0.4 0.8" calcMode="spline"/> -->
                              </path>
                              <path transform="translate(38)" d="M 0 12 V 20 H 4 V 12 Z">
                                <!-- <animate attributeName="d" values="M0 12 V20 H4 V12z; M0 4 V28 H4 V4z; M0 12 V20 H4 V12z; M0 12 V20 H4 V12z" dur="1.2s" repeatCount="indefinite" begin="0.2" keytimes="0;.2;.5;1" keySplines="0.2 0.2 0.4 0.8;0.2 0.6 0.4 0.8;0.2 0.8 0.4 0.8" calcMode="spline"/> -->
                              </path>
                              <path transform="translate(14)" d="M 0 10 V 20 H 4 V 10 Z">
                                <!-- <animate attributeName="d" values="M0 12 V20 H4 V12z; M0 4 V28 H4 V4z; M0 12 V20 H4 V12z; M0 12 V20 H4 V12z" dur="1.2s" repeatCount="indefinite" begin="0.2" keytimes="0;.2;.5;1" keySplines="0.2 0.2 0.4 0.8;0.2 0.6 0.4 0.8;0.2 0.8 0.4 0.8" calcMode="spline"/> -->
                              </path>
                              <path transform="translate(20)" d="M 0 7 V 20 H 4 V 7 Z">
                                <!-- <animate attributeName="d" values="M0 12 V20 H4 V12z; M0 4 V28 H4 V4z; M0 12 V20 H4 V12z; M0 12 V20 H4 V12z" dur="1.2s" repeatCount="indefinite" begin="0.2" keytimes="0;.2;.5;1" keySplines="0.2 0.2 0.4 0.8;0.2 0.6 0.4 0.8;0.2 0.8 0.4 0.8" calcMode="spline"/> -->
                              </path>
                              <path transform="translate(26)" d="M 0 4 V 20 H 4 V 4 Z"> 
                                <!-- <animate attributeName="d" values="M0 12 V20 H4 V12z; M0 4 V28 H4 V4z; M0 12 V20 H4 V12z; M0 12 V20 H4 V12z" dur="1.2s" repeatCount="indefinite" begin="0" keytimes="0;.2;.5;1" keySplines="0.2 0.2 0.4 0.8;0.2 0.6 0.4 0.8;0.2 0.8 0.4 0.8" calcMode="spline"/> -->
                              </path>
                            </svg>
                </span>
					<!-- Responsive Navbar Part 2: Place all navbar contents you want collapsed withing .navbar-collapse.collapse. -->
					<div class="nav-collapse collapse">
						<ul class="nav">
<!--
							<li>
								<a href="#about" id="about">درباره ما</a>
							</li>
-->
                            <li class="<?=$activeMenu['home']?>">
								<a href="<?=URL?>" id="contact">خانه</a>
							</li>
							<li class="<?=$activeMenu['pricing']?>">
								<a href="<?=URL?>pricing">قیمت ها</a>
							</li>
<!--
							<li class="<?=$activeMenu['help']?>">
								<a href="#">راهنما</a>
							</li>
-->
                            <?php
                            if (!userCheck())
                            {
                                ?>
							<li class="<?=$activeMenu['signup']?>">
								<a href="<?=URL?>signin" id="contact">ورود</a>
							</li>
							<li class="<?=$activeMenu['signup']?>">
								<a href="<?=URL?>signup" id="contact">عضویت</a>
							</li>
                            <?php
                            }else
                            {
                                ?>
                                <li><img class="profile-pic" src="<?=URL."profiles/".$_SESSION['userId'].'.jpg'?>"></li>
                                <li class="dropdown pull-right">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><?=$_SESSION['userName']?> <b class="caret"></b></a>
                                    <ul class="dropdown-menu">
                                        <li><a href="<?=URL?>profile">ویرایش پروفایل</a></li>
                                        <li><a href="<?=URL?>repass">ویرایش رمز عبور</a></li>
                                        <li class="divider"></li>
                                        <li><a href="<?=URL?>exit">خروج</a></li>
                                    </ul>
                                </li>
                                <li  class="<?=$activeMenu['ganttmanager']?>">
                                    <a class="" href="<?=URL?>ganttmanager">مدیریت </a>
                                </li>
                                <?php
                            }
                                ?>
							<!-- Read about Bootstrap dropdowns at http://twitter.github.com/bootstrap/javascript.html#dropdowns -->
<!--
							<li class="dropdown">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown">افتادنی <b class="caret"></b></a>
								<ul class="dropdown-menu">
									<li>
										<a href="#">عمل</a>
									</li>
									<li>
										<a href="#">عملی دیگر</a>
									</li>
									<li>
										<a href="#">یک چیز دیگر</a>
									</li>
									<li class="divider"></li>
									<li class="nav-header">
										عنوان ناوبری
									</li>
									<li>
										<a href="#">پیوند جداشده</a>
									</li>
									<li>
										<a href="#">یک پیوند جداشده دیگر</a>
									</li>
								</ul>
							</li>
						
-->
                        </ul>
					</div><!--/.nav-collapse -->
<!--
					<?php
                    if (!userCheck())
                        echo '<a class="btn btn-primary brand" id="btn-signin1" href="'.URL.'signin" role="button" data-toggle="modal" style="float: left;margin-top: 8px; padding:5px 39px 16px; height: 10px;">ورود - عضویت</a>';
                    else {
                        echo '<a class="btn btn-primary brand" href="ganttmanager" style="float: left;margin-top: 8px; padding:5px 39px 16px; height: 10px;">ورود به مدیریت </a>';

                    }
					?>
-->
				</div><!-- /.navbar-inner -->
			</div><!-- /.navbar -->

		</div>
		<!-- /.container -->
	</div>
