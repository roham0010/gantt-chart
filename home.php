<?php

$description = "نمودار گانت به نمودار زمانی انجام کارهای پروژه گفته میشود. این نمودار را به صورت نرم افزارهای تحت وب و همچنین تحت ویندوز طراحی کرد و در این سایت اولین گانت ساز آنلاین که با تاریخ فارسی کار میکند را میتوانید مشاهده کیند.";
$title = "ساخت نمودار گانت | رسم گانت آنلاین| گانت | Gantt Diagram | گانت با تاریخ فارسی | دیاگرام گانت با تاریخ هجری شمسی";
{
$style = "
<style>
    /* GLOBAL STYLES
    -------------------------------------------------- */
    /* Padding below the footer and lighter body text */

    body {
      padding-bottom: 40px;
      color: #5a5a5a;
    }
    /* CUSTOMIZE THE NAVBAR
    -------------------------------------------------- */

    /* Special class on .container surrounding .navbar, used for positioning it into place. */
    

    /* CUSTOMIZE THE CAROUSEL
    -------------------------------------------------- */



    /* MARKETING CONTENT
    -------------------------------------------------- */

    /* Center align the text within the three columns below the carousel */
    .marketing .span4 {
      text-align: center;
    }
    .marketing h2 {
      font-weight: normal;
    }
    .marketing .span4 p {
      margin-left: 10px;
      margin-right: 10px;
    }


    /* Featurettes
    ------------------------- */

    

    /* Thin out the marketing headings */
    /* RESPONSIVE CSS
    -------------------------------------------------- */

    @media (max-width: 979px) {

      .container.navbar-wrapper {
        margin-bottom: 0;
        width: auto;
      }
      .navbar-inner {
        border-radius: 0;
        margin: -20px 0;
      }

      .carousel .item {
        height: 500px;
      }
      .carousel img {
        width: auto;
        height: 500px;
      }

      .featurette {
        height: auto;
        padding: 0;
      }
      .featurette-image.pull-left,
      .featurette-image.pull-right {
        display: block;
        float: none;
        max-width: 40%;
        margin: 0 auto 20px;
      }
    }


    @media (max-width: 767px) {

      .navbar-inner {
        margin: -20px;
      }

      .carousel {
        margin-left: -20px;
        margin-right: -20px;
      }
      .carousel .container {

      }
      .carousel .item {
        height: 300px;
      }
      .carousel img {
        height: 300px;
      }
      .carousel-caption {
        width: 65%;
        padding: 0 70px;
        margin-top: 100px;
      }
      .carousel-caption h1 {
        font-size: 30px;
      }

      .marketing .span4 + .span4 {
        margin-top: 40px;
      }

      .featurette-heading {
        font-size: 30px;
      }
    }
    </style>
    ";
}
include_once 'header.php';
//print_r($_GET);
global $errSignup;
$errSignup=0;
if (isset($_POST['signup-gantt'])) {
    include_once DIR."classes/formValidation.php";
    $validation=new formValidation();
    $inputValid = array(
        'email' => 'ایمیل|r|@|uniq=tusers'
    );
    global $validationMsg;
    $validationMsg=$validation->validation($inputValid);
    if(count($validationMsg)==0 || $validationMsg=='')
    {
        $email = $_POST['email'];
        $user = $sqlOPR -> select('users', 'id', "email=$email");
        if (count($user) == 0 || empty($user[0]['email']))
        {
            include_once 'classes/hashMaker.php';
            $hashMaker = new hashMaker();
            $emailHash = $hashMaker -> hash($email);
            $inserts = "email=$email;hash=$emailHash";
//            echo $inserts;
            if(count($user) == 0)
                $id = $sqlOPR -> insert1('users',array('email','hash'),array($email,$emailHash));
            else if(empty($user[0]['email']))
            {
                $id=$user[0]['id'];
                $sqlOPR -> update('update', $inserts,"id=$id");
            }
//            ec($id);
            if(@$id!='')
            {
//                echo "email sended check your email";
                $emailHash2 = md5($emailHash);
                $link="<a href='" . URL . "signup/".$email."/" . $emailHash . "/$emailHash2'>لینک ثبت نام تکمیلی</a>";
                $msg="با سلام خدمت شما کاربر گرامی...<br>این ایمیل نسبت به درخواست شما برای ثبت نام در وب سایت ".SITENAME." ارسال گردیده است لطفا بر روی لینک زیر جهت تکمیل مراحل ثبت نام کلیک نمایید.<br>
                    $link
                    <br>
                    با تشکر...";
                $array=array('to'=>$email,'subject'=>'ثبت نام در '.SITENAME.'','text'=>$msg);
                if (Mailgun($array))
                {
                    $validationMsg['email']='لینک ثبت نام برای شما ارسال شد ایمیلتان را چک کنید!';
                    $emailSend=1;
                    $errSignup=1;
                }
                else
                {
                    $validationMsg['email']='ایمیل برای شما ارسال نشد لطفا دوباره تلاش کنید!';
                    $errSignup=1;
                    $sqlOPR->delete('tusers',"id=$id");
                }
            }
            else
            {
                $validationMsg['email']='لطفا دوباره بعدا تلاش کنید!';
                $errSignup=1;
                $sqlOPR->delete('tusers',"id=$id");
            }
        }
        else if (count($user) > 0)
        {
            $email = $_POST['email'];
            $emailSite = explode('@', $email);
            $ln = count($emailSite) - 1;
            $errMessageForm = "
            ایمیل قبلا استفاده شده برای استفاده از امکانات به
            <a href='http://www.$emailSite[$ln]' class='label label-info' target='_blank'>
             ایمیل خود</a>
              مراحعه کنید";
            $emailSend = 0;
            echo $errMessageForm;
            $validationMsg['email']='این ایمیل قبلا استفاده شده است!';
            $errSignup=1;
        }
    }
    else
    {
        $validationMsg['email']='درستی ایمیل را بررسی نمایید!';
        $errSignup=1;
    }

}

if (isset($_POST['signin-gantt']) || isset($_POST['emaili'])) {
    $email = $_POST['emaili'];
    $pass = $_POST['passi'];

    if ($email == '' || $pass == '') {
        $string = "location:" . URL . "signin/empty-login";

    } else {
        $sqlOPR = new sqlOPR();
        $user = $sqlOPR -> select('users', '', "email=$email,pass=$pass");
        if (count($user) > 0) {
            include DIR . "/subfiles/sessionSet.php";

        } else {
            $string = "location:" . URL . "signin/wrong-login";

        }
    }
    header(@$string);
}
if(@$params[0]=='unset')
{
    session_destroy();
}

?>
<script>
    $(".passwd, input[name='emaili']").keypress(function(e) {
        if (e.which == 13) {
            e.preventDefault();

            form = $(this).closest('form');
            form.submit();
        }
    });
</script>

<body>
	<!-- NAVBAR
	================================================== -->
	<?php include_once DIR."menu1.php";?><!-- /.navbar-wrapper -->

	<!-- Modal -->
	<div id="signupGanttModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
				×
			</button>
			<h3 id="myModalLabel">تیتر مدال</h3>
		</div>
		<div class="modal-body">
			<p>
				<h3>فرم ثبت نام نمودار گانت فارسی</h3>
			</p>

			<?php

            include_once DIR . 'classes/formHelper.php';
            $formHelper = new formHelper();
            $caption = '';
            $name = 'signup-Gantt';
            $action = "ثبت-نام-gantt-گانت";
            $method = 'post';
            $formHelper -> form($caption, $name, $action, $method, 'email|r| | |@|users|onkeyup');
            echo "<span id='errMes'>" . @$errMessageForm . "</span>";
            $formHelper -> input('ایمیل', "1ایمیل خود را وارد کنید", 'email', '', '', 'text', 'email1', 'dir="ltr" autocomplete="off" onkeydown="if (event.keyCode == 13)return false;"');
			?>
			<!--
			<form method="post" action="?ثبت نام gantt گانت" name="signup-gantt">
			<input name="email" type="text" placeholder="ایمیل خود را وارد کنید" autocomplete=""/>
			<input class="btn" style="" margin-top: -10px;" type="submit" name="signup-gantt" value="ثبت نام رایگان گانت ساز" />
			</form>-->
		</div>
		<div class="modal-footer">
			<button class="btn" data-dismiss="modal" aria-hidden="true">
				بستن
			</button>
			<?php $formHelper -> submit('signup-gantt', "ثبت نام رایگان گانت ساز", 'btn', '', ""); ?>
		</div>
	</div>
	<!-- Modal -->
	<div id="signinGanttModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
				×
			</button>
			<h3 id="myModalLabel">ورود به گانت ساز</h3>
		</div>
		<div class="modal-body">
			<p>
				<h3>فرم  ورور نمودار گانت فارسی</h3>
			</p>

			<?php
            include_once DIR . 'classes/formHelper.php';
            $formHelper = new formHelper();
            $caption = '';
            $name = 'signin-Gantt';
            $action = "signin";
            $method = 'post';
            $formHelper -> form($caption, $name, $action, $method);
            echo "<span id='errMes'>" . @$errMessageForm . "</span>";
            $formHelper -> input('ایمیل', "1ایمیل خود را وارد کنید", 'email', '', '', 'text', 'email1', '');
            $formHelper -> input('رمز عبور', "1رمز عبور خود را وارد کنید", 'pass', '', '', 'password', 'passwd', '');
            $md=md5(rand(1,99999999).time());
            echo $md;
			$_SESSION['tokenSignIn2']=$md;
			$formHelper->input('','','token','',$_SESSION['tokenSignIn2'],'hidden','','','',1);

			if(isset($_SESSION['token'])&&$_SESSION['token']>=5)
			{
				include_once DIR."captcha/simple-php-captcha-master/simple-php-captcha.php";
				$_SESSION['captcha12'] = simple_php_captcha(array(
															    'min_font_size' => 25,
															    'max_font_size' => 25,
															    'color' => '#666',
															    'angle_min' => 0,
															    'angle_max' => 4));
				echo "<img width='90px;' height='40px;' src='".$_SESSION['captcha12']['image_src']."'><br>";
				$formHelper->input('کد تصویر',"1کد تصویر بالا را وارد کنید",'captcha','','','text','input-block-level','autocomplete="off"','',1);
			}
			?>
			<!--
			<form method="post" action="?ثبت نام gantt گانت" name="signup-gantt">
			<input name="email" type="text" placeholder="ایمیل خود را وارد کنید" autocomplete=""/>
			<input class="btn" style="color: #0044CC; margin-top: -10px;" type="submit" name="signup-gantt" value="ثبت نام رایگان گانت ساز" />
			</form>-->
		</div>
		<div class="modal-footer">
			<button class="btn" data-dismiss="modal" aria-hidden="true">
				بستن
			</button>
			<?php $formHelper -> submit('signin-gantt', "ورود به گانت ساز", 'btn', '', ""); ?>
		</div>
	</div>
	<div id="emailGanttNotif" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
				×
			</button>
			<h3 id="myModalLabel">تیتر مدال</h3>
		</div>
		<div class="modal-body">
			<p>
				<h4>پیامی حاوی لینک فعالسازی برای کار با گانت ساز برای شما ارسال شد با استفاده از آن میتوانید از خدمات  گانت ساز آنلاین با تاریخ فارسی (هجری شمسی) به صورت رایگان استفاده کنید </h4>
				<?php
                $email = $_POST['email'];
                $emailSite = explode('@', $email);
                $ln = count($emailSite) - 1;
                echo "<a href='http://www.$emailSite[$ln]' target='_blank'>ورود به ایمیل</a>";
				?>
			</p>
		</div>
		<div class="modal-footer">
			<button class="btn" data-dismiss="modal" aria-hidden="true">
				بستن
			</button>
		</div>
	</div>

	<!-- Carousel
	================================================== -->
	<div id="myCarousel" class="carousel">
		<div class="carousel-inner" style="height:100vh;">
                <span class="darken"></span>
            <div class="item active">
                <video class="video-slider" autoplay loop poster="<?=URL?>img/slide-01.jpg" id="bgvid">
<!--                    <source src="polina.webm" type="video/webm">-->
                    <source src="<?=URL?>img/video2.mp4" type="video/mp4">
                </video>
<!--				<img src="<?php echo URL; ?>img/slide-01.jpg" alt="">-->
				<div class="container">
					<div class="carousel-caption slider-cap" style="">
						<h1 class="text-center slider-h1" style="">رسم نمودار حرفه ای گانت با <span style="color:#CD2122;"><?=SITENAME?></span></h1>
						<p class="lead text-center" >
                            حرفه ای کار کنید، حرفه ای مدیریت کنید
<!--							برای اولین بار در ایران نمودار گانت ساز فارسی را در این سایت میتوانید مشاهده کنید. دیگر نیاز به سم این دیاگرام با ابزارهای دشوار و تقویم میلادی ندارید به راحتی بکشید و استفاده کنید-->
<!--                            ابزاری برای مدیریت هرچه بهتر پروژه ها-->
                            <br>
                            
<!--						<a class="btn btn-large btn-primary slider-btn" style="" href="#signupGanttModal" role="button" data-toggle="modal">14 روز استفاده رایگان</a>-->
						<a class="btn btn-large btn-primary slider-btn" style="" href="<?=URL?>signup" role="button" data-toggle="modal">اکنون رایگان ثبت نام کنید</a>
						</p>

					</div>
				</div>
			</div>
<!--
			<div class="item">
				<img src="<?php echo URL; ?>img/slide-01.jpg" alt="">
				<div class="container">
					<div class="carousel-caption">
						<h1>رسم نمودار گانت با تاریخ فارسی</h1>
						<p class="lead">
							برای اولین بار در ایران نمودار گانت ساز فارسی را در این سایت میتوانید مشاهده کنید. دیگر نیاز به سم این دیاگرام با ابزارهای دشوار و تقویم میلادی ندارید به راحتی بکشید و استفاده کنید
						</p>
						<a class="btn btn-large btn-primary" href="#signupGanttModal" role="button" data-toggle="modal">14 روز استفاده رایگان</a>

					</div>
				</div>
			</div>
			<div class="item">
				<img src="<?php echo URL; ?>img/slide-02.jpg" alt="">
				<div class="container">
					<div class="carousel-caption">
						<h1>تیتر نمونهٔ دیگر</h1>
						<p class="lead">
							نرم‌افزار آزاد در مورد آزادی کاربران برای اجرا، کپی، توزیع، بررسی، تغییر و بهبود دادن نرم‌افزار می‌باشد.
						</p>
						<a class="btn btn-large btn-primary" href="#">بیشتر بدانید</a>
					</div>
				</div>
			</div>
			<div class="item">
				<img src="<?php echo URL; ?>img/slide-03.jpg" alt="">
				<div class="container">
					<div class="carousel-caption">
						<h1>نرم‌افزار آزاد</h1>
						<p class="lead">
							نرم‌افزار آزاد در مورد آزادی کاربران برای اجرا، کپی، توزیع، بررسی، تغییر و بهبود دادن نرم‌افزار می‌باشد.
						</p>
						<a class="btn btn-large btn-primary" href="#">مشاهده گالری</a>
					</div>
				</div>
			</div>
-->
		</div>
<!--
		<a class="right carousel-control" href="#myCarousel" data-slide="prev">&lsaquo;</a>
		<a class="left carousel-control" href="#myCarousel" data-slide="next">&rsaquo;</a>
-->
	</div><!-- /.carousel -->
<!--
    <div class="container magic">
        <div class="magic-text">
            با ثبت نام در<a class='signup1' href="<?=URL?>signup"> <?=SITENAME?> به راحتی </a>پروژه هایتان را با کیفیت و به راحتی مدیریت کنید!
        </div>
            <a class="btn btn-large btn-primary slider-btn" style="margin-top:100px;" href="<?=URL?>signup" role="button" data-toggle="modal">اکنون رایگان ثبت نام کنید</a>
    </div>
-->
    <?php
    function signupMaker($formHelper)
    {
        global $errSignup;
        global $validationMsg;
        if(!isset($_SESSION['userId']))
        {
            if(!isset($_POST['signup-gantt']) || $errSignup==1)
            {

                ?>
            <div class="container-fluide" style="height: 260px;">
                <div class="container well">
                    <div class="" style="position:relative;z-index:1;">
                        <p><h4  class="text-center" style="margin:25px;"> اکنون رایگان ثبت نام کنید</h4></p>

                            <p>
                            <?php
                            $caption = '';$name = 'signup-Gantt';$action = "";$method = 'post';
                            $formHelper -> form($caption, $name, $action, $method);
                            echo "<span id='errMes'>" . @$msgSignup . "</span>";
                            echo "<div class='' style='text-align: center;'>";
                                $formHelper -> input('ایمیل'
                                                 , "1ex: mail@dom.com", 'email', '', '', 'text', 'email-signup span5','dir="ltr"');
                            echo "<div class='text-center'>".@$validationMsg['email']."</div></div>";
                            echo "<div class='' style='text-align: center;'>";
                            $formHelper -> submit('signup-gantt',
                                                  "شروع!", 'btn home-signup', '', "");
                            echo "</div>";
                            echo "<div class='span5' style='margin-right:0px;'>";
                            echo "</div>";

                            ?>
        <!--
                            <form>
                                <input id="email-signup" name="email" type="text" >
                                <button class="btn home-signup"></button>
                            </form>
        -->
        <!--                    <a class="btn home-signup" href="#signupGanttModal" role="button" data-toggle="modal">14 روز استفاده رایگان</a>-->
                        </p>
                    </div>
                </div>
            </div>
                <?php
            }
        }
        else
            return '';
    }
    ?>
    <div class="container-fluide why">
        <div class="why-text">
            خوب حالا چرا مدیریت پروژه و چرا <?=SITENAME?>!؟
        </div>
        <div class="why-mini-text">
            به دلایل بسیار زیاد برای هر پروژه و مدیران پروژه، ولی مهمترین دلایل...
        </div>
    </div>
	<div class="container marketing">
		<div class="row" style='margin-bottom:50px;'>
			<div class="span4">
				<img class="" src="<?=URL?>img/project256.png" width="120" data-src="holder.js/150x150">
				<h2 class="title-futhers">پروژه تعریف کنید</h2>
				<p class='text-futhers'>
                    <?=SITENAME?> به راحتی برای شما امکان تعریف پروژه را از بخش پنل کاربری در بخش مدیریت پروژه در بخش <a href="<?=URL?>">ثبت نام در <?=SITENAME?></a> پروژه های متعدد تعریف کرده و آنها را با تعریف افراد و منابع مختلف مدیریت کنید.
				</p>
			</div><!-- /.span4 -->
			<div class="span4">
				<img class="" src="<?=URL?>img/people2561.png" width="120" data-src="holder.js/150x150">
				<h2 class="title-futhers">مدیریت افراد و منابع</h2>
				<p  class='text-futhers'>
                    میتوانید برای پروژه هایی که تعریف کرده اید به راحتی از دوستان و همکاران خود دعوت کنید و آنها را مدیریت کنید به آنها کار بسپارید و ...، همچنین میتوانید منابع تعریف کرده و همانند همکاران آنها را نیز مدیریت کنید.
                </p>
			</div><!-- /.span4 -->
			<div class="span4">
				<img class="" src="<?=URL?>img/access256-2.png" width="120" data-src="holder.js/150x150">
				<h2 class="title-futhers">سطوح دسترسی ایجاد کنید</h2>
				<p class='text-futhers'>
					پس از تعریف پروژه و دعوت از دوستان و منابع میتوانید به راحتی کارها را به منابع و افراد تخصیص داده و از عملکرد و پیشرفت آنها با ارائه فایل و کامنت به ازای هر کار با خبر شوید و پروژه ها را به اتمام برسانید.
				</p>
			</div><!-- /.span4 -->
		</div><!-- /.row -->
    </div>

		<!-- START THE FEATURETTES -->
    <div class="container-fluide f1" style=''>
        <div class="container featurette" style=''>
            <div class="container text-center feature-title">همه چیز را با کشیدن و رها کردن موس انجام دهید</div>
            <div class="featurette-image pull-right ">
                <img class="" src="<?php echo URL; ?>img/home/futhers6.png">
                <div class="img-caption muted">درگ گروه کار</div>
            </div>
            <div class="feature-text pull-left" style="height:350px;left: 80px;">
                <h2 class="featurette-heading">با درگ کردن گروه های کاری را مدیریت کنید<span class="muted"></span></h2>
                <p class="lead">
                    بخش های مختلف پروژه را به صورت گروه های کاری تعریف کنید، با
                    <?=SITENAME?>
                     میتوانید به راحتی گروه های کاریتان را با درگ کردن زمان ابتدا و انتهایشان را مشخص کنید، مرتب کنید، اولویت بندی کنید و کارهای مدیریتی دیگر.
                </p>
                <h2 class="featurette-heading">با درگ کردن کارهای زیر مجموعه هر گروه را مدیریت کنید<span class="muted"></span></h2>
                <p class="lead">
                    میتوانید برای گروه های کاری به راحتی کار تعریف کنید، <?=SITENAME?> 
                    مدیریت کارها در گروه ها برای شما به راحتی فراهم ساخته، به گونه ای که میتوانید با درگ کردن زمان شروع و پایان، درصد پیشرفت و... برای هر کار مشخص کنید.
                </p>
            </div>
        </div>
    </div>
    <div class="magic1">
        <div class="magic-text1 mt1">
            فقط به مدیریت فکر کنید و درگیر حاشیه ها نشوید
        </div>
    </div>

    <div class="container-fluide f1" style=''>
        <div class="container featurette" style=''>
            <div class="container text-center feature-title">به افراد پروژه کار بسپارید و نحوه و میزان کار آنها را مدیریت کنید</div>
            <div class="featurette-image pull-left ">
                <img class="" src="<?php echo URL; ?>img/home/futhers6.png">
                <div class="img-caption muted">تخصیص کارها به منابع و افراد</div>
            </div>
            <div class="feature-text" style="height:290px;">
                <h2 class="featurette-heading">کارها را به افراد و منابع تخصیص دهید<span class="muted"></span></h2>
                <p class="lead">
                     هر کار میتواند خصوصیات افراد و منابع مختص خود را داشته باشد، <?=SITENAME?>
                    به شما امکان تخصیص یک یا چند فرد، یا منابع مورد نیاز پروژه را میدهد.
                </p>
                <h2 class="featurette-heading">فایل و گفتگو برای هر کار به صورت مجزا داشته باشید<span class="muted"></span></h2>
                <p class="lead">
                    شما میتوانید برای هر کار فایل های مخصوص به همان کار را داشته باشید، یا در رابطه با هر کار در بخش کامنتهای مربوط به همان کار با همکاران به گفتگو بپردازید.
                </p>
            </div>
        </div>
    </div>

    <div class="magic1 m1">
        <div class="magic-text1">
            با <?=SITENAME?>، به راحتی پروژه هایتان را مدیریت کنید!
        </div>
        <div class="magic-text1">
            <a class="btn btn-large btn-primary slider-btn" style="" href="<?=URL?>signup" role="button" data-toggle="modal">اکنون رایگان ثبت نام کنید</a>
        </div>
    </div>
    <div class="container-fluide f1" style=''>
        <div class="container featurette" style=''>
            <div class="container text-center feature-title">راحت در پروژه جستجو کنید، کارهای روزانه از تمام پروژه ها را مشاهده کنید</div>
            <div class="featurette-image pull-left ">
                <img class="" src="<?php echo URL; ?>img/home/futhers6.png">
                <div class="img-caption muted">جستجو و نمایش کارهای روزانه</div>
            </div>
            <div class="feature-text pull-left" style="height:350px;">
                <h2 class="featurette-heading">بین کارها جستجو کنید<span class="muted"></span></h2>
                <p class="lead">
                    <?=SITENAME?> به شما امکان میدهد بین تمامی کارهای موجود در پروژه جستجو انجام دهید، جستجو با معیار های مختلف مانند اشخاص، درصد پیشرفت و همچنین تاریخ شروع و پایان کار میتواند انجام پذیرد.
                </p>
                <h2 class="featurette-heading">کارهای روزانه را مشاهده نمایید <span class="muted"></span></h2>
                <p class="lead">
                    هر شخص در پنل کاربری خود میتواند کارهای روزانه خود را مشاهده کند، لیست کارها متعلق به تمامی پروژه هاییست که کاربر در آنها مشارکت دارد و آن روز کاری در آن پروژه به او محول شده است.
                </p>
            </div>
        </div>
    </div>
    <div class="magic1">
        <div class="magic-text1 mt1">
            بدون اتلاف وقت و انرژی طبق نیاز بدون محدودیت مکانی و زمانی مدیریت کنید
        </div>
    </div>
    <div class="container-fluide f1" style=''>
            <div class="container featurette" style=''>
                <div class="container text-center feature-title">پروژه را در نمایی که دوست دارید یا طبق نیاز ببینید</div>
                <div class="featurette-image pull-right ">
                    <img class="" src="<?php echo URL; ?>img/home/futhers6.png">
                    <div class="img-caption muted">نماهای مختلف پروژه</div>
                </div>
                <div class="feature-text pull-left" style="height:350px;left: 80px;">
                    <h2 class="featurette-heading">چندین نما از پروژه ببینید و بسته به نیاز انتخاب کنید <span class="muted"></span></h2>
                    <p class="lead">
                         میتوانید از منو در بخش مدیریت پروژه، به چندین نما، پروژه را مشاهده کنید به صورت نموداری یا چارت و یا به صورت لیست کارها  میتوانید پروژه را مشاهده و مدیریت کنید.(در صورت استقبال برد کارها نیز به این منو اضافه خواهد شد)
                    </p>
                    <h2 class="featurette-heading">مدیریت یکپارچه در نماهای مختلف داشته باشید<span class="muted"></span></h2>
                    <p class="lead">
                        در نماهای مختلف فقط به مدیریت فکر کنید و از ابزار مورد علاقه خود استفاده کنید، <?=SITENAME?> مسائل را با مدیریت یکپارچه در نماهای مختلف برای شما آسان نموده است.
                    </p>
                </div>
            </div>
		</div>

    <?php signupMaker($formHelper); ?>

    <!-- /END THE FEATURETTES -->
    <?php
    include_once 'footer.php';
    ?>

    <script type="text/javascript">
        $('#loading').hide();
        $('#loading1').hide();
        postt="<?php echo @$emailSend; ?>
        ";

        if(postt!=""&&postt==1)
        {
        //$('#myModal').modal('toggle');
        $("#emailGanttNotif").modal();
        $("#emailGanttNotif").modal({ keyboard: false });
        $("#emailGanttNotif").modal('show') ;
        }
        else if(postt===0)
        {
        $("#signupGanttModal").modal();
        $("#signupGanttModal").modal({ keyboard: false });
        $("#signupGanttModal").modal('show') ;
        setTimeout(function(){ $('#errMes').hide(); }, 8000);
        }
        $('.email1').keydown(function(){
        $('#errMes').hide();
        });
    </script>
