<?php
$description="از این صفحه کاربران مشتاق به استفاده از نمودار گانت ساز فارسی یا Gantt Diagram Maker ورود کرده و رایگان از آن استفاده کنند.";
$title="ساخت نمودار گانت | رسم گانت آنلاین| گانت | Gantt Diagram | گانت با تاریخ فارسی | دیاگرام گانت با تاریخ هجری شمسی | ورود گانت";
{
$style="<style>
body
  {
    padding-top: 40px;
    padding-bottom: 40px;
    background-color: #f5f5f5;
  }

  .form-signin
  {
    max-width: 300px;
    padding: 19px 29px 29px;
    margin: 80px auto 20px;
    background-color: #fff;
    border: 1px solid #e5e5e5;
    -webkit-border-radius: 5px;
       -moz-border-radius: 5px;
            border-radius: 5px;
    -webkit-box-shadow: 0 1px 2px rgba(0,0,0,.05);
       -moz-box-shadow: 0 1px 2px rgba(0,0,0,.05);
            box-shadow: 0 1px 2px rgba(0,0,0,.05);
  }
  .form-signin .form-signin-heading,
  .form-signin .checkbox
  {
    margin-bottom: 10px;
  }
  .form-signin input[type='text'],
  .form-signin input[type='password']
  {
    font-size: 16px;
    height: auto;
    margin-bottom: 15px;
    padding: 7px 9px;
  }
</style>";
}
//session_start();
//echo $_SESSION['captcha1']['code'];
include_once 'header.php';
include_once DIR.'classes/formHelper.php';
include_once DIR.'classes/hashMaker.php';
echo '<div class="container">
<div class="form-signin">';
if(isset($_GET['params'])&&$_GET['params']!='')
{
	global $params;
	if(isset($params[1])&&$params[1]=='forget-password')
	{
        
        $errMsg=0;
        $formHelper=new formHelper();
        $caption='';
        $name='frm-forget-password-gantt';
        $action=URL.'signin/forget-password';
        $method='post';
        $formHelper->form($caption, $name, $action, $method,'','','form-signin',"");
			echo '<h3 class="form-signin-heading"> فرم بازیابی رمز عبور </h3>';
		if(isset($_POST['forget-password-gantt'])&&$_SESSION['tokenSignIn']==$_POST['token'])
		{
			$md=md5(rand(1,99999999).time());
			$_SESSION['tokenSignIn']=$md;
            $email=$_POST['email'];
            $array=array('to'=>$email,'subject'=>'ویرایش رمز عبور','text'=>"با سلام خدمت شما کاربر گرامی.<br> لینک فراموشی رمز عبور برای شما ارسال شده اگر شما درخواست آن را نداده اید میتوانید این ایمیل در نظر نگیرید!<br>
            پیوند ویرایش رمز عبور: $link");
            $md=rand(1,99999999);
            $hash=new hashMaker();
            $code=$hash->hash($md);
            if(!$sqlopr->update("users","forgetPassCode=$code,accountNumber=$md","email=$email"))
            {
                return 'user-not-exist';
            }
            else
            {
                $to=$email;
                $text="کاربر گرامی به جهت درخواست فراموشی رمز عبور شما این ایمیل حاوی رمز عبور ارسال شده است لطفا این عدد را در کادر مشخص شده در سایت وارد نمایید: $code";
                $subject=SITENAME."- ویرایش فراموشی رمز عبور";
                if(Mailgun(array('to'=>$email,'text'=>$text,'subject'=>$subject)))
                {
                    $string='location:'.URL."signin/enter-code/$email";
                    echo "$string";
            //        header($string);
                }
            }
            if(sendCode($email,$sqlOPR);)
                message("پیوندی برای شما برای ویرایش رمز عبور ارسال شده، لطفا به ایمیل خود مراجعه کنید و بر روی آن کلیک کنید!");
            else
            {
                alert("لطفا بعدا دوباره تلاش کنید!");
                $errMsg=1;
            }
		}
		elseif(isset($_POST['forget-password-gantt'])&&$_SESSION['tokenSignIn']!=$_POST['token'])
		{
			$string='location:'.URL."signin";
			echo "$string";
			header($string);
		}
		if(!isset($_POST['forget-password-gantt']) || $errMsg===1)
		{
			$formHelper->input('1ایمیل'
                               ,"1ایمیل خود را وارد کنید",'email','','','text','input-block-level','');

			$md=md5(rand(1,99999999).time());
			$_SESSION['tokenSignIn']=$md;
			$formHelper->input('','','token','',$_SESSION['tokenSignIn'],'hidden','','','',1);

		    $formHelper->submit('forget-password-gantt', "ارسال رمز عبور",'btn w100','',"",0);
            echo "<div class='footer-signin'><a class='signup-lnk' href='".URL."signin' >ورود به ".SITENAME."</a></div>";
	    }
        echo "</form>";
	}
	else
	{
        $signinError=0;
		$state=@$params[1];
		switch ($state) {
			case 'empty-login':
				$errMessageForm='نام کاربری و رمز عبور را وارد کنید';
				$signinError=1;
				break;
			case 'wrong-login':
				$errMessageForm='نام کاربری یا رمز عبور اشتباه است';
				$signinError=1;
				break;
		}
            
		if(isset($_POST['signin-gantt']) && ($_POST['token']==@$_SESSION['tokenSignIn1'] || $_POST['token']==@$_SESSION['tokenSignIn2']))
		{
			if(!isset($_SESSION['token']))
				$_SESSION['token']=1;
			else
				$_SESSION['token']++;

			$email=$_POST['email'];
			$pass=$_POST['pass'];
			if($email==''||$pass=='')
			{
				$errMessageForm='نام کاربری و رمز عبور را وارد کنید';
				$signinError=1;
			}
			elseif(isset($_POST['captcha1'])&&strtolower($_SESSION['captcha1']['code'])!=strtolower($_POST['captcha1']))
			{
				echo '<br>'.$_SESSION['captcha1']['code']."!=".$_POST['captcha1'];
				$errMessageForm='تصویر امنیتی را به درستی وارد کنید';
				$signinError=1;
			}
			else
			{
                if(isset($_POST['captcha1'])&&strtolower($_SESSION['captcha1']['code'])==strtolower($_POST['captcha1']))
                    $_SESSION['token']=1;
				$sqlOPR=new sqlOPR();
				$user=$sqlOPR->select('users','',"email=$email,pass=$pass");
//				$user1=$sqlOPR->select('users','',"email=$email,hash=$pass");
				if(count($user)>0)
				{
                    
                    $flagUserSigned=false;
                    $hash = new hashMaker();
                    $hashcookie=$hash->hash($user[0]['email'].$user[0]['name'].$user[0]['id'].time());
                    $sqlOPR->update('users',"hashCookie=$hashcookie","id=".$user[0]['id']);
                    $user[0]['hashCookie']=$hashcookie;

					include DIR . "/subfiles/sessionSet.php";
					$string="location:".URL."ganttmanager/true-login";
//					echo "sign innnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnn";
					header($string);
				}
//				elseif(count($user1)>0)
//				{
//				    $user=$user1;
//					include DIR . "/subfiles/sessionSet.php";
//					$sqlOPR->update('users', "pass=$pass,hash=", "email=$email");
//					$string="location:".URL."ganttmanager/true-login";
//                    echo "sign innnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnn222222";
//					//header($string);
//				}
				else
				{
					$errMessageForm='نام کاربری یا رمز عبور اشتباه است';
					$signinError=1;
				}
			}

		}

		/*elseif(isset($params[1]))
		{
			$signinError=1;
			$errMessageForm='خطایی رخ داده است دوباره تلاش کنید';
		}*/
//        echo '<br>'.$_SESSION['captcha1']['code'];
		if(!isset($_POST['signup-gantt']) || $signinError==1)
		{
			$formHelper=new formHelper();
			$caption='';
			$name='frm-Gantt-Maker-Persian';
			$action=URL.'signin';
			$method='post';
			$formHelper->form($caption, $name, $action, $method,'','','','border-radius:0px;');
			echo '<h1 class="form-head"> فرم  ورود گانت ساز فارسی </h1>';
//			if(isset($_SESSION['token']))
//			{
//				$leftTry=5-$_SESSION['token'];
//				if($leftTry>0 && $leftTry<3)echo "
//				<p class='text-center' style='color:red; font-size:16px;'></p>
//				<div class='alert'>
//				  <button type='button' class='close' data-dismiss='alert'>×</button>
//				  <strong>هشدار!</strong> $leftTry تلاش دیگر تا نمایش کد امنیتی
//				</div>
//				";
//			}
			if(isset($errMessageForm))
            {
                alert($errMessageForm);//<strong>هشدار!</strong>
            }
			$formHelper->input('1ایمیل',
                               "1مثال  {name@mail.com}",'email','',@$params[1],'text','input-block-level','');
			$formHelper->input('1رمز عبور',
                               "1مثال {••••••••••••}",'pass','','','password','input-block-level','');
            echo "
                    <input id='remember-me' type='checkbox' name='remember' style='float:right;'><label for='remember-me' class='remember'> مرا به خاطر بسپار </label>
                    </br>
            ";
			$md=md5(rand(1,99999999).time());
			$_SESSION['tokenSignIn1']=$md;
			$formHelper->input('','','token','',$_SESSION['tokenSignIn1'],'hidden','','','',1);
			if(isset($_SESSION['token'])&&$_SESSION['token']>=5)
			{
				include_once DIR."captcha/simple-php-captcha-master/simple-php-captcha.php";
				$_SESSION['captcha1'] = simple_php_captcha(array(
															    'min_font_size' => 25,
															    'max_font_size' => 25,
															    'color' => '#666',
															    'angle_min' => 0,
															    'angle_max' => 4));
//                echo $_SESSION['captcha1']['code'];
				echo "<img width='90px;' height='40px;' src='".$_SESSION['captcha1']['image_src']."' style='fload:right;'>";
				$formHelper->input('کد تصویر',"1کد تصویر بالا را وارد کنید",'captcha1','',$_SESSION['captcha1']['code'],'text','input-captcha','autocomplete="off"','',1);
                
			}
		    $formHelper->submit('signin-gantt', "ورود به گانت ساز",'btn center w100','',"",0);
            
			echo "
            <div class='footer-signin'><a class='forget-pass' href='".URL."signin/forget-password' >رمز عبورم یادم نیست! :|</a></form>";
			echo "<a class='signup-lnk' href='".URL."signup' >ثبت نام</a></div></form>";
		}
	}
	?>
</div>

<?php
include_once 'footer.php';
}

?>
