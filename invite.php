<?php
include_once 'jdf.php';

$description = "بخش دعوت از دوستان برای پروژه";
$title = "ساخت نمودار گانت | رسم گانت آنلاین| گانت | Gantt Diagram | گانت با تاریخ فارسی | دیاگرام گانت با تاریخ هجری شمسی";
$style = "
    <link href='" . URL . "css/invite.css' rel='stylesheet'>
    <link href='" . URL . "css/gantt.css' rel='stylesheet'>
<style>
</style>";
include_once 'header.php';
//print_r($_GET);

$project = $sqlOPR->select('projects', '', "REPLACE(name#*' '#*'-' )=$params[1]");
$projectId=$_SESSION['projectId']=$_GET['projectId'];
$_SESSION['ajaxRequest'] = 'schedulelogedin';

?>
<body onload="">

    <!-- NAVBAR onload="updateScrollbar(document.getElementById('content-schedule'),document.getElementById('scrollbar-schedule'));updateScrollbar(document.getElementById('tasks'),document.getElementById('scrollbar-task'));"
    ================================================== -->
    <?php
    include_once 'ganttHeader.php';
    ?>

    <div class="container marketing">
        <h3 style="position:relative;top:25px;" class="">افراد و منابع پروژه</h3>
        <hr class="featurette-divider" style="margin:15px 0px 0px;">
        <div class="row-fluid marketing" style="padding-top:15px;">
          <a href="<?php echo URL . "gantt/$params[1]/schedule"; ?>" class="btn" style="float: left;" name="done" type="submit" value="" />انجام شد</a>
          <div class="tabbable"> <!-- Only required for left/right tabs -->
            <ul class="nav nav-tabs">
              <li class="active"><a href="#tab1" data-toggle="tab">افراد پروژه</a></li>
              <li><a href="#tab2" data-toggle="tab">منابع پروژه</a></li>
            </ul>
            <div class="tab-content">
              <div class="tab-pane active" id="tab1">
                <p>
                  <h6>از دوستان و همکارانی که در این پروژه هستند دعوت کنید به این پروژه بپیوندند</h6>
                  <div class="people-content">
                    <?php
                    include_once DIR . 'classes/formHelper.php';
                    $formHelper = new formHelper;
                    $caption = '';
                    $name = 'new-people';
                    $action = "";
                    $method = 'post';
//                    $formHelper->form($caption, $name, $action, $method);
                    echo "<span id='errMes'>" . @$errMessageForm . "</span>";
                    $formHelper->input('1ایمیل'
                                       , "1نام یا ایمیل", 'nameOrEmail', '', '', 'text', 'input-insert w100', '');
//                    $formHelper->input('نام', "1نام کاربر را وارد کنید", 'name', '', '', 'text', 'input-insert w100', 'autocomplete="off"');
                    $formHelper->input('1متن'
                                       , "1متن برای دعوت کاربر وارد کنید", 'text', '', "سلام $_SESSION[userName] هستم."
                            . "" . PHP_EOL . "شما را به پروژه ".$project[0]['name']." در وب سایت " . SITENAME . " به همکاری دعوت میکنم.", 'textarea', 'input-insert w100', 'autocomplete="off" col="50"', 'height:60px !important; resize:none;');
                    $permitions=$sqlOPR->select('permitions','',"creatorId=$_SESSION[userId]");
                    $optionsPermition='';
                    foreach ($permitions as $permition)
                    {
                      $optionsPermition.="<option value='$permition[id]'>".$permition['name']."</option>";
                    }
                    echo "
                    <label for='permition'>سطح دسترسی</label>
                    <select name='permiton' id='permition'><option value='-1'>بدون محدودیت</option>";
                    echo $optionsPermition;
                    echo "</select><br>";
                    $formHelper->submit('btn-invite', "ارسال دعوت نامه", 'btn', '', "", '0', 'nameOrEmail|r| | |@| ');
                    ?>
                    <div class="popover popover-person fade bottom in" style="border-radius: 2px;">
                        <button type="button" class="close popover-close" style="padding: 2px 4px;float: left;" aria-hidden="true">
                            &times;
                        </button>

                        <div class="popover-content" style="width: auto; min-width:250px;max-width:1000;float:right; top:-4px;position:relative; padding: 0px 18px;">
                        </div>
                    </div>
                  </div>
                  <h4>لیست افراد پروژه</h4>
                  <?php
                  $projectUsers = $sqlOPR->selectJoin('projectuser',
                    'users.email,users.name as uname,users.id as uid,projectuser.id as pid,permitions.name as pername,permitions.id as perid',
                    'users,projects,permitions',
                    'projectuser.userid=users.id,projectuser.projectid=projects.id,projectuser.permitionId=permitions.id',
                    "projects.id=$projectId", '', '', 'projectuser.id desc');
                  if(isset($projectUsers['rows'])&&count($projectUsers['rows'])>0)
                        $projectUsers = $projectUsers['rows'];
                  ?>
                  <div class="row" style="margin-top: 30px;margin-right:0px;">
                  <?php
                  
                  ?>
                      <table class='table table-striped' dir="rtl">
                          <thead>
                              <tr>
                                  <th>نام</th>
                                  <th>سطح دسترسی</th>
                                  <th>عملیات</th>
                              </tr>
                          </thead>
                          <tbody id='bodyUser'>
                  <?php
                  if(count($projectUsers)<=1)
                  {
                      echo '<tr id="emptyTblUser">
                            <td colspan="3">
                            <strong> کاربری وجود ندارد!</strong>
                            </td>

                          </tr>';
                  }
                  foreach ($projectUsers as $project) {
                        $name=$project['uname']!=''?$project['uname']:$project['email'];
                    if($project['uid']!=$_SESSION['userId'])
                      echo "
                          <tr>
                            <td>
                              <a href='" . URL . "users/$project[uname]' class='thumbnail' alt='$project[uname]' title='$project[uname]'
                              style='width:auto; text-decoration:none; margin-right:15px; vertical-align: middle; float: right;'>
                                      $name
                                    </a>

                            </td>
                            <td>
                               <select name='permition' class='user-permition'>
                                <option value='$project[perid]'>$project[pername]</option>
                                $optionsPermition
                            </td>
                            <td> <i class='remove-user ico ico-delete' title='حذف' sid='$project[uid]'></i>
                                <i class='invite-user-again ico ico-invite' title='دعوت دوباره' sid='$project[pid]' email='$project[email]'></i>
                            </td>
                          </tr>
                        ";
                  }
                  ?>
                        </tbody>

                        </table>
                    </div>
            </p>
              </div>
              <div class="tab-pane" id="tab2">
                <p>
                  <h6>منابع مورد نیاز پروژه را میتوانید وارد و مدیریت نمایید</h6>
                  <div class="people-content">
                      <?php
                      include_once DIR . 'classes/formHelper.php';
                      $formHelper = new formHelper;
                      $caption = '';
                      $name = 'new-people';
                      $action = "";
                      $method = 'post';
                      //$formHelper->form($caption, $name, $action, $method);
                      echo "<span id='errMes'>" . @$errMessageForm . "</span>";
                      $formHelper->input('1نام منبع'
                                         , "1نام منبع را وارد کنید", 'rname', '', '', 'text', 'input-insert w100', 'autocomplete="off"');
                      $formHelper->submit('btn-resource', "اضافه کردن به منابع", 'btn', '', "", '0', '');
                      ?>
                      <div class="popover popover-person fade bottom in" style="border-radius: 2px;   padding: 19px 10px;">
                          <button type="button" class="close popover-close" style="padding: 2px 4px;float: left;" aria-hidden="true">
                              &times;
                          </button>

                          <div class="popover-content" style="width: auto; min-width:250px;max-width:1000;float:right; top:-4px;position:relative; padding: 0px!important;">
                          </div>
                      </div>
                  </div>
                   <h4>لیست منابع پروژه</h4>
                      <div class="people-content" id="resources">
                        <?php

                        //SELECT * FROM `projectuser`,users,projects WHERE projects.id=21 and projects.id=projectUser.projectid and users.id=projectuser.userid
                        $projectResources = $sqlOPR->select('projectresource','' , "projectId=$projectId", '', 'id desc');

                        ?>
                            <table class='table table-striped' dir="rtl">
                                <thead>
                                    <tr>
                                        <th>نام</th>
                                        <th width="30">عملیات</th>
                                    </tr>
                                </thead>
                                <tbody id='bodyResource'>
                        <?php
                        if(count($projectResources)<=0)
                        {
                             echo '<tr id="emptyTblRes">
                                  <td colspan="3">
                                  <strong> منبعی وجود ندارد!</strong>
                                  </td>

                                </tr>';
                        }
                        foreach ($projectResources as $projectResource) {

                            echo " <tr>
                                    <td>
                                      <span>$projectResource[name]</a>
                                    </td>
                                    <td>
                                        <i class='remove-resource ico ico-delete' title='حذف' sid='$projectResource[id]'></i>
                                    </td>
                                  </tr>";
                                  /*
                            <div class='people-row'>
                                        <div class='people-name' title='full name of user which is bigger'>$projectResource[name]</div>
                                        <i class='remove-resource icon-remove' title='حذف' sid='$projectResource[id]'></i>
                                       </div>*/
                        }
                        ?>
                          </tbody>
                         </table>
                      </div>
              </div>
                </p>
              </div>
            </div>
          </div>



<?php
//print_r($_POST);

if (isset($_POST['done'])) {
    $string = "Refresh: 3;url=" . URL . "gantt/$params[1]/schedule";
    header($string);
}
?>
<script src=<?php echo URL; ?>js/invite.js></script>
<script src=<?php echo URL; ?>js/inviteFuncs.js></script>
<script src=<?php echo URL; ?>js/default.js></script>

        <script>
            ajaxRequest = '<?php echo $_SESSION['ajaxRequest']; ?>';
            projectId = '<?php echo $projectId; ?>';
        </script>
