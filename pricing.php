<?php

$description = "نمودار گانت به نمودار زمانی انجام کارهای پروژه گفته میشود. این نمودار را به صورت نرم افزارهای تحت وب و همچنین تحت ویندوز طراحی کرد و در این سایت اولین گانت ساز آنلاین که با تاریخ فارسی کار میکند را میتوانید مشاهده کیند.";
$title = "ساخت نمودار گانت | رسم گانت آنلاین| گانت | Gantt Diagram | گانت با تاریخ فارسی | دیاگرام گانت با تاریخ هجری شمسی";
{
$style = "
<style>
    /* GLOBAL STYLES
    -------------------------------------------------- */
    /* Padding below the footer and lighter body text */

    body {
      padding-bottom: 40px;
      color: #5a5a5a;
    }
    /* CUSTOMIZE THE NAVBAR
    -------------------------------------------------- */

    /* Special class on .container surrounding .navbar, used for positioning it into place. */
    

    /* CUSTOMIZE THE CAROUSEL
    -------------------------------------------------- */



    /* MARKETING CONTENT
    -------------------------------------------------- */

    /* Center align the text within the three columns below the carousel */
    .marketing .span4 {
      text-align: center;
    }
    .marketing h2 {
      font-weight: normal;
    }
    .marketing .span4 p {
      margin-left: 10px;
      margin-right: 10px;
    }


    /* Featurettes
    ------------------------- */

    

    /* Thin out the marketing headings */
    /* RESPONSIVE CSS
    -------------------------------------------------- */

    @media (max-width: 979px) {

      .container.navbar-wrapper {
        margin-bottom: 0;
        width: auto;
      }
      .navbar-inner {
        border-radius: 0;
        margin: -20px 0;
      }

      .carousel .item {
        height: 500px;
      }
      .carousel img {
        width: auto;
        height: 500px;
      }

      .featurette {
        height: auto;
        padding: 0;
      }
      .featurette-image.pull-left,
      .featurette-image.pull-right {
        display: block;
        float: none;
        max-width: 40%;
        margin: 0 auto 20px;
      }
    }


    @media (max-width: 767px) {

      .navbar-inner {
        margin: -20px;
      }

      .carousel {
        margin-left: -20px;
        margin-right: -20px;
      }
      .carousel .container {

      }
      .carousel .item {
        height: 300px;
      }
      .carousel img {
        height: 300px;
      }
      .carousel-caption {
        width: 65%;
        padding: 0 70px;
        margin-top: 100px;
      }
      .carousel-caption h1 {
        font-size: 30px;
      }

      .marketing .span4 + .span4 {
        margin-top: 40px;
      }

      .featurette-heading {
        font-size: 30px;
      }
    }
    </style>
    ";
}
include_once 'header.php';
//print_r($_GET);
global $errSignup;
$errSignup=0;
if (isset($_POST['signup-gantt'])) {
    include_once DIR."classes/formValidation.php";
    $validation=new formValidation();
    $inputValid = array(
        'email' => 'ایمیل|r|@|uniq=tusers'
    );
    global $validationMsg;
    $validationMsg=$validation->validation($inputValid);
    if(count($validationMsg)==0 || $validationMsg=='')
    {
        $email = $_POST['email'];
        $user = $sqlOPR -> select('users', 'id', "email=$email");
        if (count($user) == 0 || empty($user[0]['email']))
        {
            include_once 'classes/hashMaker.php';
            $hashMaker = new hashMaker();
            $emailHash = $hashMaker -> hash($email);
            $inserts = "email=$email;hash=$emailHash";
//            echo $inserts;
            if(count($user) == 0)
                $id = $sqlOPR -> insert1('users',array('email','hash'),array($email,$emailHash));
            else if(empty($user[0]['email']))
            {
                $id=$user[0]['id'];
                $sqlOPR -> update('update', $inserts,"id=$id");
            }
//            ec($id);
            if(@$id!='')
            {
//                echo "email sended check your email";
                $emailHash2 = md5($emailHash);
                $link="<a href='" . URL . "signup/".$email."/" . $emailHash . "/$emailHash2'>لینک ثبت نام تکمیلی</a>";
                $msg="با سلام خدمت شما کاربر گرامی...<br>این ایمیل نسبت به درخواست شما برای ثبت نام در وب سایت ".SITENAME." ارسال گردیده است لطفا بر روی لینک زیر جهت تکمیل مراحل ثبت نام کلیک نمایید.<br>
                    $link
                    <br>
                    با تشکر...";
                $array=array('to'=>$email,'subject'=>'ثبت نام در '.SITENAME.'','text'=>$msg);
                if (Mailgun($array))
                {
                    $validationMsg['email']='لینک ثبت نام برای شما ارسال شد ایمیلتان را چک کنید!';
                    $emailSend=1;
                    $errSignup=1;
                }
                else
                {
                    $validationMsg['email']='ایمیل برای شما ارسال نشد لطفا دوباره تلاش کنید!';
                    $errSignup=1;
                    $sqlOPR->delete('tusers',"id=$id");
                }
            }
            else
            {
                $validationMsg['email']='لطفا دوباره بعدا تلاش کنید!';
                $errSignup=1;
                $sqlOPR->delete('tusers',"id=$id");
            }
        }
        else if (count($user) > 0)
        {
            $email = $_POST['email'];
            $emailSite = explode('@', $email);
            $ln = count($emailSite) - 1;
            $errMessageForm = "
            ایمیل قبلا استفاده شده برای استفاده از امکانات به
            <a href='http://www.$emailSite[$ln]' class='label label-info' target='_blank'>
             ایمیل خود</a>
              مراحعه کنید";
            $emailSend = 0;
            echo $errMessageForm;
            $validationMsg['email']='این ایمیل قبلا استفاده شده است!';
            $errSignup=1;
        }
    }
    else
    {
        $validationMsg['email']='درستی ایمیل را بررسی نمایید!';
        $errSignup=1;
    }

}

if (isset($_POST['signin-gantt']) || isset($_POST['emaili'])) {
    $email = $_POST['emaili'];
    $pass = $_POST['passi'];

    if ($email == '' || $pass == '') {
        $string = "location:" . URL . "signin/empty-login";

    } else {
        $sqlOPR = new sqlOPR();
        $user = $sqlOPR -> select('users', '', "email=$email,pass=$pass");
        if (count($user) > 0) {
            include DIR . "/subfiles/sessionSet.php";

        } else {
            $string = "location:" . URL . "signin/wrong-login";

        }
    }
    header(@$string);
}
if(@$params[0]=='unset')
{
    session_destroy();
}

?>
<script>
    $(".passwd, input[name='emaili']").keypress(function(e) {
        if (e.which == 13) {
            e.preventDefault();

            form = $(this).closest('form');
            form.submit();
        }
    });
</script>

<body>
	<!-- NAVBAR
	================================================== -->
	<?php include_once DIR."menu1.php";?><!-- /.navbar-wrapper -->
    <div class="container-fluid pricing-title-header">
        <div class="row">
            <div class="pricing-title">
                حرفه ای انتخاب کنید، حرفه ای مدیریت کنید
            </div>
        </div>
    </div>
	<!-- Modal -->
	<div class="container marketing pricing">
		<div class="row" style='margin-bottom:50px;'>
			
            <div class="span3 plans plan1">
				<h2 class="title-futhers plan-title plan1-title">رایگان
                    <div class="price">۵۷ هزار تومان\ماه</div>
                </h2>
				<div class='text-futhers'>
                    <div class="futhers">۵ کاربر</div>
                    <div class="futhers">۳ پروژه</div>
                    <div class="futhers">۳ گیگ حجم</div>
                    <div class="futhers">* ۴ ماه اول رایگان *</div>
                    <a href="#" class="btn-plans btp1">انتخاب</a>
				</div>
			</div><!-- /.span3 -->
			<div class="span3 plans plan2">
				<h2 class="title-futhers plan-title plan2-title">پایه
                    <div class="price">۷۹ هزار تومان\ماه</div>
                </h2>
				<div  class='text-futhers'>
                    <div class="futhers">۱۰ کاربر</div>
                    <div class="futhers">۱۰ پروژه</div>
                    <div class="futhers">۷ گیگ حجم</div>
                    <a href="#" class="btn-plans btp2">انتخاب</a>
                </div>
			</div><!-- /.span3 -->
			<div class="span3 plans plan3">
				<h2 class="title-futhers plan-title plan3-title">پیشرفته
                    <div class="price">۱۴۳ هزار تومان\ماه</div>
                </h2>
				<div class='text-futhers'>
                    <div class="futhers">۲۰ کاربر</div>
                    <div class="futhers">۲۵ پروژه</div>
                    <div class="futhers">۱۲ گیگ حجم</div>
                    <a href="#" class="btn-plans btp3">انتخاب</a>
				</div>
			</div>
            <div class="span3 plans plan4">
				<h2 class="title-futhers plan-title plan4-title">نامحدود
                    <div class="price">۲۶۷ هزار تومان\ماه</div>
                </h2>
				<div class='text-futhers'>
                    <div class="futhers">کاربر نامحدود</div>
                    <div class="futhers">پروژه نامحدود</div>
                    <div class="futhers">۵۰ گیگ حجم</div>
                    <a href="#" class="btn-plans btp4">انتخاب</a>
				</div>
			</div>
            <!-- /.span4 -->
		</div><!-- /.row -->
    </div>
    <script type="text/javascript">
        $('#loading').hide();
        $('#loading1').hide();
    </script>

    <!-- /END THE FEATURETTES -->
<?php
include_once 'footer.php';
?>