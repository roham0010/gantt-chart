<?php
include_once 'config/config.php';
$file = $params[1];
$fileOldName=$params[2];
$filePath=DIR."tasksFile/$file";
outputFile($filePath, $file);
function outputFile($filePath, $fileOldName, $mimeType = '') {
    // Setup
    $mimeTypes = array(
    	'pdf' => 'application/pdf',
    	'txt' => 'text/plain',
    	'html'=> 'text/html',
    	'zip' => 'application/zip',
    	'doc' => 'application/msword',
    	'xls' => 'application/vnd.ms-excel',
    	'ppt' => 'application/vnd.ms-powerpoint',
        'mp4' => 'video/mp4',
    	'exe' => 'application/octet-stream',
    	'gif' => 'image/gif',
    	'png' => 'image/png',
    	'jpeg'=> 'image/jpeg',
    	'jpg' => 'image/jpg'
    );

    $fileSize = filesize($filePath);
    $fileName = rawurldecode($fileName);
    $fileExt = '';

    // Determine MIME Type
    if($mimeType == '') {
    	$fileExt = strtolower(substr(strrchr($filePath, '.'), 1));

    	if(array_key_exists($fileExt, $mimeTypes)) {
    		$mimeType = $mimeTypes[$fileExt];
    	}
    	else {
    		$mimeType = 'application/force-download';
    	}
    }

    // Disable Output Buffering
    @ob_end_clean();

    // IE Required
    if(ini_get('zlib.output_compression')) {
    	ini_set('zlib.output_compression', 'Off');
    }

    // Send Headers
    header('Content-Type: ' . $mimeType);
    header('Content-Disposition: attachment; filename="' . $fileName . '"');
    header('Content-Transfer-Encoding: binary');
    header('Accept-Ranges: bytes');

    // Send Headers: Prevent Caching of File
    header('Cache-Control: private');
    header('Pragma: private');
    header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');

    // Multipart-Download and Download Resuming Support
    if(isset($_SERVER['HTTP_RANGE'])) {
    	list($a, $range) = explode('=', $_SERVER['HTTP_RANGE'], 2);
    	list($range) = explode(',', $range, 2);
    	list($range, $rangeEnd) = explode('-', $range);

    	$range = intval($range);

    	if(!$rangeEnd) {
    		$rangeEnd = $fileSize - 1;
    	}
    	else {
    		$rangeEnd = intval($rangeEnd);
    	}

    	$newLength = $rangeEnd - $range + 1;

    	// Send Headers
    	header('HTTP/1.1 206 Partial Content');
    	header('Content-Length: ' . $newLength);
    	header('Content-Range: bytes ' . $range - $rangeEnd / $size);
    }
    else {
    	$newLength = $size;
    	header('Content-Length: ' . $size);
    }

    // Output File
    $chunkSize = 1 * (1024*1024);
    $bytesSend = 0;
    if($file = fopen($filePath, 'r')) {
    	if(isset($_SERVER['HTTP_RANGE'])) {
    		fseek($file, $range);

    		while(!feof($file) && !connection_aborted() && $bytesSend < $newLength) {
    			$buffer = fread($file, $chunkSize);
    			echo $buffer;
    			flush();
    			$bytesSend += strlen($buffer);
    		}

    		fclose($file);
    	}
    }
    exit();
}
/*
if (file_exists($filePath)) {
    echo 'aaaaaaaa';
    header('Content-Description: File Transfer');
    header('Content-Type: application/octet-stream');
    header('Content-Disposition: attachment; filename='.basename($filePath));
    header('Expires: 0');
    header('Cache-Control: must-revalidate');
    header('Pragma: public');
    header('Content-Length: ' . filesize($filePath));
    readfile($filePath);
    exit;
}

