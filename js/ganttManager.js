page=0;
function log(str)
{
	console.log(str);
}
$(function(){
	$(document).on('click','#next,#prev,#default',function(e){
		mthis=$(this);
		e.preventDefault();
        switch(mthis.attr('id'))
        {
            case 'next':
                page+=5;
                $('#prev').removeClass('muted');
                $('#prev').css('background-color','#fff').css('cursor','pointer');;
                break;
            case 'prev':
                $('#next').removeClass('muted');
                $('#next').css('background-color','#fff').css('cursor','pointer');;
                page-=5;
                break;
            case 'default':
                $('#next').removeClass('muted');
                $('#next').css('background-color','#fff').css('cursor','pointer');;
                $('#prev').removeClass('muted');
                $('#prev').css('background-color','#fff').css('cursor','pointer');;

                page=0;
                break;
        }
        ajaxStart();
		$.ajax({
            type: 'POST',
            url: window.location.origin + '/gantt/ajaxes/ganttManager.php',
            data: {action: 'select-next', page, projectType, token: ajaxRequest},
            dataType: 'json',
            success: function (data) {
            	response=data['response'];
	            answer=data['answer'];
	            switch(response)
	            {
	                case 1:
	                    if (data['data']['end']==0)
		                {
		                	$('table tbody').html(data['data']['rows']);
		                }
		                else
		                {
		                	switch(mthis.attr('id'))
							{
								case 'next':
									page-=5;
									break;
								case 'prev':
									page+=5;
									break;
								case 'default':
									page=0;
									break;
							}
							mthis.addClass('muted');
							mthis.css('background-color','#eee');
							mthis.css('cursor','default');
						}
	                break;
	                case 2:
	                    unSuccess(answer);
	                break;
	                case 3:
	                    myModal(keyModal+'97',answer);
	                break;
	                case 4:
	                    tryAgain(answer);
	                break;
	                case 5:
	                    ajaxRequestError(answer);
	                break;
	                case 6:
	                    liginError(answer);
	                break;
	                case 7:
	                    httpRequestError(answer);
	                break;
	                case 8:
	                    emptySuccess(answer);
	                break;
	            }
                ajaxEnd();
            },error:function(){
                ajaxEnd();
                myPrompt('projectsPagination');
            }
        });
        return false;
	});/*
	$(document).on('click','',function(){
		page-=5
		$.ajax({
            type: 'POST',
            url: window.location.origin + '/gantt/ajaxes/ganttManager.php',
            data: {action: 'select-next', page, projectType, token: ajaxRequest},
            dataType: 'json',
            success: function (projects) {
                if (projects['rows'].length>0)
                {
                	$('table tbody').html(projects['rows']);
                }
                else
                	page+=5;
            },
            error: function (data) {
                myModal('خطا در ارسال اصلاعات!');
            }
        });
        return false;
	});*/
});
