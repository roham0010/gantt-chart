
if(window.location.hash=='')
    window.location.hash="#persons=&progress=&date=&hasTimeline=";
function showAllRows()
{
    $('.task-row').each(function(){
        rowId=$(this).attr('sid');
        $('#schedule-row-'+rowId).show();
        $('#percent-row-'+rowId).show();
        $('#person-'+rowId).show();
        $('#task-'+rowId).show();
    });
}

function hideShowRows(rowId,type)
{
    if(type=='hide')
    {
        $('#schedule-row-'+rowId).hide();
        $('#percent-row-'+rowId).hide();
        $('#person-'+rowId).hide();
        $('#task-'+rowId).hide();
    }
    else
    {
        $('#schedule-row-'+rowId).show();
        $('#percent-row-'+rowId).show();
        $('#person-'+rowId).show();
        $('#task-'+rowId).show();
    }
}
function getFiltersInput()
{
    filterInput=[];
    users=$('.stop-close-persons .persons-resources-filter:checked').map(function(){
        return $(this).val();
    }).get();
    percents=$('.stop-close-percent .percent-filter:checked').map(function(){
        return $(this).val();
    }).get();
    date=$('.date-filters .date-filter:checked').val();
    if(date=='undefined'||!date)
    {
        date='';
    }
    checked=false;
    if($('.without-timeline-checked').is(':checked'))
    {
        checked=true;
    }

    filterInput['persons']=users;
    filterInput['progress']=percents;
    filterInput['date']=date;
    filterInput['checked']=checked;
    usersString='';
    percentsString='';
    if(percents.length>0)percentsString=users.join(',');
    if(users.length>0)usersString=users.join(',');

    window.location.hash="#persons="+usersString+"&progress="+percents+"&date="+date+"&hasTimeline="+checked;
    filter();
}
filter();
function filter()
{
    states='';
    filtersArray=[];
    filterString=window.location.hash;
    filters=filterString.split('#');
    filters=filters[1].split('&');
    for(i=0;i<filters.length;i++)
    {
        params=filters[i].split('=');
        value=params[1].split(',');
        if(value.length>1)
            filtersArray[params[0]]=value;
        else 
        {
            filtersArray[params[0]]=[params[1]];
        }
    }
    //console.log(filtersArray);
    if(filtersArray['persons'].length>0&&filtersArray['persons']!='')
    {
        $('.stop-close-persons').prev('a').addClass('btn-inverse');
        $('#remove-person-filters').show();
    }        
    else    
    {
        $('.stop-close-persons').prev('a').removeClass('btn-inverse');
        $('#remove-person-filters').hide();
    }
    if(filtersArray['progress'].length>0&&filtersArray['progress']!='')
    {
        $('.stop-close-percent').prev('a').addClass('btn-inverse');
        $('#remove-percent-filters').show();
    }
    else
    {
        $('.stop-close-percent').prev('a').removeClass('btn-inverse');
        $('#remove-percent-filters').hide();
    }    
    if(filtersArray['date'][0])
    {
        $('.date-filters').prev('a').addClass('btn-inverse');
        $('#remove-date-filters').show();
    }
    else
    {
        $('.date-filters').prev('a').removeClass('btn-inverse');
        $('#remove-date-filters').hide();
    }
    $('.percent-row').each(function(){
        rowId=$(this).attr('sid');
        //filter personssss
        person='';
        persons=$('#person-'+rowId).html();
        if(persons!='' && persons!=' ' && persons)
        {
            persons=persons.split(userResourceSplit);
            if(persons[0]!=''&&persons[0])
                person=persons[0].split(userSplit);
            resources='';
            resources1='';
            if(persons[1]!=''&&persons[1])
                resources1=persons[1].split(userSplit);
            if(resources1.length>0&&person.length>0)
                person=$.merge(person,resources1);
            if(resources1.length>0)
                person=resources1;
            if(person.length>0)
                person=person;
        } 
        personsFlag='2';
        for(i=0;i<filtersArray['persons'].length;i++)
        {
            if (filtersArray['persons'][i]==''||(filtersArray['persons'][i]=='unassigned'&&(persons==''||persons==' '||!persons))||jQuery.inArray(filtersArray['persons'][i], person) !== -1)
            {
                if(filtersArray['persons'][i]!='')
                    $('.persons-resources-filter[value='+filtersArray['persons'][i]+']').prop('checked',true);
                personsFlag=true;
                break;
            }
            else
                personsFlag=false;

        }
        //filter dateeeeeee
        dateFlag='2';
        todayRight=scrollSchedule-resizeDragX+1;
        mthis=$('#schedule-'+rowId);
        aa=parseInt(parseInt(mthis.css('width'))+parseInt(mthis.css('right')));
        //console.log(parseInt(mthis.css('right'))+'---w:'+parseInt(mthis.css('width'))+'---r+w:'+aa+'.---tr:'+todayRight+'---s+rdx:'+(parseInt(scrollSchedule)+parseInt(resizeDragX*6))+'---'+rowId)   
        if(filtersArray['date'][0]==''
          ||(filtersArray['date'][0]=='oneweek'&&parseInt(mthis.css('right'))+3>todayRight && parseInt(parseInt(mthis.css('width'))+parseInt(mthis.css('right')))<(parseInt(scrollSchedule)+parseInt(resizeDragX*6)))
          ||(filtersArray['date'][0]=='towweek'&&parseInt(mthis.css('right'))+3>todayRight)&& parseInt(parseInt(mthis.css('width'))+parseInt(mthis.css('right')))<(parseInt(scrollSchedule)+parseInt(resizeDragX*13))
          ||(filtersArray['date'][0]=='fourweek'&&parseInt(mthis.css('right'))+3>todayRight)&& parseInt(parseInt(mthis.css('width'))+parseInt(mthis.css('right')))<(parseInt(scrollSchedule)+parseInt(resizeDragX*27))
          ||(filtersArray['date'][0]=='inprogress'&&parseInt(mthis.css('right'))-3<=todayRight)&& parseInt(parseInt(mthis.css('width'))+parseInt(mthis.css('right')))>(todayRight)
          ||(filtersArray['date'][0]=='befortoday'&&parseInt(mthis.css('right'))-3<=todayRight)&& parseInt(parseInt(mthis.css('width'))+parseInt(mthis.css('right')))-5<(todayRight)
          ||(filtersArray['date'][0]=='untilltoday' && (parseInt(mthis.css('width'))+parseInt(mthis.css('right')))-resizeDragX-3<=(todayRight))
          ||(filtersArray['date'][0]=='aftertoday' && (parseInt(mthis.css('right')))+3>=(todayRight))
          )
        {
            if(filtersArray['date'][0]!='')
                $('.date-filter[value='+filtersArray['date'][0]+']').prop('checked',true);
            dateFlag=true;
        }
        else    
            dateFlag=false;
        //filter percentsssssss
        percent='';
        percent=$('#percent-'+rowId).html();
        percentsFlag='2';
        for(i=0;i<filtersArray['progress'].length;i++)
        {
            if ((filtersArray['progress'][i]=='')||(filtersArray['progress'][i]=='done'&&percent==100)||(filtersArray['progress'][i]=='notdone'&&percent<100)
                ||(filtersArray['progress'][i]=='between-25-50'&&percent>25&&percent<50)
                ||(filtersArray['progress'][i]=='under'&&percent<parseInt($('#percent-under').val()))||(filtersArray['progress'][i]=='above'&&percent>parseInt($('#percent-above').val()))
                ||(filtersArray['progress'][i]=='between'&&percent>parseInt($('#percent-above-bet').val())&&percent<parseInt($('#percent-under-bet').val())))
            {
                if(filtersArray['progress'][i]!='')
                    $('.percent-filter[value='+filtersArray['progress'][i]+']').prop('checked',true);
                percentsFlag=true;
                break;
            }
            else
                percentsFlag=false;
        }
        //filter without timeline
        // withoutTimeLineFlag='2';
       
        withoutTimeLineFlag=filtersArray['hasTimeline'][0];
//        alert(withoutTimeLineFlag);
        
        //console.log(personsFlag+'==true && '+percentsFlag+'==true && '+dateFlag+'==true && '+withoutTimeLineFlag+'==true');

        if((personsFlag=='2'||personsFlag==true) && (percentsFlag=='2' || percentsFlag==true) && (dateFlag=='2' || dateFlag==true) && ((withoutTimeLineFlag=='' || withoutTimeLineFlag=='true') || mthis.parent().parent().attr('show')=='true'))
        {
            states+=' show='+rowId;
            hideShowRows(rowId,'show');
        }
        else
        {
            states+=' hide='+rowId;
            hideShowRows(rowId,'hide');
        }
    });
    if(withoutTimeLineFlag=='' || filtersArray['hasTimeline'][0]=='true')
        $('.without-timeline-checked').prop('checked',true);
    else
        $('.without-timeline-checked').prop('checked',false);
 
        // console.log(states)
}

$(function(){

    $(document).on('click', '.dropdown-menu.stop-close-persons', function (ev) {
        getFiltersInput();
        ev.stopPropagation();
    });
    
    $('#percent-under ,#percent-above, #percent-under-bet, #percent-above-bet').keyup(function (ev) {
        $('.dropdown-menu.stop-close-percent').click();
    });

    $(document).on('click', '.dropdown-menu.stop-close-percent', function (ev) {
        getFiltersInput();
        ev.stopPropagation();
    });

    $(document).on('click', '.dropdown-menu.date-filters', function (ev) {
        getFiltersInput();
    });
    $(document).on('click', '.without-timeline', function (ev) {
        getFiltersInput();
        todayRight=scrollSchedule-resizeDragX-5;
        $('.schedule-row[show="false"]').each(function(){
            rowId=$(this).attr('sid');
        });
    });
});

/*

$(function(){
    $(document).on('click', '.dropdown-menu.stop-close-persons', function (ev) {
        users=$('.stop-close-persons .persons-resources-filter:checked').map(function(){
            return $(this).val();
        }).get();
        $('.task-persons').each(function(){
            rowId=$(this).parent().parent().parent().attr('sid');
            removeFlag=false;
            person='';
            persons=$(this).html();
            if(persons!='') persons=persons.split(userResourceSplit);
            resources='';

            if(persons[0]!=''&&persons[0])
                person=persons[0].split(userSplit);

            resources1='';
            if(persons[1]!=''&&persons[1])
                resources1=persons[1].split(userSplit);
            console.log(resources1)
            if(resources1.length>0&&person.length>0)
                person=$.merge(person,resources1);
            if(resources1.length>0)
                person=resources1;
            if(person.length>0)
                person=person;
            console.log(' rrrrrrrrrrr '+person+' rrrrrrrrrrr');
            for(i=0;i<users.length;i++)
            {
                if (users[0]=='all'||(users[i]=='unassigned'&&(persons==''||persons==' '||!persons))||jQuery.inArray(users[i], person) !== -1)
                {
                    removeFlag=true;
                    break;
                }
            }
            if(removeFlag==false)
            {

                $('#schedule-row-'+rowId).hide();
                $('#percent-row-'+rowId).hide();
                $('#person-'+rowId).hide();
                $('#task-'+rowId).hide();

            }
            else
            {
                $('#schedule-row-'+rowId).show();
                $('#percent-row-'+rowId).show();
                $('#person-'+rowId).show();
                $('#task-'+rowId).show();
            }

        });
        console.log('aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa');
        ev.stopPropagation();
    });

    $('#percent-under ,#percent-above, #percent-under-bet, #percent-above-bet').keyup(function (ev) {

        $('.dropdown-menu.stop-close-percent').click();
    });

    $(document).on('click', '.dropdown-menu.stop-close-percent', function (ev) {
        percents=$('.stop-close-percent .percent-filter:checked').map(function(){
            return $(this).val();
        }).get();

        $('.cpercent').each(function(){
            rowId=$(this).attr('sid');
            removeFlag=false;
            percent='';
            percent=$(this).html();

            //console.log(' rrrrrrrrrrr '+resources+' rrrrrrrrrrr');
            for(i=0;i<percents.length;i++)
            {
                if ((percents[0]=='all')||(percents[i]=='done'&&percent==100)||(percents[i]=='notdone'&&percent<100)
                    ||(percents[i]=='between-25-50'&&percent>25&&percent<50)
                    ||(percents[i]=='under'&&percent<parseInt($('#percent-under').val()))||(percents[i]=='above'&&percent>parseInt($('#percent-above').val()))
                    ||(percents[i]=='between'&&percent>parseInt($('#percent-above-bet').val())&&percent<parseInt($('#percent-under-bet').val())))
                {
                    console.log($('#percent-above').val());
                    removeFlag=true;
                    break;
                }
            }
            if(removeFlag==false)
            {

                $('#schedule-row-'+rowId).hide();
                $('#percent-row-'+rowId).hide();
                $('#person-'+rowId).hide();
                $('#task-'+rowId).hide();

            }
            else
            {
                $('#schedule-row-'+rowId).show();
                $('#percent-row-'+rowId).show();
                $('#person-'+rowId).show();
                $('#task-'+rowId).show();
            }

        });
        console.log('aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa');
        ev.stopPropagation();
    });

    $(document).on('click', '.dropdown-menu.date-filters', function (ev) {
        date=$('.date-filters .date-filter:checked').val();
        todayRight=scrollSchedule-resizeDragX-5;
        $('.resize-drag').each(function(){
            rowId=$(this).attr('sid');
            removeFlag=false;
            mthis=$(this);

            console.log(date+"=='oneweek'"+"&&"+parseInt(mthis.css('right'))+">"+scrollSchedule+"&&"+(mthis.width()+parseInt(mthis.css('right')))+"<"+(scrollSchedule+(resizeDragX*6)));

            if(
              (date=='all')
              ||(date=='oneweek'&&parseInt(mthis.css('right'))>todayRight && (mthis.width()+parseInt(mthis.css('right')))<(parseInt(scrollSchedule)+parseInt(resizeDragX*6)))
              ||(date=='towweek'&&parseInt(mthis.css('right'))>todayRight)&& (mthis.width()+parseInt(mthis.css('right')))<(parseInt(scrollSchedule)+parseInt(resizeDragX*13))
              ||(date=='fourweek'&&parseInt(mthis.css('right'))>todayRight)&& (mthis.width()+parseInt(mthis.css('right')))<(parseInt(scrollSchedule)+parseInt(resizeDragX*27))
              ||(date=='inprogress'&&parseInt(mthis.css('right'))-5<=todayRight)&& (mthis.width()+parseInt(mthis.css('right')))>=(todayRight)
              ||(date=='befortoday'&&parseInt(mthis.css('right'))<=todayRight)&& (mthis.width()+parseInt(mthis.css('right')))-5<(todayRight)
              ||(date=='untilltoday' && (mthis.width()+parseInt(mthis.css('right')))-resizeDragX-5<=(todayRight))
              ||(date=='aftertoday' && (parseInt(mthis.css('right')))>=(todayRight))
              )
                removeFlag=true;

            if(removeFlag==false)
            {

                $('#schedule-row-'+rowId).hide();
                $('#percent-row-'+rowId).hide();
                $('#person-'+rowId).hide();
                $('#task-'+rowId).hide();

            }
            else
            {
                $('#schedule-row-'+rowId).show();
                $('#percent-row-'+rowId).show();
                $('#person-'+rowId).show();
                $('#task-'+rowId).show();
            }

        });
        console.log('aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa');
    });



    $(document).on('click', '.without-timeline', function (ev) {
        checked=false;
        if($('.without-timeline-checked').is(':checked'))
        {
            checked=true;
        }
        todayRight=scrollSchedule-resizeDragX-5;
        $('.schedule-row[show="false"]').each(function(){
            rowId=$(this).attr('sid');
            if(checked==false)
            {
                $('#schedule-row-'+rowId).hide();
                $('#percent-row-'+rowId).hide();
                $('#person-'+rowId).hide();
                $('#task-'+rowId).hide();

            }
            else
            {
                $('#schedule-row-'+rowId).show();
                $('#percent-row-'+rowId).show();
                $('#person-'+rowId).show();
                $('#task-'+rowId).show();
            }

        });
        console.log('aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa');
    });
});

*/