
/*START VARS*/
scheduleGridX = 41;
scheduleGridY = 41;

scheduleCell = 41;

resizeDragX = 42;
resizeDragY = 42;


/*------------STATIC VARS------------*/
var sortableTasks = false;
var changeGroup = false;
/*END VARS \\\\\\\\\\*/

(function ($) {
    'use strict';
    var definer = $('<div dir="rtl" id="qwert" style="font-size: 14px; width: 1px; height: 1px; position: absolute; top: -1000px; overflow: scroll">A</div>')
    .appendTo('body')[0],
        type = 'reverse';
	definer=$('<div dir="rtl" id="qwert" style="font-size: 14px; width: 1px; height: 1px; position: absolute; top: -1000px; overflow: scroll">A</div>')
	.appendTo('body')[0];
    if (definer.scrollLeft > 0) {
        type = 'default';
    } else {
        definer.scrollLeft = 1;
        if (definer.scrollLeft === 0) {
            type = 'negative';
        }
    }

    $(definer).remove();
    $.support.rtlScrollType = type;
}(jQuery));


// scrollLeft replacement

var origScrollLeft = jQuery.fn.scrollLeft;
jQuery.fn.scrollLeft = function(isc) {
    var value = origScrollLeft.apply(this, arguments);

    if (isc === undefined) {
        switch(jQuery.support.rtlScrollType) {
            case "negative":
                return value + this[0].scrollWidth - this[0].clientWidth;
            case "reverse":
                return this[0].scrollWidth - value - this[0].clientWidth;
        }
    }
    return value;
};
/*START NEEDS AT FIRST RUN*/
$(function () {

    $('.resize-drag').css({'min-width': resizeDragX});
    $('.schedule-cell').css({'width': scheduleCell});
    $('.schedule-date').css({'width': scheduleCell});
    $('.schedule-row').css({'width': ((scheduleGridX + 1) * daysNumber)});
    $('.date-content').css({'width': ((scheduleGridX + 1) * daysNumber)+scheduleGridX+1});
    $('.schedule-content').css({'width': ((scheduleGridX + 1) * daysNumber)});
    $('#innerScrollbar-schedule').css({'width': (scheduleGridX * daysNumber) + 100});

    $('.left-tool img ,.right-tool img').hide();

    scrolll=Math.abs(scrollSchedule - ((scheduleCell * daysNumber) - 350));
    brs=browser();
    if(brs=='firefox')
	{
		$('#scrollbar-schedule').scrollLeft(-scrollSchedule + 350);
	}
    else
    	$('#scrollbar-schedule').scrollLeft(scrolll);
    $('.row-fluid .span9').css('width',$('body').width()-$('.right-body').width()-2);
});
/*END NEEDS AT FIRST RUN \\\\\\*/
$(window).resize(function(){
    $('.row-fluid .span9').css('width',$('body').width()-$('.right-body').width()-2);
});
$(window).scroll(function () {
    //$('.navbar-wrapper').hide();
    $('.dropdown').removeClass('open');
   // $('.date-content').css('margin-top', $(window).scrollTop() + 'px');
    ganttWindowScrollTop = $(window).scrollTop();
    if (ganttWindowScrollTop > 0)
    {
        //$('#date-container').css({'margin-top':ganttScrollTop+ganttWindowScrollTop});
    }
    else
    {
        //$('#date-container').css({'margin-top':0});
        //$('#scrollbar-schedule').css('top', '45px');
    }
    $('#scrollbar-task').css({'margin-right': 15 + $(this).scrollLeft() + 'px'});
    $('#scrollbar-schedule').css({'margin-right': $(this).scrollLeft() + 'px'});
    console.log($(this).scrollLeft());
});

// TODO:

//++new group zir dorost shavad na ro
//++task beyne group ha ham jabaja beshe
//++nod next haye group va tasks
//++namayesh bar hasbe nod nextha
//++tasks chera scroll mikhore vaghti kare jadid misazam?!!!!
//++takhsise persons anjam shavad kamel
//++emroz neshan dade shavad va dokme baraye raftan be emroz
//++tarikh shoro va payan neshon dade beshe
//+export ax timelineha //yeki taghsim bandi ax monde yeki ham modiriate downloadesh
//++delete groups
//++sort groups
//++delete tasks


//
//rang bandi vase karha
//etesalate time line ha be ham sakhte shavad
//masire bohrani neshan dade shavad

/*START scrolls move secoundary scrollbar-schedule position*/
$('.gantt-body').scroll(function () {
   if(browser()!='firefox') {
   	$('.schedule-rows').scrollTop($(this).scrollTop());
   	//$('#content-schedule').scrollTop($(this).scrollTop());
   }
   else
   {
   		$('.schedule-rows').scrollTop($(this).scrollTop());
   	//$('#content-schedule').scrollTop($(this).scrollTop());
   }
    console.log($(this).scrollLeft());
});
$('.schedule-rows').scroll(function () {
    $('.gantt-body').scrollTop($(this).scrollTop());
});
$('.tasks').scroll(function () {
    $('#scrollbar-task').scrollLeft($(this).scrollLeft());
});

//function updateScrollbar(content, scrollbarSchedule)
//{
//    width1 = content.scrollWidth + "px";
//    scrollbarSchedule.getElementsByTagName('*')[0].style.width = width1;
//    console.log(scrollbarSchedule.getElementsByTagName('*')[0].style.left)
//    content.scrollLeft = scrollbarSchedule.scrollLeft;
//}
/*END move scroll/////////////////////////////////////////////////*/

/*START hovers on task and schedule/////////////////////////*/
$(document).on('mouseover', '.right-panel-row', function () {
    id = $(this).attr('sid');
    $('#schedule-row-' + id).addClass('row-hover');
    $('#percent-row-' + id).addClass('row-hover');
    $('#person-' + id).addClass('row-hover');
    $('#task-' + id).addClass('row-hover');
    $('#operation-' + id).addClass('row-hover');
    $('#date-start-row-' + id).addClass('row-hover');
    $('#date-end-row-' + id).addClass('row-hover');
    if ($('#schedule-row-' + id).attr('show') == 'true' && resizable == false)
        $('#schedule-row-' + id + ' img').css('display', 'inline-block');
});

$(document).on('mouseleave', '.right-panel-row', function () {
    id = $(this).attr('sid');
    $('#schedule-row-' + id).removeClass('row-hover');
    $('#percent-row-' + id).removeClass('row-hover');
    $('#person-' + id).removeClass('row-hover');
    $('#task-' + id).removeClass('row-hover');
    $('#operation-' + id).removeClass('row-hover');
    $('#date-start-row-' + id).removeClass('row-hover');
    $('#date-end-row-' + id).removeClass('row-hover');
    $('#schedule-row-' + id + ' img').css('display', 'none');
});

/*END hover//////\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

/*START change height of container panels to parent (container) when first load and resize container*/

h1 = $('.span3').height();
h2 = $('.span9').height();
h = h1 > h2 ? h1 : h2;
h = h - 70;
//$('.gantt-schedule-cell').css('height',h);

$('.row-fluid').resize(function () {
    height = $('.row-fluid').height();

});
function log(str)
{
    console.log(str);
}
function browser(chrome, firefox, safari, ms, opera)
{
    /**/
    //console.log(navigator.userAgent);
    var isOpera = !!window.opera || navigator.userAgent.indexOf(' OPR/') >= 0;
    // Opera 8.0+ (UA detection to detect Blink/v8-powered Opera)
    var isFirefox = typeof InstallTrigger !== 'undefined';   // Firefox 1.0+
    var isSafari = Object.prototype.toString.call(window.HTMLElement).indexOf('Constructor') > 0;
    // At least Safari 3+: "[object HTMLElementConstructor]"
    var isChrome = !!window.chrome && !isOpera;              // Chrome 1+
    var isIE = /*@cc_on!@*/false || !!document.documentMode; // At least IE6
    if (isOpera) {
        console.log('opera');
        return 'opera';
    } else if (isFirefox) {
        console.log('firefox');
        return 'firefox';
    } else if (isSafari) {
        console.log('safari');
        return 'safari';
    } else if (isIE) {
        return 'ms';
        console.log('ms');
    }
    else if (isChrome) {
        console.log('chrome');
        return 'chrome';
    }
}
function updateToolbar(ths, parent)
{
}
/*
 function updateTasks()
 {
 if($('.tasks').height()+8>$('.gantt-body').height())
 $('.right-body').css('right','-14px');
 console.log('UPDATE TASKSSSSS'+$('.tasks').height()+">"+$('.gantt-body').height());
 }*/
////////////////////
$(function () {

    /*----------START LEFT AND RRIGHT TOOL-----*/
    /*START EDIT TASKS PANEL\\\\\\\\\\\\\\\\\\*/
    /*START EDIT TASK NAME 1111*/

    taskBlur = false;
    $(document).on('click', '.task-row', function () {
        if((parseInt(permition) & 1)>0)
        {
            id = $(this).attr('sid');
            if ($('#input-task-' + id).css('display') == 'none' && taskBlur == false && taskRemove==false)
            {
                //console.log('taskblur false')
                taskame = $('#task-name-' + id).html();
                $('#task-name-' + id).hide();
                $('#input-task-' + id).show();
                $('#input-task-' + id).addClass('input-task');
                $('#input-task-' + id).focus();
                $('#input-task-' + id).select();
                $('#input-task-' + id).width($('.ctask-group').width());
                $('#input-task-' + id).val(taskame);
                taskBlur = true;
            }
            oldTaskName = $(this).children('input').val();
            if (taskBlur == false)
                console.log(id + ' click');
            else
                console.log('fdsafdsa');
        }
    });
    $(document).on('click', '.operation-row:not(:first-child)', function () {
        // $('.popover-operation').css({'position': 'absolute !important', 'top': $(this).offset().top+10 + 'px', 'left': $(this).position().left + 'px', 'display': 'block'});
        // $('.popover-operation').children('.task-row').children('input').focus();
        // operationId=$(this).attr('sid');
    });
    $(document).on('click', '.oprs', function () {
        $('.popover-operation').css({'display': 'none'});
        operationId=$(this).parent().attr('sid');
        switch($(this).attr('operation'))
        {
            case 'delete':
                deleteType='tasks';
                mthis = $('#task-'+operationId);
                deleteThisPrev='';
                deleteId=mthis.attr('sid');
                deleteName=mthis.children('div').html();
                deleteThisPrev=mthis.attr('prev');
                deleteNextId=mthis.next().attr('sid');
                deleteTaskGroupTable='tasks';
                if((deleteThisPrev!='0') || deleteNextId!=undefined)
                {
                    $("#delete-modal .modal-body").html(window.sh4+'آیا مایل به حذف کار " '+deleteName+'" هستید؟'+window.eh4);
                    $("#delete-modal").modal();
                    $("#delete-modal").modal({keyboard: false});
                    $("#delete-modal").modal('show');
                }
                else
                {
                    $('#alert-modal').children('.modal-body').html(canNotDeleteLastTask);
                    $('#alert-modal').modal();
                    $('#alert-modal').modal({keyboard: false});
                    $('#alert-modal').modal('show');
                }
                console.log(deleteId+" : "+deleteName);
            
            break;
            case 'files':
                mthis = $('#schedule-row-'+operationId);
                taskId = mthis.attr('sid');
                $('#uploads #myModalLabel').html('' + $('#task-name-' + taskId).html());
                $("#uploads").modal();
                $("#uploads").modal({keyboard: false});
                $("#uploads").modal('show');
                parent = $('#uploads');
                html=noCommentErr;
                //parent.find('.modal-body').html(html);

                selectFiles(taskId);
                console.log('click on left-tool upload icon');
            break;
            case 'comments':
                mthis = $('#schedule-row-'+operationId);
                taskId = mthis.attr('sid');
                $('#comments #myModalLabel').html('' + $('#task-name-' + taskId).html());
                $("#comments").modal();
                $("#comments").modal({keyboard: false});
                $("#comments").modal('show');
                parent = $('#comments');
                html=noCommentErr;
                parent.find('.modal-body').html(html);

                selectComments(taskId);
            break;
        }
        operationId=$(this).attr('sid');
    });


    sid = 400;
    newTask = false;
    $(document).on('blur', '.input-task', function () {
        if((parseInt(permition) & 1)>0)
        {
            taskBlur = false;
            parent = $(this).parent();
            id = parent.attr('sid');
            newValue = $(this).val();
            ths = $(this);

            gid = parent.parent().prev().attr('sid');
            prev = parent.prev();
            previd = prev.attr('sid');
            if (parent.is(':first-child'))
                previd = 0;

            if (oldTaskName != newValue&&newValue!='')
            {
                ths = $(this);
                editTaskName($(this),id);
                console.log('edit task ' + id);
            }

            ths.hide();
            $('#task-name-' + id).show();
    		if(newValue!='')
            	$('#task-name-' + id).html(newValue);
//            updateScrollbar(document.getElementById('tasks'), document.getElementById('scrollbar-task'));
            $('.ctask-group').css('width', $('#innerScrollbar-task').width());
            console.log(id + ' blur');
            newTask = false;
        }
    });
    // function editTask(id)
    // {
    //     taskRow = $('#task-' + id);
    //     id = taskRow.attr('sid');
    //     newValue = $('#input-task-' + id).val();
    //     if (newValue == '')
    //         newValue = $('#task-name-' + id).html();
    //     gid = taskRow.parent().prev().attr('sid');
    //     prev = taskRow.prev();
    //     previd = prev.attr('sid');
    //     if (taskRow.is(':first-child'))
    //         previd = 0;

    //     percent = $('#percent-' + id).html();
    //     person = $('#person-' + id).html();
    //     pos = $('.schedule-content').width() - $('#schedule-' + id).width() - $('#schedule-' + id).position().left + 1327 + ($('#content-schedule').scrollLeft() - $('.schedule-rows').width());
    //     pos1 = $('.schedule-content').width() - $('#schedule-' + id).width() - $('#schedule-' + id).position().left;

    //     divi=(pos1)/resizeDragX;
    //     divi=Math.round(divi);
    //     pos1s=divi*resizeDragX;
    //     pos1=pos1s+1;

    //     startDate = scrollDate[pos1];
    //     endDate = scrollDate[pos1 + $('#schedule-' + id).width()];
    //     width=$('#schedule-' + id).width();
        
    //     divi=(width+1)/resizeDragX;
    //     width1=divi*resizeDragX;
    //     modi=(width+1)%resizeDragX;
    //     if(modi>10)
    //             width1+=resizeDragX;
    //     else if(modi<4&&modi>1)
    //             width1-=resizeDragX;
    //     width1=Math.round(width1);

    //     divi=(width)/resizeDragX;
    //     divi=Math.round(divi);
    //     width1=divi*resizeDragX;
    //     width=width1+1;

    //     $('#schedule-' + id).width(width-2);
    //     $('#schedule-' + id).css('right',pos1-2);
    //     console.log(width1+" true width: "+width+" start:"+pos1);
    //     ajaxStart();
    //     $.ajax({
    //         url: URL + 'ajaxes/task.php',
    //         type: 'post',
    //         data: {action: 'edit',projectId:projectId , name: newValue, gid: gid, prevId: previd, id: id, percent: percent, person: person, startDate: startDate, endDate: endDate,marginRight:pos1,width:width, token: ajaxRequest},
    //         dataType: 'json',
    //         success: function (data) {
    //             console.log('success edit');
    //             ajaxEnd();
    //         },error:function(){
    //             ajaxEnd();
    //             myPrompt('');
    //         }
    //     });
    // }

    /*---NEW GROUPS---*/
        $(document).on('click', '.group-row', function (event) {
            if((parseInt(permition) & 1)>0)
            {
                if (sortableTasks == true)
                    return;
                id = $(this).attr('sid');
                if ($('#input-group-' + id).css('display') == 'none' && taskBlur == false && groupRemove == false)
                {
                    //console.log('taskblur false')
                    oldTaskName = $('#group-name-' + id).html();
                    $('#group-name-' + id).hide();
                    $('#input-group-' + id).show();
                    $('#input-group-' + id).addClass('input-group');
                    $('#input-group-' + id).focus();
                    $('#input-group-' + id).select();
                    $('#input-group-' + id).width($('.ctask-group').width());
                    $('#input-group-' + id).val(oldTaskName);
                    taskBlur = true;
                    console.log(id + ' click')
                }
                if (taskBlur == false)
                    console.log('fdsafdsa' + taskBlur);
            }
        }).children('.icon-group').click(function (e) {
                return false;
        });

        groupId = 800;
        newGroup = false;

        $(document).on('blur', '.input-group', function () {

            parent = $(this).parent();
            id = parent.attr('sid');
            newValue = $(this).val();
            thisGroup = parent.parent();
            prevId = thisGroup.prev().attr('sid');
            nextId = thisGroup.next().attr('sid');
            if(oldTaskName!=newValue){
                editGroupName($(this),id);
            }

            $('#group-name-' + id).show();
            $('#group-name-' + id).html(newValue);
            $(this).hide();
            //console.log('insert the new name of task: '+id);
//            updateScrollbar(document.getElementById('tasks'), document.getElementById('scrollbar-task'));
            $('.ctask-group').css('width', $('#innerScrollbar-task').width());
            newGroup = false;
            taskBlur = false;
        });
    
    if((parseInt(permition) & 4194304)>0)
    {
        /*-------REMOVE GROUPS--------*/
        $(document).on('mouseover', '.group-row', function () {
            parent = $(this);
            parent.children('.icon-group-remove').show();
        });
        $(document).on('mouseleave', '.group-row', function () {
            parent = $(this);
            parent.children('.icon-group-remove').hide();
        });

        $(document).on('mouseover', '.icon-group-remove', function () {
            groupRemove=true;
        });
        groupRemove=false;
        $(document).on('mouseleave', '.icon-group-remove', function () {
            groupRemove=false;
        });

        $(document).on('mousedown', '.icon-group-remove', function (){
            deleteType='groups';
            parent = $(this).parent();
            parent.children('input');
            deleteId=parent.attr('sid');
            deleteName=parent.children('div').html();
            deleteThisPrev=parent.parent().attr('prev');
            deleteNextId=parent.parent().next().attr('sid');
            if(deleteThisPrev!='' || deleteNextId!=undefined)
            {
                $("#delete-modal .modal-body").html('آیا مایل به حذف گروه " '+deleteName+'" این هستید؟ ');
                $("#delete-modal").modal();
                $("#delete-modal").modal({keyboard: false});
                $("#delete-modal").modal('show');
            }
            else
            {
                $('#alert-modal').children('.modal-body').html(canNotDeleteLastGroup);
                $('#alert-modal').modal();
                $('#alert-modal').modal({keyboard: false});
                $('#alert-modal').modal('show');
            }
            deleteTaskGroupTable='groups';
            console.log(deleteGroupId+" : "+deleteGroupName);
        });
    }
    /*-------REMOVE TASKS--------*/
    if((parseInt(permition) & 4194304)>0)
    {
//        $(document).on('mouseover', '.task-row', function () {
//            parent = $(this);
//            parent.children('.icon-task-remove').show();
//        });
         $(document).on('mouseover', '.icon-task-remove', function () {
            parent = $(this);
            parent.show();
            taskRemove=true;
        });
        taskRemove=false;
        $(document).on('mouseleave', '.task-row , .icon-task-remove', function () {
            parent = $(this);
            parent.children('.icon-task-remove').hide();
            taskRemove=false;
        });
        $(document).on('mouseleave', '.icon-task-remove', function () {
            parent = $(this);
            parent.hide();
        });
        $(document).on('mouseup', '.icon-task-remove', function () {
            deleteType='tasks';
            mthis = $(this).parent('.task-row');
            deleteThisPrev='';
            deleteId=mthis.attr('sid');
            deleteName=mthis.children('div').html();
            deleteThisPrev=mthis.attr('prev');
            deleteNextId=mthis.next().attr('sid');
            deleteTaskGroupTable='tasks';
            if((deleteThisPrev!='0') || deleteNextId!=undefined)
            {
                $("#delete-modal .modal-body").html(window.sh4+'آیا مایل به حذف کار " '+deleteName+'" هستید؟'+window.eh4);
                $("#delete-modal").modal();
                $("#delete-modal").modal({keyboard: false});
                $("#delete-modal").modal('show');
            }
            else
            {
                $('#alert-modal').children('.modal-body').html(canNotDeleteLastTask);
                $('#alert-modal').modal();
                $('#alert-modal').modal({keyboard: false});
                $('#alert-modal').modal('show');
            }
            console.log(deleteId+" : "+deleteName);
        });
    }

    $(document).on('click', '#deleteTaskGroup', function () {
        if(deleteType=='groups'||deleteType=='tasks')
        {
            deleteTaskGroup();
        }
        else if(deleteType=='tasksfile')
        {
            if((parseInt(permition) & 262144)>0)
            {
                deleteFile();
            }
        }
        else if(deleteType=='comment')
        {
            deleteComment();
        }
    });

    $(document).on('mouseover', '.icon-group-remove, .icon-group', function () {
        sortableTasks = true;
    });
    $(document).on('mouseleave', '.icon-group-remove, .icon-group', function () {
        sortableTasks = false;
    });
    /*||*/
    $(document).on('keypress', '.input-group', function (event) {
        if (event.which == 13) {
            $(this).blur();
            $(this).parent().parent().children('.news .new-task').click();
        }
    });
    $(document).on('keypress', '.input-task', function (event) {
        if (event.which == 13) {
            $(this).blur();
        }
    });
    /*----------*/
    $(document).on('click', '.icon-group', function () {
        group = $(this).parent().parent();
        id = group.children('.group-row').attr('sid');
        if (group.children('.groups-task').is(':visible'))
        {
            group.children('.groups-task').hide();
            group.children('.news').hide();
            $('#person-group' + id + ' .person-row:not(:first-child)').hide();
            $('#percent-group' + id + ' .percent-row:not(:first-child)').hide();
            $('#schedule-group' + id + ' .schedule-row:not(:first-child)').hide();
            $('#dates-group' + id + ' .dates-row:not(:first-child)').hide();
            $('#datee-group' + id + ' .datee-row:not(:first-child)').hide();
            $('#operation-group' + id + ' .operation-row:not(:first-child)').hide();
            $(this).removeClass('icon-minus');
            $(this).addClass('icon-plus');
        }
        else
        {
            group.children('.groups-task').show();
            group.children('.news').show();
            $('#person-group' + id + ' .person-row:not(:first-child)').show();
            $('#percent-group' + id + ' .percent-row:not(:first-child)').show();
            $('#schedule-group' + id + ' .schedule-row:not(:first-child)').show();
            $('#dates-group' + id + ' .dates-row:not(:first-child)').show();
            $('#datee-group' + id + ' .datee-row:not(:first-child)').show();
            $('#operation-group' + id + ' .operation-row:not(:first-child)').show();
            $(this).addClass('icon-minus');
            $(this).removeClass('icon-plus');
        }
    });

    /*END EDIT TASK NAME 1111*/
    /*START EDIT PERSONS 2222*/

    $(document).on('click', '.person-row', function () {
        personRowId = $(this).attr('sid');
        personsResourcesSetted = $('#person-' + personRowId).html();
        personsResourcesSettedArray=[];
        personsResourcesSettedArray[1]='';
        personsResourcesSettedArray = personsResourcesSetted.split(userResourceSplit);
        personsSetted=personsResourcesSettedArray[0];
        resourcesSetted=personsResourcesSettedArray[1];
        personsSettedArray=personsResourcesSettedArray[0].split(userSplit);
        if(personsResourcesSettedArray[1]!=undefined)
            resourcesSettedArray=personsResourcesSettedArray[1].split(userSplit);
        else
            resourcesSettedArray='';

        $('.persons').each(function () {
            if (jQuery.inArray($(this).parent().prev().html(), personsSettedArray) !== -1)
                $(this).prop('checked', true);
            else
                $(this).prop('checked', false);
        }).get();

        $('.resources').each(function () {
            if (jQuery.inArray($(this).parent().prev().html(), resourcesSettedArray) !== -1)
                $(this).prop('checked', true);
            else
                $(this).prop('checked', false);
        }).get();
        personsIdBefore = $('.persons:checkbox:checked').map(function () {
            return $(this).val();
        }).get();
        resourcesIdBefore = $('.resources:checkbox:checked').map(function () {
            return $(this).val();
        }).get();
        $('.popover-person').css({'position': 'absolute !important', 'top': $(this).offset().top+10 + 'px', 'right': $(this).position().left + 215 + 'px', 'display': 'block'});
        $('.popover-person').children('.task-row').children('input').focus();
    });
    $(document).on('click', '.popover-close', function () {
        $('.popover-person').css({'display': 'none'});
        $('.popover-operation').css({'display': 'none'});
    });
    if((parseInt(permition) & 128)>0)
    {
        $(document).on('click', '.set-persons', function () {
            personsId = $('.persons:checkbox:checked').map(function () {
                return $(this).val();
            }).get();
            personsRemoveId=[];
            ii=0;
            for(i=0;i<personsIdBefore.length;i++)
        	{
        		if(jQuery.inArray(personsIdBefore[i],personsId)===-1)
        			personsRemoveId[ii++]=personsIdBefore[i];
        	}
            for(j=0;j<personsIdBefore.length;j++)
            {
                removeItem=personsIdBefore[j];
                personsId = jQuery.grep(personsId, function(value) {
                    return value != removeItem;
                });
            }

            resourcesId = $('.resources:checkbox:checked').map(function () {
                return $(this).val();
            }).get();
            resourcesRemoveId=[];
            ii=0;
            for(i=0;i<resourcesIdBefore.length;i++)
        	{
        		if(jQuery.inArray(resourcesIdBefore[i],resourcesId)===-1)
        			resourcesRemoveId[ii++]=resourcesIdBefore[i];
        	}
            for(j=0;j<resourcesIdBefore.length;j++)
            {
                removeItem=resourcesIdBefore[j];
                resourcesId = jQuery.grep(resourcesId, function(value) {
                    return value != removeItem;
                });
            }
            console.log('rrrrrrrrrrrrrrrrrrrrrrrr'+resourcesRemoveId);
            console.log(personsId+' personssssssss '+ resourcesId+' resourcesssssssssssssss');
            personsName = $('.persons:checkbox:checked').map(function () {
                return $(this).parent().prev().html();
            }).get();
            resourcesName = $('.resources:checkbox:checked').map(function () {
                return $(this).parent().prev().html();
            }).get();
            personsStringName = personsName.join(userSplit);
            resourcesStringName = resourcesName.join(userSplit);
            if(resourcesName!='')
                personsResourcesStringName = personsName.join(userSplit)+userResourceSplit+resourcesName.join(userSplit);
            else
                personsResourcesStringName = personsName.join(userSplit);
            $('#person-' + personRowId).html(personsResourcesStringName);
            $('#person-' + personRowId).attr('title', personsResourcesStringName);

            $('#schedule-row-' + personRowId + ' .task-persons').html(personsResourcesStringName);
            $('.popover-person').css({'display': 'none'});
            flgResources=0;
            flgPersons=0;
            if (personsSetted != personsStringName)
            {
                flgPersons=1;
            }
            if (resourcesSetted != resourcesStringName)
            {
                flgResources=1;
            }
            if(flgPersons==1||flgResources==1)
            {
                console.log(personsIdBefore);
                console.log(personsId);

                setPerson();
                console.log(personsStringName + ' must change in task row with id: ' + personRowId);
            }


            console.log('set persons is clicked');
        });
    }
    /*END EDIT PERSONS 2222*/
    /*START EDIT PERCENT 3333*/
    if((parseInt(permition) & 64)>0)
    {
        $(document).on('click', '.percent-row', function () {
            id = $(this).attr('sid');
            if ($('#input-percent-' + id).css('display') == 'none')
            {
                taskame = $('#percent-' + id).html();
                $('#percent-' + id).hide();
                $('#input-percent-' + id).show();
                //$('#input-percent-'+id).addClass('input-task');
                $('#input-percent-' + id).focus();
                $('#input-percent-' + id).select();
                $('#input-percent-' + id).val(taskame);
            }
        });
        $(document).on('blur', '.input-percent', function () {
            id = $(this).attr('sid');
            newValue = $(this).val();
            $('#percent-' + id).show();
            $('#percent-' + id).html(newValue);
            $(this).hide();
            $('#schedule-' + id + ' div div').css('width', newValue + '%');

            editPercent(id);
        });
        $(document).on('keypress', '.input-percent', function (event) {
            if (event.which == 13) {
                id = $(this).attr('sid');
                newValue = $(this).val();
                $('#percent-' + id).show();
                $('#percent-' + id).html(newValue);
                $(this).hide();
                console.log('percent keypress setted');
            }
            key = String.fromCharCode(event.which);
            value = $(this).val();
            if (value.length > 2 || isNaN(key) || (value.length > 1 && key != '0') || (value.length > 1 && value > 10))
            {
                return false;
            }
            else if (isNaN)
            {

            }
            console.log('bozorgtar az 333333333333333');
        });
    }
    /*END EDIT PERCENT 3333*/
    taskHeight = $('.tasks').height();
    /*if (taskHeight < $('.gantt-body').height())
    {
        $('.tasks').css('margin-right', '13px');
    }
    else
    {
        $('.tasks').css('margin-right', '0px');
    }*/
    resizable = false;

    max = 0;
    $(document).on('mouseover', '.percent', function () {
        if((parseInt(permition) & 64)>0)
        {
        $(this).css('cursor','pointer');
        if (!$(this).is('.ui-resizable'))
            $(this).resizable({
                axis: 'x',
                handles: "w",
                minWidth: 2,
                resize: function () {
                    $(this).css({'right': '0'});
                    id = $(this).attr('sid');
                    parentWidth = $(this).parent().width();
                    console.log(parentWidth);
                    if (parentWidth < $(this).width())
                    {
                        $(this).width(parentWidth);
                    }
                    else
                    {
                        a = $(this).width();
                        b = $(this).parent().width();
                        c = a / b;
                        d = c * 100;
                        d = Math.round(d);
                        $('#percent-' + id).html(d);
                        $(this).css('width', d + '%');
                    }
                },
                stop: function () {
                    $(this).css({'right': '0'});
                    id = $(this).attr('sid');
                    parentWidth = $(this).parent().width();
                    console.log(parentWidth);
                    if (parentWidth < $(this).width())
                    {
                        $(this).width(parentWidth);
                    }
                    else
                    {
                        a = $(this).width();
                        b = $(this).parent().width();
                        c = a / b;
                        d = c * 100;
                        d = Math.round(d);
                        $('#percent-' + id).html(d);
                        $(this).css('width', d + '%');
                    }
                    editPercent(id);
                }
            });
            $(this).css('cursor','pointer');
        }
        else
        {
            $(this).css('cursor','default');
        }
    });
    /*
     $('.task-row').each(function(){
     $(this).attr('sid',parseInt($(this).attr('sid'))+1)
     });*/
    /*----------START COMMENTING right and left of schedules----------*/
    commentingId = -1;

    $(document).on('click', '.left-tool .comment', function () {
        mthis = $(this).parent();
        taskId = mthis.parent().parent().attr('sid');
        $('#comments #myModalLabel').html('' + $('#task-name-' + taskId).html());
        $("#comments").modal();
        $("#comments").modal({keyboard: false});
        $("#comments").modal('show');
        parent = $('#comments');
        html=noCommentErr;
        parent.find('.modal-body').html(html);

        selectComments(thisId);
        console.log('click on right-tool message icon');
    });

    $(document).on('click', '.left-tool .upload', function () {
        mthis = $(this).parent();
        taskId = mthis.parent().parent().attr('sid');
        $('#uploads #myModalLabel').html('' + $('#task-name-' + taskId).html());
        $("#uploads").modal();
        $("#uploads").modal({keyboard: false});
        $("#uploads").modal('show');
        parent = $('#uploads');
        html=noCommentErr;
        //parent.find('.modal-body').html(html);

        selectFiles(taskId);
        console.log('click on left-tool upload icon');
    });
    if((parseInt(permition) & 262144)>0)
    {
        $(document).on('click', '.file-link .remove-file', function () {
            mthis=$(this);
            fileName=mthis.attr('file-name');
            fileId=mthis.attr('sid');
            deleteType='file';
            parent = $(this).parent();
            parent.children('input');
            deleteId=fileId;
            deleteName=fileName;
            deleteThisPrev=parent.parent().attr('prev');
            deleteNextId=parent.parent().next().attr('sid');
            $("#delete-modal .modal-body").html('آیا مایل به حذف فایل " '+deleteName+'" این هستید؟ ');
            $("#delete-modal").modal();
            $("#delete-modal").modal({keyboard: false});
            $("#delete-modal").modal('show');
            deleteTaskGroupTable='tasksfile';
            deleteType='tasksfile';
        });
    }
    if((parseInt(permition) & 65536)>0)
    {
        $(document).on('click', '#comments .comment-row .icon-remove', function () {
            $('#delete-modal').children('.modal-body').html(commentDelete);
            $('#delete-modal').modal();
            $('#delete-modal').modal({keyboard: false});
            $('#delete-modal').modal('show');

            deleteType='comment';
            mthis=$(this);
            commentId=mthis.attr('sid');


        });
    }
    if((parseInt(permition) & 131072)>0)
    {
        $('#task-file').change(function(){
            var file = this.files[0];
            name = file.name;
            size = file.size;
            type = file.type;
            type = name.split('.').pop().toLowerCase();

            /* case 'image/png':
            case 'image/gif':
            case 'image/jpeg':
            case 'image/pjpeg':
            case 'text/plain':
            case 'text/html': //html file
            case 'application/x-zip-compressed':
            case 'application/zip':
            case 'application/pdf':
            case 'application/vnd.ms-excel':
            case 'application/octet-stream':
            case 'application/msword':
            case 'video/mp4':
            case 'application/vnd.openxmlformats-officedocument.wordprocessingml.document':*/
            flagUpload=true;
            errUpload='';
            /*if(type!='image/png'&&type!='image/gif'&&type!='image/jpeg'&&type!='image/pjpeg'&&type!='text/plain'&&
                    type!='text/html'&&type!='application/x-zip-compressed'&&type!='application/zip'&&type!='application/pdf'&&
                    type!='application/vnd.ms-excel'&&type!='application/octet-stream'&&type!='application/msword'&&
                    type!='video/mp4'&&type!='application/vnd.openxmlformats-officedocument.wordprocessingml.document')
            */
            if($.inArray(type, ['gif','png','jpg','jpeg','pdf','pjpeg','txt','mp4','docx'
                                ,'zip','rar','doc','ppt','xls']) == -1)
            {
                errUpload=fileFormatErr;
                flagUpload=false;
            }
            if(size>5242880)
            {
               errUpload=fileSizeErr;
                flagUpload=false;
            }
            if(name.length<1||name=='undefined')
            {
                errUpload=fileNotSelectedErr;
                flagUpload=false;
            }
            if(name.length>300)
            {
                errUpload=fileBigNameErr;
                flagUpload=false;
            }
            if(flagUpload==false)
            {
                $('#alert-modal').children('.modal-body').html(errUpload);
                //$('#comments').modal('hide');
                $('#alert-modal').modal();
                $('#alert-modal').modal({keyboard: false});
                $('#alert-modal').modal('show');
            }
            if(flagUpload==false)
            {
                $('#alert-modal').children('.modal-body').html(errUpload);
                $('#alert-modal').modal();
                $('#alert-modal').modal({keyboard: false});
                $('#alert-modal').modal('show');
                return false;
            }
            //flagUpload=false;
            //errUpload=sh4+'فایلی انتخاب کنید'+eh4;
            $('progress').css('display','block');
            var fileData = $('#task-file').prop('files')[0];
            var formData = new FormData();
            formData.append('file', fileData);
            formData.append('taskId', taskId);
            formData.append('user', userLogin);
            formData.append('userName', userName);
            formData.append('projectId',projectId );
            console.log(formData);
            ajaxStart();
            $.ajax({
                url: URL + 'ajaxes/upload.php',
                type: 'POST',
                xhr: function() {  // Custom XMLHttpRequest
                    var myXhr = $.ajaxSettings.xhr();
                    if(myXhr.upload){ // Check if upload property exists
                        myXhr.upload.addEventListener('progress',progressHandlingFunction, false); // For handling the progress of the upload
                    }
                    return myXhr;
                },
                //Ajax events
                beforeSend: function(){

                },
                success: function(data){
                    console.log('success upload and insert');
                    if(data['newName'].length>0)
                    {
                        newName=data['newName'];
                        oldName=data['oldName'];
                        fileId=data['fileId'];
                        tag="<div class='file-link'><i class='icon-remove remove-file' id='file"+fileId+"' sid='"+fileId+"' file-name='"+newName+"'></i>"+"<a target='_blank' href='"+URL+"downloadTFile/"+newName+"/"+oldName+"' class='thumbnail file-link' title='توسط: "+userName+"'>"+
                                oldName+"</a></div>";
                        uploads= $('#uploads');
                        uploads.children('.files').html(tag+uploads.children('.files').html());
                        $('progress').css('display','none');
                        $('#task-file').val('');
                        console.log('add file link to files link');
                    }
                    ajaxEnd();
                },
                //error: errorHandler,
                // Form data
                //Options to tell jQuery not to process data or worry about content-type.
                cache: false,
                data: formData,
                dataType:'json',
                contentType: false,
                processData: false,
                error:function(data){
                    ajaxEnd();
                    console.log(data);
                    myPrompt('uploadFile');
                }

            });
        });
    }
    $('#task-file').click(function() {
        if((parseInt(permition) & 131072)==0)
        {
            myModal(keyModal+'96','عدم دسترسی آپلود فایل');
            return false;
        }
    });
    function progressHandlingFunction(e){
        if(e.lengthComputable){
            $('progress').attr({value:e.loaded,max:e.total});
        }
    }
    if((parseInt(permition) & 32768)>0)
    {
        $(document).on('click', '#comments .insert-comment', function () {
            parent = $(this).closest('#comments');

            percent = $('#percent-' + taskId).html();
            comment=parent.children('textarea').val();
            parent.children('textarea').val('');
            console.log(comment);
            if(comment!='')
            insertComment();

            console.log('click on insert message\ this comment.id: ' + taskId);
        });
    }
    /*END COMMENTING*/
    /*------------START CREATE TIMELINE-------------*/
    creating = false;
    mouseMove = false;

    $(document).on('mouseleave', '.schedule-row', function (event) {
        id = $(this).attr('sid');
        if ($(this).attr('show') == 'false')
            $('#schedule-' + id).css({'display': 'none'});
        $(this).children('.ui-resizable-w').css({'width': '5px'});
        $(this).find('.right-tool img , .left-tool img').css({'display': 'none'});
    });
    $(document).on('mouseup', '.schedule-row', function (event) {
        id = $(this).attr('sid');
        if (creating == true && $('#schedule-' + id).css('display') == 'none')
        {
            var parentOffset = $(this).parent().offset();
            mouseEndX = event.pageX - parentOffset.left;
            mouseEndY = event.pageY - parentOffset.top;

            margin = mouseStartX;
            width = mouseStartX - mouseEndX;
            margin = $(this).width() - mouseStartX + 950 + ($('#content-schedule').scrollLeft() - $('.schedule-rows').width());
            mod = width % resizeDragX;
            width -= mod;
            mod = margin % resizeDragX;
            margin -= mod;

            $('#schedule-' + id).css({'display': 'block', 'right': margin, 'width': width});
            //$(this).html(timeLine);
            console.log('create timeline: mouse down and drag' + mouseStartX + ' ' + mouseStartY + ' ' + mouseEndX + ' ' + mouseEndY + ' ' + margin + ' ');
            creating = false;
        }
    });
    $(document).on('mousedown', '.schedule-row', function (event) {
        id = $(this).attr('sid');
        if ($('#schedule-' + id).css('display') == 'none' || mouseMove == true && $(this).attr('show') == 'false')
        {
            $(this).attr('show', 'true');
            var parentOffset = $(this).parent().offset();

            mouseX = parentOffset.left - event.pageX;
            margin = mouseX;
            margin += $('.schedule-rows').width();

            mod = margin % resizeDragX;
            margin -= mod;
            $('#schedule-' + id).css({'display': 'block', 'right': margin - 1, 'width': resizeDragX});
            $(this).find('.right-tool , .left-tool').css({'display': 'block'});
            $(this).children('div').children('.resize-drag').children('.ui-resizable-w').attr('style', 'width:7px;z-index:90;');

//            updateToolbar($(this).find('.resize-drag'), $(this).children('div'));
            console.log('schedule has staticed');

            //scheduleStaticed=true;
            editTask(id);

        }

    });
    iid=0;
    datePickerShow=false;
    $('.date-start-input, .date-end-input').mousedown(function(){
        if(datePickerShow==true)return;
        iid=$(this).attr('sid');
        mthis=$(this);
    });

    $('.date-start-input, .date-end-input').persianDatepicker({
        altField: '#date-selected',
        timePicker: {
                enabled: false
        },
        observe:true,
        onShow:function(){
            $('.date-picker').css({'opacity':'0','position':'absolute','top':'0'});
            $('.date-picker').parent().children('div').before('<input class="lie date-picker" id="" style="background-color:#111111;width: 20px;float: left;position: relative;">');
        },
        onHide:function(){
            $('.date-picker').parent().children('.lie').remove();
            $('.date-picker').css({'opacity':'1','position':'relative'});
            selectedDate=$('#date-selected').val();
            if(selectedDate!='')
            if( $('#date-start-'+iid).html()!=$('#date-selected').val())
            {

                if(mthis.parent().hasClass('dates-row'))
                {
                    dateType='startDate';
                }
                else
                {
                    dateType='endDate';
                }
                taskId=iid;
                dateStart=$('#date-start-input-'+iid).val();
                dateEnd=$('#date-end-input-'+iid).val();
                $.ajax({
                    url: URL + 'ajaxes/datePicker.php',
                    type: 'post',
                    data: {action: 'edit',projectId:projectId, projectStartDate:projectStartDate , taskId:taskId, dateStart:dateStart, dateEnd:dateEnd, dateType:dateType, token: ajaxRequest},
                    dataType: 'json',
                    success: function (data) {
                        if(mthis.parent().hasClass('dates-row'))
                            $('#date-start-'+iid).html(selectedDate)
                        else
                            $('#date-end-'+iid).html(selectedDate)
                    }
                });
            }
            $('#date-selected').val('');
        },
        onSelect:function(){
        },
        altFormat:"YYYY-MM-DD 00:00:00"
    });
    $('#date-selected').val('');
});
