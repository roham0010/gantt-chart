var URL=window.location.origin+"/gantt/";
var userSplit='• ';
var userResourceSplit='•• ';
var sh4="<h4>";
var eh4="</h4>";
var fileSizeErr=sh4+"اندازه فایل باید کمتر از 5 مگابایت باشد"+eh4;
var fileFormatErr=sh4+"نوع فایل پشتیبانی نمیشود"+eh4;
var fileNotSelectedErr=sh4+"فایلی برای ارسال انتخاب نشده است"+eh4;
var noCommentErr=sh4+"کامنتی درج نشده است"+eh4;
var noFileErr=sh4+"فایلی آپلود نشده است"+eh4;
var commentDelete =sh4+"آیا مایل به حذف کامنت هستید؟"+eh4;
var fileBigNameErr=sh4+"اندازه نام فایل زیاد است لطفا اندازه آن را کاهش دهید"+"</br> آقا من اینو 300 کاراکتر گذاشتم چه خبره بابا فایل با نام بیش از 300 کاراکتر! ;)"+eh4;
var canNotDeleteLastGroup=sh4+"حداقل یک گروه باید موجود باشد"+eh4;
var canNotDeleteLastTask=sh4+"حداقل یک کار باید موجود باشد"+eh4;
var accessDenied=sh4+"دستریس به این عملیات نآری اوکی فمیتی!؟!؟!؟ ها ها چیه!؟!؟؟!؟"+eh4;
var errChangeName=sh4+"خطای تغییر نام"+eh4;
var errNewTask=sh4+"خطای نام جدید"+eh4;
var errNewPercent=sh4+"خطای درصد جدید"+eh4;
var errNewPerson=sh4+"خطای درج شخص جدید"+eh4;
var errNewPerson=sh4+"خطای درج شخص جدید"+eh4;
rulesUser = {
     'newTask' : 1
    ,'newGroup' : 2
    ,'newMile' : 4
    ,'editTask' : 8
    ,'editGroup' : 16
    ,'editPercent' : 64
    ,'setPerson' : 128
    ,'sortTask' : 256
    ,'sortGroup' : 512
    ,'dragTask' : 1024
    ,'dragGroup' : 2048
    ,'resizeTask' : 4096
    ,'newComment' : 32768
    ,'removeComment' : 65536
    ,'upload' : 131072
    ,'removeFile' : 262144
    ,'removeTask' : 2097152
    ,'removeGroup' : 4194304
    ,'removeMile' : 8388608
    ,'sendEmail' : 33554432
    };
keyModal='';
function myModal(key,str)
{
    
    notif="<div class='notif notif-error' style=''><strong>"+key+"</strong>:"+str+"</div>";

    if($('.notifs > .notif').length>2)$('.notif').first().remove();
    $('.notifs').append(notif);
    setTimeout(function(){$('.notif').addClass('notif-show')}, 10);
    setTimeout(function(){$('.notif').first().addClass('notif-hide')}, 3000);
    setTimeout(function(){$('.notif').first().remove()}, 3300);
}
function myPrompt(prCase)
{
    switch(prCase)
    {
        case 'changeName':
            myModal(keyModal+'1',errChangeName);
        break;
        case 'newTask':
            myModal(keyModal+'2',errNewTask);

        break;
        case 'newGroup':
            myModal(keyModal+'3','edit group error');

        break;
        case 'setPercent':
            myModal(keyModal+'4',errNewPercent);

        break;
        case 'setPerson':
            myModal(keyModal+'5',errNewPerson);

        break;
        case 'editAll':
            myModal(keyModal+'6','editAll error');

        break;
        case 'editAllUp':
            myModal(keyModal+'7','editAll up error');

        break;
        case 'deletetg':
            myModal(keyModal+'8','متعصفانه حذف گروه انجام نشد');

        break;
        case 'deleteFile':
            myModal(keyModal+'9','خطای حذف فایل');

        break;
        case 'deleteComment':
            myModal(keyModal+'10','حذف کامنت');

        break;
        case 'dragTask':
            myModal(keyModal+'11','ویرایش زمان کار');

        break;
        case 'dragGroup':
            myModal(keyModal+'12','ویرایش زمان گروه کار');

        break;
        case 'resizeTask':
            myModal(keyModal+'13','خطای تغییر اندازه کار');

        break;
        case 'selectComment':
            myModal(keyModal+'14','خطای دریافت کامنتها');

        break;
        case 'selectFile':
            myModal(keyModal+'15','خطای دریافت فایلها');

        break;
        case 'uploadFile':
            myModal(keyModal+'16','خطای آپلود فایل');

        break;
        case 'newComment':
            myModal(keyModal+'17','خطای درج کامت\نت جدید');

        break;
        case 'projectsPagination':
            myModal(keyModal+'18','خطای نمایش پروژه ها');

        break;
        case 'deleteUser':
            myModal(keyModal+'19','خطای حذف کاربر');

        break;
        case 'inviteAgain':
            myModal(keyModal+'20','خطای ارسال مجدد ایمیل');

        break;
        case 'searchUser':
            myModal(keyModal+'21','خطای جستجوی کاربران');

        break;
        case 'inviteUser':
            myModal(keyModal+'22','خطای دعوت از کاربران');

        break;
        case 'resourceInsert':
            myModal(keyModal+'23','خطای درج منابع پروژه');

        break;
        case 'resourceDelete':
            myModal(keyModal+'24','خطای حذف منابع');

        break;
       
        default:
            myModal(keyModal+'25','خطای سیستم!');
        break;
    }
}

function loadJS(file) {
    // DOM: Create the script element
    var jsElm = document.createElement("script");
    // set the type attribute
    jsElm.type = "application/javascript";
    // make the script element load file
    jsElm.src = file;
    // finally insert the element to the body element in order to load the script
    document.body.appendChild(jsElm);
}
function getHash()
{
	var hash = top.location.hash.replace('#', '');
    var params = hash.split('&');
    var hashes = {};
    for(var i = 0; i < params.length; i++){
       var propval = params[i].split('=');
       hashes[propval[0]] = propval[1];
    }
    return hashes;
}
function ajaxStart()
{
    $('#loading').show();
    $('#logo').hide();
    $('#loading1').show();
    $('#logo1').hide();
    console.log('ajaxStart--------------');
//    setTimeout(function(){ajaxEnd()},7000);
}
function ajaxEnd()
{
    $('#logo').show(); 
    $('#loading').hide();
    $('#loading1').hide();
    $('#logo1').show();
    console.log('ajaxEnd--------------')
}
