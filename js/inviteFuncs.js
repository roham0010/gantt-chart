function getParameterByName(name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(location.search);
    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}
function accessDenied(answer)
{
    answer = typeof answer !== 'undefined' ? answer : 'دسترسی به این مورد ندارید';
    myModal(keyModal+'28',answer);
}

function removeUser(mythis)
{
    ajaxStart();
    projectId=getParameterByName('projectId');
    $.ajax({
        type: 'POST',
        url: window.location.origin + '/gantt/ajaxes/invite.php',
        data: {action: 'delete',projectId:projectId , sid: sid, token: ajaxRequest},
        dataType: 'json',
        success: function (data) {
            response=data['response'];
            answer=data['answer'];
            switch(response)
            {
                case 1:
                    mythis.parent().parent().remove();
                break;
                case 2:
                    unSuccess(answer);
                break;
                case 3:
                    myModal(keyModal+'78',answer);
                break;
                case 4:
                    tryAgain(answer);
                break;
                case 5:
                    ajaxRequestError(answer);
                break;
                case 6:
                    liginError(answer);
                break;
                case 7:
                    httpRequestError(answer);
                break;
                case 8:
                    emptySuccess(answer);
                break;
            }
            ajaxEnd();
        },error:function(){
            ajaxEnd();
            myPrompt('deleteUser');
        }
    });
}
function inviteAgain(mythis)
{
    ajaxStart();
    projectId=getParameterByName('projectId');
    $.ajax({
        type: 'POST',
        url: window.location.origin + '/gantt/ajaxes/invite.php',
        data: {action: 'invitAgain',projectId:projectId , sid: sid,email:email, token: ajaxRequest},
        dataType: 'json',
        success: function (data) {
            response=data['response'];
            answer=data['answer'];
            switch(response)
            {
                case 1:
                    mythis.prop('disabled','disabled');
                    mythis.attr('invit','invited');
                    console.log('invite again is done');
                break;
                case 2:
                    unSuccess(answer);
                break;
                case 3:
                    myModal(keyModal+'79',answer);
                break;
                case 4:
                    tryAgain(answer);
                break;
                case 5:
                    ajaxRequestError(answer);
                break;
                case 6:
                    liginError(answer);
                break;
                case 7:
                    httpRequestError(answer);
                break;
                case 8:
                    emptySuccess(answer);
                break;
            }
            ajaxEnd();
        },error:function(){
            ajaxEnd();
            myPrompt('invitAgain');
        }
    });
}
/*
function (mythis)
{

}

            response=data['response'];
            answer=data['answer'];
            switch(response)
            {
                case 1:
                    
                break;
                case 2:
                    unSuccess(answer);
                break;
                case 3:
                    myModal(keyModal+'',answer);
                break;
                case 4:
                    tryAgain(answer);
                break;
                case 5:
                    ajaxRequestError(answer);
                break;
                case 6:
                    liginError(answer);
                break;
                case 7:
                    httpRequestError(answer);
                break;
                case 8:
                    emptySuccess(answer);
                break;
            }

*/
function selectUser(mythis)
{
    ajaxStart();
    projectId=getParameterByName('projectId');
    $.ajax({
        type: 'POST',
        url: window.location.origin + '/gantt/ajaxes/invite.php',
        data: {action: 'search', nameOrEmail: nameOrEmail ,projectId:projectId , token: ajaxRequest},
        dataType: 'json',
        success: function (data) {
            response=data['response'];
            answer=data['answer'];
            switch(response)
            {
                case 1:
                data=data['data'];
                    if (data['rows'].length > 0)
                    {
                        console.log('success keyup');
//                        personName = '<div class="invite invite-name">';
                        personTag = '<div class="invite invite-content">';
                        for (i = 0; i < data['rows'].length; i++)
                        {

                            personTag+='<div><a class="person" href="" sid="' + data['rows'][i]['id'] + '" name="' + data['rows'][i]['name'] + '" email="' + data['rows'][i]['email'] + '">';
                            if(data['rows'][i]['name']!='')
                                personName =data['rows'][i]['name'];
                            if(data['rows'][i]['email']!='')
                                personEmail=data['rows'][i]['email'];

                            personSign=''
                            if(data['rows'][i]['email']!='' && data['rows'][i]['name']!='')
                                personSign=' : '
                            personTag+=personEmail// + personSign + personName;
                            personTag+='</a><br></div>';

                            personEmail='';
                            personName='';
                        }
                        personName += '</div>';
                        personEmail += '</div>';
                        $('.popover-person').children('.popover-content').html(personTag);
                        $('.popover-person').css({'position': 'absolute ', 'left': 'auto', 'top': mthis.offset().top + 15, 'right': mthis.offset().left + 'px', 'display': 'block','padding':'10px 20px'});
//                        $("input[name='nameOrEmail']").next($('.popover-person'));
                    }
                break;
                case 2:
                    unSuccess(answer);
                break;
                case 3:
                    myModal(keyModal+'80',answer);
                break;
                case 4:
                    tryAgain(answer);
                break;
                case 5:
                    ajaxRequestError(answer);
                break;
                case 6:
                    liginError(answer);
                break;
                case 7:
                    httpRequestError(answer);
                break;
                case 8:
                    //emptySuccess(answer);
                break;
            }
            ajaxEnd();
        },error:function(){
            ajaxEnd();
            myPrompt('searchUser');
        }
    });
}
function inviteUser()
{
    ajaxStart();
    projectId=getParameterByName('projectId');
    $.ajax({
        type: 'POST',
        url: window.location.origin + '/gantt/ajaxes/invite.php',
        data: {action: 'invite', projectId:projectId, nameOrEmail: nameOrEmail ,permition:permition, text: text, selectedUserId: selectedUserId, token: ajaxRequest},
        dataType: 'json',
        success: function (data) {
            response=data['response'];
            answer=data['answer'];
            switch(response)
            {
                case 1:
                    $('#emptyTblUser').remove();
//                    <tr><td><a href="http://127.0.0.1/gantt/users/" class="thumbnail" alt="" title="" style="width:auto; text-decoration:none; margin-right:15px; vertical-align: middle; float: right;">
//                                  payam.aryanfard@gmail.com
//                            </a>
//
//                        </td>
//                        <td>
//                           <select name="permition" class="user-permition">
//                            <option value="1">ایجاد کار</option>
//
//                        </select></td>
//                        <td> <i class="remove-user ico ico-delete" title="حذف" sid="112"></i>
//                            <i class="invite-user-again ico ico-invite" title="دعوت دوباره" sid="102" email="payam.aryanfard@gmail.com"></i>
//                        </td>
//                    </tr>
                    html='<tr><td><a href="http://127.0.0.1/gantt/users/' + nameOrEmail + '" class="thumbnail" alt="' + nameOrEmail + '" title="' + nameOrEmail + '" style="width:auto; text-decoration:none; margin-right:15px; vertical-align: middle; float: right;">' + nameOrEmail + '</a></td>'+
                        '<td>'+
                        '<select name="permition" class="user-permition"><option value="'+$("#permition option:selected").attr("value")+'">'+ $('#permition').find(":selected").text() + $('#permition').html()+
                        '</select></td><td><i class="remove-user ico ico-delete" title="حذف" sid="'+ data['data']['userId'] +'"></i>'+
                        '<i class="invite-user-again ico ico-invite" title="دعوت دوباره" sid="' + data['id'] + '" email="'+nameOrEmail+'"></i></td></tr>';
                    $('#bodyUser').html(html+$('#bodyUser').html());
                    console.log('success invite');
                    $('#name').val('');
                    $('#email').val('');
                    html = "<div class='people-row'><i class='insert icon-chevron-left' title='اضافه به  پروژه'></i><select class='select-access'>  </select> " + name + "</div>";
                    if (email != '')
                        $('#out-of-project').html(html + $('#out-of-project').html());
                    validationError = 1;
                
                break;
                case 2:
                    unSuccess(answer);
                break;
                case 3:
                    myModal(keyModal+'81',answer);
                break;
                case 4:
                    tryAgain(answer);
                break;
                case 5:
                    ajaxRequestError(answer);
                break;
                case 6:
                    liginError(answer);
                break;
                case 7:
                    httpRequestError(answer);
                break;
                case 8:
                    emptySuccess(answer);
                break;
            }
            ajaxEnd();
        },error:function(){
            ajaxEnd();
            myPrompt('inviteUser');
        }
    });
}
function removeResource(mythis)
{
    ajaxStart();
    projectId=getParameterByName('projectId');
    $.ajax({
        type: 'POST',
        url: window.location.origin + '/gantt/ajaxes/invite.php',
        data: {action: 'resource-delete',projectId:projectId , sid: sid, token: ajaxRequest},
        dataType: 'json',
        success: function (data) {
            response=data['response'];
            answer=data['answer'];
            switch(response)
            {
                case 1:
                    mythis.parent().parent().remove();
                break;
                case 2:
                    unSuccess(answer);
                break;
                case 3:
                    myModal(keyModal+'82',answer);
                break;
                case 4:
                    tryAgain(answer);
                break;
                case 5:
                    ajaxRequestError(answer);
                break;
                case 6:
                    liginError(answer);
                break;
                case 7:
                    httpRequestError(answer);
                break;
                case 8:
                    emptySuccess(answer);
                break;
            }
            ajaxEnd();
        },error:function(){
            ajaxEnd();
            myPrompt('resourceDelete');
        }
    });
}
function insertResource()
{
    ajaxStart();
    projectId=getParameterByName('projectId');
    $.ajax({
        type: 'POST',
        url: window.location.origin + '/gantt/ajaxes/invite.php',
        data: {action: 'resource-insert', projectId:projectId, name: name, token: ajaxRequest},
        dataType: 'json',
        success: function (data) {
            response=data['response'];
            answer=data['answer'];
            switch(response)
            {
                case 1:
                    $('#emptyTblRes').remove();
                    resourceId=data['id'];
                            
                    html = "<tr>"+
                                "<td><span>"+name+"</a></td>"+
                                "<td><i class='remove-resource ico ico-delete' title='حذف' sid='"+resourceId+"'></i></td>"+
                              "</tr>";
                    $('#bodyResource').html(html + $('#bodyResource').html());
                    console.log('success insert resource');
                    $('#rname').val('');
                break;
                case 2:
                    unSuccess(answer);
                break;
                case 3:
                    myModal(keyModal+'82',answer);
                break;
                case 4:
                    tryAgain(answer);
                break;
                case 5:
                    ajaxRequestError(answer);
                break;
                case 6:
                    liginError(answer);
                break;
                case 7:
                    httpRequestError(answer);
                break;
                case 8:
                    emptySuccess(answer);
                break;
            }
            ajaxEnd();
        },error:function(){
            ajaxEnd();
            myPrompt('resourceInsert');
        }
    });
}
function unSuccess(answer)
{
    myModal(keyModal+'29',answer)
}
function tryAgain(answer)
{
    myModal(keyModal+'30',answer)
}
function ajaxRequestError(answer)
{
    myModal(keyModal+'31',answer)
}
function loginError(answer)
{
    myModal(keyModal+'32',answer)
}
function httpRequestError(answer)
{
    myModal(keyModal+'33',answer)
}
function emptySuccess(answer)
{
    myModal(keyModal+'34',answer)
}
