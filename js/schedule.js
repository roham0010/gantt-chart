
/*START VARS*/
scheduleGridX = intervall-1;
scheduleGridY = intervall-1;

scheduleCell = intervall-1;

resizeDragX = intervall;
resizeDragY = intervall;
//alert(resizeDragX)

toolDisplay='none';
scheduleDateSpliter=' تا ';
/*------------STATIC VARS------------*/
var sortableTasks = false;
var changeGroup = false;
/*END VARS \\\\\\\\\\*/

(function ($) {
    'use strict';
    var definer = $('<div dir="rtl" id="qwert" style="font-size: 14px; width: 1px; height: 1px; position: absolute; top: -1000px; overflow: scroll">A</div>')
    .appendTo('body')[0],
        type = 'reverse';
	definer=$('<div dir="rtl" id="qwert" style="font-size: 14px; width: 1px; height: 1px; position: absolute; top: -1000px; overflow: scroll">A</div>')
	.appendTo('body')[0];
    if (definer.scrollLeft > 0) {
        type = 'default';
    } else {
        definer.scrollLeft = 1;
        if (definer.scrollLeft === 0) {
            type = 'negative';
        }
    }

    $(definer).remove();
    $.support.rtlScrollType = type;
}(jQuery));


// scrollLeft replacement

var origScrollLeft = jQuery.fn.scrollLeft;
jQuery.fn.scrollLeft = function(isc) {
    var value = origScrollLeft.apply(this, arguments);

    if (isc === undefined) {
        switch(jQuery.support.rtlScrollType) {
            case "negative":
                return value + this[0].scrollWidth - this[0].clientWidth;
            case "reverse":
                return this[0].scrollWidth - value - this[0].clientWidth;
        }
    }
    return value;
};

/*START NEEDS AT FIRST RUN*/
$(function () {

    $('.resize-drag').css({'min-width': resizeDragX});
    $('.schedule-cell').css({'width': scheduleCell});
    $('.schedule-date').css({'width': scheduleCell});
    $('.schedule-row').css({'width': ((scheduleGridX + 1) * daysNumber)});
    $('.date-content').css({'width': ((scheduleGridX + 1) * daysNumber)+scheduleGridX+1});
    $('.schedule-content').css({'width': ((scheduleGridX + 1) * daysNumber)});
    $('#innerScrollbar-schedule').css({'width': (scheduleGridX * daysNumber) + 100});

    $('.left-tool i ,.right-tool i').css('display',toolDisplay);

    scrolll=Math.abs(scrollSchedule - ((scheduleCell * daysNumber) - 350));
    brs=browser();
    if(brs=='firefox')
	{
		$('#scrollbar-schedule').scrollLeft(-scrollSchedule + 350);
	}
    else
    	$('#scrollbar-schedule').scrollLeft(scrolll);
    //set width of schedules content-schedule

    setInterval(function(){
        $('.row-fluid .span9').css('width',$('body').width()-$('.right-body').width()-15);
        $('#scrollbar-schedule').css('width',$('body').width()-$('.right-body').width()-15);
    },100);
});
/*END NEEDS AT FIRST RUN \\\\\\*/
$(document).resize(function(){
    $('.row-fluid .span9').css('width',$('body').width()-$('.right-body').width()-15);
});
$(window).scroll(function () {
    //$('.navbar-wrapper').hide();
    $('.dropdown').removeClass('open');
   // $('.date-content').css('margin-top', $(window).scrollTop() + 'px');
    ganttWindowScrollTop = $(window).scrollTop();
    if (ganttWindowScrollTop > 0)
    {
        //$('#date-container').css({'margin-top':ganttScrollTop+ganttWindowScrollTop});
    }
    else
    {
        //$('#date-container').css({'margin-top':0});
        //$('#scrollbar-schedule').css('top', '45px');
    }
    $('#scrollbar-task').css({'margin-right': 15 + $(this).scrollLeft() + 'px'});
    $('#scrollbar-schedule').css({'margin-right': $(this).scrollLeft() + 'px'});
    console.log($(this).scrollLeft());
});


/*START scrolls move secoundary scrollbar-schedule position*/
$('.gantt-body').scroll(function () {
   if(browser()!='firefox') {
   	$('.schedule-rows').scrollTop($(this).scrollTop());
   	$('#content-schedule').css('top',$(this).scrollTop());
       console.log('aaaaaaaaaaaaaaa');
   }
   else
   {
   		$('.schedule-rows').scrollTop($(this).scrollTop());
   	$('#content-schedule').css('top',$(this).scrollTop());
   }
    console.log($(this).scrollLeft());
});
$('.schedule-rows').scroll(function () {
    $('.gantt-body').scrollTop($(this).scrollTop());
});
$('.tasks').scroll(function () {
    $('#scrollbar-task').scrollLeft($(this).scrollLeft());
});

function updateScrollbar(content, scrollbarSchedule)
{
//    console.log("-"+$('.schedule-rows').offset().left+"px");
//    $('#scrollbar-schedule').css('right',pos+"px");
    width1 = content.scrollWidth + "px";
    scrollbarSchedule.getElementsByTagName('*')[0].style.width = width1;
    console.log(scrollbarSchedule.getElementsByTagName('*')[0].style.left)
    content.scrollLeft = scrollbarSchedule.scrollLeft;
}
function userTaskPermition(tid)
{
    var xxx = "victoria";
    var yyy = "5";
    var re = new RegExp(userName, 'g');
    usersString=$('#person-'+tid).html();
    if(usersString.match(re))
        return true;
    else
        return false;
}
/*END move scroll/////////////////////////////////////////////////*/

/*START hovers on task and schedule/////////////////////////*/
$(document).on('mouseover', '.task-row, .schedule-row, .person-row, .percent-row', function () {
    id = $(this).attr('sid');
    if(!sortHover)
    {
        $('#schedule-row-' + id).addClass('row-hover');
        $('#percent-row-' + id).addClass('row-hover');
        $('#person-' + id).addClass('row-hover');
        $('#task-' + id).addClass('row-hover');
        if ($('#schedule-row-' + id).attr('show') == 'true' && draggable == false && resizable == false)
            $('#schedule-row-' + id + ' i').css('display', 'inline-block');
    }
});

$(document).on('mouseleave', '.task-row, .schedule-row, .person-row, .percent-row', function () {
    if(!sortHover)
    {
        id = $(this).attr('sid');
        $('#schedule-row-' + id).removeClass('row-hover');
        $('#percent-row-' + id).removeClass('row-hover');
        $('#person-' + id).removeClass('row-hover');
        $('#task-' + id).removeClass('row-hover');
        if ($(this).attr('show') == 'false')
            $('#schedule-row-' + id + ' i').css({'display': 'none'});
        else
            $('#schedule-row-' + id + ' i').css({'display': toolDisplay});
    }
});

/*END hover//////\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

/*START change height of container panels to parent (container) when first load and resize container*/

h1 = $('.span3').height();
h2 = $('.span9').height();
h = h1 > h2 ? h1 : h2;
h = h - 70;
//$('.gantt-schedule-cell').css('height',h);

$('.row-fluid').resize(function () {
    height = $('.row-fluid').height();

});
sortHover=false;
function rowHover(ssid,type)
{
    console.log('hover type:  '+ssid+type)
    if(type=0)
    {
        sortHover=false;
        $('#schedule-row-' + ssid).removeClass('row-hover');
        $('#percent-row-' + ssid).removeClass('row-hover');
        $('#person-' + ssid).removeClass('row-hover');
        $('#task-' + ssid).removeClass('row-hover');
    }
    else
    {
        sortHovrer=true;
        $('#schedule-row-' + ssid).addClass('row-hover');
        $('#percent-row-' + ssid).addClass('row-hover');
        $('#person-' + ssid).addClass('row-hover');
        $('#task-' + ssid).addClass('row-hover');    
    }

}
function log(str)
{
    console.log(str);
}
function browser(chrome, firefox, safari, ms, opera)
{
    /**/
    //console.log(navigator.userAgent);
    var isOpera = !!window.opera || navigator.userAgent.indexOf(' OPR/') >= 0;
    // Opera 8.0+ (UA detection to detect Blink/v8-powered Opera)
    var isFirefox = typeof InstallTrigger !== 'undefined';   // Firefox 1.0+
    var isSafari = Object.prototype.toString.call(window.HTMLElement).indexOf('Constructor') > 0;
    // At least Safari 3+: "[object HTMLElementConstructor]"
    var isChrome = !!window.chrome && !isOpera;              // Chrome 1+
    var isIE = /*@cc_on!@*/false || !!document.documentMode; // At least IE6
    if (isOpera) {
        console.log('opera');
        return 'opera';
    } else if (isFirefox) {
        console.log('firefox');
        return 'firefox';
    } else if (isSafari) {
        console.log('safari');
        return 'safari';
    } else if (isIE) {
        return 'ms';
        console.log('ms');
    }
    else if (isChrome) {
        console.log('chrome');
        return 'chrome';
    }
}
function updateToolbar(ths, parent)
{
    pos = $('.schedule-content').width() - ths.width() - ths.position().left;
    ths.css({'left': 'auto', 'right': pos - 2 + 'px'});
    parent.children('.right-tool').css({'right': pos - 23 + 'px'});
    parent.children('.left-tool').css({'right': pos + ths.width() + 1 + 'px'});
}
/*
 function updateTasks()
 {
 if($('.tasks').height()+8>$('.gantt-body').height())
 $('.right-body').css('right','-14px');
 console.log('UPDATE TASKSSSSS'+$('.tasks').height()+">"+$('.gantt-body').height());
 }*/
////////////////////
$(function () {
    $('.input-taski').hide();
    $('.input-group').hide();
    $('.input-percent').hide();
    taskBlur = false;//    1
    sid = 400;
    newTask1 = false;//    2
    groupId = 800;
    newGroup1 = false;//    3
    groupRemove=false;//    4
    taskRemove=false;//    5
    taskRemove=false;//    6
    taskHeight = $('.tasks').height();
    resizable = false;
    draggable = false;
    groupDragg=false;//    7
    max = 0;//    8
    commentingId = -1;//    9
    creating = false;
    mouseMove = false;
    
                                                                    /*-------------------START EDIT TASK NAME 1111*/
    //click on task about edit task text
    $(document).on('click', '.task-row .ctask', function () {
        mythis=$(this).parent();
        id = mythis.attr('sid');
        if((parseInt(permition) & rulesUser['editTask'])>0 || userTaskPermition(id))
        {
            if ($('#input-task-' + id).css('display') == 'none' && taskBlur==false && taskRemove==false)
            {
                //console.log('taskblur false')
                taskame = $('#task-name-' + id).html();
                $('#task-name-' + id).hide();
                $('#input-task-' + id).show();
                $('#input-task-' + id).addClass('input-task');
                $('#input-task-' + id).focus();
                $('#input-task-' + id).select();
                $('#input-task-' + id).width($('.ctask-group').width());
                $('#input-task-' + id).val(taskame);
            }
            if (taskBlur == false)
            {
                console.log(id + ' click');
                taskBlur = true;
            }
            else
            {
                $('.input-taski:focus').blur();
                console.log('unfocus');
            }
            oldTaskName = mythis.children('input').val();
        }
        else
        {
            accessDenied(keyModal+'61','عدم دسترسی به تغییر نام');
        }
    });
    //blur eddited task for edit task text in database
    $(document).on('blur', '.input-taski', function () {
        parent = $(this).parent();
        id = parent.attr('sid');
        
        taskBlur = false;
        newValue = $(this).val();
        ths = $(this);

        gid = parent.parent().prev().attr('sid');
        prev = parent.prev();
        previd = prev.attr('sid');
        if (parent.is(':first-child'))
            previd = 0;
        if (newTask1)
        {
            if (newValue != '')
            {
                if(newRowType=='task')
                    type=0;
                else
                    type=1;
                //id come from database after insert
                sid++;
                newTask($(this));

            }
            else
            {
                parent.remove();
                $('#schedule-row-9999999999').remove();
                $('#percent-row-9999999999').remove();
                $('#person-9999999999').remove();
            }
            console.log('new task');
            
        }
        else
        {
            if (oldTaskName != newValue&&newValue!='')
            {
                editTaskName($(this),id);
                console.log('edit task ' + id);
            }
        }
        ths.hide();
        $('#task-name-' + id).show();
        updateScrollbar(document.getElementById('tasks'), document.getElementById('scrollbar-task'));
        $('.ctask-group').css('width', $('#innerScrollbar-task').width());
        console.log(id + ' blur');
        newTask1 = false;
        
    });
                                                                    /*-----------------END EDIT TASK*/
    
                                                                    /*--------------------START NEW TASK*/    
    $(document).on('click', '.new-task', function () {
            newRowType=$(this).attr('type');
        if((parseInt(permition) & rulesUser['newTask'])>0)
        {
            if(newTask1==true)
            	return;
            scheduleType=$(this).attr('type');
            parent = $(this).parent().prev('.groups-task');
            newTask1 = true;
            if (parent.children().length > 0)
            {
                // done
                if(newRowType=='task')
                {
                    parent.append('<div class="task-row right-panel-row" id="task-9999999999" sid="9999999999" prev="">'+
                                '<i class="icon-task-remove material-icons">clear</i>'+
                                '<div id="task-name-9999999999" class="ctask" style="padding-right:14px;"></div>'+
                                '<input type="text" class="input-taski input-task" sid="249" id="input-task-9999999999" value="" style="display: none; width: 220px;">'+
                                '</div>');
                    id = $('#task-9999999999').prev().attr('sid');
                    
                    $("<div class='person-row right-panel-row' id='person-9999999999' sid='9999999999' >" +
                            "</div>").insertAfter('#person-' + id);

                    $(" <div class='percent-row right-panel-row' id='percent-row-9999999999' sid='9999999999' >" +
                            "<div id='percent-9999999999' sid='9999999999' class='cpercent' style='height:100%;'>1</div>" +
                            "<input type='text' class='input-percent' sid='9999999999' id='input-percent-9999999999' value='' style='display:none;'>" +
                            "</div>").insertAfter('#percent-row-' + id);
                    console.log('new task happend after tasks ' + " this id:" + parent.attr('sid'));
               
                    $(" <div class='schedule-row' id='schedule-row-9999999999' sid='9999999999' show='false'>" +
                    "<div>" +
                        "<i class='right-tool' style='position:absolute; right:0px;display:none;'><img class='ico' src='" + URL + "img/comments2.png' style='display:none;'></i>" +
                        "<div class='resize-drag task ui-widget-content' style='width:"+ resizeDragX +"px; right:" + scheduleGridX + "px; display:none;' id='schedule-9999999999' sid='9999999999'>" +
                        "       <div class='percent80'>" +
                        "           <div class='percent' style='width:1%;' sid='9999999999'></div>" +
                        "       </div>" +
                        "</div>" +
                        "<i class='left-tool' style='position:absolute; right:" + scheduleGridX+scheduleGridX + "px;display:none;'>" +
                        "<i id='img-9999999999' class='ico comment material-icons' style='display:none;'>forum</i>" +
                        "<i id='img-9999999999' class='ico upload material-icons icofile' style='display:none;'>attachment</i>" +
                        "<div class='task-persons'></div>" +
                        "</i>" +
                    "</div>" +
                    "</div>").insertAfter('#schedule-row-' + id);
                    
                }
                else if(newRowType=='mile')
                {
                    parent.append('<div class="task-row right-panel-row" id="task-9999999999" sid="9999999999" prev="">'+
                                '<i class="icon-task-remove material-icons">clear</i>'+
                                '<div id="task-name-9999999999" class="ctask" style="padding-right:14px;"></div>'+
                                '<input type="text" class="input-taski input-task" sid="9999999999" id="input-task-9999999999" value="" style="display: none;">'+
                                '</div>');
                    id = $('#task-9999999999').prev().attr('sid');
                    $("<div class='person-row right-panel-row' id='person-9999999999' sid='9999999999' >" +
                            "</div>").insertAfter('#person-' + id);
                    $("<div class='percent-row right-panel-row' id='percent-row-9999999999' sid='9999999999' >" +
                        "<div id='percent-9999999999' sid='9999999999' class='cpercent' style='height:100%;'>1</div>" +
                        "<input type='text' class='input-percent' sid='9999999999' id='input-percent-9999999999' value='' style='display:none;'>" +
                        "</div>").insertAfter('#percent-row-' + id);
                    console.log('new task happend after tasks ' + " this id:" + parent.attr('sid'));
                    $("<div class='schedule-row' id='schedule-row-9999999999' sid='9999999999' show='false'>" +
                        "<div>" +
                            "<i class='right-tool' style='position:absolute; right:0px;display:none;'><img class='ico' src='" + URL + "img/comments2.png'  style='display: none;''></i>" +
                            "<div class='resize-drag mile ui-widget-content' style='width:"+ resizeDragX +"px; right:" + scheduleGridX + "px; display:none;' id='schedule-9999999999' sid='9999999999'>" +
                            
                            "</div>" +
                            "<span class='left-tool' style='position:absolute; right:" + scheduleGridX+scheduleGridX + "px;display:none;'>" +
                            "<i class='ico comment material-icons' style='display:none;'>forum</i>" +
                            "<i class='ico upload material-icons icofile' style='display:none;'>attachment</i>" +
                            "<div class='task-persons'></div>" +
                            "</span>" +
                        "</div>" +
                        "</div>").insertAfter('#schedule-row-' + id);
                }

            }
            else
            {

                id = parent.prev().attr('sid');
                parent.append('<div class="task-row" id="task-9999999999" sid="9999999999" next-nod="9999999999"> ' +
                        '<div id="task-name-9999999999" class="ctask" style="padding-right:14px;"></div>' +
                        '	<input type="text" class="input-taski input-task" sid="9999999999" id="input-task-9999999999" value="" style="display: none;">'+
                        '</div>');
                $("<div class='person-row' id='person-9999999999' sid='9999999999' >" +
                        "		<div class='person-row right-panel-row' id='person9999999999' sid='d9999999999'>"+
                        "        </div>" +
                        "</div>").insertAfter('#person' + id);

                $("<div class='percent-row' id='percent-row-9999999999' sid='9999999999' >" +
                        "	<div id='percent-9999999999' sid='9999999999' class='cpercent' style='height:100%;'>1</div>" +
                        "	<input type='text' class='input-percent' sid='9999999999' id='input-percent-9999999999' value='' style='display:none;'>" +
                        "</div>").insertAfter('#percent-row' + id);

                $("<div class='schedule-row' id='schedule-row-9999999999' sid='9999999999' show='false' style='width:100%;'>" +
                        "<div>" +
                        "	<i class='right-tool' style='position:absolute; right:0px;display:none;'><img class='ico' src='" + window.location.origin + "/gantt/img/comments2.png'></i>" +
                        "	<div class='resize-drag ui-widget-content' style='display:none;width:23px; right:20px;' id='schedule-9999999999' sid='9999999999'> " +
                        "		<div class='percent80'>" +
                        "			<div class='percent' style='width:1%;' sid='9999999999'></div>" +
                        "		</div>" +
                        "	</div>" +
                        "	<i class='left-tool' style='position:absolute; right:47px;display:none;'><i class='ico comment material-icons'>forum</i><div class='task-persons'></div></i>" +
                        "</div> " +
                        "</div>").insertAfter('#schedule-row' + id);

                console.log('new task happend bfore group ' + " this id:" + parent.attr('sid'));
            }
            $('#task-9999999999  .ctask').click();

        }
        else
        {
            if(newRowType=='mile') 
                tname='مایل استون';
            else
                tname='کار';
            accessDenied(keyModal+'62','عدم دسترسی به ایجاد '+tname);
        }
    });
                                                                    /*END NEW TASK--------------------*/
    
                                                                    /*--------------------START NEW GROUP*/    
    $(document).on('click', '.new-group', function () {
        if((parseInt(permition) & rulesUser['newGroup'])>0)
        {
        	if(taskBlur==true)
        	{
        		return;
        	}
            /*$('.input-task:visible').each(function () {
                $(this).blur();
            });
            $('.input-group:visible').each(function () {
                $(this).blur();
            });*/
            parent = $(this).parent().parent();
            newGroup1 = true;
            parent.after("<div class='ctask-group' id='itask-group8888888888' sid='8888888888' prev='" + parent.attr('sid') + "' number='0'>" +
                        "<div class='group-row right-panel-row' id='group-8888888888' sid='8888888888'><i class='icon-group-remove material-icons'>clear</i><i class='icon-minus icon-group'></i>" +
                        "<div id='group-name-8888888888' class='ctask' style='padding-right:0px;font-weight:bolder;'></div>" +
                        "<input type='text' class='input-group' sid='8888888888' id='input-group-8888888888' value='' style='display:none;'>" +
                        "</div>" +
                        "<div class='groups-task'></div>" +
                        "<div class='news' id='new-task' sid='' >" +
                        "<a href='#' class='new-task $muted link-task' type='task'>کار جدید</a>|"+
                        "<a href='#' class='new-task $muted link-task' type='mile'>مایل استون جدید</a>|"+
                        "<a href='#' class='new-group $muted link-task'>گروه جدید</a></div>" +
                        "</div>");
            rows = $(this).parent().prev().prev();
            gId = rows.attr('sid');

            $("<div class='percent-group' id='percent-group8888888888'>"+
                "<div class='percent-row right-panel-row' id='percent-row8888888888' sidg='8888888888' >0</div>"+
                "<div class='percent-row right-panel-row' id='percent-row8888888888N' sid='d8888888888'>"+
                "</div></div>").insertAfter('#percent-group' + gId);

            $("<div class='person-group' id='person-group8888888888'>" +
                "<div class='person-row right-panel-row' id='person8888888888' sidg='8888888888' >" +
                "</div>"+
                "<div class='person-row right-panel-row' id='person8888888888N' sid='d8888888888'>"+
                "</div></div>").insertAfter('#person-group' + gId);

            $("<div class='schedule-group' id='schedule-group8888888888'>"+
                "<div class='schedule-row' id='schedule-row8888888888' sidg='8888888888'>"+
                    "<div><div class='resize-drag-group ui-widget-content' id='schedule-group-8888888888N' sid='8888888888N' style='right:-7px;width:3px;'>"+
                       "<div class='dragg-group-left dragg-group-arrow'></div>"+
                        "<div class='dragg-group-right dragg-group-arrow'></div>"+
                    "</div>"+
                    "</div>"+
                "<div></div>"+
                "</div>"+
                "<div class='schedule-row' id='schedule-row8888888888N' sid='d8888888888'>"+
                "</div>"+
                "</div>").insertAfter('#schedule-group' + gId);

            //console.log(prev.find('.group-row').attr('sid')+" hahaaaaa");
            $('#group-8888888888 .ctask').click();
            console.log('new group happend' + " this id:" + parent.parent().attr('sid'));
        }
        else
        {
            accessDenied(keyModal+'63','عدم دسترسی به ایجاد گروه');
        }
    });
                                                                    /*END NEW GROUP--------------------*/
    
                                                                    /*---------------------START NEW GROUPS---*/
    //click on group about edit group text
    $(document).on('click', '.group-row .ctask', function (event) {
        if((parseInt(permition) & rulesUser['editGroup'])>0)
        {
            if (sortableTasks == true)
                return;
            id = $(this).parent().attr('sid');
            if ($('#input-group-' + id).css('display') == 'none' && taskBlur == false && groupRemove == false)
            {
                //console.log('taskblur false')
                oldTaskName = $('#group-name-' + id).html();
                $('#group-name-' + id).hide();
                $('#input-group-' + id).show();
                $('#input-group-' + id).addClass('input-group');
                $('#input-group-' + id).focus();
                $('#input-group-' + id).select();
                $('#input-group-' + id).width($('.ctask-group').width());
                $('#input-group-' + id).val(oldTaskName);
                taskBlur = true;
                console.log(id + ' click')
            }
            if (taskBlur == false)
                console.log('fdsafdsa' + taskBlur);
        }
        else
        {
            accessDenied(keyModal+'64','عدم دسترسی ویرایش گروه');
        }
    }).children('.icon-group').mousedown(function (e) {return false;});
    //2
    //blur eddited group for edit group text in database
    $(document).on('blur', '.input-group', function () {

        parent = $(this).parent();
        id = parent.attr('sid');
        newValue = $(this).val();
        thisGroup = parent.parent();
        prevId = thisGroup.prev().attr('sid');
        nextId = thisGroup.next().attr('sid');
        if (newGroup1)
        {
            if (newValue !== '')
            {
                //id come from database after insert
                newGroup($(this));

                console.log('new group');
            }
            else
            {
                delGroup();
            }
        }
        else if(oldTaskName!=newValue){
            editGroupName($(this),id);
        }


        $('#group-name-' + id).show();
        
        $(this).hide();
        //console.log('insert the new name of task: '+id);
        updateScrollbar(document.getElementById('tasks'), document.getElementById('scrollbar-task'));
        $('.ctask-group').css('width', $('#innerScrollbar-task').width());
        newGroup1 = false;
        taskBlur = false;
    });
                                                                    /*END EDIT GROUP-----------------*/
    
                                                                    /*-----------------REMOVE GROUPS*/
    $(document).on('mouseover', '.group-row', function () {
        parent = $(this);
        parent.children('.icon-group-remove').show();
    });
    $(document).on('mouseleave', '.group-row', function () {
        parent = $(this);
        parent.children('.icon-group-remove').hide();
    });
    $(document).on('mouseover', '.icon-group-remove', function () {
        groupRemove=true;
    });
//    3
    $(document).on('mouseleave', '.icon-group-remove', function () {
        groupRemove=false;
    });
    $(document).on('mousedown', '.icon-group-remove', function (){
        mythis=$(this);
        if((parseInt(permition) & rulesUser['removeGroup'])>0)
        {
            groupRemove=true;
            deleteType='groups';
            parent = $(this).parent();
            parent.children('input');
            deleteId=parent.attr('sid');
            deleteName=parent.children('div').html();
            deleteThisPrev=parent.parent().attr('prev');
            deleteNextId=parent.parent().next().attr('sid');
            if(deleteThisPrev!='' || deleteNextId!=undefined)
            {
                $("#delete-modal .modal-body").html('آیا مایل به حذف گروه " '+deleteName+'" این هستید؟ ');
                $("#delete-modal").modal();
                $("#delete-modal").modal({keyboard: false});
                $("#delete-modal").modal('show');
            }
            else
            {
                $('#alert-modal').children('.modal-body').html(canNotDeleteLastGroup);
                $('#alert-modal').modal();
                $('#alert-modal').modal({keyboard: false});
                $('#alert-modal').modal('show');
            }
            deleteTaskGroupTable='groups';
            // console.log(deleteGroupId+" : "+deleteGroupName);
        }
        else
        {
            mythis.parent().parent().mouseup();
            accessDenied(keyModal+'65','عدم دسترسی به حذف گروه');
        }
    });
                                                                    /*END REMOVE GROUP--------------------*/
    
//    4
                                                                    /*--------------------REMOVE TASKS*/
    $(document).on('mouseover', '.task-row', function () {
        parent = $(this);
        parent.children('.icon-task-remove').show();
    });
    $(document).on('mouseover', '.icon-task-remove', function () {
        parent = $(this);
        parent.show();
        taskRemove=true;
    });
//    5
    $(document).on('mouseleave', '.task-row , .icon-task-remove', function () {
        parent = $(this);
        parent.children('.icon-task-remove').hide();
        taskRemove=false;
    });
    $(document).on('mouseleave', '.icon-task-remove', function () {
        parent = $(this);
        parent.hide();
    });
    $(document).on('mouseup', '.icon-task-remove', function () {
        mythis=$(this);
        if((parseInt(permition) & rulesUser['removeTask'])>0)
        {
            deleteType='tasks';
            mthis = $(this).parent('.task-row');
            deleteThisPrev='';
            deleteId=mthis.attr('sid');
            deleteName=mthis.children('div').html();
            deleteThisPrev=mthis.attr('prev');
            deleteNextId=mthis.next().attr('sid');
            if((deleteThisPrev!='0') || deleteNextId!=undefined)
            {
                $("#delete-modal .modal-body").html(window.sh4+'آیا مایل به حذف کار " '+deleteName+'" هستید؟'+window.eh4);
                $("#delete-modal").modal();
                $("#delete-modal").modal({keyboard: false});
                $("#delete-modal").modal('show');
            }
            else
            {
                $('#alert-modal').children('.modal-body').html(canNotDeleteLastTask);
                $('#alert-modal').modal();
                $('#alert-modal').modal({keyboard: false});
                $('#alert-modal').modal('show');
            }
            deleteTaskGroupTable='tasks';
            console.log(deleteId+" : "+deleteName);
        }
        else
        {
            mythis.parent().mouseup();
            accessDenied(keyModal+'66','عدم دسترسی به حذف کار');
        }
    });
                                                                    /*END REMOVE TASKS--------------------*/
    
                                                                    /*--------------------REMOVE GROUPTASK*/
    $(document).on('click', '#deleteTaskGroup', function () {
        if(deleteType=='groups'||deleteType=='tasks')
        {
            deleteTaskGroup();
        }
        else if(deleteType=='tasksfile')
        {
            if((parseInt(permition) & 262144)>0)
            {
                deleteFile();
            }
        }
        else if(deleteType=='comment')
        {
            deleteComment(mthis);
        }
    });
                                                                    /*END REMOVE GROUPTASK--------------------*/
    
    $(document).on('mouseover', '.icon-group-remove, .icon-group', function () {
        sortableTasks = true;
    });
    $(document).on('mouseleave', '.icon-group-remove, .icon-group', function () {
        sortableTasks = false;
    });
    
                                                                    /*--------------------WHEN ENTER IS PRESSED ON NEW TASK OR NEW GROUP IT WILL BRING ANOTHER NEW TASK!*/
    $(document).on('keypress', '.input-group', function (event) {
        if (event.which == 13) {
            $(this).blur();
            $(this).parent().parent().children('.news .new-task').click();
        }
    });
    $(document).on('keypress', '.input-taski', function (event) {
        if (event.which == 13) {
            $(this).blur();
        }
    });
                                                                    /*END WHEN ENTER IS PRESSED ON NEW TASK OR NEW GROUP IT WILL BRING ANOTHER NEW TASK--------------------*/

                                                                    /*--------------------START EDIT PERSON*/    
    $(document).on('click', '.person-row', function () {
        personRowId = $(this).attr('sid');
        personsResourcesSetted = $('#person-' + personRowId).html();
        personsResourcesSettedArray=[];
        personsResourcesSettedArray[1]='';
        personsResourcesSettedArray = personsResourcesSetted.split(userResourceSplit);
        personsSetted=personsResourcesSettedArray[0];
        resourcesSetted=personsResourcesSettedArray[1];
        personsSettedArray=personsResourcesSettedArray[0].split(userSplit);
        if(personsResourcesSettedArray[1]!=undefined)
            resourcesSettedArray=personsResourcesSettedArray[1].split(userSplit);
        else
            resourcesSettedArray='';

        $('.persons').each(function () {
            if (jQuery.inArray($(this).parent().prev().html(), personsSettedArray) !== -1)
                $(this).prop('checked', true);
            else
                $(this).prop('checked', false);
        }).get();

        $('.resources').each(function () {
            if (jQuery.inArray($(this).parent().prev().html(), resourcesSettedArray) !== -1)
                $(this).prop('checked', true);
            else
                $(this).prop('checked', false);
        }).get();
        personsIdBefore = $('.persons:checkbox:checked').map(function () {
            return $(this).val();
        }).get();
        resourcesIdBefore = $('.resources:checkbox:checked').map(function () {
            return $(this).val();
        }).get();
        $('.popover-person').css({'position': 'absolute !important', 'top': $(this).offset().top+10 + 'px', 'right': $(this).position().left + 215 + 'px', 'display': 'block'});
        $('.popover-person').children('.task-row').children('input').focus();
    });
    $(document).on('click', '.popover-close', function () {
        $('.popover-person').css({'position': 'absolute !important', 'top': $(this).position().top + 73 + 'px', 'right': $(this).position().left + 215 + 'px', 'display': 'none'});
    });
    $(document).on('click', '.set-persons', function () {
        if((parseInt(permition) & rulesUser['setPerson'])>0)
        {
            personsId = $('.persons:checkbox:checked').map(function () {
                return $(this).val();
            }).get();
            personsRemoveId=[];
            ii=0;
            for(i=0;i<personsIdBefore.length;i++)
        	{
        		if(jQuery.inArray(personsIdBefore[i],personsId)===-1)
        			personsRemoveId[ii++]=personsIdBefore[i];
        	}
            for(j=0;j<personsIdBefore.length;j++)
            {
                removeItem=personsIdBefore[j];
                personsId = jQuery.grep(personsId, function(value) {
                    return value != removeItem;
                });
            }

            resourcesId = $('.resources:checkbox:checked').map(function () {
                return $(this).val();
            }).get();
            resourcesRemoveId=[];
            ii=0;
            for(i=0;i<resourcesIdBefore.length;i++)
        	{
        		if(jQuery.inArray(resourcesIdBefore[i],resourcesId)===-1)
        			resourcesRemoveId[ii++]=resourcesIdBefore[i];
        	}
            for(j=0;j<resourcesIdBefore.length;j++)
            {
                removeItem=resourcesIdBefore[j];
                resourcesId = jQuery.grep(resourcesId, function(value) {
                    return value != removeItem;
                });
            }
            console.log('rrrrrrrrrrrrrrrrrrrrrrrr'+resourcesRemoveId);
            console.log(personsId+' personssssssss '+ resourcesId+' resourcesssssssssssssss');
            personsName = $('.persons:checkbox:checked').map(function () {
                return $(this).parent().prev().html();
            }).get();
            resourcesName = $('.resources:checkbox:checked').map(function () {
                return $(this).parent().prev().html();
            }).get();
            personsStringName = personsName.join(userSplit);
            resourcesStringName = resourcesName.join(userSplit);
            if(resourcesName!='')
                personsResourcesStringName = personsName.join(userSplit)+userResourceSplit+resourcesName.join(userSplit);
            else
                personsResourcesStringName = personsName.join(userSplit);
            $('#person-' + personRowId).html(personsResourcesStringName);
            $('#person-' + personRowId).attr('title', personsResourcesStringName);

            $('#schedule-row-' + personRowId + ' .task-persons').html(personsResourcesStringName);
            $('.popover-person').css({'display': 'none'});
            flgResources=0;
            flgPersons=0;
            if (personsSetted != personsStringName)
            {
                flgPersons=1;
            }
            if (resourcesSetted != resourcesStringName)
            {
                flgResources=1;
            }
            if(flgPersons==1||flgResources==1)
            {
                console.log(personsIdBefore);
                console.log(personsId);
                setPerson();
                console.log(personsStringName + ' must change in task row with id: ' + personRowId);
            }


            console.log('set persons is clicked');
        }
        else
        {
            accessDenied(keyModal+'67','عدم دسترسی به تغییر اشخاص');
        }
    });
                                                                    /*END EDIT PERSON--------------------*/
    
                                                                    /*--------------------START EDIT PERCENT*/    
    $(document).on('click', '.percent-row', function () {
        if((parseInt(permition) & rulesUser['editPercent'])>0  || userTaskPermition(id))
        {
            id = $(this).attr('sid');
            if ($('#input-percent-' + id).css('display') == 'none')
            {
                taskame = $('#percent-' + id).html();
                $('#percent-' + id).hide();
                $('#input-percent-' + id).show();
                //$('#input-percent-'+id).addClass('input-task');
                $('#input-percent-' + id).focus();
                $('#input-percent-' + id).select();
                $('#input-percent-' + id).val(taskame);
            }
        }
        else
        {
            accessDenied(keyModal+'68','عدم دسترسی به تغییر پیشرفت');
        }
        
    });
    $(document).on('blur', '.input-percent', function () {
        id = $(this).attr('sid');
        newValue = $(this).val();
        $('#percent-' + id).show();
        $('#percent-' + id).html(newValue);
        $(this).hide();
        $('#schedule-' + id + ' div div').css('width', newValue + '%');
        editPercent($(this),id);
    });
    $(document).on('keypress', '.input-percent', function (event) {
        if (event.which == 13) {
            id = $(this).attr('sid');
            newValue = $(this).val();
            $('#percent-' + id).show();
            $('#percent-' + id).html(newValue);
            $(this).hide();
            console.log('percent keypress setted');
        }
        key = String.fromCharCode(event.which);
        value = $(this).val();
        if (value.length > 2 || isNaN(key) || (value.length > 1 && key != '0') || (value.length > 1 && value > 10))
        {
            return false;
        }
        else if (isNaN)
        {

        }
        console.log('bozorgtar az 333333333333333');
    });
    $(document).on('mouseover', '.percent', function () {
        
            $(this).css('cursor','pointer');
            if (!$(this).is('.ui-resizable'))
            $(this).resizable({
                axis: 'x',
                handles: "w",
                minWidth: 2,
                start: function () {
                    if((parseInt(permition) & rulesUser['editPercent'])==0 && !userTaskPermition(id))
                    {
                        accessDenied(keyModal+'97','عدم دسترسی به تغییر پیشرفت');
                        return false;
                    }

                },
                resize: function () {
                    $(this).css({'right': '0'});
                    id = $(this).attr('sid');
                    parentWidth = $(this).parent().width();
                    console.log(parentWidth);
                    if (parentWidth < $(this).width())
                    {
                        $(this).width(parentWidth);
                    }
                    else
                    {
                        a = $(this).width();
                        b = $(this).parent().width();
                        c = a / b;
                        d = c * 100;
                        d = Math.round(d);
                        $('#percent-' + id).html(d);
                        $(this).css('width', d + '%');
                    }
                },
                stop: function () {
                    $(this).css({'right': '0'});
                    id = $(this).attr('sid');
                    parentWidth = $(this).parent().width();
                    console.log(parentWidth);
                    if (parentWidth < $(this).width())
                    {
                        $(this).width(parentWidth);
                    }
                    else
                    {
                        a = $(this).width();
                        b = $(this).parent().width();
                        c = a / b;
                        d = c * 100;
                        d = Math.round(d);
                        $('#percent-' + id).html(d);
                        $(this).css('width', d + '%');
                    }
                    editPercent($(this),id);
                }
            });
            $(this).css('cursor','pointer');
//        }
//        else
//        {
//            $(this).css('cursor','default');
//        }
    });
                                                                    /*END EDIT PERCENT--------------------*/

//    6
                                                                    /*--------------------START DRAG TASK*/
    $(document).on('mouseover', ".resize-drag", function () {
        
            $(this).css('cursor','pointer');
            if (!$(this).is('.ui-draggable')&&(groupDragg==false))
                $(this).css('cursor','pointer');
            $(this).draggable({
                axis: 'x',
                grid: [resizeDragX, resizeDragY],
                containment: '.schedule-rows',
                start: function (e, ui) {
                    if((parseInt(permition) & rulesUser['dragTask'])==0)
                    {
                        accessDenied(keyModal+'69','عدم دسترسی به تغییر تاریخ کار');
                        return false;   
                    }
                    $(this).parent().find('.right-tool , .left-tool').css({'display': 'none'});
                    x = 0;
                    //draggable = true;
                    x = (e.pageX - $(this).offset().left) - $(this).width() + 30;
                    scheduleGroup = $(this).parent().parent().parent();
                    scheduleGroupId=scheduleGroup.attr('id');
                },
                drag: function (e, ui) {
                    parent = $(this).parent();
                    $(this).css('cursor', 'none');
                    //console.log(pos);
                    //console.log(($('#content-schedule').scrollLeft()+$('#schedule-rows').width())-$('#schedule-rows').width());
                    pos = $('.schedule-content').width() - $(this).width() - $(this).position().left + 1327 + ($('#content-schedule').scrollLeft() - $('.schedule-rows').width());
                    pos1 = $('.schedule-content').width() - $(this).width() - $(this).position().left;
					/*mod=pos%resizeDragX;
					mod1=pos1%resizeDragX;
					pos=pos-mod;
					pos1=pos1-mod1;*/
					pos+=1;
					pos1+=1;
					console.log(pos+":"+pos1);
                    x = Math.abs(x);
                    $('.date-drag').css({'position': 'absolute !important', 'top': $(this).offset().top + 10 + 'px', 'right': pos + x, 'display': 'block'});
                    $('.date-drag').children('.date-show').html(scrollDate[pos1] + scheduleDateSpliter +'<span dir="rtl">'+ scrollDate[pos1 + $(this).width()]+'</span>');

                    $('#schedule-row-' + id).removeClass('row-hover');
                    $('#percent-row-' + id).removeClass('row-hover');
                    $('#person-' + id).removeClass('row-hover');
                    $('#task-' + id).removeClass('row-hover');

                    /*cell that dragged with this element*/
                    $('.drag-resize-cell').css({'position': 'absolute !important', 'display': 'block', 'right': pos1-2, 'width': $(this).width()});


                    draggable = true;
                    mouseMove = false;
                },
                stop: function () {
                    draggable = false;

                    parent = $(this).parent();
                    $(this).css({'cursor': 'pointer', 'top': 'auto'});

                    //$(this).css({'left':'auto','right':'7000px'});
                    $('#schedule-row-' + id).addClass('row-hover');
                    $('#percent-row-' + id).addClass('row-hover');
                    $('#person-' + id).addClass('row-hover');
                    $('#task-' + id).addClass('row-hover');

                    $('.date-drag').css({'display': 'none'});
                    $('.drag-resize-cell').css('display', 'none');
                    updateToolbar($(this), parent);
                    $(this).parent().find('.right-tool , .left-tool').css({'display': 'block'});

                    taskRow = $('#task-' + id);
                    id = taskRow.attr('sid');

                    dragTask($(this));
                    refreshStartEnd(scheduleGroupId);
                }
            });
//        }
//        else
//        {
//            $(this).css('cursor','default');
//        }
    });
                                                                    /*END DRAG TASK--------------------*/

                                                                    /*--------------------START DRAG GROUP*/
    $(document).on('mouseover', ".resize-drag-group", function () {
        
            $(this).css('cursor','pointer');
            if (!$(this).is('.ui-draggable'))
            $(this).draggable({
                axis: 'x',
                grid: [resizeDragX, resizeDragY],
                containment: '.schedule-group',
                start: function (e, ui) {
                    if((parseInt(permition) & rulesUser['dragGroup'])==0)
                    {
                        accessDenied(keyModal+'70','عدم دسترسی به تغییر تاریخ گروه');
                        return false;
                    }
                    mthis=ui.item;
                    draggGroup=true;
                    dragg=-1;
                    startGroupRight=pos1 = $('.schedule-content').width() - $(this).width() - $(this).position().left;
                    x = 0;
                    draggable = true;
                    scheduleGroup = $(this).parent().parent().parent();
                    scheduleGroupId=scheduleGroup.attr('id');
                    x = (e.pageX - $(this).offset().left) - $(this).width() + 30;
                    $('#'+scheduleGroupId+' .schedule-row').each(function(){
                        id=$(this).attr('sid');
                         $('#percent-row-'+id).show();
                        $('#person-'+id).show();
                        $('#task-'+id).show();
                        $(this).show();
                    });

                    // console.log($('.schedule-content').width() +"-"+ mthis.width() +' - '+ mthis.position().left +' x'+resizeDragX+' y'+resizeDragY)
                },
                drag: function (e, ui) {

                    console.log($('.schedule-content').width() +"-"+ $(this).width() +' - '+$(this).position().left +' x'+resizeDragX+' y'+resizeDragY)
                    //console.log(pos);
                    //console.log(($('#content-schedule').scrollLeft()+$('#schedule-rows').width())-$('#schedule-rows').width());
                    /*mod=pos%resizeDragX;
                    mod1=pos1%resizeDragX;
                    pos=pos-mod;
                    pos1=pos1-mod1;*/
                    $(this).css('cursor', 'none');
                    pos = $('.schedule-content').width() - $(this).width() - $(this).position().left + 1327 + ($('#content-schedule').scrollLeft() - $('.schedule-rows').width());
                    pos1 = $('.schedule-content').width() - $(this).width() - $(this).position().left;
                    pos+=1;
                    pos1+=1;
                    x = Math.abs(x);

                    $('.date-drag').css({'position': 'absolute !important', 'top': $(this).offset().top + 'px', 'right': pos + x, 'display': 'block'});
                    $('.date-drag').children('.date-show').html(scrollDate[pos1] + scheduleDateSpliter +'<span dir="rtl">'+ scrollDate[pos1 + $(this).width()]+'</span>');

                    /*$('#schedule-row-' + id).removeClass('row-hover');
                    $('#percent-row-' + id).removeClass('row-hover');
                    $('#person-' + id).removeClass('row-hover');
                    $('#task-' + id).removeClass('row-hover');*/

                    /*cell that dragged with this element*/
                    $('.drag-resize-cell').css({'position': 'absolute !important', 'display': 'block', 'right': pos1-2, 'width': $(this).width()});

                    draggable = true;
                    mouseMove = false;
                },
                stop: function () {
                    console.log('stop drag group');
                    scheduleGroup = $(this).parent().parent().parent();
                    scheduleGroupId=scheduleGroup.attr('id');

                    taskRow = $('#task-' + id);
                    id = taskRow.attr('sid');
                    mythis=$(this);
                    pos = $('.schedule-content').width() - mythis.width() - mythis.position().left + 1327 + ($('#content-schedule').scrollLeft() - $('.schedule-rows').width());
                    pos1 = $('.schedule-content').width() - mythis.width() - mythis.position().left;

                    divi=(pos1)/resizeDragX;
                    divi=Math.round(divi);
                    pos1s=divi*resizeDragX;
                    pos1=pos1s+1;
                    
                    mythis.css('right',pos1-2);

                    console.log($(this).attr('id'))
                    dragGroup($(this));

                    draggGroup=false;
                    draggable = false;
                }
            });
    });
                                                                    /*END DRAG GROUP--------------------*/

                                                                    /*--------------------START RESIZE TASK*/
    $(document).on('mouseover', ".resize-drag:not(.mile)", function () {
//        if((parseInt(permition) & rulesUser['dragTask'])>0)
//        {
            $(this).css('cursor','pointer');
            if (!$(this).is('.ui-resizable'))
            $(this).resizable({
                axis: 'x',
                grid: [resizeDragX],
                handles: "w",
                start: function () {
                    if((parseInt(permition) & rulesUser['dragTask'])==0)
                    {
                        accessDenied(keyModal+'71','عدم دسترسی به تغییر تاریخ کار');
                        return false;   
                    }
                    resizable = true;
                    mouseMove = false;
                    $(this).parent().find('.right-tool , .left-tool').css({'display': 'none'});
                    scheduleGroup = $(this).parent().parent().parent();
                    scheduleGroupId=scheduleGroup.attr('id');
                },
                resize: function () {
                    $('#schedule-row-' + id).removeClass('row-hover');
                    $('#percent-row-' + id).removeClass('row-hover');
                    $('#person-' + id).removeClass('row-hover');
                    $('#task-' + id).removeClass('row-hover');

                    pos1 = $('.schedule-content').width() - $(this).width() - $(this).position().left;
                    $('.drag-resize-cell').css({'position': 'absolute !important', 'display': 'block', 'right': pos1-1, 'width': $(this).width()});
                },
                stop: function () {
                    resizable = false;

                    parent = $(this).parent();

                    $('#schedule-row-' + id).addClass('row-hover');
                    $('#percent-row-' + id).addClass('row-hover');
                    $('#person-' + id).addClass('row-hover');
                    $('#task-' + id).addClass('row-hover');

                    pos = $('.schedule-content').width() - $(this).width() - $(this).position().left + 940 + ($('#content-schedule').scrollLeft() - $('.schedule-rows').width());
                    parent.children('.left-tool').css({'right': pos + $(this).width() + 11 + 'px'});

                    updateToolbar($(this), parent);
                    $(this).parent().find('.right-tool , .left-tool').css({'display': 'block'});
                    $('.drag-resize-cell').css('display', 'none');

                    scheduleGroupMinRight=999999999;
                    scheduleGroupMaxRightWidth=0;
                    minRight=999999999;
                    $('#'+scheduleGroupId+' .schedule-row[show="true"]:not(:first-child):not(:last-child) .resize-drag').each(function(){
                        right=$(this).css('right');
                        right=parseInt(right);
                        if(minRight>right)
                        {
                            minRight=right;
                        }
                        width=$(this).css('width');
                        width=parseInt(width);
                        widthRight=right+width;
                        if(right<scheduleGroupMinRight)
                            scheduleGroupMinRight=right;
                        if(scheduleGroupMaxRightWidth<widthRight)
                        {
                            scheduleGroupMaxRightWidth=widthRight;
                        }

                    });
                    console.log('g right: '+scheduleGroupMaxRightWidth)
                    scheduleGroupMaxRightWidth-=minRight;
                    $('#'+scheduleGroupId+' .schedule-row:first-child .resize-drag-group').css({'right':scheduleGroupMinRight+'px','width':scheduleGroupMaxRightWidth+'px'});
                    
                    resizeTask($(this));

                    console.log(width1+" true width: "+width+" start:"+pos1);
                },
                minWidth: 8
            });
            $(this).css('cursor','pointer');
    });
                                                                    /*END RESIZE TASK--------------------*/

//    7
//    8
                                                                    /*--------------------START REMOVE FILE*/
    $(document).on('click', '.file-link .remove-file', function () {
        if((parseInt(permition) & rulesUser['removeFile'])>0)
        {
            mthis=$(this);
            fileName=mthis.attr('file-name');
            fileId=mthis.attr('sid');
            deleteType='file';
            parent = $(this).parent();
            parent.children('input');
            deleteId=fileId;
            deleteName=fileName;
            deleteThisPrev=parent.parent().attr('prev');
            deleteNextId=parent.parent().next().attr('sid');
            $("#delete-modal .modal-body").html('آیا مایل به حذف فایل " '+deleteName+'" این هستید؟ ');
            $("#delete-modal").modal();
            $("#delete-modal").modal({keyboard: false});
            $("#delete-modal").modal('show');
            deleteTaskGroupTable='tasksfile';
            deleteType='tasksfile';
        }
        else
        {
            accessDenied(keyModal+'72','عدم دسترسی به حذف فایل');
            $(this).css('cursor','default');
        }
    });
                                                                    /*END REMOVE FILE--------------------*/

                                                                    /*--------------------START UPLOAD FILE*/
    $('#task-file').change(function(){
        if((parseInt(permition) & rulesUser['upload'])>0)
        {
            var file = this.files[0];
            name = file.name;
            size = file.size;
            type = file.type;
            type = name.split('.').pop().toLowerCase();

            /* case 'image/png':
            case 'image/gif':
            case 'image/jpeg':
            case 'image/pjpeg':
            case 'text/plain':
            case 'text/html': //html file
            case 'application/x-zip-compressed':
            case 'application/zip':
            case 'application/pdf':
            case 'application/vnd.ms-excel':
            case 'application/octet-stream':
            case 'application/msword':
            case 'video/mp4':
            case 'application/vnd.openxmlformats-officedocument.wordprocessingml.document':*/
            flagUpload=true;
            errUpload='';
            /*if(type!='image/png'&&type!='image/gif'&&type!='image/jpeg'&&type!='image/pjpeg'&&type!='text/plain'&&
                    type!='text/html'&&type!='application/x-zip-compressed'&&type!='application/zip'&&type!='application/pdf'&&
                    type!='application/vnd.ms-excel'&&type!='application/octet-stream'&&type!='application/msword'&&
                    type!='video/mp4'&&type!='application/vnd.openxmlformats-officedocument.wordprocessingml.document')
            */
            if($.inArray(type, ['gif','png','jpg','jpeg','pdf','pjpeg','txt','mp4','docx'
                                ,'zip','rar','doc','ppt','xls']) == -1)
            {
                errUpload=fileFormatErr;
                flagUpload=false;
            }
            if(size>5242880)
            {
               errUpload=fileSizeErr;
                flagUpload=false;
            }
            if(name.length<1||name=='undefined')
            {
                errUpload=fileNotSelectedErr;
                flagUpload=false;
            }
            if(name.length>300)
            {
                errUpload=fileBigNameErr;
                flagUpload=false;
            }
            if(flagUpload==false)
            {
                $('#alert-modal').children('.modal-body').html(errUpload);
                //$('#comments').modal('hide');
                $('#alert-modal').modal();
                $('#alert-modal').modal({keyboard: false});
                $('#alert-modal').modal('show');
            }
            if(flagUpload==false)
            {
                $('#alert-modal').children('.modal-body').html(errUpload);
                $('#alert-modal').modal();
                $('#alert-modal').modal({keyboard: false});
                $('#alert-modal').modal('show');
                return false;
            }
            //flagUpload=false;
            //errUpload=sh4+'فایلی انتخاب کنید'+eh4;
            $('progress').css('display','block');
            var fileData = $('#task-file').prop('files')[0];
            var formData = new FormData();
            formData.append('file', fileData);
            formData.append('taskId', taskId);
            formData.append('user', userLogin);
            formData.append('userName', userName);
            formData.append('projectId',projectId );
            console.log(formData);
            ajaxStart();
            $.ajax({
                url: URL + 'ajaxes/upload.php',
                type: 'POST',
                xhr: function() {  // Custom XMLHttpRequest
                    var myXhr = $.ajaxSettings.xhr();
                    if(myXhr.upload){ // Check if upload property exists
                        myXhr.upload.addEventListener('progress',progressHandlingFunction, false); // For handling the progress of the upload
                    }
                    return myXhr;
                },
                //Ajax events
                beforeSend: function(){

                },
                success: function(data){
                    console.log('success upload and insert');
                    if(data['newName'].length>0)
                    {
                        newName=data['newName'];
                        oldName=data['oldName'];
                        fileId=data['fileId'];
                        tag="<div class='file-link'><i class='icon-remove remove-file' id='file"+fileId+"' sid='"+fileId+"' file-name='"+newName+"'></i>"+"<a target='_blank' href='"+URL+"downloadTFile/"+newName+"/"+oldName+"' class='thumbnail file-link' title='توسط: "+userName+"'>"+
                                oldName+"</a></div>";
                        uploads= $('#uploads');
                        uploads.children('.files').html(tag+uploads.children('.files').html());
                        $('progress').css('display','none');
                        $('#task-file').val('');
                        console.log('add file link to files link');
                    }
                    ajaxEnd();
                },
                //error: errorHandler,
                // Form data
                //Options to tell jQuery not to process data or worry about content-type.
                cache: false,
                data: formData,
                dataType:'json',
                contentType: false,
                processData: false,
                error:function(data){
                    ajaxEnd();
                    console.log(data);
                    myPrompt('uploadFile');
                }

            });
        }
        else
        {
            accessDenied(keyModal+'73','عدم دسترسی آپلود فایل');
            $(this).css('cursor','default');
        }
    });
    $('#task-file').click(function() {
        if((parseInt(permition) & rulesUser['upload'])==0)
        {
            accessDenied(keyModal+'74','عدم دسترسی به آپلود فایل');
            return false;
        }
    });
    function progressHandlingFunction(e){
        if(e.lengthComputable){
            $('progress').attr({value:e.loaded,max:e.total});
        }
    }
                                                                    /*END UPLOAD FILE--------------------*/
    
                                                                    /*--------------------START REMOVE COMMENT*/
    $(document).on('click', '#comments .comment-row .icon-remove', function () {
        if((parseInt(permition) & rulesUser['removeComment'])>0)
        {
            $('#delete-modal').children('.modal-body').html(commentDelete);
            $('#delete-modal').modal();
            $('#delete-modal').modal({keyboard: false});
            $('#delete-modal').modal('show');

            deleteType='comment';
            mthis=$(this);
            commentId=mthis.attr('sid');


        }
        else
        {
            accessDenied(keyModal+'75','عدم دسترسی به حذف کامنت');
            $(this).css('cursor','default');
        }
    });
                                                                    /*END REMOVE COMMENT--------------------*/

                                                                    /*--------------------START NEW COMMENT*/
    $(document).on('click', '#comments .insert-comment', function () {
        if((parseInt(permition) & rulesUser['newComment'])>0)
        {
            parent = $(this).closest('#comments');

            percent = $('#percent-' + taskId).html();
            comment=parent.children('textarea').val();
            parent.children('textarea').val('');
            console.log(comment);
            if(comment!='')
            {
                insertComment();
            }

            console.log('click on insert message\ this comment.id: ' + taskId);
        }
        else
            accessDenied(keyModal+'76','عدم درسترسی به درج کامنت');
    });
                                                                    /*END NEW COMMENT--------------------*/

//    9
                                                                    /*--------------------START SET TIMELINE*/    
    $(document).on('mousemove', '.schedule-row', function (event) {
        id = $(this).attr('sid');
        if ($(this).attr('show') == 'false' && draggable == false && resizable == false)
        {
            var parentOffset = $(this).parent().offset();
            mouseX = parentOffset.left - event.pageX;

            /*$('.schedule-content').width()-$(this).width()-$(this).position().left;
             chrome=$(this).width()+mouseX+($('#content-schedule').scrollLeft()-$('.schedule-rows').width());
             firefox=$(this).width()+mouseX+($('#content-schedule').scrollLeft());
             margin=browser(chrome,firefox);*/
            margin = mouseX;
            margin += $('.schedule-rows').width();
            //console.log($(this).find('.right-tool').width());
            mod = margin % resizeDragX;
            margin -= mod;
            $('#schedule-' + id).css({'display': 'block', 'right': margin - 2, 'width': resizeDragX});
            //console.log($(this).children('div').children('.resize-drag').children('.ui-resizable-w').width());
            $(this).find('.right-tool , .left-tool').css({'display': 'none'});
            $(this).children('div').children('.resize-drag').children('.ui-resizable-w').attr('style', 'width:100% !important;z-index:90;');
            ths = $(this).find('.resize-drag');
            updateToolbar(ths, $(this).children('div'));
            mouseMove = true;
            $(this).find('.right-tool > i , .left-tool > i').css({'display': 'none'});
        }
        else
            $(this).find('.right-tool > i , .left-tool > i').css({'display': 'inline-block'});
    });
    $(document).on('mouseleave', '.schedule-row', function (event) {
        id = $(this).attr('sid');
        if ($(this).attr('show') == 'false')
            $('#schedule-' + id).css({'display': 'none'});
        $(this).children('.ui-resizable-w').css({'width': '5px'});
        if ($(this).attr('show') == 'false')
            $(this).find('.right-tool i , .left-tool i').css({'display': 'none'});
        else
            $(this).find('.right-tool i , .left-tool i').css({'display': toolDisplay});
            
    });
    $(document).on('mouseup', '.schedule-row', function (event) {
        id = $(this).attr('sid');
        if (creating == true && $('#schedule-' + id).css('display') == 'none')
        {
            var parentOffset = $(this).parent().offset();
            mouseEndX = event.pageX - parentOffset.left;
            mouseEndY = event.pageY - parentOffset.top;

            margin = mouseStartX;
            width = mouseStartX - mouseEndX;
            margin = $(this).width() - mouseStartX + 950 + ($('#content-schedule').scrollLeft() - $('.schedule-rows').width());
            mod = width % resizeDragX;
            width -= mod;
            mod = margin % resizeDragX;
            margin -= mod;

            $('#schedule-' + id).css({'display': 'block', 'right': margin, 'width': width});
            //$(this).html(timeLine);
            console.log('create timeline: mouse down and drag' + mouseStartX + ' ' + mouseStartY + ' ' + mouseEndX + ' ' + mouseEndY + ' ' + margin + ' ');
            creating = false;
        }
    });
    $(document).on('mousedown', '.schedule-row .resize-drag', function (event) {
        mythis=$(this).parent().parent();
        if((parseInt(permition) & rulesUser['dragTask'])==0 && event.target.className!='ico comment'  && !userTaskPermition(id))
        {
            mythis.mouseup();
//            console.log();
            accessDenied(keyModal+'77','عدم دسترسی به تغییر پیشرفت');
//            return false;   
        }
        else// if(event.target.className!='ico comment')
        {
            id = mythis.attr('sid');
            if ($('#schedule-' + id).css('display') == 'none' || mouseMove == true && mythis.attr('show') == 'false')
            {
                mythis.attr('show', 'true');
                var parentOffset = mythis.parent().offset();

                mouseX = parentOffset.left - event.pageX;
                margin = mouseX;
                margin += $('.schedule-rows').width();

                mod = margin % resizeDragX;
                margin -= mod;
                $('#schedule-' + id).css({'display': 'block', 'right': margin - 1, 'width': resizeDragX});
                mythis.find('.right-tool , .left-tool').css({'display': 'block'});
                mythis.children('div').children('.resize-drag').children('.ui-resizable-w').attr('style', 'width:7px;z-index:90;');

                updateToolbar(mythis.find('.resize-drag'), mythis.children('div'));
                console.log('schedule has staticed');

                //scheduleStaticed=true;
                taskRow = $('#task-' + id);
                id = taskRow.attr('sid');

                scheduleGroup = mythis.parent();
                scheduleGroupId=scheduleGroup.attr('id');
                scheduleGroupMinRight=999999999;
                scheduleGroupMaxRightWidth=0;
                minRight=999999999;
                $('#'+scheduleGroupId+' .schedule-row[show="true"]:not(:first-child):not(:last-child) .resize-drag').each(function(){
                    right=$(this).css('right');
                    right=parseInt(right);
                    if(minRight>right)
                    {
                        minRight=right;
                    }
                    width=$(this).css('width');
                    width=parseInt(width);
                    widthRight=right+width;
                    if(right<scheduleGroupMinRight)
                        scheduleGroupMinRight=right;
                    if(scheduleGroupMaxRightWidth<widthRight)
                    {
                        scheduleGroupMaxRightWidth=widthRight;
                    }

                });
                console.log('g right: '+scheduleGroupMaxRightWidth)
                scheduleGroupMaxRightWidth-=minRight;
                $('#'+scheduleGroupId+' .schedule-row:first-child .resize-drag-group').css({'right':scheduleGroupMinRight+'px','width':scheduleGroupMaxRightWidth+'px'});
                fixeTimeline(mythis);
            }
        }

    });
                                                                    /*END SET TIMELINE--------------------*/
    
                                                                    /*--------------------START HIDDING OR SHOWING TASKS*/    
    $(document).on('click', '.icon-group', function () {
        group = $(this).parent().parent();
        id = group.children('.group-row').attr('sid');
        if (group.children('.groups-task').is(':visible'))
        {
            group.children('.groups-task').hide();
            group.children('.news').hide();
            $('#person-group' + id + ' .person-row:not(:last-child)').hide();
            $('#percent-group' + id + ' .percent-row:not(:last-child)').hide();
            $('#schedule-group' + id + ' .schedule-row:not(:last-child)').hide();
//            $(this).removeClass('icon-minus');
            $(this).html('add');
        }
        else
        {
            group.children('.groups-task').show();
            group.children('.news').show();
            $('#person-group' + id + ' .person-row:not(:last-child)').show();
            $('#percent-group' + id + ' .percent-row:not(:last-child)').show();
            $('#schedule-group' + id + ' .schedule-row:not(:last-child)').show();
            $(this).html('remove');
//            $(this).removeClass('icon-plus');
        }
    });
                                                                    /*END HIDDING OR SHOWING TASKS--------------------*/
    
                                                                    /*--------------------START SHOW COMMENTS*/    
    $(document).on('click', '.schedule-row .left-tool .comment', function () {
        mthis = $(this).parent();
        taskId = mthis.parent().parent().attr('sid');
        $('#comments #myModalLabel').html('' + $('#task-name-' + taskId).html());
        $("#comments").modal();
        $("#comments").modal({keyboard: false});
        $("#comments").modal('show');
        parent = $('#comments');
        html=noCommentErr;
        parent.find('.modal-body').html(html);
        selectComments(taskId);
        console.log('click on right-tool message icon');
    });
                                                                    /*END SHOW COMMENTS--------------------*/
    
                                                                    /*--------------------START SHOW FILES*/    
    $(document).on('click', '.left-tool .upload', function () {
        mthis = $(this).parent();
        taskId = mthis.parent().parent().attr('sid');
        $('#uploads #myModalLabel').html('' + $('#task-name-' + taskId).html());
        $("#uploads").modal();
        $("#uploads").modal({keyboard: false});
        $("#uploads").modal('show');
        parent = $('#uploads');
        html=noCommentErr;
        //parent.find('.modal-body').html(html);
        selectFiles(taskId);
    });
                                                                    /*END SHOW FILES--------------------*/
    
                                                                    /*--------------------START HIDE OF SHOW GROUP TASKS*/    


});
