
/*START VARS*/
scheduleGridX = 41;
scheduleGridY = 41;

scheduleCell = 41;

resizeDragX = 42;
resizeDragY = 42;


/*------------STATIC VARS------------*/
var sortableTasks = false;
var changeGroup = false;
/*END VARS \\\\\\\\\\*/

(function ($) {
    'use strict';
    var definer = $('<div dir="rtl" id="qwert" style="font-size: 14px; width: 1px; height: 1px; position: absolute; top: -1000px; overflow: scroll">A</div>')
    .appendTo('body')[0],
        type = 'reverse';
	definer=$('<div dir="rtl" id="qwert" style="font-size: 14px; width: 1px; height: 1px; position: absolute; top: -1000px; overflow: scroll">A</div>')
	.appendTo('body')[0];
    if (definer.scrollLeft > 0) {
        type = 'default';
    } else {
        definer.scrollLeft = 1;
        if (definer.scrollLeft === 0) {
            type = 'negative';
        }
    }

    $(definer).remove();
    $.support.rtlScrollType = type;
}(jQuery));


// scrollLeft replacement

var origScrollLeft = jQuery.fn.scrollLeft;
jQuery.fn.scrollLeft = function(isc) {
    var value = origScrollLeft.apply(this, arguments);

    if (isc === undefined) {
        switch(jQuery.support.rtlScrollType) {
            case "negative":
                return value + this[0].scrollWidth - this[0].clientWidth;
            case "reverse":
                return this[0].scrollWidth - value - this[0].clientWidth;
        }
    }
    return value;
};

/*START NEEDS AT FIRST RUN*/
$(function () {

    $('.resize-drag').css({'min-width': resizeDragX});
    $('.schedule-cell').css({'width': scheduleCell});
    $('.schedule-date').css({'width': scheduleCell});
    $('.schedule-row').css({'width': ((scheduleGridX + 1) * daysNumber)});
    $('.date-content').css({'width': ((scheduleGridX + 1) * daysNumber)+scheduleGridX+1});
    $('.schedule-content').css({'width': ((scheduleGridX + 1) * daysNumber)});
    $('#innerScrollbar-schedule').css({'width': (scheduleGridX * daysNumber) + 100});

    $('.left-tool img ,.right-tool img').hide();

    scrolll=Math.abs(scrollSchedule - ((scheduleCell * daysNumber) - 350));
    brs=browser();
    if(brs=='firefox')
	{
		$('#scrollbar-schedule').scrollLeft(-scrollSchedule + 350);
	}
    else
    	$('#scrollbar-schedule').scrollLeft(scrolll);
    $('.row-fluid .span9').css('width',$('body').width()-$('.right-body').width()-2);
});
/*END NEEDS AT FIRST RUN \\\\\\*/
$(window).resize(function(){
    $('.row-fluid .span9').css('width',$('body').width()-$('.right-body').width()-2);
});
$(window).scroll(function () {
    //$('.navbar-wrapper').hide();
    $('.dropdown').removeClass('open');
   // $('.date-content').css('margin-top', $(window).scrollTop() + 'px');
    ganttWindowScrollTop = $(window).scrollTop();
    if (ganttWindowScrollTop > 0)
    {
        //$('#date-container').css({'margin-top':ganttScrollTop+ganttWindowScrollTop});
    }
    else
    {
        //$('#date-container').css({'margin-top':0});
        //$('#scrollbar-schedule').css('top', '45px');
    }
    $('#scrollbar-task').css({'margin-right': 15 + $(this).scrollLeft() + 'px'});
    $('#scrollbar-schedule').css({'margin-right': $(this).scrollLeft() + 'px'});
    console.log($(this).scrollLeft());
});


/*START scrolls move secoundary scrollbar-schedule position*/
$('.gantt-body').scroll(function () {
   if(browser()!='firefox') {
   	$('.schedule-rows').scrollTop($(this).scrollTop());
   	//$('#content-schedule').scrollTop($(this).scrollTop());
   }
   else
   {
   		$('.schedule-rows').scrollTop($(this).scrollTop());
   	//$('#content-schedule').scrollTop($(this).scrollTop());
   }
    console.log($(this).scrollLeft());
});
$('.schedule-rows').scroll(function () {
    $('.gantt-body').scrollTop($(this).scrollTop());
});
$('.tasks').scroll(function () {
    $('#scrollbar-task').scrollLeft($(this).scrollLeft());
});

function updateScrollbar(content, scrollbarSchedule)
{
    width1 = content.scrollWidth + "px";
    scrollbarSchedule.getElementsByTagName('*')[0].style.width = width1;
    console.log(scrollbarSchedule.getElementsByTagName('*')[0].style.left)
    content.scrollLeft = scrollbarSchedule.scrollLeft;
}
/*END move scroll/////////////////////////////////////////////////*/

/*START hovers on task and schedule/////////////////////////*/
$(document).on('mouseover', '.task-row, .schedule-row, .person-row, .percent-row', function () {
    id = $(this).attr('sid');
    if(!sortHover)
    {
        $('#schedule-row-' + id).addClass('row-hover');
        $('#percent-row-' + id).addClass('row-hover');
        $('#person-' + id).addClass('row-hover');
        $('#task-' + id).addClass('row-hover');
        if ($('#schedule-row-' + id).attr('show') == 'true' && draggable == false && resizable == false)
            $('#schedule-row-' + id + ' img').css('display', 'inline-block');
    }
});

$(document).on('mouseleave', '.task-row, .schedule-row, .person-row, .percent-row', function () {
    if(!sortHover)
    {
        id = $(this).attr('sid');
        $('#schedule-row-' + id).removeClass('row-hover');
        $('#percent-row-' + id).removeClass('row-hover');
        $('#person-' + id).removeClass('row-hover');
        $('#task-' + id).removeClass('row-hover');

        $('#schedule-row-' + id + ' img').css('display', 'none');
    }
});

/*END hover//////\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

/*START change height of container panels to parent (container) when first load and resize container*/

h1 = $('.span3').height();
h2 = $('.span9').height();
h = h1 > h2 ? h1 : h2;
h = h - 70;
//$('.gantt-schedule-cell').css('height',h);

$('.row-fluid').resize(function () {
    height = $('.row-fluid').height();

});
sortHover=false;
function rowHover(ssid,type)
{
    console.log('hover type:  '+ssid+type)
    if(type=0)
    {
        sortHover=false;
        $('#schedule-row-' + ssid).removeClass('row-hover');
        $('#percent-row-' + ssid).removeClass('row-hover');
        $('#person-' + ssid).removeClass('row-hover');
        $('#task-' + ssid).removeClass('row-hover');
    }
    else
    {
        sortHovrer=true;
        $('#schedule-row-' + ssid).addClass('row-hover');
        $('#percent-row-' + ssid).addClass('row-hover');
        $('#person-' + ssid).addClass('row-hover');
        $('#task-' + ssid).addClass('row-hover');    
    }

}
function log(str)
{
    console.log(str);
}
function browser(chrome, firefox, safari, ms, opera)
{
    /**/
    //console.log(navigator.userAgent);
    var isOpera = !!window.opera || navigator.userAgent.indexOf(' OPR/') >= 0;
    // Opera 8.0+ (UA detection to detect Blink/v8-powered Opera)
    var isFirefox = typeof InstallTrigger !== 'undefined';   // Firefox 1.0+
    var isSafari = Object.prototype.toString.call(window.HTMLElement).indexOf('Constructor') > 0;
    // At least Safari 3+: "[object HTMLElementConstructor]"
    var isChrome = !!window.chrome && !isOpera;              // Chrome 1+
    var isIE = /*@cc_on!@*/false || !!document.documentMode; // At least IE6
    if (isOpera) {
        console.log('opera');
        return 'opera';
    } else if (isFirefox) {
        console.log('firefox');
        return 'firefox';
    } else if (isSafari) {
        console.log('safari');
        return 'safari';
    } else if (isIE) {
        return 'ms';
        console.log('ms');
    }
    else if (isChrome) {
        console.log('chrome');
        return 'chrome';
    }
}
function updateToolbar(ths, parent)
{
    pos = $('.schedule-content').width() - ths.width() - ths.position().left;
    ths.css({'left': 'auto', 'right': pos - 2 + 'px'});
    parent.children('.right-tool').css({'right': pos - 23 + 'px'});
    parent.children('.left-tool').css({'right': pos + ths.width() + 1 + 'px'});
}
/*
 function updateTasks()
 {
 if($('.tasks').height()+8>$('.gantt-body').height())
 $('.right-body').css('right','-14px');
 console.log('UPDATE TASKSSSSS'+$('.tasks').height()+">"+$('.gantt-body').height());
 }*/
////////////////////
$(function () {

    /*----------START LEFT AND RRIGHT TOOL-----*/
    /*START EDIT TASKS PANEL\\\\\\\\\\\\\\\\\\*/
    /*START EDIT TASK NAME 1111*/
    $('.input-task').hide();
    $('.input-group').hide();
    taskBlur = false;
    $(document).on('click', '.task-row', function () {
        if((parseInt(permition) & 1)>0)
        {
            id = $(this).attr('sid');
            if ($('#input-task-' + id).css('display') == 'none' && taskBlur == false && taskRemove==false)
            {
                //console.log('taskblur false')
                taskame = $('#task-name-' + id).html();
                $('#task-name-' + id).hide();
                $('#input-task-' + id).show();
                $('#input-task-' + id).addClass('input-task');
                $('#input-task-' + id).focus();
                $('#input-task-' + id).select();
                $('#input-task-' + id).width($('.ctask-group').width());
                $('#input-task-' + id).val(taskame);
                taskBlur = true;
            }
            oldTaskName = $(this).children('input').val();
            if (taskBlur == false)
                console.log(id + ' click');
            else
                console.log('fdsafdsa');
        }
    });

    sid = 400;
    newTask = false;
    $(document).on('blur', '.input-task', function () {
        if((parseInt(permition) & 1)>0)
        {
            taskBlur = false;
            parent = $(this).parent();
            id = parent.attr('sid');
            newValue = $(this).val();
            ths = $(this);

            gid = parent.parent().prev().attr('sid');
            prev = parent.prev();
            previd = prev.attr('sid');
            if (parent.is(':first-child'))
                previd = 0;
            if (newTask)
            {

                if (newValue != '')
                {
                    if(newRowType=='task')
                        type=0;
                    else
                        type=1;
                    //id come from database after insert
                    sid++;
                    ajaxStart();
                    $.ajax({
                        url: URL + 'ajaxes/task.php',
                        type: 'post',
                        data: {action: 'new',projectId:projectId ,type:type , name: newValue, gid: gid, prevId: previd, token: ajaxRequest},
                        dataType: 'json',
                        success: function (data) {
                            console.log('success new task with new id' + data['id']);
                            sid = data['id'];
                            //nextNod=prev.attr('next-nod');
                            //prev.attr('next-nod',sid);
                            //parent.attr('next-nod',nextNod);

                            ths.attr('sid', sid);
                            ths.attr('prev', previd);
                            ths.attr('id', 'input-task-' + sid);
                            parent.attr('sid', sid);
                            parent.attr('id', 'task-' + sid);
                            parent.children('div').attr('id', 'task-name-' + sid);
                            //person
                            /* <div class='person-row' id='person-9999999999' sid='9999999999' >\
                             <div style='height:100%;'>9 pssp</div>\
                             </div>").insertAfter('#person-'+id);
                             */
                            $('#person-9999999999').attr({'id': 'person-' + sid, 'sid': sid});
                            /*
                             $("<div class='percent-row' id='percent-row-9999999999' sid='9999999999' >\
                             <div id='percent-9999999999' sid='9999999999' class='cpercent' style='height:100%;'>19</div>\
                             <input type='text' class='input-percent' sid='9999999999' id='input-percent-9999999999' value='' style='display:none;'>\
                             </div>
                             */
                            $('#percent-row-9999999999').attr({'id': 'percent-row-' + sid, 'sid': sid});
                            $('#percent-9999999999').attr({'id': 'percent-' + sid, 'sid': sid});
                            $('#input-percent-9999999999').attr({'id': 'input-percent-' + sid, 'sid': sid});

                            /*
                             <div class='schedule-row' id='schedule-row-9999999999' sid='9999999999' show='false'>\
                             <div>\
                             <i class='right-tool' style='position:absolute; right:0px;'><img class='ico' src='"+window.location.origin+"/gantt/img/comments2.png'></i>\
                             <div class='resize-drag ui-widget-content' style='display:none;width:23px; right:20px;' id='schedule-9999999999' sid='9999999999'> \
                             <div class='percent80'>\
                             <div class='percent' style='width:1%;' sid='9999999999'></div>\
                             </div>\
                             </div>\
                             <i class='left-tool' style='position:absolute; right:47px;'><img class='ico' src='"+window.location.origin+"/gantt/img/comments2.png'></i>\
                             </div> \
                             </div>*/
                            $('#schedule-row-9999999999').attr({'id': 'schedule-row-' + sid, 'sid': sid});
                            $('#schedule-9999999999 div .percent').attr({'sid': sid});
                            $('#schedule-9999999999').attr({'id': 'schedule-' + sid, 'sid': sid});
                            id = sid;
                            parent.parent().next().children('.new-task[type="'+newRowType+'"]').click();
                            ajaxEnd();
                        },error:function(){
                            ajaxEnd();
                            myPrompt('newTask');
                        }
                    });

                }
                else
                {
                    parent.remove();
                    $('#schedule-row-9999999999').remove();
                    $('#percent-row-9999999999').remove();
                    $('#person-9999999999').remove();
                }
                console.log('new task');
            }
            else
            {
                if (oldTaskName != newValue&&newValue!='')
                {
                    ths = $(this);

                    taskRow = $('#task-' + id);
                    id = taskRow.attr('sid');
                    newValue = $('#input-task-' + id).val();
                    if (newValue == '')
                        newValue = $('#task-name-' + id).html();
                    $.ajax({
                        url: URL + 'ajaxes/task.php',
                        type: 'post',
                        data: {action: 'editName',projectId:projectId , name: newValue, id: id, token: ajaxRequest},
                        dataType: 'json',
                        success: function (data) {
                            console.log('success edit');
                          ajaxEnd();
                        },error:function(){
                            ajaxEnd();
                            myPrompt('changeName');
                        }
                    });
                    console.log('edit task ' + id);
                }
            }
            ths.hide();
            $('#task-name-' + id).show();
    		if(newValue!='')
            	$('#task-name-' + id).html(newValue);
            updateScrollbar(document.getElementById('tasks'), document.getElementById('scrollbar-task'));
            $('.ctask-group').css('width', $('#innerScrollbar-task').width());
            console.log(id + ' blur');
            newTask = false;
        }
    });
    function editTask(id)
    {
        taskRow = $('#task-' + id);
        id = taskRow.attr('sid');
        newValue = $('#input-task-' + id).val();
        if (newValue == '')
            newValue = $('#task-name-' + id).html();
        gid = taskRow.parent().prev().attr('sid');
        prev = taskRow.prev();
        previd = prev.attr('sid');
        if (taskRow.is(':first-child'))
            previd = 0;

        percent = $('#percent-' + id).html();
        person = $('#person-' + id).html();
        pos = $('.schedule-content').width() - $('#schedule-' + id).width() - $('#schedule-' + id).position().left + 1327 + ($('#content-schedule').scrollLeft() - $('.schedule-rows').width());
        pos1 = $('.schedule-content').width() - $('#schedule-' + id).width() - $('#schedule-' + id).position().left;

        divi=(pos1)/resizeDragX;
        divi=Math.round(divi);
        pos1s=divi*resizeDragX;
        pos1=pos1s+1;

        startDate = scrollDate[pos1];
        endDate = scrollDate[pos1 + $('#schedule-' + id).width()];
        width=$('#schedule-' + id).width();

        divi=(width)/resizeDragX;
        divi=Math.round(divi);
        width1=divi*resizeDragX;
        width=width1+1;

        $('#schedule-' + id).width(width-2);
        $('#schedule-' + id).css('right',pos1-2);
        console.log(width1+" true width: "+width+" start:"+pos1);
        ajaxStart();
        $.ajax({
            url: URL + 'ajaxes/task.php',
            type: 'post',
            data: {action: 'edit',projectId:projectId , name: newValue, gid: gid, prevId: previd, id: id, percent: percent, person: person, startDate: startDate, endDate: endDate,marginRight:pos1,width:width, token: ajaxRequest},
            dataType: 'json',
            success: function (data) {
                console.log('success edit');
            ajaxEnd();
            },error:function(){
                ajaxEnd();
                myPrompt('editAll');
            }
        });
    }

    /*---NEW GROUPS---*/
    if((parseInt(permition) & 8)>0)
    {
        $(document).on('click', '.group-row', function (event) {
            if((parseInt(permition) & 1)>0)
            {
                if (sortableTasks == true)
                    return;
                id = $(this).attr('sid');
                if ($('#input-group-' + id).css('display') == 'none' && taskBlur == false && groupRemove == false)
                {
                    //console.log('taskblur false')
                    oldTaskName = $('#group-name-' + id).html();
                    $('#group-name-' + id).hide();
                    $('#input-group-' + id).show();
                    $('#input-group-' + id).addClass('input-group');
                    $('#input-group-' + id).focus();
                    $('#input-group-' + id).select();
                    $('#input-group-' + id).width($('.ctask-group').width());
                    $('#input-group-' + id).val(oldTaskName);
                    taskBlur = true;
                    console.log(id + ' click')
                }
                if (taskBlur == false)
                    console.log('fdsafdsa' + taskBlur);
            }
        }).children('.icon-group').click(function (e) {
                return false;
        });

        groupId = 800;
        newGroup = false;

        $(document).on('blur', '.input-group', function () {

            parent = $(this).parent();
            id = parent.attr('sid');
            newValue = $(this).val();
            thisGroup = parent.parent();
            prevId = thisGroup.prev().attr('sid');
            nextId = thisGroup.next().attr('sid');
            if (newGroup)
            {
                if (newValue !== '')
                {
                    //id come from database after insert
                    groupId++;
                    ajaxStart();
                    $.ajax({
                        url: URL + 'ajaxes/newGroup.php',
                        type: 'post',
                        data: {action: 'new', projectId:projectId, name: newValue, prevId: prevId, nextId: nextId, token: ajaxRequest},
                        dataType: 'json',
                        success: function (data) {
                            console.log(data+" "+ data['id'] + ' success insert ' + newValue + ' ' + prevId + ' ' + ajaxRequest);
                            groupId=data['id'];
                            prev = parent.prev();

                            /*<div class='ctask-group' id='itask-group'><i class='icon-minus icon-group'></i> \
                             <div class='group-row' id='group-' sid=''>\
                             <div id='group-name-' class='ctask' style='padding-right:0px;font-weight:bolder;'>ssss</div>\
                             <input type='text' class='input-group' sid='' id='input-group-' value='' style='display:none;'>\
                             </div>\
                             <div class='groups-task'>\
                             </div>\
                             <div class='news' id='new-task' sid='' >\
                             <a href='#' class='new-task link-task'>ع©ط§ط± ط¬ط¯غŒط¯</a>\
                             <a href='#' class='new-group link-task'>ع¯ط±ظˆظ‡ ط¬ط¯غŒط¯</a>\
                             </div>\
                             </div>*/
                            $(this).attr('sid', groupId);
                            $(this).attr('id', 'input-group-' + groupId);
                            parent.attr('sid', groupId);
                            parent.attr('id', 'group-' + groupId);
                            parent.children('div').attr('id', 'group-name-' + groupId);
                            parent.children('input').attr('sid', groupId);
                            parent.children('input').attr('id', 'input-group-' + groupId);

                            parent.parent().attr('id','itask-group'+groupId);
                            parent.parent().attr('sid',data['id']);

                            if(!parent.parent().is(':last-child'))
                            {
                            	parent.parent().attr('prev',parent.parent().next().attr('prev'));
                            	parent.parent().next().attr('prev',groupId);
                            }

                            //person
                            $('#person8888888888').attr({'id': 'person' + groupId, 'sid': groupId});
                            $('#person-group8888888888').attr({'id': 'person-group' + groupId, 'sid': groupId});
                            $('#person8888888888N').attr({'id': 'person' + groupId + 'N', 'sid': groupId});

                            /*
                             <div class='person-row' id='person8888888888' sid='8888888888' >\
                             <div style='height:100%;'></div>\
                             </div><div class='person-row' id='person8888888888N' sid='8888888888N' >\
                             <div style='height:100%;'></div>\
                             </div>*/

                            //percent
                            $('#percent-row8888888888').attr({'id': 'percent-row' + groupId, 'sid': groupId});
                            $('#percent8888888888').attr({'id': 'percent' + groupId, 'sid': groupId});
                            $('#percent-group8888888888').attr({'id': 'percent-group' + groupId, 'sid': groupId});
                            $('#input-percent8888888888').attr({'id': 'input-percent' + groupId, 'sid': groupId});

                            $('#percent-row8888888888N').attr({'id': 'percent-row' + groupId + 'N', 'sid': groupId});
                            $('#percent8888888888N').attr({'id': 'percent' + groupId + 'N', 'sid': groupId});
                            $('#input-percent8888888888N').attr({'id': 'input-percent' + groupId + 'N', 'sid': groupId});

                            /*<div class='percent-group' id='percent-group8888888888'>
                             <div class='percent-row' id='percent-row8888888888' sid='8888888888' >new</div>\
                             <div class='percent-row' id='percent-row8888888888N' sid='8888888888N' >new</div>*/

                            //schedule
                            $('#schedule-row8888888888').attr({'id': 'schedule-row' + groupId, 'sid': groupId});
                            //$('#schedule8888888888 div .percent').attr({'sid':groupId});
                            $('#schedule8888888888').attr({'id': 'schedule' + groupId, 'sid': groupId});
                            $('#schedule-group8888888888').attr({'id': 'schedule-group' + groupId, 'sid': groupId});

                            $('#schedule-row8888888888N').attr({'id': 'schedule-row' + groupId + 'N', 'sid': groupId});
                            //$('#schedule8888888888N div .percent').attr({'sid':groupId});
                            $('#schedule8888888888N').attr({'id': 'schedule' + groupId + 'N', 'sid': groupId});
                            /*<div class='schedule-row' id='schedule-row8888888888' sid='8888888888'>\
                             <div></div> \
                             </div><div class='schedule-row' id='schedule-row8888888888N' sid='8888888888N'>\
                             <div></div> \
                             </div>*/

                            id = groupId;
                            ajaxEnd();
                        },error:function(){
                            ajaxEnd();
                            myPrompt('newGroup');
                        }
                    });

                console.log('new group');
                }
                else
                {
                    parent.remove();
                }
            }
            else if(oldTaskName!=newValue){
                ajaxStart();
                $.ajax({
                    url: URL + 'ajaxes/newGroup.php',
                    type: 'post',
                    data: {action: 'editGroup',projectId:projectId , name: newValue, id: id, token: ajaxRequest},
                    dataType: 'json',
                    success: function (data) {
                        console.log('success edit group');
                        ajaxEnd();
                    },error:function(){
                        ajaxEnd();
                        myPrompt('editGroupName');
                    }
                });
            }


            $('#group-name-' + id).show();
            $('#group-name-' + id).html(newValue);
            $(this).hide();
            //console.log('insert the new name of task: '+id);
            updateScrollbar(document.getElementById('tasks'), document.getElementById('scrollbar-task'));
            $('.ctask-group').css('width', $('#innerScrollbar-task').width());
            newGroup = false;
            taskBlur = false;
        });
    }
    
        /*-------REMOVE GROUPS--------*/
    $(document).on('mouseover', '.group-row', function () {
        parent = $(this);
        parent.children('.icon-group-remove').show();
    });
    $(document).on('mouseleave', '.group-row', function () {
        parent = $(this);
        parent.children('.icon-group-remove').hide();
    });

    $(document).on('mouseover', '.icon-group-remove', function () {
        groupRemove=true;
    });
    groupRemove=false;
    $(document).on('mouseleave', '.icon-group-remove', function () {
        groupRemove=false;
    });

    $(document).on('mousedown', '.icon-group-remove', function (){
        if((parseInt(permition) & 4194304)>0)
        {
            deleteType='groups';
            parent = $(this).parent();
            parent.children('input');
            deleteId=parent.attr('sid');
            deleteName=parent.children('div').html();
            deleteThisPrev=parent.parent().attr('prev');
            deleteNextId=parent.parent().next().attr('sid');
            if(deleteThisPrev!='' || deleteNextId!=undefined)
            {
                $("#delete-modal .modal-body").html('آیا مایل به حذف گروه " '+deleteName+'" این هستید؟ ');
                $("#delete-modal").modal();
                $("#delete-modal").modal({keyboard: false});
                $("#delete-modal").modal('show');
            }
            else
            {
                $('#alert-modal').children('.modal-body').html(canNotDeleteLastGroup);
                $('#alert-modal').modal();
                $('#alert-modal').modal({keyboard: false});
                $('#alert-modal').modal('show');
            }
            deleteTaskGroupTable='groups';
            console.log(deleteGroupId+" : "+deleteGroupName);
        }
        else
        {
            accessDenied();
        }
    });
    
    /*-------REMOVE TASKS--------*/
    taskRemove=false;

    $(document).on('mouseover', '.task-row', function () {
        parent = $(this);
        parent.children('.icon-task-remove').show();
    });
     $(document).on('mouseover', '.icon-task-remove', function () {
        parent = $(this);
        parent.show();
        taskRemove=true;
    });
    taskRemove=false;
    $(document).on('mouseleave', '.task-row , .icon-task-remove', function () {
        parent = $(this);
        parent.children('.icon-task-remove').hide();
        taskRemove=false;
    });
    $(document).on('mouseleave', '.icon-task-remove', function () {
        parent = $(this);
        parent.hide();
    });
    $(document).on('mouseup', '.icon-task-remove', function () {
        if((parseInt(permition) & 2097152)>0)
        {
            deleteType='tasks';
            mthis = $(this).parent('.task-row');
            deleteThisPrev='';
            deleteId=mthis.attr('sid');
            deleteName=mthis.children('div').html();
            deleteThisPrev=mthis.attr('prev');
            deleteNextId=mthis.next().attr('sid');
            if((deleteThisPrev!='0') || deleteNextId!=undefined)
            {
                $("#delete-modal .modal-body").html(window.sh4+'آیا مایل به حذف کار " '+deleteName+'" هستید؟'+window.eh4);
                $("#delete-modal").modal();
                $("#delete-modal").modal({keyboard: false});
                $("#delete-modal").modal('show');
            }
            else
            {
                $('#alert-modal').children('.modal-body').html(canNotDeleteLastTask);
                $('#alert-modal').modal();
                $('#alert-modal').modal({keyboard: false});
                $('#alert-modal').modal('show');
            }
            deleteTaskGroupTable='tasks';
            console.log(deleteId+" : "+deleteName);
        }
        else
        {
            myModal('access denied');
        }
    });
    

    $(document).on('click', '#deleteTaskGroup', function () {
        if(deleteType=='groups'||deleteType=='tasks')
        {
            ajaxStart();
            $.ajax({
                url: URL + 'ajaxes/deleteTaskGroup.php',
                type: 'post',
                data: {action:'delete',projectId:projectId ,table: deleteTaskGroupTable, id: deleteId, nextId:deleteNextId,thisPrev:deleteThisPrev, token: ajaxRequest},
                dataType: 'json',
                success: function (data) {
                    deleteType='';
                    if(deleteTaskGroupTable=='groups')
                    {
                            $('#itask-group'+deleteNextId).attr('prev',deleteThisPrev);
                            $('#itask-group'+deleteId).remove();
                            $('#person-group'+deleteId).remove();
                            $('#percent-group'+deleteId).remove();
                            $('#schedule-group'+deleteId).remove();
                            console.log(data['errCode']+' success edit group');
                    }
                    else
                    {
                            $('#task-'+deleteNextId).attr('prev',deleteThisPrev);
                            $('#task-'+deleteId).remove();
                            $('#person-'+deleteId).remove();
                            $('#percent-row-'+deleteId).remove();
                            $('#schedule-row-'+deleteId).remove();
                            console.log(data['errCode']+' success edit tasks');
                    }
                    ajaxEnd();
                },error:function(){
                    ajaxEnd();
                    myPrompt('deletetg');
                }
            });
        
        
        }
        else if(deleteType=='tasksfile')
        {
            if((parseInt(permition) & 262144)>0)
            {
                ajaxStart();
                $.ajax({
                    url: URL + 'ajaxes/upload.php',
                    type: 'post',
                    data: {action:'delete',projectId:projectId ,table: deleteTaskGroupTable, fileName:fileName, fileId: deleteId, token: ajaxRequest},
                    dataType: 'json',
                    success: function (data) {
                        console.log('successfuly file deleted');
                        $('#file'+fileId).parent('.file-link').remove();
                        ajaxEnd();
                    },error:function(){
                        ajaxEnd();
                        myPrompt('deleteFile');
                    }
                });
            }
        }
        else if(deleteType=='comment')
        {
            ajaxStart();
            $.ajax({
                url: URL + 'ajaxes/commentFile.php',
                type: 'post',
                data: {action: 'delete',projectId:projectId , commentId:commentId, token: ajaxRequest},
                dataType: 'json',
                success: function (data) {
                    mthis.parent().remove();
                    deleteType='';
                    ajaxEnd();
                },error:function(){
                    ajaxEnd();
                    myPrompt('deleteComment');
                }
            });
        }
    });

    $(document).on('mouseover', '.icon-group-remove, .icon-group', function () {
        sortableTasks = true;
    });
    $(document).on('mouseleave', '.icon-group-remove, .icon-group', function () {
        sortableTasks = false;
    });
    /*||*/
    $(document).on('keypress', '.input-group', function (event) {
        if (event.which == 13) {
            $(this).blur();
            $(this).parent().parent().children('.news .new-task').click();
        }
    });
    $(document).on('keypress', '.input-task', function (event) {
        if (event.which == 13) {
            $(this).blur();
        }
    });
    /*--------------START NEW TASK AND GROUP-----------*/
    if((parseInt(permition) & 1)>0)
    {
        $(document).on('click', '.new-task', function () {
            newRowType=$(this).attr('type');
            if(newTask==true)
            	return;
            /*$('.input-task:visible').each(function () {
                $(this).blur();
            });
            $('.input-group:visible').each(function () {
                $(this).blur();
            });*/
            parent = $(this).parent().prev('.groups-task');
            newTask = true;
            if (parent.children().length > 0)
            {
                if(newRowType=='task')
                {
                    parent.append('<div class="task-row right-panel-row" id="task-9999999999" sid="9999999999" prev="" .  $prev. "">'+
                                '<i class="icon-remove icon-task-remove"></i>'+
                                '<div id="task-name-9999999999" class="ctask" style="$style"></div>'+
                                '<input type="text" class="input-task" sid="9999999999" id="input-task-9999999999" value="" style="display: none;">'+
                                '</div>');
                    id = $('#task-9999999999').prev().attr('sid');
                    $("<div class='person-row right-panel-row' id='person-9999999999' sid='9999999999' >" +
                            "</div>").insertAfter('#person-' + id);

                    $(" <div class='percent-row right-panel-row' id='percent-row-9999999999' sid='9999999999' >" +
                            "<div id='percent-9999999999' sid='9999999999' class='cpercent' style='height:100%;'>1</div>" +
                            "<input type='text' class='input-percent' sid='9999999999' id='input-percent-9999999999' value='' style='display:none;'>" +
                            "</div>").insertAfter('#percent-row-' + id);
                    console.log('new task happend after tasks ' + " this id:" + parent.attr('sid'));
                    $(" <div class='schedule-row' id='schedule-row-9999999999' sid='9999999999' show='false'>" +
                        "<div>" +
                            "<i class='right-tool' style='position:absolute; right:0px;display:none;'><img class='ico' src='" + URL + "img/comments2.png'></i>" +
                            "<div class='resize-drag $type ui-widget-content' style='width:"+ resizeDragX +"px; right:" + scheduleGridX + "px; display:none;' id='schedule-9999999999' sid='9999999999'>" +
                            "       <div class='percent80'>" +
                            "           <div class='percent' style='width:1%;' sid='9999999999'></div>" +
                            "       </div>" +
                            "</div>" +
                            "<i class='left-tool' style='position:absolute; right:" + scheduleGridX+scheduleGridX + "px;display:none;'>" +
                            "<img id='img-9999999999' class='ico comment' src='" + URL + "img/comments2.png'>" +
                            "<img id='img-9999999999' class='ico upload' src='" + URL + "img/upload4.png'>" +
                            "<div class='task-persons'></div>" +
                            "</i>" +
                        "</div>" +
                        "</div>").insertAfter('#schedule-row-' + id);
                }
                else if(newRowType=='mile')
                {
                    parent.append('<div class="task-row right-panel-row" id="task-9999999999" sid="9999999999" prev="" .  $prev. "">'+
                                '<i class="icon-remove icon-task-remove"></i>'+
                                '<div id="task-name-9999999999" class="ctask" style="$style"></div>'+
                                '<input type="text" class="input-task" sid="9999999999" id="input-task-9999999999" value="" style="display: none;">'+
                                '</div>');
                    id = $('#task-9999999999').prev().attr('sid');
                    $("<div class='person-row right-panel-row' id='person-9999999999' sid='9999999999' >" +
                            "</div>").insertAfter('#person-' + id);
                    $("<div class='percent-row right-panel-row' id='percent-row-9999999999' sid='9999999999' >" +
                            "<div id='percent-9999999999' sid='9999999999' class='cpercent' style='height:100%;'>1</div>" +
                            "<input type='text' class='input-percent' sid='9999999999' id='input-percent-9999999999' value='' style='display:none;'>" +
                            "</div>").insertAfter('#percent-row-' + id);
                    console.log('new task happend after tasks ' + " this id:" + parent.attr('sid'));
                    $("<div class='schedule-row' id='schedule-row-9999999999' sid='9999999999' show='false'>" +
                        "<div>" +
                            "<i class='right-tool' style='position:absolute; right:0px;display:none;'><img class='ico' src='" + URL + "img/comments2.png'></i>" +
                            "<div class='resize-drag $type ui-widget-content' style='width:"+ resizeDragX +"px; right:" + scheduleGridX + "px; display:none;' id='schedule-9999999999' sid='9999999999'>" +
                            "</div>" +
                            "<i class='left-tool' style='position:absolute; right:" + scheduleGridX+scheduleGridX + "px;display:none;'>" +
                            "<img id='img-9999999999' class='ico comment' src='" + URL + "img/comments2.png'>" +
                            "<img id='img-9999999999' class='ico upload' src='" + URL + "img/upload4.png'>" +
                            "<div class='task-persons'></div>" +
                            "</i>" +
                        "</div>" +
                        "</div>").insertAfter('#schedule-row-' + id);
                }

            }
            else
            {

                id = parent.prev().attr('sid');
                parent.append('<div class="task-row" id="task-9999999999" sid="9999999999" next-nod="9999999999"> ' +
                        '<div id="task-name-9999999999" class="ctask" style="padding-right:14px;"></div>' +
                        '	<input type="text" class="input-task" sid="9999999999" id="input-task-9999999999" value="" style="display: none;">' +
                        '</div>');
                $("<div class='person-row' id='person-9999999999' sid='9999999999' >" +
                        "		<div style='height:100%;'>9 pssp</div>" +
                        "</div>").insertAfter('#person' + id);

                $("<div class='percent-row' id='percent-row-9999999999' sid='9999999999' >" +
                        "	<div id='percent-9999999999' sid='9999999999' class='cpercent' style='height:100%;'>1</div>" +
                        "	<input type='text' class='input-percent' sid='9999999999' id='input-percent-9999999999' value='' style='display:none;'>" +
                        "</div>").insertAfter('#percent-row' + id);

                $("<div class='schedule-row' id='schedule-row-9999999999' sid='9999999999' show='false' style='width:100%;'>" +
                        "<div>" +
                        "	<i class='right-tool' style='position:absolute; right:0px;display:none;'><img class='ico' src='" + window.location.origin + "/gantt/img/comments2.png'></i>" +
                        "	<div class='resize-drag ui-widget-content' style='display:none;width:23px; right:20px;' id='schedule-9999999999' sid='9999999999'> " +
                        "		<div class='percent80'>" +
                        "			<div class='percent' style='width:1%;' sid='9999999999'></div>" +
                        "		</div>" +
                        "	</div>" +
                        "	<i class='left-tool' style='position:absolute; right:47px;display:none;'><img class='ico' src='" + window.location.origin + "/gantt/img/comments2.png'><div class='task-persons'></div></i>" +
                        "</div> " +
                        "</div>").insertAfter('#schedule-row' + id);

                console.log('new task happend bfore group ' + " this id:" + parent.attr('sid'));
            }

            $('#task-9999999999').click();

        });
    }
    if((parseInt(permition) & 256)>0)
    {
        $(document).on('click', '.new-group', function () {
        	if(taskBlur==true)
        	{
        		return;
        	}
            /*$('.input-task:visible').each(function () {
                $(this).blur();
            });
            $('.input-group:visible').each(function () {
                $(this).blur();
            });*/
            parent = $(this).parent().parent();
            newGroup = true;
            //ط¨ط§غŒط¯ ط¨ظˆط¯ظ†غŒ ع©ظ‡ ط§ظˆظ†غŒ ع©ظ‡ ط¨ط§غŒط¯ ط¬ط¯غŒط¯ ط³ط§ط®طھظ‡ ط¨ط´ظ‡ ط¢ط¢ط®ط±غŒظ† ع¯ط±ظˆظ‡ ظ†ط¨ط§ط´ظ‡ غŒط§ ط¨ط§ط´ظ‡ ع©ظ‡ ط¯ط±ط³طھ ط¨ط´ظ‡
            parent.after("<div class='ctask-group' id='itask-group'> " +
                    "<div class='group-row' id='group-8888888888' sid='8888888888'><i class='icon-remove icon-group-remove'></i><i class='icon-minus icon-group'></i>" +
                    "<div id='group-name-8888888888' class='ctask' style='padding-right:0px;font-weight:bolder;'>ssss</div>" +
                    "<input type='text' class='input-group' sid='8888888888' id='input-group-8888888888' value='' style='display:none;'>" +
                    "</div>" +
                    "<div class='groups-task'></div>" +
                    "<div class='news' id='new-task' sid='' >" +
                    "<a href='#' class='new-task link-task'>ع©ط§ط± ط¬ط¯غŒط¯</a>" +
                    "<a href='#' class='new-group link-task'>ع¯ط±ظˆظ‡ ط¬ط¯غŒط¯</a>" +
                    "</div>" +
                    "</div>");
            rows = $(this).parent().prev().prev();
            gId = rows.attr('sid');

            $("<div class='percent-group' id='percent-group8888888888'>" +
                    "<div class='percent-row' id='percent-row8888888888' sid='8888888888' >group</div>" +
                    "<div class='percent-row' id='percent-row8888888888N' sid='8888888888N' >new</div>").insertAfter('#percent-group' + gId);

            $("<div class='person-group' id='person-group8888888888'><div class='person-row' id='person8888888888' sid='8888888888' >" +
                    "<div style='height:100%;'></div>" +
                    "</div><div class='person-row' id='person8888888888N' sid='8888888888N' >" +
                    "	<div style='height:100%;'></div>" +
                    "</div></div>").insertAfter('#person-group' + gId);

            $("<div class='schedule-group' id='schedule-group8888888888'>" +
                    "<div class='schedule-row' id='schedule-row8888888888' sid='8888888888'>" +
                    "			<div></div> " +
                    "		</div><div class='schedule-row' id='schedule-row8888888888N' sid='8888888888N'>" +
                    "			<div></div> " +
                    "		</div></div>").insertAfter('#schedule-group' + gId);

            //console.log(prev.find('.group-row').attr('sid')+" hahaaaaa");
            $('#group-8888888888').click();
            console.log('new group happend' + " this id:" + parent.parent().attr('sid'));
        });
    }
    /*----------*/
    $(document).on('click', '.icon-group', function () {
        group = $(this).parent().parent();
        id = group.children('.group-row').attr('sid');
        if (group.children('.groups-task').is(':visible'))
        {
            group.children('.groups-task').hide();
            group.children('.news').hide();
            $('#person-group' + id + ' .person-row:not(:last-child)').hide();
            $('#percent-group' + id + ' .percent-row:not(:last-child)').hide();
            $('#schedule-group' + id + ' .schedule-row:not(:last-child)').hide();
            $(this).removeClass('icon-minus');
            $(this).addClass('icon-plus');
        }
        else
        {
            group.children('.groups-task').show();
            group.children('.news').show();
            $('#person-group' + id + ' .person-row:not(:last-child)').show();
            $('#percent-group' + id + ' .percent-row:not(:last-child)').show();
            $('#schedule-group' + id + ' .schedule-row:not(:last-child)').show();
            $(this).addClass('icon-minus');
            $(this).removeClass('icon-plus');
        }
    });

    /*END EDIT TASK NAME 1111*/
    /*START EDIT PERSONS 2222*/

    $(document).on('click', '.person-row', function () {
        personRowId = $(this).attr('sid');
        personsResourcesSetted = $('#person-' + personRowId).html();
        personsResourcesSettedArray=[];
        personsResourcesSettedArray[1]='';
        personsResourcesSettedArray = personsResourcesSetted.split(userResourceSplit);
        personsSetted=personsResourcesSettedArray[0];
        resourcesSetted=personsResourcesSettedArray[1];
        personsSettedArray=personsResourcesSettedArray[0].split(userSplit);
        if(personsResourcesSettedArray[1]!=undefined)
            resourcesSettedArray=personsResourcesSettedArray[1].split(userSplit);
        else
            resourcesSettedArray='';

        $('.persons').each(function () {
            if (jQuery.inArray($(this).parent().prev().html(), personsSettedArray) !== -1)
                $(this).prop('checked', true);
            else
                $(this).prop('checked', false);
        }).get();

        $('.resources').each(function () {
            if (jQuery.inArray($(this).parent().prev().html(), resourcesSettedArray) !== -1)
                $(this).prop('checked', true);
            else
                $(this).prop('checked', false);
        }).get();
        personsIdBefore = $('.persons:checkbox:checked').map(function () {
            return $(this).val();
        }).get();
        resourcesIdBefore = $('.resources:checkbox:checked').map(function () {
            return $(this).val();
        }).get();
        $('.popover-person').css({'position': 'absolute !important', 'top': $(this).offset().top+10 + 'px', 'right': $(this).position().left + 215 + 'px', 'display': 'block'});
        $('.popover-person').children('.task-row').children('input').focus();
    });
    $(document).on('click', '.popover-close', function () {
        $('.popover-person').css({'position': 'absolute !important', 'top': $(this).position().top + 73 + 'px', 'right': $(this).position().left + 215 + 'px', 'display': 'none'});
    });
    if((parseInt(permition) & 128)>0)
    {
        $(document).on('click', '.set-persons', function () {
            personsId = $('.persons:checkbox:checked').map(function () {
                return $(this).val();
            }).get();
            personsRemoveId=[];
            ii=0;
            for(i=0;i<personsIdBefore.length;i++)
        	{
        		if(jQuery.inArray(personsIdBefore[i],personsId)===-1)
        			personsRemoveId[ii++]=personsIdBefore[i];
        	}
            for(j=0;j<personsIdBefore.length;j++)
            {
                removeItem=personsIdBefore[j];
                personsId = jQuery.grep(personsId, function(value) {
                    return value != removeItem;
                });
            }

            resourcesId = $('.resources:checkbox:checked').map(function () {
                return $(this).val();
            }).get();
            resourcesRemoveId=[];
            ii=0;
            for(i=0;i<resourcesIdBefore.length;i++)
        	{
        		if(jQuery.inArray(resourcesIdBefore[i],resourcesId)===-1)
        			resourcesRemoveId[ii++]=resourcesIdBefore[i];
        	}
            for(j=0;j<resourcesIdBefore.length;j++)
            {
                removeItem=resourcesIdBefore[j];
                resourcesId = jQuery.grep(resourcesId, function(value) {
                    return value != removeItem;
                });
            }
            console.log('rrrrrrrrrrrrrrrrrrrrrrrr'+resourcesRemoveId);
            console.log(personsId+' personssssssss '+ resourcesId+' resourcesssssssssssssss');
            personsName = $('.persons:checkbox:checked').map(function () {
                return $(this).parent().prev().html();
            }).get();
            resourcesName = $('.resources:checkbox:checked').map(function () {
                return $(this).parent().prev().html();
            }).get();
            personsStringName = personsName.join(userSplit);
            resourcesStringName = resourcesName.join(userSplit);
            if(resourcesName!='')
                personsResourcesStringName = personsName.join(userSplit)+userResourceSplit+resourcesName.join(userSplit);
            else
                personsResourcesStringName = personsName.join(userSplit);
            $('#person-' + personRowId).html(personsResourcesStringName);
            $('#person-' + personRowId).attr('title', personsResourcesStringName);

            $('#schedule-row-' + personRowId + ' .task-persons').html(personsResourcesStringName);
            $('.popover-person').css({'display': 'none'});
            flgResources=0;
            flgPersons=0;
            if (personsSetted != personsStringName)
            {
                flgPersons=1;
            }
            if (resourcesSetted != resourcesStringName)
            {
                flgResources=1;
            }
            if(flgPersons==1||flgResources==1)
            {
                console.log(personsIdBefore);
                console.log(personsId);
                ajaxStart();
                $.ajax({
                    url: URL + 'ajaxes/person.php',
                    type: 'post',
                    data: {action: 'set',projectId:projectId ,flgPersons:flgPersons,flgResources:flgResources,
                        taskId: personRowId, personsResourcesStringName: personsResourcesStringName, personId:personsId,resourceId:resourcesId,personsRemoveId:personsRemoveId,resourcesRemoveId:resourcesRemoveId, token: ajaxRequest},
                    dataType: 'json',
                    success: function (data) {
                        console.log('success edit');
                        ajaxEnd();
                    },error:function(){
                        ajaxEnd();
                        myPrompt('setPerson');
                    }
                });
                console.log(personsStringName + ' must change in task row with id: ' + personRowId);
            }


            console.log('set persons is clicked');
        });
    }
    /*END EDIT PERSONS 2222*/
    /*START EDIT PERCENT 3333*/
        $('.input-percent').hide();
    if((parseInt(permition) & 64)>0)
    {
        $(document).on('click', '.percent-row', function () {
            id = $(this).attr('sid');
            if ($('#input-percent-' + id).css('display') == 'none')
            {
                taskame = $('#percent-' + id).html();
                $('#percent-' + id).hide();
                $('#input-percent-' + id).show();
                //$('#input-percent-'+id).addClass('input-task');
                $('#input-percent-' + id).focus();
                $('#input-percent-' + id).select();
                $('#input-percent-' + id).val(taskame);
            }
        });
    }

    $(document).on('blur', '.input-percent', function () {
        id = $(this).attr('sid');
        newValue = $(this).val();
        $('#percent-' + id).show();
        $('#percent-' + id).html(newValue);
        $(this).hide();
        $('#schedule-' + id + ' div div').css('width', newValue + '%');
        percent = $('#percent-' + id).html();
        ajaxStart();
        $.ajax({
            url: URL + 'ajaxes/task.php',
            type: 'post',
            data: {action: 'update-percent',projectId:projectId , percent: percent, id:id , token: ajaxRequest},
            dataType: 'json',
            success: function (data) {
                group=$('#percent-'+id).parent().parent();
                groupId=group.attr('id');
                percents=0;
                npercent=0;
                $('#'+groupId+' .percent-row:not(:first-child):not(:last-child)').each(function(){
                    percent=$(this).children('.cpercent').html();
                    percents+=parseInt(percent);
                    console.log(percents+":"+percent);
                    npercent++;
                });
                $('#'+groupId+' .percent-row:first-child').html((percents/npercent).toFixed(1));
                console.log('updatePercent');
                ajaxEnd();
            },error:function(){
                ajaxEnd();
                myPrompt('editAllUp');
            }
        });
    });

    $(document).on('keypress', '.input-percent', function (event) {
        if (event.which == 13) {
            id = $(this).attr('sid');
            newValue = $(this).val();
            $('#percent-' + id).show();
            $('#percent-' + id).html(newValue);
            $(this).hide();
            console.log('percent keypress setted');
        }
        key = String.fromCharCode(event.which);
        value = $(this).val();
        if (value.length > 2 || isNaN(key) || (value.length > 1 && key != '0') || (value.length > 1 && value > 10))
        {
            return false;
        }
        else if (isNaN)
        {

        }
        console.log('bozorgtar az 333333333333333');
    });
    /*END EDIT PERCENT 3333*/
    taskHeight = $('.tasks').height();
    /*if (taskHeight < $('.gantt-body').height())
    {
        $('.tasks').css('margin-right', '13px');
    }
    else
    {
        $('.tasks').css('margin-right', '0px');
    }*/
    resizable = false;
    draggable = false;
    groupDragg=false;
    /*--------------------START DRAGGABLE AND RESIZABLE--------*/
    $(document).on('mouseover', ".resize-drag", function () {
        if((parseInt(permition) & 256)>0)
        {
        $(this).css('cursor','pointer');
        if (!$(this).is('.ui-draggable')&&(groupDragg==false))
            $(this).css('cursor','pointer');
            $(this).draggable({
                axis: 'x',
                grid: [resizeDragX, resizeDragY],
                containment: '.schedule-rows',
                start: function (e, ui) {
                    $(this).parent().find('.right-tool , .left-tool').css({'display': 'none'});
                    x = 0;
                    //draggable = true;
                    x = (e.pageX - $(this).offset().left) - $(this).width() + 30;
                    scheduleGroup = $(this).parent().parent().parent();
                    scheduleGroupId=scheduleGroup.attr('id');
                },
                drag: function (e, ui) {
                    parent = $(this).parent();
                    $(this).css('cursor', 'none');
                    //console.log(pos);
                    //console.log(($('#content-schedule').scrollLeft()+$('#schedule-rows').width())-$('#schedule-rows').width());
                    pos = $('.schedule-content').width() - $(this).width() - $(this).position().left + 1327 + ($('#content-schedule').scrollLeft() - $('.schedule-rows').width());
                    pos1 = $('.schedule-content').width() - $(this).width() - $(this).position().left;
					/*mod=pos%resizeDragX;
					mod1=pos1%resizeDragX;
					pos=pos-mod;
					pos1=pos1-mod1;*/
					pos+=1;
					pos1+=1;
					console.log(pos+":"+pos1);
                    x = Math.abs(x);
                    $('.date-drag').css({'position': 'absolute !important', 'top': $(this).offset().top + 10 + 'px', 'right': pos + x, 'display': 'block'});
                    $('.date-drag').children('.date-show').html(scrollDate[pos1] + '' + scrollDate[pos1 + $(this).width()]);

                    $('#schedule-row-' + id).removeClass('row-hover');
                    $('#percent-row-' + id).removeClass('row-hover');
                    $('#person-' + id).removeClass('row-hover');
                    $('#task-' + id).removeClass('row-hover');

                    /*cell that dragged with this element*/
                    $('.drag-resize-cell').css({'position': 'absolute !important', 'display': 'block', 'right': pos1-2, 'width': $(this).width()});


                    draggable = true;
                    mouseMove = false;
                },
                stop: function () {
                    draggable = false;

                    parent = $(this).parent();
                    $(this).css({'cursor': 'pointer', 'top': 'auto'});

                    //$(this).css({'left':'auto','right':'7000px'});
                    $('#schedule-row-' + id).addClass('row-hover');
                    $('#percent-row-' + id).addClass('row-hover');
                    $('#person-' + id).addClass('row-hover');
                    $('#task-' + id).addClass('row-hover');

                    $('.date-drag').css({'display': 'none'});
                    $('.drag-resize-cell').css('display', 'none');
                    updateToolbar($(this), parent);
                    $(this).parent().find('.right-tool , .left-tool').css({'display': 'block'});

                    scheduleGroupMinRight=999999999;
                    scheduleGroupMaxRightWidth=0;
                    minRight=999999999;
                    i=0;
                    $('#'+scheduleGroupId+' .schedule-row[show="true"]:not(:first-child):not(:last-child) .resize-drag').each(function(){
                        right=$(this).css('right');
                        right=parseInt(right);
                        console.log($(this).attr('sid'));
                        if(minRight>right && right>0)
                        {
                            minRight=right;
                            i++;
                        }
                        width=$(this).css('width');
                        width=parseInt(width);
                        widthRight=right+width;
                        if(right<scheduleGroupMinRight)
                            scheduleGroupMinRight=right;
                        if(scheduleGroupMaxRightWidth<widthRight)
                        {
                            scheduleGroupMaxRightWidth=widthRight;
                        }

                    });
                    scheduleGroupMaxRightWidth-=minRight;
                    $('#'+scheduleGroupId+' .schedule-row:first-child .resize-drag-group').css({'right':scheduleGroupMinRight+'px','width':scheduleGroupMaxRightWidth+'px'});
                    taskRow = $('#task-' + id);
                    id = taskRow.attr('sid');

                    pos = $('.schedule-content').width() - $('#schedule-' + id).width() - $('#schedule-' + id).position().left + 1327 + ($('#content-schedule').scrollLeft() - $('.schedule-rows').width());
                    pos1 = $('.schedule-content').width() - $('#schedule-' + id).width() - $('#schedule-' + id).position().left;

                    divi=(pos1)/resizeDragX;
                    divi=Math.round(divi);
                    pos1s=divi*resizeDragX;
                    pos1=pos1s+1;

                    startDate = scrollDate[pos1];
                    endDate = scrollDate[pos1 + $('#schedule-' + id).width()];
                    width=$('#schedule-' + id).width();
                    /*
                    divi=(width+1)/resizeDragX;
                    width1=divi*resizeDragX;
                    modi=(width+1)%resizeDragX;
                    if(modi>10)
                            width1+=resizeDragX;
                    else if(modi<4&&modi>1)
                            width1-=resizeDragX;
                    width1=Math.round(width1);*/

                    divi=(width)/resizeDragX;
                    divi=Math.round(divi);
                    width1=divi*resizeDragX;
                    width=width1+1;

                    $('#schedule-' + id).width(width-2);
                    $('#schedule-' + id).css('right',pos1-2);
                    console.log(width1+" true width: "+width+" start:"+pos1);
                    ajaxStart();
                    $.ajax({
                        url: URL + 'ajaxes/task.php',
                        type: 'post',
                        data: {action: 'drag',projectId:projectId , id: id, startDate: startDate, endDate: endDate,marginRight:pos1,width:width, token: ajaxRequest},
                        dataType: 'json',
                        success: function (data) {
                            console.log('success edit');
                            ajaxEnd();
                        },error:function(){
                            ajaxEnd();
                            myPrompt('dragTask');
                        }
                    });
                }
            });
        }
        else
        {
            $(this).css('cursor','default');
        }
    });
    /*-------------DRAG GROUP-----------------------*/
    $(document).on('mouseover', ".resize-drag-group", function () {
        if((parseInt(permition) & 512)>0)
        {
        $(this).css('cursor','pointer');
        if (!$(this).is('.ui-draggable'))
            $(this).draggable({
                axis: 'x',
                grid: [resizeDragX, resizeDragY],
                containment: '.schedule-rows',
                start: function (e, ui) {
                    draggGroup=true;
                    dragg=-1;
                    startGroupRight=pos1 = $('.schedule-content').width() - $(this).width() - $(this).position().left;
                    x = 0;
                    draggable = true;
                    scheduleGroup = $(this).parent().parent().parent();
                    scheduleGroupId=scheduleGroup.attr('id');
                    x = (e.pageX - $(this).offset().left) - $(this).width() + 30;
                    $('#'+scheduleGroupId+' .schedule-row').each(function(){
                        id=$(this).attr('sid');
                         $('#percent-row-'+id).show();
                        $('#person-'+id).show();
                        $('#task-'+id).show();
                        $(this).show();
                    });

                },
                drag: function (e, ui) {

                    $(this).css('cursor', 'none');
                    //console.log(pos);
                    //console.log(($('#content-schedule').scrollLeft()+$('#schedule-rows').width())-$('#schedule-rows').width());
                    pos = $('.schedule-content').width() - $(this).width() - $(this).position().left + 1327 + ($('#content-schedule').scrollLeft() - $('.schedule-rows').width());
                    pos1 = $('.schedule-content').width() - $(this).width() - $(this).position().left;
					/*mod=pos%resizeDragX;
					mod1=pos1%resizeDragX;
					pos=pos-mod;
					pos1=pos1-mod1;*/
					pos+=1;
					pos1+=1;
                    x = Math.abs(x);

                    $('.date-drag').css({'position': 'absolute !important', 'top': $(this).offset().top + 'px', 'right': pos + x, 'display': 'block'});
                    $('.date-drag').children('.date-show').html(scrollDate[pos1] + '' + scrollDate[pos1 + $(this).width()]);

                    /*$('#schedule-row-' + id).removeClass('row-hover');
                    $('#percent-row-' + id).removeClass('row-hover');
                    $('#person-' + id).removeClass('row-hover');
                    $('#task-' + id).removeClass('row-hover');*/

                    /*cell that dragged with this element*/
                    $('.drag-resize-cell').css({'position': 'absolute !important', 'display': 'block', 'right': pos1-2, 'width': $(this).width()});

                    draggable = true;
                    mouseMove = false;
                },
                stop: function () {
                    console.log('stop drag group');
                    scheduleGroup = $(this).parent().parent().parent();
                    scheduleGroupId=scheduleGroup.attr('id');

                    taskRow = $('#task-' + id);
                    id = taskRow.attr('sid');
                    
                    dragGroup($(this))
                    draggGroup=false;
                    draggable = false;
                }
            });

        }
        else
        {
            $(this).css('cursor','default');
        }
    });
    /*----------------------RESIZE SCHEDULE----------------------*/
    $(document).on('mouseover', ".resize-drag:not(.mile)", function () {
        if((parseInt(permition) & 4096)>0)
        {
        $(this).css('cursor','pointer');
        if (!$(this).is('.ui-resizable'))
            $(this).resizable({
                axis: 'x',
                grid: [resizeDragX],
                handles: "w",
                start: function () {
                    resizable = true;
                    mouseMove = false;
                    $(this).parent().find('.right-tool , .left-tool').css({'display': 'none'});
                    scheduleGroup = $(this).parent().parent().parent();
                    scheduleGroupId=scheduleGroup.attr('id');
                },
                resize: function () {
                    $('#schedule-row-' + id).removeClass('row-hover');
                    $('#percent-row-' + id).removeClass('row-hover');
                    $('#person-' + id).removeClass('row-hover');
                    $('#task-' + id).removeClass('row-hover');

                    pos1 = $('.schedule-content').width() - $(this).width() - $(this).position().left;
                    $('.drag-resize-cell').css({'position': 'absolute !important', 'display': 'block', 'right': pos1-1, 'width': $(this).width()});
                },
                stop: function () {
                    resizable = false;

                    parent = $(this).parent();

                    $('#schedule-row-' + id).addClass('row-hover');
                    $('#percent-row-' + id).addClass('row-hover');
                    $('#person-' + id).addClass('row-hover');
                    $('#task-' + id).addClass('row-hover');

                    pos = $('.schedule-content').width() - $(this).width() - $(this).position().left + 940 + ($('#content-schedule').scrollLeft() - $('.schedule-rows').width());
                    parent.children('.left-tool').css({'right': pos + $(this).width() + 11 + 'px'});

                    updateToolbar($(this), parent);
                    $(this).parent().find('.right-tool , .left-tool').css({'display': 'block'});
                    $('.drag-resize-cell').css('display', 'none');

                    scheduleGroupMinRight=999999999;
                    scheduleGroupMaxRightWidth=0;
                    minRight=999999999;
                    $('#'+scheduleGroupId+' .schedule-row:not(:first-child):not(:last-child) .resize-drag').each(function(){
                        right=$(this).css('right');
                        right=parseInt(right);
                        if(minRight>right)
                        {
                            minRight=right;
                        }
                        width=$(this).css('width');
                        width=parseInt(width);
                        widthRight=right+width;
                        if(right<scheduleGroupMinRight)
                            scheduleGroupMinRight=right;
                        if(scheduleGroupMaxRightWidth<widthRight)
                        {
                            scheduleGroupMaxRightWidth=widthRight;
                        }

                    });
                    console.log('g right: '+scheduleGroupMaxRightWidth)
                    scheduleGroupMaxRightWidth-=minRight;
                    $('#'+scheduleGroupId+' .schedule-row:first-child .resize-drag-group').css({'right':scheduleGroupMinRight+'px','width':scheduleGroupMaxRightWidth+'px'});
                    
                    taskRow = $('#task-' + id);
                    id = taskRow.attr('sid');
                    
                    pos = $('.schedule-content').width() - $('#schedule-' + id).width() - $('#schedule-' + id).position().left + 1327 + ($('#content-schedule').scrollLeft() - $('.schedule-rows').width());
                    pos1 = $('.schedule-content').width() - $('#schedule-' + id).width() - $('#schedule-' + id).position().left;

                    divi=(pos1)/resizeDragX;
                    divi=Math.round(divi);
                    pos1s=divi*resizeDragX;
                    pos1=pos1s+1;

                    startDate = scrollDate[pos1];
                    endDate = scrollDate[pos1 + $('#schedule-' + id).width()];
                    width=$('#schedule-' + id).width();

                    divi=(width)/resizeDragX;
                    divi=Math.round(divi);
                    width1=divi*resizeDragX;
                    width=width1+1;

                    $('#schedule-' + id).width(width-2);
                    $('#schedule-' + id).css('right',pos1-2);
                    console.log(width1+" true width: "+width+" start:"+pos1);
                    ajaxStart();
                    $.ajax({
                        url: URL + 'ajaxes/task.php',
                        type: 'post',
                        data: {action: 'resize',projectId:projectId, id:id, endDate:endDate, width:width, token: ajaxRequest},
                        dataType: 'json',
                        success: function (data) {
                            console.log('success edit');
                            ajaxEnd();
                        },error:function(){
                            ajaxEnd();
                            myPrompt('resizeTask');
                        }
                    });
                },
                minWidth: 8
            });
            $(this).css('cursor','pointer');
        }
        else
        {
            $(this).css('cursor','default');
        }
    });

    max = 0;
    $(document).on('mouseover', '.percent', function () {
        if((parseInt(permition) & 64)>0)
        {
        $(this).css('cursor','pointer');
        if (!$(this).is('.ui-resizable'))
            $(this).resizable({
                axis: 'x',
                handles: "w",
                minWidth: 2,
                start: function () {


                },
                resize: function () {
                    $(this).css({'right': '0'});
                    id = $(this).attr('sid');
                    parentWidth = $(this).parent().width();
                    console.log(parentWidth);
                    if (parentWidth < $(this).width())
                    {
                        $(this).width(parentWidth);
                    }
                    else
                    {
                        a = $(this).width();
                        b = $(this).parent().width();
                        c = a / b;
                        d = c * 100;
                        d = Math.round(d);
                        $('#percent-' + id).html(d);
                        $(this).css('width', d + '%');
                    }
                },
                stop: function () {
                    $(this).css({'right': '0'});
                    id = $(this).attr('sid');
                    parentWidth = $(this).parent().width();
                    console.log(parentWidth);
                    if (parentWidth < $(this).width())
                    {
                        $(this).width(parentWidth);
                    }
                    else
                    {
                        a = $(this).width();
                        b = $(this).parent().width();
                        c = a / b;
                        d = c * 100;
                        d = Math.round(d);
                        $('#percent-' + id).html(d);
                        $(this).css('width', d + '%');
                    }
                    updatePercent(id);
                }
            });
            $(this).css('cursor','pointer');
        }
        else
        {
            $(this).css('cursor','default');
        }
    });
    /*
     $('.task-row').each(function(){
     $(this).attr('sid',parseInt($(this).attr('sid'))+1)
     });*/
    /*----------START COMMENTING right and left of schedules----------*/
    commentingId = -1;

    $(document).on('click', '.left-tool .comment', function () {
        mthis = $(this).parent();
        taskId = mthis.parent().parent().attr('sid');
        $('#comments #myModalLabel').html('' + $('#task-name-' + taskId).html());
        $("#comments").modal();
        $("#comments").modal({keyboard: false});
        $("#comments").modal('show');
        parent = $('#comments');
        html=noCommentErr;
        parent.find('.modal-body').html(html);
        ajaxStart();
        $.ajax({
            url: URL + 'ajaxes/commentFile.php',
            type: 'post',
            data: {action: 'select',projectId:projectId , taskId:taskId, token: ajaxRequest},
            dataType: 'json',
            success: function (data) {
                $('#img-'+taskId).prop('src',URL+'img/comments2.png');
            	taskComment=data['comments'];
                //files=data['files'];
                cookieVisit=(taskComment.length>0)?taskComment[0]['cookie']:'';
            	if(taskComment.length>0)
                {
                     parent.children('.modal-body').html('');
                }
                else
                     parent.children('.modal-body').html(html);
            	for(i=0;i<taskComment.length;i++)
            	{
                    taskVisit=taskComment[i]['intTime'];
                    commentId=taskComment[i]['id'];
                    percent=taskComment[i]['percent'];
                    comment=taskComment[i]['comment'];
                    userName1=taskComment[i]['userName'];
                    userEmail=taskComment[i]['userEmail'];
                    userPic=taskComment[i]['userEmail'];
                    cdate=taskComment[i]['cdate'];

                    if (percent < 25){
                        lable = 'progress-danger';
                        face = '';
                    }else if (percent >= 25 & percent < 50){
                        lable = 'progress-warning';
                        face = 1;
                    }else if (percent >= 50 & percent < 75){
                        lable = 'progress-info';
                        face = 2;
                    }else{
                        lable = 'progress-success';
                        face = 3;
                    }
                    console.log(lable+';lable:'+percent);
                    styleVisit='';
                    console.log(cookieVisit+" < "+taskVisit);
                    classs='';
                    if(cookieVisit<taskVisit)
                    {
                        classs='comment-row-color';
                    }
                    remove='';
                    widthPercent=percent<10?10:percent;
                    if(userLogin==userEmail)
                        remove="<i class='icon-remove' sid='"+commentId+"'></i>";
                    html= "<div  class='comment-row "+classs+"'>"+remove+"<div class='user-name'></div><div class='label label-info'>"+userName1+"</div><span class='label label-info' style='position:relative;float:left;padding-left:4px;'>"+cdate+"</span>"+
                        "<br><div style='position:relative;float:right;padding-left:4px;margin-top: 5px;margin-bottom: 5px;'><img class='' width='45px' height='20px' src='" + window.location.origin + "/gantt/profiles/" + userPic + ".png'></div>"+
                        "<div class='progress  " + lable + " ' style='min-width:50px;margin-top: 5px;margin-bottom: 5px;'><div class='bar' style='width: " + widthPercent + "%;'>"+
                        "<img style='float:right;' width='20px' height='20px' src='" + window.location.origin + "/gantt/img/face" + face + ".png'>" +
                        percent + '%</div> <br>' +
                        "</div><div class='comment-text'>"+ comment +"</div>";
                    parent.children('.modal-body').append(html);
            	}
                console.log('success select task comments');
                ajaxEnd();
            },error:function(){
                ajaxEnd();
                myPrompt('selectComment');
            }
        });
        console.log('click on right-tool message icon');
    });

    $(document).on('click', '.left-tool .upload', function () {
        mthis = $(this).parent();
        taskId = mthis.parent().parent().attr('sid');
        $('#uploads #myModalLabel').html('' + $('#task-name-' + taskId).html());
        $("#uploads").modal();
        $("#uploads").modal({keyboard: false});
        $("#uploads").modal('show');
        parent = $('#uploads');
        html=noCommentErr;
        //parent.find('.modal-body').html(html);
        ajaxStart();
        $.ajax({
            url: URL + 'ajaxes/commentFile.php',
            type: 'post',
            data: {action: 'select-file',projectId:projectId , taskId:taskId, token: ajaxRequest},
            dataType: 'json',
            success: function (data) {
                //$('#img-'+taskId).prop('src',URL+'img/comments2.png');
                files=data['files'];
                //cookieVisit=(taskComment.length>0)?taskComment[0]['cookie']:'';
                parent.children('.files').html('');
                if(files.length<0)parent.children('.files').html(noFileErr);
                for(i=0;i<files.length;i++)
            	{
                    newName=files[i]['newName'];
                    oldName=files[i]['oldName'];
                    userName=files[i]['userName'];
                    fileId=files[i]['id'];
                    html= "<div class='file-link'><i class='icon-remove remove-file' id='file"+fileId+"' sid='"+fileId+"' file-name='"+newName+"'></i>"+"<a target='_blank' href='"+URL+"downloadTFile/"+newName+"/"+oldName+"' class='thumbnail file-link' title='توسط: "+userName+"'>"+
                            oldName+"</a></div>";
                    parent.children('.files').append(html);
            	}
                console.log('success select task files');
                ajaxEnd();
            },error:function(){
                ajaxEnd();
                myPrompt('selectFile');
            }
        });
        console.log('click on left-tool upload icon');
    });
    if((parseInt(permition) & 262144)>0)
    {
        $(document).on('click', '.file-link .remove-file', function () {
            mthis=$(this);
            fileName=mthis.attr('file-name');
            fileId=mthis.attr('sid');
            deleteType='file';
            parent = $(this).parent();
            parent.children('input');
            deleteId=fileId;
            deleteName=fileName;
            deleteThisPrev=parent.parent().attr('prev');
            deleteNextId=parent.parent().next().attr('sid');
            $("#delete-modal .modal-body").html('آیا مایل به حذف فایل " '+deleteName+'" این هستید؟ ');
            $("#delete-modal").modal();
            $("#delete-modal").modal({keyboard: false});
            $("#delete-modal").modal('show');
            deleteTaskGroupTable='tasksfile';
            deleteType='tasksfile';
        });
    }
    if((parseInt(permition) & 65536)>0)
    {
        $(document).on('click', '#comments .comment-row .icon-remove', function () {
            $('#delete-modal').children('.modal-body').html(commentDelete);
            $('#delete-modal').modal();
            $('#delete-modal').modal({keyboard: false});
            $('#delete-modal').modal('show');

            deleteType='comment';
            mthis=$(this);
            commentId=mthis.attr('sid');


        });
    }
    if((parseInt(permition) & 131072)>0)
    {
        $('#task-file').change(function(){
            var file = this.files[0];
            name = file.name;
            size = file.size;
            type = file.type;
            type = name.split('.').pop().toLowerCase();

            /* case 'image/png':
            case 'image/gif':
            case 'image/jpeg':
            case 'image/pjpeg':
            case 'text/plain':
            case 'text/html': //html file
            case 'application/x-zip-compressed':
            case 'application/zip':
            case 'application/pdf':
            case 'application/vnd.ms-excel':
            case 'application/octet-stream':
            case 'application/msword':
            case 'video/mp4':
            case 'application/vnd.openxmlformats-officedocument.wordprocessingml.document':*/
            flagUpload=true;
            errUpload='';
            /*if(type!='image/png'&&type!='image/gif'&&type!='image/jpeg'&&type!='image/pjpeg'&&type!='text/plain'&&
                    type!='text/html'&&type!='application/x-zip-compressed'&&type!='application/zip'&&type!='application/pdf'&&
                    type!='application/vnd.ms-excel'&&type!='application/octet-stream'&&type!='application/msword'&&
                    type!='video/mp4'&&type!='application/vnd.openxmlformats-officedocument.wordprocessingml.document')
            */
            if($.inArray(type, ['gif','png','jpg','jpeg','pdf','pjpeg','txt','mp4','docx'
                                ,'zip','rar','doc','ppt','xls']) == -1)
            {
                errUpload=fileFormatErr;
                flagUpload=false;
            }
            if(size>5242880)
            {
               errUpload=fileSizeErr;
                flagUpload=false;
            }
            if(name.length<1||name=='undefined')
            {
                errUpload=fileNotSelectedErr;
                flagUpload=false;
            }
            if(name.length>300)
            {
                errUpload=fileBigNameErr;
                flagUpload=false;
            }
            if(flagUpload==false)
            {
                $('#alert-modal').children('.modal-body').html(errUpload);
                //$('#comments').modal('hide');
                $('#alert-modal').modal();
                $('#alert-modal').modal({keyboard: false});
                $('#alert-modal').modal('show');
            }
            if(flagUpload==false)
            {
                $('#alert-modal').children('.modal-body').html(errUpload);
                $('#alert-modal').modal();
                $('#alert-modal').modal({keyboard: false});
                $('#alert-modal').modal('show');
                return false;
            }
            //flagUpload=false;
            //errUpload=sh4+'فایلی انتخاب کنید'+eh4;
            $('progress').css('display','block');
            var fileData = $('#task-file').prop('files')[0];
            var formData = new FormData();
            formData.append('file', fileData);
            formData.append('taskId', taskId);
            formData.append('user', userLogin);
            formData.append('userName', userName);
            formData.append('projectId',projectId );
            console.log(formData);
            ajaxStart();
            $.ajax({
                url: URL + 'ajaxes/upload.php',
                type: 'POST',
                xhr: function() {  // Custom XMLHttpRequest
                    var myXhr = $.ajaxSettings.xhr();
                    if(myXhr.upload){ // Check if upload property exists
                        myXhr.upload.addEventListener('progress',progressHandlingFunction, false); // For handling the progress of the upload
                    }
                    return myXhr;
                },
                //Ajax events
                beforeSend: function(){

                },
                success: function(data){
                    console.log('success upload and insert');
                    if(data['newName'].length>0)
                    {
                        newName=data['newName'];
                        oldName=data['oldName'];
                        fileId=data['fileId'];
                        tag="<div class='file-link'><i class='icon-remove remove-file' id='file"+fileId+"' sid='"+fileId+"' file-name='"+newName+"'></i>"+"<a target='_blank' href='"+URL+"downloadTFile/"+newName+"/"+oldName+"' class='thumbnail file-link' title='توسط: "+userName+"'>"+
                                oldName+"</a></div>";
                        uploads= $('#uploads');
                        uploads.children('.files').html(tag+uploads.children('.files').html());
                        $('progress').css('display','none');
                        $('#task-file').val('');
                        console.log('add file link to files link');
                    }
                    ajaxEnd();
                },
                //error: errorHandler,
                // Form data
                //Options to tell jQuery not to process data or worry about content-type.
                cache: false,
                data: formData,
                dataType:'json',
                contentType: false,
                processData: false,
                error:function(){
                    ajaxEnd();
                    myPrompt('uploadFile');
                }

            });
        });
    }
    $('#task-file').click(function() {
        if((parseInt(permition) & 131072)==0)
        {
            accessDenied();
            return false;
        }
    });
    function progressHandlingFunction(e){
        if(e.lengthComputable){
            $('progress').attr({value:e.loaded,max:e.total});
        }
    }
    if((parseInt(permition) & 32768)>0)
    {
        $(document).on('click', '#comments .insert-comment', function () {
            parent = $(this).closest('#comments');

            percent = $('#percent-' + taskId).html();
            comment=parent.children('textarea').val();
            parent.children('textarea').val('');
            console.log(comment);
            if(comment!='')
            {
                ajaxStart();
                $.ajax({
                    url: URL + 'ajaxes/commentFile.php',
                    type: 'post',
                    data: {action: 'new',projectId:projectId , taskId:taskId,percent:percent,comment:comment, token: ajaxRequest},
                    dataType: 'json',
                    success: function (data) {
        		        if (percent < 25)
        		        {
        		            lable = 'label-warning';
        		            face = '';
        		        }
        		        else if (percent >= 25 & percent < 50)
        		        {
        		            lable = '';
        		            face = 1;

        		        }
        		        else if (percent >= 50 & percent < 75)
        		        {
        		            lable = 'label-important';
        		            face = 2;

        		        }
        		        else
        		        {
        		            lable = 'label-success';
        		            face = 3;
        		        }
                                widthPercent=percent-(0.12*percent);
                                commentId=data['id'];
        		        html= "<div class='comment-row'><i class='icon-remove' sid='"+commentId+"'></i><div class='label label-info'>"+userName+"</div><span class='label label-info' style='position:relative;float:left;padding-left:4px;'>ط«ط¨طھ ط´ط¯</span>"+
        			        		"<br><div style='position:relative;float:right;padding-left:4px;margin-top: 5px;margin-bottom: 5px;'><img class='' width='45px' height='20px' src='" + window.location.origin + "/gantt/profiles/" + userLogin + ".png'></div>"+
        			         "<span class='label " + lable + "' style='width:" +widthPercent + "%; min-width:50px;margin-top: 5px;margin-bottom: 5px;'>"+
        			         "<img class='' width='20px' height='20px' src='" + window.location.origin + "/gantt/img/face" + face + ".png'>" +
        			         percent + '%:</span> <br>' + comment +
        			         "</div>";
        		        parent.children('.modal-body').html(html + parent.children('.modal-body').html());
                        console.log('success edit');
                        ajaxEnd();
                    },error:function(){
                        ajaxEnd();
                        myPrompt('newComment');
                    }
                });
            }

            console.log('click on insert message\ this comment.id: ' + taskId);
        });
    }
    /*END COMMENTING*/
    /*------------START CREATE TIMELINE-------------*/
    creating = false;
    mouseMove = false;
    $(document).on('mousemove', '.schedule-row', function (event) {

        id = $(this).attr('sid');
        if ($(this).attr('show') == 'false' && draggable == false && resizable == false)
        {
            var parentOffset = $(this).parent().offset();
            mouseX = parentOffset.left - event.pageX;

            /*$('.schedule-content').width()-$(this).width()-$(this).position().left;
             chrome=$(this).width()+mouseX+($('#content-schedule').scrollLeft()-$('.schedule-rows').width());
             firefox=$(this).width()+mouseX+($('#content-schedule').scrollLeft());
             margin=browser(chrome,firefox);*/
            margin = mouseX;
            margin += $('.schedule-rows').width();
            //console.log($(this).find('.right-tool').width());
            mod = margin % resizeDragX;
            margin -= mod;
            $('#schedule-' + id).css({'display': 'block', 'right': margin - 2, 'width': resizeDragX});
            //console.log($(this).children('div').children('.resize-drag').children('.ui-resizable-w').width());
            $(this).find('.right-tool , .left-tool').css({'display': 'none'});
            $(this).children('div').children('.resize-drag').children('.ui-resizable-w').attr('style', 'width:100% !important;z-index:90;');
            ths = $(this).find('.resize-drag');
            updateToolbar(ths, $(this).children('div'));
            mouseMove = true;
        }
        else
            $(this).find('.right-tool > img , .left-tool > img').css({'display': 'inline-block'});
    });
    $(document).on('mouseleave', '.schedule-row', function (event) {
        id = $(this).attr('sid');
        if ($(this).attr('show') == 'false')
            $('#schedule-' + id).css({'display': 'none'});
        $(this).children('.ui-resizable-w').css({'width': '5px'});
        $(this).find('.right-tool img , .left-tool img').css({'display': 'none'});
    });
    $(document).on('mouseup', '.schedule-row', function (event) {
        id = $(this).attr('sid');
        if (creating == true && $('#schedule-' + id).css('display') == 'none')
        {
            var parentOffset = $(this).parent().offset();
            mouseEndX = event.pageX - parentOffset.left;
            mouseEndY = event.pageY - parentOffset.top;

            margin = mouseStartX;
            width = mouseStartX - mouseEndX;
            margin = $(this).width() - mouseStartX + 950 + ($('#content-schedule').scrollLeft() - $('.schedule-rows').width());
            mod = width % resizeDragX;
            width -= mod;
            mod = margin % resizeDragX;
            margin -= mod;

            $('#schedule-' + id).css({'display': 'block', 'right': margin, 'width': width});
            //$(this).html(timeLine);
            console.log('create timeline: mouse down and drag' + mouseStartX + ' ' + mouseStartY + ' ' + mouseEndX + ' ' + mouseEndY + ' ' + margin + ' ');
            creating = false;
        }
    });
    $(document).on('mousedown', '.schedule-row', function (event) {
        id = $(this).attr('sid');
        if ($('#schedule-' + id).css('display') == 'none' || mouseMove == true && $(this).attr('show') == 'false')
        {
            $(this).attr('show', 'true');
            var parentOffset = $(this).parent().offset();

            mouseX = parentOffset.left - event.pageX;
            margin = mouseX;
            margin += $('.schedule-rows').width();

            mod = margin % resizeDragX;
            margin -= mod;
            $('#schedule-' + id).css({'display': 'block', 'right': margin - 1, 'width': resizeDragX});
            $(this).find('.right-tool , .left-tool').css({'display': 'block'});
            $(this).children('div').children('.resize-drag').children('.ui-resizable-w').attr('style', 'width:7px;z-index:90;');

            updateToolbar($(this).find('.resize-drag'), $(this).children('div'));
            console.log('schedule has staticed');

            //scheduleStaticed=true;
            taskRow = $('#task-' + id);
            id = taskRow.attr('sid');

            pos1 = $('.schedule-content').width() - $('#schedule-' + id).width() - $('#schedule-' + id).position().left;

            divi=(pos1)/resizeDragX;
            divi=Math.round(divi);
            pos1s=divi*resizeDragX;
            pos1=pos1s+1;

            startDate = scrollDate[pos1];
            endDate = scrollDate[pos1 + $('#schedule-' + id).width()];
            width=$('#schedule-' + id).width();
           
            divi=(width)/resizeDragX;
            divi=Math.round(divi);
            width1=divi*resizeDragX;
            width=width1+1;

            $('#schedule-' + id).width(width-2);
            $('#schedule-' + id).css('right',pos1-2);
            console.log(width1+" true width: "+width+" start:"+pos1);
            ajaxStart();
            $.ajax({
                url: URL + 'ajaxes/task.php',
                type: 'post',
                data: {action: 'fixeTimeline',projectId:projectId, id: id, startDate: startDate, endDate: endDate,marginRight:pos1,width:width, token: ajaxRequest},
                dataType: 'json',
                success: function (data) {
                    console.log('success edit');
                    ajaxEnd();
                },error:function(){
                    ajaxEnd();
                    myPrompt('fixeTimeline');
                }
            });

        }

    });


});
