<?php
$_SESSION['ajaxRequest'] = 'schedulelogedin';
$style='';
include_once 'header1.php';
?>
<body>
    <?php
    include_once 'menu2.php';
    ?>
    <div class="container marketing" style="margin-top:90px;">
        <p><span class="label label-info"><h1 class="text-center">ویرایش رمز عبور</h1></span></p>
        <?php
        $actives=array('profile'=>'','repass'=>'',2=>'');
        $actives[$params[0]]='active';
        $validateMsg='';
        global $validateMsg;
        $user=$sqlOPR->select('users','',"id=$_SESSION[userId]");
        ?>
        <hr class="">
        <ul class="nav nav-pills">
            <li class="<?=$actives['profile']?>"><a href="<?=URL?>profile">ویرایش پروفایل</a></li>
            <li class="<?=$actives['repass']?>"><a href="<?=URL?>repass">ویرایش رمز عبور</a></li>
        </ul>
        <div class="row">
            <div class="span4"></div>
            <div class="span3 text-center">

                <?php

                if(sizeof($user)>0)
                {
                    $user=$user[0];
                    if(isset($_POST['editPass']))
                    {
                        include_once DIR.'classes/formValidation.php';
                        $validation=new formValidation();
                        $inputValid = array(
                            'pass' => 'رمز عبور|sc|len=6#-'
                            );
                        $validateMsg=$validation->validation($inputValid);
                        //pr($validateMsg);
                        if($validateMsg=='' || count($validateMsg)==0)
                        {
                            $sqlOPR = new sqlOPR();
                            $pass=$_POST['pass'];
                            $rePass=$_POST['passre'];
                            $oldPass=$_POST['oldPass'];
                            include_once DIR.'classes/hashMaker.php';
                            $hash=new hashMaker();
                            $pass=$hash->hash($pass);
                            $oldPass=$hash->hash($oldPass);
                            $userId=$_SESSION['userId'];
                            $user=$sqlOPR->select('users','',"id=$userId,pass=$oldPass");
                            $updates="pass=$pass";
                            if($user&&count($user)>0)
                            {
                                if($upd=$sqlOPR->update('users',$updates,"id=$userId"))
                                    echo "ویرایش با موفقیت انجام شد.<br>";
                                else
                                    $validateMsg='دوباره تلاش کنید<br>';
                                    
        //                        header('location:'.URL.'ganttmanager');
                            }
                            else
                                $validateMsg='رمز عبور قبلی را چک کنید<br>';
                        }
                    }
                    if(!isset($_POST['editPass'])||($validateMsg!=''||count($validateMsg)>0))
                    {
                        include_once DIR.'classes/formHelper.php';
                        echo '<script src="'.URL.'js/formValidation.js"></script>
                        <div class="edit-row">';
                        $formHelper=new formHelper();
                        $caption='';
                        $name='signup';
                        $action='';
                        $method='post';
                        $onsubmit="passre|r| |6#-|sc| |onblur";
                        $formHelper->form($caption, $name, $action, $method,$onsubmit,'','form-signin','',"multipart/form-data");
                        echo @$validateMsg;
                        $formHelper->input('رمز عبور قبلی','1رمز عبور قبلی','oldPass','oldPass|r| | | | |onblur','','text',"");
                        $formHelper->input('رمز عبور','1رمز عبور جدید','pass','pass|r| |6#-|sc| | ',"",'password',"");
                        $formHelper->input('تکرار رمز عبور',
                                           '1تکرار رمز عبور جدید','passre','passre|r| |6#-| | |onblur','','password',"");
                        echo "<br>";
                        $formHelper->submit('editPass', "ویرایش",'waves-effect waves-light btn','',"");
                    }            
                }
                else
                    signin();
                ?>

            </div>
        </div>
    </div>
        <hr class="">

        <script type="text/javascript">
            ajaxRequest = "<?php echo $_SESSION['ajaxRequest']; ?>";
            userLogin = "<?php echo $_SESSION['userLogin']; ?>";
            userName = "<?php echo $_SESSION['userName']; ?>";
            projectType = "<?php echo $projectType; ?>";
        </script>
<?php
include_once "footer1.php";